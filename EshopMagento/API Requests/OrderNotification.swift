//
//  OrderNotification.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 14/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Alamofire

class OrderNotification:MainRequest {
    /// set the url for request and setup parameters for request
    init(orderId:String) {
        super.init()
        self.url = "\(ConfigUrl.baseUrl)orderIOSNotification/\(orderId)/\(deviceId)"
    }
    
    /// call to start the request
    override func start() {
        self.manager!.request(self.url!, method: .get,  encoding: JSONEncoding.default, headers: self.headers)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    if "\(String(describing: response.response!.statusCode))" == "200" {
                        
                    } else {
                        Logger.error(tag: "OrderNotification", message: response.result.value)
                    }
                }
                
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Logger.error(tag: "OrderNotification", message: error.localizedDescription)
                }
        }
    }
}
