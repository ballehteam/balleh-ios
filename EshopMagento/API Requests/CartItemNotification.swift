//
//  CartItemNotification.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 18/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Alamofire

class CartItemNotification:MainRequest {
    /// set the url for request and setup parameters for request
    init(quoteId:String) {
        super.init()
        self.url = "\(ConfigUrl.baseUrl)cartItemNotification/\(quoteId)/\(deviceId)"
    }
    
    /// call to start the request
    override func start() {
        self.manager!.request(self.url!, method: .get,  encoding: JSONEncoding.default, headers: self.headers)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    if "\(String(describing: response.response!.statusCode))" == "200" {
                        
                    } else {
                        Logger.error(tag: "CartItemNotification", message: response.result.value as Any)
                    }
                }
                
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Logger.error(tag: "CartItemNotification", message: error.localizedDescription)
                }
        }
    }
}
