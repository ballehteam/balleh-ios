//
//  GetWishlist.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 14/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Alamofire

class GetWishlist:MainRequest {
    /// set the url for request and setup parameters for request
    init() {
        super.init()
        self.headers!["Content-Type"] = "application/json"
        self.headers!["Authorization"] = "Bearer \(userIDStr)"
        self.url = "\(mainUrl)Wishlist/items"
    }
    
    var response: FetchWishlistResponse? = nil
    
    /// call to start the request
    override func start() {
        self.manager!.request(self.url!, method: .get,  encoding: JSONEncoding.default, headers: self.headers)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    if "\(String(describing: response.response!.statusCode))" == "200",
                        let result = response.result.value {
                        
                        var wishList = [WishList]()
                        if let dataResponse = result as? NSArray {
                            for data in dataResponse {
                                if let dataDic = data as? NSDictionary {
                                    wishList.append(WishList(data: dataDic))
                                }
                            }
                        }
                        
                        self.response?.getAll(wishlist: wishList)
                        
                        print(response.result.value)
                    } else {
                        Logger.error(tag: "GetWishlist", message: response.result.value)
                    }
                }
                
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Logger.error(tag: "GetWishlist", message: error.localizedDescription)
                }
        }
    }
}
