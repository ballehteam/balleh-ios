//
//  AddWishlist.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 14/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Alamofire

class AddWishlist:MainRequest {
    /// set the url for request and setup parameters for request
    init(productId:String) {
        super.init()
        self.headers!["Content-Type"] = "application/json"
        self.headers!["Authorization"] = "Bearer \(userIDStr)"
        self.url = "\(mainUrl)Wishlist/add/\(productId)"
    }
    
    /// call to start the request
    override func start() {
        self.manager!.request(self.url!, method: .post,  encoding: JSONEncoding.default, headers: self.headers)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    if "\(String(describing: response.response!.statusCode))" == "200" {
                        if let wishlistId = response.result.value as? String {
                            print(wishlistId)
                        }
                        print(response.result.value)
                    } else {
                        Logger.error(tag: "AddWishlist", message: response.result.value)
                    }
                }
                
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Logger.error(tag: "AddWishlist", message: error.localizedDescription)
                }
        }
    }
}
