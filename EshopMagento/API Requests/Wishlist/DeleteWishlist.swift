//
//  DeleteWishlist.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 14/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Alamofire

class DeleteWishlist:MainRequest {
    /// set the url for request and setup parameters for request
    init(productId:String) {
        super.init()
        self.headers!["Content-Type"] = "application/json"
        self.headers!["Authorization"] = "Bearer \(userIDStr)"
        
        //self.url = "\(mainUrl)Wishlist/delete/\(wishlistId)"
        
        self.url = "\(mainUrl)Wishlist/deletebyproductid/\(productId)"
        
    }
    
    /// call to start the request
    override func start() {
        self.manager!.request(self.url!, method: .delete,  encoding: JSONEncoding.default, headers: self.headers)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    if "\(String(describing: response.response!.statusCode))" == "200" {
                        print(response.result.value)
                        self.mainResponse?.onSuccess(message: "deleted")
                    } else {
                        Logger.error(tag: "DeleteWishlist", message: response.result.value)
                    }
                }
                
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Logger.error(tag: "DeleteWishlist", message: error.localizedDescription)
                }
        }
    }
}
