//
//  MainRequest.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 13/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Foundation
import Alamofire
import Firebase

/// Base class for network requests and configuration
class MainRequest {
    
    /// needed for authorization
    var token:String? = nil
    
    /// Delegate object to response the result
    var mainResponse: MainResponse? = nil
    
    /// Delegate object to handle fail request
    //var socketConnection: SocketConnection? = nil
    
    /// api url for request
    internal var url:String? = nil
    
    internal let TAG = "MainRequest.swift"
    
    /// needed to configuration headers
    internal var headers:HTTPHeaders? = ["":""]
    
    /// Request body to send informations
    internal var body: Dictionary<String, AnyObject>
    
    /// almofire configuration
    internal var manager:SessionManager? = nil
    
    /// needed for send information in JSON format
    internal var parameters: Parameters? = nil
    
    static var idToken:String? = nil
    
    init(token:String = "") {
        self.token = token
        self.headers = HTTPHeaders()
        self.headers!["Content-Type"] = "application/json"
        self.body = [:]
        self.manager = Alamofire.SessionManager.default
        self.manager!.session.configuration.timeoutIntervalForRequest = 1220
    }
    
    /// function to start requset
    func start(){}
    
    
}
