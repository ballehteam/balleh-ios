//
//  FetchWishlistResponse.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 14/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//


protocol FetchWishlistResponse: MainResponse {
    func getAll(wishlist: [WishList])
}
