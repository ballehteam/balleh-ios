//
//  MainResponse.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 14/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Foundation

/// Delegate for network request
protocol MainResponse {
    
    /// this method is call when the request is success
    ///
    /// - Parameter message: message of successful request
    func onSuccess(message:String)
    
    /// the method is call when the request is fail
    ///
    /// - Parameter message: message of fail request
    func onFail(message:String)
}
