//
//  CustomLabel.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 07/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Foundation
import Foundation
import UIKit

@IBDesignable class CustomLabelUI : UILabel {
    
    /// Custom the border width
    @IBInspectable var borderWidth : Int {
        set {
            self.layer.borderWidth = CGFloat(newValue)
        } get {
            return Int(self.layer.borderWidth)
        }
    }
    
    /// Custom the corner radius
    @IBInspectable var cornerRadius : Int {
        set {
            self.layer.cornerRadius = CGFloat(newValue)
        } get {
            return Int(self.layer.cornerRadius)
        }
    }
    
    /// Custom the color of the border
    @IBInspectable var borderColor : UIColor = UIColor.white {
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var setFont : CGFloat {
        set {
            self.font = UIFont.appFontWith(size: newValue)
        } get {
            return CGFloat(0)
        }
    }
}
