//
//  ShippingMethodVC.swift
//  EshopMagento
//
//  Created by Apple on 19/04/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire

class ShippingMethodVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblList: UITableView!
    var selectedMethod = Int()
    var shippingMethodArr = NSMutableArray()
    
    @IBOutlet var stepView1: UIView!
    @IBOutlet var stepView2: UIView!
    @IBOutlet var stepView3: UIView!
    @IBOutlet var stepView4: UIView!
    
    @IBOutlet var img1: UIImageView!
    @IBOutlet var img2: UIImageView!
    @IBOutlet var line: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var headerImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerImage.image = Tools.headerImage()
        btnTitle.addTarget(self, action: #selector(self.clickTitle), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(self.clickClose(sender:)), for: .touchUpInside)
        
        if languageID == "2"
        {
            imgBack.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        var value = CGFloat ()
        if SharedManager.DeviceType.IS_IPHONE_5 || SharedManager.DeviceType.IS_IPHONE_4_OR_LESS
        {
            value = 2.35
        }
        else
        {
            value = 2
        }
        
        stepView1.layer.cornerRadius = stepView1.frame.size.height/value
        stepView2.layer.cornerRadius = stepView1.frame.size.height/value
        stepView3.layer.cornerRadius = stepView1.frame.size.height/value
        stepView4.layer.cornerRadius = stepView1.frame.size.height/value
        
        stepView1.layer.borderColor = themeColor.cgColor
        stepView2.layer.borderColor = UIColor.gray.cgColor
        stepView3.layer.borderColor = UIColor.gray.cgColor
        stepView4.layer.borderColor = UIColor.gray.cgColor
        
        line.backgroundColor = themeColor
        img1.image = img1.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        img1.tintColor = themeColor
        
        print(shippingAddressDict)
        
        if !isGuest
        {
            getShippingMethods(type: "user")
        }
        else
        {
            getShippingMethods(type: "guest")
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func clickTitle()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func clickClose(sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return shippingMethodArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ShippingMethodCell = self.tblList.dequeueReusableCell(withIdentifier: "Cell") as! ShippingMethodCell
        
        cell.lblTitle.text = "    \((self.shippingMethodArr.object(at: indexPath.row) as AnyObject).value(forKey: "carrier_title")!)"
        cell.lblType.text = "\((self.shippingMethodArr.object(at: indexPath.row) as AnyObject).value(forKey: "method_title")!) (\(Tools.getCurrency()) \((self.shippingMethodArr.object(at: indexPath.row) as AnyObject).value(forKey: "amount")!))"
        
        if selectedMethod == indexPath.row
        {
            cell.radioImage.image = UIImage (named : "ic_radio_selected")
        }
        else
        {
            cell.radioImage.image = UIImage (named : "ic_radio_unselected")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 68
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedMethod = indexPath.row
        self.tblList.reloadData()
    }
    
    @IBAction func clickContinue(_ sender: AnyObject)
    {
        let selectedDict = ["shipping_address": shippingAddressDict.value(forKey: "address")!,
                            "billing_address": shippingAddressDict.value(forKey: "address")!,
                            "shipping_carrier_code": "\((shippingMethodArr.object(at: selectedMethod) as AnyObject).value(forKey: "carrier_code")!)",
            "shipping_method_code": "\((shippingMethodArr.object(at: selectedMethod) as AnyObject).value(forKey: "method_code")!)"
            ] as [String : Any]
        
        let shipStr = "\((self.shippingMethodArr.object(at: selectedMethod) as AnyObject).value(forKey: "carrier_title")!) - \((self.shippingMethodArr.object(at: selectedMethod) as AnyObject).value(forKey: "method_title")!) (\(Tools.getCurrency()) \((self.shippingMethodArr.object(at: selectedMethod) as AnyObject).value(forKey: "amount")!))"
        
        mainCartDict.setObject(shipStr, forKey: "shipping_method" as NSCopying)
        mainCartDict.setObject("\((shippingMethodArr.object(at: selectedMethod) as AnyObject).value(forKey: "carrier_code")!)_\((shippingMethodArr.object(at: selectedMethod) as AnyObject).value(forKey: "method_code")!)", forKey: "shipping_method_code" as NSCopying)
        
        shippingMethodDict.setObject(selectedDict, forKey: "addressInformation" as NSCopying)
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentMethodVC") as! PaymentMethodVC
        self.present(viewController, animated: false, completion: {})
    }
    
    func getShippingMethods(type: String)
    {
        if type == "user"
        {
            userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
            //  let params = ["cart_item" : shippingAddressDict] as [String : Any]
            
            let urlStr = "\(ConfigUrl.baseUrl)carts/mine/estimate-shipping-methods"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = shippingAddressDict as! [String : Any]
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)!
                print(jsonString as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject)
                            let arr = responseObject.result.value! as! NSArray
                            
                            if arr.count == 0
                            {
                                let alert = UIAlertController(title: "\(NSLocalizedString("Sorry", comment: ""))", message: "You don't have shipping method for the selected address", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                    
                                    self.dismiss(animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                self.shippingMethodArr = (responseObject.result.value! as! NSArray).mutableCopy() as! NSMutableArray
                                self.tblList.reloadData()
                            }
                            
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/estimate-shipping-methods"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
          //  request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = shippingAddressDict as! [String : Any]
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)!
                print(jsonString as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            self.shippingMethodArr = (responseObject.result.value! as! NSArray).mutableCopy() as! NSMutableArray
                            self.tblList.reloadData()
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
    }
}
