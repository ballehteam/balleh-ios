//
//  ShipAddressTableViewCell.swift
//  EshopMagento
//
//  Created by Apple on 19/04/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit

class ShipAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var radioImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
