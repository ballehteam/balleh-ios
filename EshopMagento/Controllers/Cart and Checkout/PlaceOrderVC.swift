//
//  PlaceOrderVC.swift
//  EshopMagento
//
//  Created by Apple on 19/04/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire
import CheckoutKit

class PlaceOrderVC: ParentViewController {
    
    @IBOutlet weak var tblProducts: UITableView!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblShippingMethod: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewMethods: UIView!
    @IBOutlet weak var couponView: UIView!
    @IBOutlet weak var txtCoupon: UITextField!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    var selectedMethod = Int()
    var productsArr = NSMutableArray()
    var totalsArr = NSMutableArray()
    var imagesArr = NSMutableArray()
    
    var totalAmount = ""
    
    @IBOutlet var stepView1: UIView!
    @IBOutlet var stepView2: UIView!
    @IBOutlet var stepView3: UIView!
    @IBOutlet var stepView4: UIView!
    
    @IBOutlet var img1: UIImageView!
    @IBOutlet var img2: UIImageView!
    @IBOutlet var img3: UIImageView!
    @IBOutlet var img4: UIImageView!
    @IBOutlet var line: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var headerImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerImage.image = Tools.headerImage()
        btnTitle.addTarget(self, action: #selector(self.clickTitle), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(self.clickClose(sender:)), for: .touchUpInside)
        
        if languageID == "2"
        {
            imgBack.transform = CGAffineTransform(scaleX: -1, y: 1)
            txtCoupon.textAlignment = .right
        }
        else
        {
            txtCoupon.textAlignment = .left
        }
        var value = CGFloat ()
        if SharedManager.DeviceType.IS_IPHONE_5 || SharedManager.DeviceType.IS_IPHONE_4_OR_LESS {
            value = 2.35
        } else {
            value = 2
        }
        
        stepView1.layer.cornerRadius = stepView1.frame.size.height/value
        stepView2.layer.cornerRadius = stepView1.frame.size.height/value
        stepView3.layer.cornerRadius = stepView1.frame.size.height/value
        stepView4.layer.cornerRadius = stepView1.frame.size.height/value
        
        stepView1.layer.borderColor = themeColor.cgColor
        stepView2.layer.borderColor = themeColor.cgColor
        stepView3.layer.borderColor = themeColor.cgColor
        stepView4.layer.borderColor = UIColor.gray.cgColor
        
        line.backgroundColor = themeColor
        img1.image = img1.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        img1.tintColor = themeColor
        img2.image = img2.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        img2.tintColor = themeColor
        img3.image = img3.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        img3.tintColor = themeColor
        
      //  print(paymentMethodDict)
        
        productsArr = (mainCartDict.value(forKey: "products") as! NSArray).mutableCopy() as! NSMutableArray
        imagesArr = (mainCartDict.value(forKey: "product_images") as! NSArray).mutableCopy() as! NSMutableArray
        
        if !isGuest {
            getTotals(type: "user")
        } else {
            getTotals(type: "guest")
        }
        
        self.lblShippingAddress.text = mainCartDict.value(forKey: "shipping_address") as? String
        self.lblShippingMethod.text = mainCartDict.value(forKey: "shipping_method") as? String
        self.lblPaymentMethod.text = mainCartDict.value(forKey: "payment_method") as? String
        self.lblEmail.text = mainCartDict.value(forKey: "email") as? String
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if orderStatus == "canceled"
        {
           // cartID = ""
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var btnApplyCoupon: UIButton!
    @IBAction func clickApplyCoupon(_ sender: Any) {
        if btnApplyCoupon.titleLabel?.text == "APPLY" {
            if txtCoupon.hasText {
                if !isGuest {
                    userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
                    print(userIDStr)
                    
                    let urlStr = "\(ConfigUrl.baseUrl)carts/mine/coupons/\(self.txtCoupon.text!)"
                    
                    SharedManager.showHUD(viewController: self)
                    
                    let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                    var request = URLRequest(url: URL(string: setFinalURl)!)
                    request.httpMethod = HTTPMethod.put.rawValue
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                    
                    let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
                    
                    if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                        let jsonString = String(data: jsonData , encoding: .utf8)!
                        print(jsonString as Any)
                        request.httpBody = jsonData
                    }
                    
                    if Connectivity.isConnectedToInternet() {
                        Alamofire.request(request).responseJSON { (responseObject) -> Void in
                            
                            if responseObject.result.isSuccess {
                                SharedManager.dismissHUD(viewController: self)
                                print(responseObject.result.value!, responseObject.response!.statusCode)
                                
                                if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                                    self.btnApplyCoupon.titleLabel?.text = NSLocalizedString("Cancel", comment: "")
                                    self.btnApplyCoupon.backgroundColor = .lightGray
                                    
                                    self.getTotals(type: "user")
                                    // self.tblProducts.reloadData()
                                } else {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                }
                            }
                            if responseObject.result.isFailure {
                                SharedManager.dismissHUD(viewController: self)
                                let error : Error = responseObject.result.error!
                                print(error.localizedDescription)
                            }
                        }
                    } else {
                        SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                    }
                } else {
                    let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/coupons/\(self.txtCoupon.text!)"
                    
                    SharedManager.showHUD(viewController: self)
                    
                    let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                    var request = URLRequest(url: URL(string: setFinalURl)!)
                    request.httpMethod = HTTPMethod.put.rawValue
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                  //  request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                    
                    let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
                    
                    if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                        let jsonString = String(data: jsonData , encoding: .utf8)!
                        print(jsonString as Any)
                        request.httpBody = jsonData
                    }
                    
                    if Connectivity.isConnectedToInternet()
                    {
                        Alamofire.request(request).responseJSON { (responseObject) -> Void in
                            
                            if responseObject.result.isSuccess
                            {
                                SharedManager.dismissHUD(viewController: self)
                                print(responseObject.result.value!)
                                
                                if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                {
                                    self.btnApplyCoupon.titleLabel?.text = NSLocalizedString("Cancel", comment: "")
                                    self.btnApplyCoupon.backgroundColor = .lightGray
                                    
                                    self.getTotals(type: "guest")
                                    // self.tblProducts.reloadData()
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                }
                            }
                            if responseObject.result.isFailure
                            {
                                SharedManager.dismissHUD(viewController: self)
                                let error : Error = responseObject.result.error!
                                print(error.localizedDescription)
                            }
                        }
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Plese enter the Coupon code to apply", comment: ""))", viewController: self)
            }
        }
        else
        {
            if !isGuest
            {
                userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
                print(userIDStr)
                
                let urlStr = "\(ConfigUrl.baseUrl)carts/mine/coupons"
                
                SharedManager.showHUD(viewController: self)
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.delete.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
                
                if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                    let jsonString = String(data: jsonData , encoding: .utf8)!
                    print(jsonString as Any)
                    request.httpBody = jsonData
                }
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess {
                            SharedManager.dismissHUD(viewController: self)
                            print(responseObject.result.value!)
                            
                            if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                                self.txtCoupon.text = ""
                                self.btnApplyCoupon.titleLabel?.text = NSLocalizedString("Apply", comment: "")
                                self.btnApplyCoupon.backgroundColor = positiveBtnColor
                                // self.tblProducts.reloadData()
                                
                                self.getTotals(type: "user")
                            } else {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                    }
                }
                else {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            } else {
                let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/coupons"
                
                SharedManager.showHUD(viewController: self)
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.delete.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
              //  request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
                
                if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                    let jsonString = String(data: jsonData , encoding: .utf8)!
                    print(jsonString as Any)
                    request.httpBody = jsonData
                }
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            print(responseObject.result.value!)
                            
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                self.txtCoupon.text = ""
                                self.btnApplyCoupon.titleLabel?.text = NSLocalizedString("Apply", comment: "")
                                self.btnApplyCoupon.backgroundColor = positiveBtnColor
                                // self.tblProducts.reloadData()
                                
                                self.getTotals(type: "guest")
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
        }
    }
    
    @IBAction func clickPlaceOrder(_ sender: AnyObject)
    {
        let paymentMethod = "\(paymentMethodDict.value(forKey: "paymentMethod")!)"
        print(paymentMethod)
       
        if paymentMethod.lowercased().contains("cashondelivery")
        {
            if !isGuest
            {
                userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
                print(userIDStr)
                
                let urlStr = "\(ConfigUrl.baseUrl)carts/mine/payment-information"
                
                SharedManager.showHUD(viewController: self)
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.post.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
                
                if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                    let jsonString = String(data: jsonData , encoding: .utf8)!
                    print(jsonString as Any)
                    request.httpBody = jsonData
                }
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                let orderID = "\(responseObject.result.value!)"
                                print(orderID)
                                
                                UserDefaults.standard.removeObject(forKey: kAddToCart)
                                cartCount = ""
                                mainCartDict.removeAllObjects()
                                cartList.removeAllObjects()
                                cartID = ""
                                
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ResultSuccessVC") as! ResultSuccessVC
                                viewController.orderID = orderID
                                self.present(viewController, animated: true, completion: nil)
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
            else
            {
                let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/payment-information"
                
                paymentMethodDict.setObject(lblEmail.text, forKey: "email" as NSCopying)
                print(paymentMethodDict)
                
                SharedManager.showHUD(viewController: self)
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.post.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                //request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
                
                if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                    let jsonString = String(data: jsonData , encoding: .utf8)!
                    print(jsonString as Any)
                    request.httpBody = jsonData
                }
                
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let result = responseObject.result.value as AnyObject
                            print(result)
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                let orderID = "\(responseObject.result.value!)"
                                print(orderID)
                                
                                UserDefaults.standard.removeObject(forKey: kAddToCart)
                                cartCount = ""
                                mainCartDict.removeAllObjects()
                                cartList.removeAllObjects()
                                cartID = ""
                                
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ResultSuccessVC") as! ResultSuccessVC
                                viewController.orderID = orderID
                                self.present(viewController, animated: true, completion: nil)
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
        }
        else if paymentMethod.lowercased().contains("hyperpay")
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            viewController.orderID = "0"
            viewController.totalAmount = totalAmount
            self.present(viewController, animated: false, completion: {})
        }
        else if paymentMethod.lowercased().contains("checkout")
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CheckoutDotComVC") as! CheckoutDotComVC
            viewController.totalAmount = totalAmount
            viewController.currency = "SAR"
            print(self.productsArr)
            self.present(viewController, animated: false, completion: {})
        } else {
            
        }
    }
    
   /* func createCheckoutID()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        viewController.checkoutID = checkoutID
        self.present(viewController, animated: false, completion: {})
       /* userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
        print(userIDStr)
        
        let urlStr = "https://balleh.com/hyperpay/index/index?amount=10&currency=INR"
        
        SharedManager.showHUD(viewController: self)
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
        
        let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
        
        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
            let jsonString = String(data: jsonData , encoding: .utf8)!
            print(jsonString as Any)
            request.httpBody = jsonData
        }
        
        var checkoutID = ""
        
        Alamofire.request(request).responseJSON { (responseObject) -> Void in
            
            if responseObject.result.isSuccess
            {
                SharedManager.dismissHUD(viewController: self)
                print(responseObject.result.value!)
                if "\(String(describing: responseObject.response!.statusCode))" == "200"
                {
                    let result = responseObject.result.value! as AnyObject
                    print(result)
                    
                    checkoutID = "\(result.value(forKey: "id")!)"
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                    viewController.checkoutID = checkoutID
                    self.present(viewController, animated: false, completion: {})
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                }
            }
            if responseObject.result.isFailure
            {
                SharedManager.dismissHUD(viewController: self)
                let error : Error = responseObject.result.error!
                print(error.localizedDescription)
            }
        }*/
    }*/
    
    func getTotals(type: String)
    {
        if type == "user"
        {
            userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
            
            let params = ["shipping_carrier_code" : "\((shippingMethodDict.value(forKey: "addressInformation") as AnyObject).value(forKey: "shipping_carrier_code")!)",
                "shipping_method_code" : "\(String(describing: (shippingMethodDict.value(forKey: "addressInformation") as AnyObject).value(forKey: "shipping_method_code")))!",
                "paymentMethod": paymentMethodDict.value(forKey: "paymentMethod")!] as [String : Any]
            
            let urlStr = "\(ConfigUrl.baseUrl)carts/mine/collect-totals"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.put.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)!
                print(jsonString as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            print(responseObject.result.value!)
                            
                            let testArr = (responseObject.result.value! as AnyObject).value(forKey: "total_segments") as! NSArray
                            print(testArr)
                            let result = responseObject.result.value as AnyObject
                            
                            let grandTotal : String = {
                                if let amount = result.value(forKey: "base_grand_total") as? Double {
                                    return "\(amount)"
                                } else if let amount = result.value(forKey: "base_grand_total") as? Int {
                                    return "\(amount)"
                                } else {
                                    return "\(String(describing: result.value(forKey: "base_grand_total")!))"
                                }
                            }()
                            
                            self.totalAmount = grandTotal
                            print("totInt:\(self.totalAmount)")
                            
                            self.totalsArr.removeAllObjects()
                            
                            for i in 0..<testArr.count {
                                let value = "\((testArr[i] as AnyObject).value(forKey: "value")!)"
                                
                                if Int(value) != 0 {
                                    self.totalsArr.add(testArr[i])
                                }
                            }
                            
                            self.tblProducts.reloadData()
                            
                            // Frames
                            var frame = self.tblProducts.frame
                            frame.size.width = self.view.bounds.width - 16
                            frame.size.height = CGFloat(self.productsArr.count * 136) + CGFloat(self.totalsArr.count * 37)
                            self.tblProducts.frame = frame
                            self.tblProducts.translatesAutoresizingMaskIntoConstraints = true
                            
                            //  self.couponView.frame.origin.y = self.tblProducts.frame.origin.y + self.tblProducts.frame.size.height + 10
                            //  self.couponView.translatesAutoresizingMaskIntoConstraints = true
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            let params = ["shipping_carrier_code" : "\((shippingMethodDict.value(forKey: "addressInformation")! as AnyObject).value(forKey: "shipping_carrier_code")!)",
                "shipping_method_code" : "\(String(describing: (shippingMethodDict.value(forKey: "addressInformation") as AnyObject).value(forKey: "shipping_method_code")))!",
                "paymentMethod": paymentMethodDict.value(forKey: "paymentMethod")!] as [String : Any]
            
            let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/collect-totals"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.put.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
         //   request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)!
                print(jsonString as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            let testArr = (responseObject.result.value! as AnyObject).value(forKey: "total_segments") as! NSArray
                            
                            for i in 0..<testArr.count
                            {
                                let value = "\((testArr[i] as AnyObject).value(forKey: "value")!)"
                                
                                if Int(value) != 0
                                {
                                    self.totalsArr.add(testArr[i])
                                }
                            }
                            
                            self.tblProducts.reloadData()
                            
                            // Frames
                            var frame = self.tblProducts.frame
                            frame.size.width = self.view.bounds.width - 16
                            frame.size.height = CGFloat(self.productsArr.count * 136) + CGFloat(self.totalsArr.count * 37)
                            self.tblProducts.frame = frame
                            self.tblProducts.translatesAutoresizingMaskIntoConstraints = true
                            
                            //self.couponView.frame.origin.y = self.tblProducts.frame.origin.y + self.tblProducts.frame.size.height + 10
                            // self.couponView.translatesAutoresizingMaskIntoConstraints = true
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
    }
    
    // MARK: TableView Methods
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == tblProducts
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblProducts
        {
            if productsArr.count != 0
            {
                return self.productsArr.count + totalsArr.count
            }
            else
            {
                return 0
            }
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let view = UIView()
        return view
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblProducts
        {
            if indexPath.row >= self.productsArr.count
            {
                let cell:CheckOutTableViewCell = self.tblProducts.dequeueReusableCell(withIdentifier: "totalCell") as! CheckOutTableViewCell
                
                let name = "\((totalsArr[indexPath.row - (self.productsArr.count)] as AnyObject).value(forKey: "title") as! String)"
                
                cell.lblTotalTitle.text = Tools.cartText(text: name)
                
                /* if name == "Sub Total"
                 {
                 cell.lblTotalTitle.text = NSLocalizedString("Sub_Total", comment: "")
                 }*/
                print(totalsArr)
//                 if name.lowercased() == "total"
//                 {
//                    totalAmount = "\((totalsArr[indexPath.row - (self.productsArr.count)] as AnyObject).value(forKey: "value")!)"
//                 }
                
                if languageID == "1"
                {
                    cell.lblTotalValue.textAlignment = .right
                }
                else
                {
                    cell.lblTotalValue.textAlignment = .left
                }
                cell.lblTotalValue.text = "\((totalsArr[indexPath.row - (self.productsArr.count)] as AnyObject).value(forKey: "value")!) \(Tools.getCurrency())"
                
                return cell
            }
            else
            {
                let cell:CheckOutTableViewCell = self.tblProducts.dequeueReusableCell(withIdentifier: "cell") as! CheckOutTableViewCell
                cell.titleLbl.text = (self.productsArr.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
                cell.quantityLbl.text = "\(NSLocalizedString("Quantity", comment: "")) : \((self.productsArr.object(at: indexPath.row) as AnyObject).value(forKey: "qty")!)"
                let price = "\((self.productsArr.object(at: indexPath.row) as AnyObject).value(forKey: "price")!) \(Tools.getCurrency())"
                cell.priceLbl.text = price
                
                if imagesArr.count == productsArr.count
                {
                    let imageUrl =  "\(ConfigUrl.imageUrl)\(imagesArr.object(at: indexPath.row))"
                    let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                    cell.productImage.sd_setImage(with: URL(string: trimmedUrl))
                }
                
                
                /*  let optionArray = (self.productsArr.object(at: indexPath.row) as AnyObject).value(forKey: "option") as! NSArray
                 var y:CGFloat = 42.0
                 
                 for temp in optionArray
                 {
                 if languageID == "1"
                 {
                 cell.nameLbl.frame = CGRect(x: 145, y: y, width: 52, height: 20)
                 cell.colonLbl.frame = CGRect(x: 210, y: y, width: 10, height: 20)
                 cell.textLbl.frame =  CGRect(x: 225, y: y, width: 52, height: 20)
                 }
                 else
                 {
                 cell.nameLbl.frame = CGRect(x: self.view.bounds.size.width - (cell.productImage.frame.size.width + 92), y: y, width: 52, height: 20)
                 cell.colonLbl.frame = CGRect(x: cell.nameLbl.frame.origin.x - 20, y: y, width: 10, height: 20)
                 cell.textLbl.frame =  CGRect(x: cell.colonLbl.frame.origin.x - 72, y: y, width: 52, height: 20)
                 }
                 
                 cell.nameLbl.font = UIFont(name: "Avenir", size: 12.0)
                 cell.nameLbl.text = (temp as AnyObject).object(forKey: "name") as? String
                 
                 cell.colonLbl.font = UIFont(name: "Avenir", size: 12.0)
                 cell.colonLbl.text = ":"
                 
                 cell.textLbl.font = UIFont(name: "Avenir", size: 12.0)
                 cell.textLbl.text = (temp as AnyObject).object(forKey: "value") as? String
                 
                 cell.contentView.addSubview(cell.nameLbl)
                 cell.contentView.addSubview(cell.colonLbl)
                 cell.contentView.addSubview(cell.textLbl)
                 
                 y = y + 20
                 }*/
                
                return cell
            }
        }
        else
        {
            let cell:CheckOutTableViewCell = self.tblProducts.dequeueReusableCell(withIdentifier: "totalCell") as! CheckOutTableViewCell
            
            return cell
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == self.tblProducts
        {
            if indexPath.row >= self.productsArr.count
            {
                return 37
            }
            else
            {
                return 134
            }
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}


