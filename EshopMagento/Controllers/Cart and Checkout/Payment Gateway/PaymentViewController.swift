//
//  PaymentViewController.swift
//  EshopMagento
//
//  Created by Apple on 26/06/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire

class PaymentViewController: ParentViewController, OPPCheckoutProviderDelegate
{
    var checkoutProvider: OPPCheckoutProvider?
    var transaction: OPPTransaction?
    
    var checkoutID = ""
    var amount : String = ""
    var totalAmount : String = ""
    var currency : String = "\(Tools.getCurrency())"
    var orderID : String = ""
    
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var headerImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerImage.image = Tools.headerImage()
//        let totalVal = Double(totalAmount)?.rounded()
//        amount = "\(Int(totalVal!))"
//        print("totInt:\(amount)")
        
        let billingDetail = shippingMethodDict.value(forKeyPath: "addressInformation.billing_address") as! NSDictionary
        
        let BillingCity = "\(String(describing: billingDetail.value(forKey: "city")!))"
       
        let BillingStreet = "\((billingDetail.value(forKey: "street") as AnyObject).object(at: 0))"
        let BillingFname = "\(String(describing: billingDetail.value(forKey: "firstname")!))"
        let BillingLname = "\(String(describing: billingDetail.value(forKey: "lastname")!))"
        let BillingEmail = "\(String(describing: billingDetail.value(forKey: "email")!))"
        let BillingCountry = "\(String(describing: billingDetail.value(forKey: "country_id")!))"
        let Billingpostcode = "\(String(describing: billingDetail.value(forKey: "postcode")!))"
        
        if languageID == "2" {
            imgBack.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        SharedManager.showHUD(viewController: self)
        
        let values:NSString = NSString(format: "amount=%@&currency=%@&billing_city=%@&billing_street=%@&customer_name=%@&customer_surname=%@&customer_email=%@&customer_country=%@&postcode=%@&", totalAmount,currency,BillingCity,BillingStreet,BillingFname,BillingLname,BillingEmail,BillingCountry,Billingpostcode)
        
        print(values)
        let urlStr = "http://magento2.adyas.com/hyperpay/index/index"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let testData = (values as NSString).data(using: String.Encoding.utf8.rawValue)
        request.httpBody = testData
        
        /*let params = [
            "amount": amount,
            "currency": currency,
            "billing_city": "\(String(describing: billingDetail.value(forKey: "city")!))",
            "billing_street": "\(String(describing: billingDetail.value(forKey: "street")!))",
            "customer_name": "\(String(describing: billingDetail.value(forKey: "firstname")!))",
            "customer_surname": "\(String(describing: billingDetail.value(forKey: "lastname")!))",
            "customer_email": "\(String(describing: billingDetail.value(forKey: "email")!))",
            "customer_country": "\(String(describing: billingDetail.value(forKey: "country_id")!))",
            "postcode": "\(String(describing: billingDetail.value(forKey: "postcode")!))"
        ]
        
        print(params)
        
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let setTemp: [String : Any] = params
        
        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
            let jsonString = String(data: jsonData , encoding: .utf8)
            print(jsonString as Any)
            request.httpBody = jsonData
        }*/
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                
                SharedManager.dismissHUD(viewController: self)
                if responseObject.result.isSuccess {
                    let result = responseObject.result.value as AnyObject
                    print(result)
                    if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                        let result = responseObject.result.value! as AnyObject
                        print(result)
                        
                        self.checkoutID = "\(result.value(forKey: "id")!)"
                        
                        print(self.checkoutID)
                        self.checkoutProvider = self.configureCheckoutProvider(checkoutID: self.checkoutID)
                        self.checkoutProvider?.delegate = self
                        self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                            DispatchQueue.main.async {
                                self.handleTransactionSubmission(transaction: transaction, error: error)
                            }
                        }, cancelHandler: {
                            print("canceled")
                            
                            orderStatus = "canceled"
                            
                            self.dismiss(animated: true, completion: nil)
                            self.navigationController?.popViewController(animated: true)
                            // self.cancelPayment()
                        })
                    } else {
                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                    }
                }
                if responseObject.result.isFailure {
                    SharedManager.dismissHUD(viewController: self)
                    let error : Error = responseObject.result.error!
                    print(error.localizedDescription)
                    
                    let data = responseObject.data
                    
                    let errorStr = NSString.init(data: responseObject.data!, encoding: String.Encoding.utf8.rawValue)! as String
                    print(errorStr)
                }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        /*userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
        print(userIDStr)
        
        let urlStr = "https://balleh.com/hyperpayex/index/index?amount=\(amount)&currency=\(currency)"
        
        SharedManager.showHUD(viewController: self)
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
        
      /*  let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
        
        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
            let jsonString = String(data: jsonData , encoding: .utf8)!
            print(jsonString as Any)
            request.httpBody = jsonData
        }*/
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                
                if responseObject.result.isSuccess
                {
                    SharedManager.dismissHUD(viewController: self)
                    print(responseObject.result.value!)
                    if "\(String(describing: responseObject.response!.statusCode))" == "200"
                    {
                        let result = responseObject.result.value! as AnyObject
                        print(result)
                        
                        self.checkoutID = "\(result.value(forKey: "id")!)"
                        
                        print(self.checkoutID)
                        self.checkoutProvider = self.configureCheckoutProvider(checkoutID: self.checkoutID)
                        self.checkoutProvider?.delegate = self
                        self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                            DispatchQueue.main.async {
                                self.handleTransactionSubmission(transaction: transaction, error: error)
                            }
                        }, cancelHandler: {
                            print("canceled")
                            
                            orderStatus = "canceled"
                            
                            self.dismiss(animated: true, completion: nil)
                            self.navigationController?.popViewController(animated: true)
                            // self.cancelPayment()
                        })
                        
                        /*let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                         let viewController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                         viewController.checkoutID = checkoutID
                         self.present(viewController, animated: false, completion: {})*/
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                    }
                }
                if responseObject.result.isFailure
                {
                    SharedManager.dismissHUD(viewController: self)
                    let error : Error = responseObject.result.error!
                    print(error.localizedDescription)
                    
                    let data = responseObject.data
                    
                    let errorStr = NSString.init(data: responseObject.data!, encoding: String.Encoding.utf8.rawValue)! as String
                    print(errorStr)
                }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }*/
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickBack(_ sender: Any)
    {
        let alert = UIAlertController(title: "\(NSLocalizedString("Warning", comment: ""))", message: "\(NSLocalizedString("Do you want to cancel this transaction ?", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
            
            cartID = ""
            orderStatus = "canceled"
            
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - OPPCheckoutProviderDelegate methods
    
    // This method is called right before submitting a transaction to the Server.
    func checkoutProvider(_ checkoutProvider: OPPCheckoutProvider, continueSubmitting transaction: OPPTransaction, completion: @escaping (String?, Bool) -> Void) {
        // To continue submitting you should call completion block which expects 2 parameters:
        // checkoutID - you can create new checkoutID here or pass current one
        // abort - you can abort transaction here by passing 'true'
        completion(transaction.paymentParams.checkoutID, false)
    }
    
    // MARK: - Payment helpers
    
    func handleTransactionSubmission(transaction: OPPTransaction?, error: Error?) {
        guard let transaction = transaction else {
            Utils.showResult(presenter: self, success: false, message: error?.localizedDescription)
            return
        }
        
        self.transaction = transaction
        if transaction.type == .synchronous {
            // If a transaction is synchronous, just request the payment status
            self.requestPaymentStatus()
        } else if transaction.type == .asynchronous {
            // If a transaction is asynchronous, SDK opens transaction.redirectUrl in a browser
            // Subscribe to notifications to request the payment status when a shopper comes back to the app
            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: Config.asyncPaymentCompletedNotificationKey), object: nil)
        } else {
            Utils.showResult(presenter: self, success: false, message: "Invalid transaction")
        }
    }
    
    func configureCheckoutProvider(checkoutID: String) -> OPPCheckoutProvider? {
        let provider = OPPPaymentProvider.init(mode: .test)
        let checkoutSettings = Utils.configureCheckoutSettings()
        checkoutSettings.storePaymentDetails = .prompt
        return OPPCheckoutProvider.init(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
    }
    
    func requestPaymentStatus() {
        guard let resourcePath = self.transaction?.resourcePath else {
            Utils.showResult(presenter: self, success: false, message: "Resource path is invalid")
            return
        }
        
        print(resourcePath)
        print(self.transaction)
        
        self.transaction = nil
        
        let urlStr = "https://balleh.com/hyperpayex/index/status?resource_path=\(resourcePath)"
        
        SharedManager.showHUD(viewController: self)
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      //  request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                
                if responseObject.result.isSuccess
                {
                    SharedManager.dismissHUD(viewController: self)
                    print(responseObject.result.value!)
                    if "\(String(describing: responseObject.response!.statusCode))" == "200"
                    {
                        let result = responseObject.result.value! as AnyObject
                        print(result)
                        
                        if let message = result.value(forKey: "message")
                        {
                            if "\(message)".lowercased() == "success"
                            {
                                self.placeOrder()
                            }
                            else
                            {
                                let alert = UIAlertController(title: "\(NSLocalizedString("Sorry", comment: ""))", message: "Your Transaction Failed", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                    
                                    //  cartID = ""
                                    orderStatus = "canceled"
                                    
                                    self.dismiss(animated: true, completion: nil)
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        else
                        {
                            let alert = UIAlertController(title: "\(NSLocalizedString("Sorry", comment: ""))", message: "Your Transaction Failed", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                
                                //  cartID = ""
                                orderStatus = "canceled"
                                
                                self.dismiss(animated: true, completion: nil)
                                self.navigationController?.popViewController(animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        // SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        
                        let alert = UIAlertController(title: "\(NSLocalizedString("Sorry", comment: ""))", message: "\(((responseObject.result.value) as AnyObject).value(forKeyPath: "message")!)", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                            
                            //  cartID = ""
                            orderStatus = "canceled"
                            
                            self.dismiss(animated: true, completion: nil)
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                if responseObject.result.isFailure
                {
                    SharedManager.dismissHUD(viewController: self)
                    let error : Error = responseObject.result.error!
                    print(error.localizedDescription)
                    
                    let errorStr = NSString.init(data: responseObject.data!, encoding: String.Encoding.utf8.rawValue)! as String
                    print(errorStr)
                    
                    let alert = UIAlertController(title: "\(NSLocalizedString("Sorry", comment: ""))", message: "Your Transaction Failed", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                        
                        //  cartID = ""
                        orderStatus = "canceled"
                        
                        self.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func placeOrder()
    {
        if !isGuest
        {
            userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
            print(userIDStr)
            
            let urlStr = "\(ConfigUrl.baseUrl)carts/mine/payment-information"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)!
                print(jsonString as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let orderID = "\(responseObject.result.value!)"
                            print(orderID)
                            UserDefaults.standard.removeObject(forKey: kAddToCart)
                            cartCount = ""
                            mainCartDict.removeAllObjects()
                            cartList.removeAllObjects()
                            cartID = ""
                            
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ResultSuccessVC") as! ResultSuccessVC
                            viewController.orderID = orderID
                            self.present(viewController, animated: true, completion: nil)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/payment-information"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            //  request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = paymentMethodDict as! [String : Any]
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)!
                print(jsonString as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            
                            let orderID = "\(responseObject.result.value!)"
                            print(orderID)
                            UserDefaults.standard.removeObject(forKey: kAddToCart)
                            cartCount = ""
                            mainCartDict.removeAllObjects()
                            cartList.removeAllObjects()
                            cartID = ""
                            
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ResultSuccessVC") as! ResultSuccessVC
                            viewController.orderID = orderID
                            self.present(viewController, animated: true, completion: nil)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
                
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
    }
    
    func cancelPayment()
    {
        let alert = UIAlertController(title: "\(NSLocalizedString("Warning", comment: ""))", message: "\(NSLocalizedString("Do want to cancel this transaction ?", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
            
            let urlStr = "https://balleh.com/hyperpayex/index/cancel?order_id=\(self.orderID)"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        print(responseObject.result.value!)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            orderStatus = "canceled"
                            let result = responseObject.result.value! as AnyObject
                            print(result)
                            
                            self.dismiss(animated: true, completion: {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "Your Transaction Canceled", viewController: self)
                            })
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Async payment callback
    
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: Config.asyncPaymentCompletedNotificationKey), object: nil)
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                self.requestPaymentStatus()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
