//
//  CheckoutDotComVC.swift
//  EshopMagento
//
//  Created by Apple on 21/09/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import CheckoutKit
import QuartzCore
import Alamofire

class CheckoutDotComVC: UIViewController {
    
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtCvv: UITextField!
    @IBOutlet weak var txtExpiryDate: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnPaynow: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    
    @IBOutlet weak var headerImage: UIImageView!
    var datePicker : UIPickerView!
    
    var amount : String = ""
    var totalAmount : String = ""
    var currency : String = ""
    var orderID : String = ""
    var picker : UIPickerView!
    var pickerContent: [[String]] = []
    let months = [1,2,3,4,5,6,7,8,9,10,11,12]
    let years = [2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2034,2035,2036]
    var month = "1"
    var year = "2018"
    
    let errorColor = UIColor(red: 204.0/255.0, green: 112.0/255.0, blue: 115.0/255.0, alpha: 0.3)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerImage.image = Tools.headerImage()
//        let totalVal = Double(totalAmount)?.rounded()
//        amount = "\(Int(totalVal!))"
//        print("totInt:\(amount)")
        
        if languageID == "1" {
            self.backButton.setImage(UIImage(named: "right-arrow"), for: .normal)
        } else {
            self.backButton.setImage(UIImage(named: "left-arrowback"), for: .normal)
        }
        
        datePicker = UIPickerView()
        datePicker.delegate = self
        datePicker.dataSource = self
        
        self.txtExpiryDate.inputView = datePicker
        self.txtExpiryDate.delegate = self
        
        pickerContent.append([])
        for m in 0  ..< months.count  {
            pickerContent[0].append(months[m].description)
        }
        pickerContent.append([])
        for y in 0 ..< years.count {
            pickerContent[1].append(years[y].description)
        }
        
        btnPaynow.isHidden = false
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickPaynow(_ sender: Any) {
        SharedManager.showHUD(viewController: self)
        let publicKey = "pk_7471d4e5-5329-4973-8b75-e0bf006ac7be"
        var ck: CheckoutKit? = nil
        do {
            try ck = CheckoutKit.getInstance(publicKey)
        } catch _ as NSError {
            //let errorController = self.storyboard?.instantiateViewController(withIdentifier: "ErrorController") as! ErrorController
            //self.present(errorController, animated: true, completion: nil)
            //self.present(Tools.alertWithOk(message: Texts.), animated: true, completion: nil)
            SharedManager.dismissHUD(viewController: self)
        }
        if ck != nil {
            if (validateCardInfo(txtCardNumber.text!, expYear: year, expMonth: month, cvv: txtCvv.text!))
            {
                resetFieldsColor()
                var card: Card? = nil
                do {
                    try card = Card(name: txtName.text!, number: txtCardNumber.text!, expYear: year, expMonth: month, cvv: txtCvv.text!, billingDetails: nil)
                }
                catch let err as CardError {
                    switch(err) {
                    case CardError.invalidCVV: txtCvv.backgroundColor = errorColor
                    case CardError.invalidExpiryDate: txtExpiryDate.backgroundColor = errorColor
                    case CardError.invalidNumber: txtCardNumber.backgroundColor = errorColor
                    }
                } catch _ as NSError {
                    SharedManager.dismissHUD(viewController: self)
                }
                if card != nil {
                    ck!.createCardToken(card!, completion:{ (response: Response<CardTokenResponse>) -> Void in
                        
                        if (response.hasError) {
                            print("Failure")
                            SharedManager.dismissHUD(viewController: self)
                            self.present(Tools.alertWithOk(message: "\(String(describing: response.error))"), animated: true, completion: nil)
                        } else {
                            print("Success")
                            print("Card Token - \(String(describing: response.model!.cardToken!))")
                            let token = response.model!.cardToken!
                            self.createCharge(cardToken: token)
                        }
                    })
                }
            }
        }
    }
    
    func chargeAuth(cardToken: String, link: String) {
        let liveSecretKey = "sk_adaaf6ae-be5c-4f3d-bbcd-2a7de62d4e45"
        
        let urlStr = "https://api2.checkout.com/v2/charges/token"
        let setFinalURl = URL(string: urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!)
        
        let email = "\(mainCartDict.value(forKey: "email")!)"
        
        let headers:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":"\(liveSecretKey)"
        ]
        
        let parameters : Parameters = [
            "cardToken":"\(cardToken)",
            "chargeMode":"2",
            "currency":"\(self.currency)",
            "email":"\(email)",
            "value":"\(self.totalAmount)"
            //"successUrl" : "ios_token"
        ]
        
        print(headers.values, parameters.values)
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(setFinalURl!, method: .post, parameters: parameters ,encoding: JSONEncoding.default, headers: headers).responseJSON
                { (responseObject) -> Void in
                    if responseObject.result.isSuccess {
                        print(responseObject.value)
                        print(responseObject.response?.statusCode)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            let result = responseObject.result.value as AnyObject
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmWebView") as! ConfirmWebView
                            
                            if let redirectUrl =  result.value(forKey: "redirectUrl") as? String {
                                viewController.urlToLoad = redirectUrl
                            }
                        } else {
                            if let message = ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as? String {
                                self.present(Tools.alertWithOk(message: message), animated: true, completion: nil)
                            }
                        }
                    }
                    if responseObject.result.isFailure {
                        
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            self.present(Tools.alertWithOk(message: Texts.NO_INTERNET_CONNECTION), animated: true, completion: nil)
        }
    }
    
    func createCharge(cardToken : String) {
        userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
        
        let tempArr = (mainCartDict.value(forKey: "cart_product_details") as! NSArray).mutableCopy() as! NSMutableArray
        
        let productsArr = NSMutableArray()
        
        for i in 0..<tempArr.count
        {
            var temp : [String : Any] = [:]
            
            if let options = (tempArr.object(at: i) as AnyObject).value(forKey: "options")
            {
                if "\(options)" == "" || "\(options)" == "[]"
                {
                    temp = ["id" : "\((tempArr.object(at: i) as AnyObject).value(forKey: "product_id")!)",
                        "quantity" : "\((tempArr.object(at: i) as AnyObject).value(forKey: "qty")!)"]
                }
                else
                {
                    temp = ["id" : "\((tempArr.object(at: i) as AnyObject).value(forKey: "product_id")!)",
                        "quantity" : "\((tempArr.object(at: i) as AnyObject).value(forKey: "qty")!)",
                        "options" : (tempArr.object(at: i) as AnyObject).value(forKey: "options")!]
                }
            }
            else
            {
                temp = ["id" : "\((tempArr.object(at: i) as AnyObject).value(forKey: "product_id")!)",
                    "quantity" : "\((tempArr.object(at: i) as AnyObject).value(forKey: "qty")!)"]
            }
            
            productsArr.add(temp)
        }
        print(productsArr)
        let email = "\(mainCartDict.value(forKey: "email")!)"
        let shippingMethod = "\(mainCartDict.value(forKey: "shipping_method_code")!)"
        
        let values = ["cardToken" : cardToken,
                      "email" : email,
                      "quote_id" : cartID] as [String : Any]
        
        let urlStr = "https://balleh.com/index.php/rest/en/v1/charge"
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        
        var jsonString = ""
        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: values, options: .prettyPrinted) {
            jsonString = String(data: jsonData , encoding: .utf8)!
            print(jsonString as Any)
            
        }
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"/*,
            "Authorization": "Bearer \(userIDStr)"*/
        ]
        let testData = "\(jsonString)".data(using: String.Encoding.utf8)
        let decoded = String(decoding: testData!, as: UTF8.self)
        print(decoded)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append("\(jsonString)".data(using: String.Encoding.utf8)!, withName: "data" as String)
        }, usingThreshold: UInt64.init(), to: setFinalURl, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let result = response.result.value as AnyObject
                    print(result)
                    
                    if "\(String(describing: response.response!.statusCode))" == "200",
                        let url = result as? String,
                        url.contains("https") {
                        
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmWebView") as! ConfirmWebView
                        viewController.delegate = self
                        viewController.urlToLoad = url
                        self.present(viewController, animated: true, completion: nil)
                        
                    } else {
                        SharedManager.dismissHUD(viewController: self)
                        let message = "\(result.value(forKey: "message")!)"
                        self.present(Tools.alertWithOk(message: message), animated: true, completion: nil)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                SharedManager.dismissHUD(viewController: self)
              //  onError?(error)
            }
        }
    }
    
    func orderStatus() {
        let urlStr = "\(ConfigUrl.baseUrl)Orderstatuscheck/\(orderID)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        
                        let result = responseObject.result.value as AnyObject
                        print(result)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            self.deleteCartID()
                        } else {
//                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
//                            self.present(viewController, animated: true, completion: nil)
                            
                        }
                    }
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                        let message = error.localizedDescription
                        self.present(Tools.alertWithOk(message: message), animated: true, completion: nil)
                    }
            }
        } else {
            self.present(Tools.alertWithOk(message: Texts.NO_INTERNET_CONNECTION), animated: true, completion: nil)
        }
    }
    
    func deleteCartID() {
        let urlStr = "\(ConfigUrl.baseUrl)quotedelete/\(cartID)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
        
        SharedManager.showHUD(viewController: self)
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        
                        let result = responseObject.result.value as AnyObject
                        print(result)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            UserDefaults.standard.removeObject(forKey: kAddToCart)
                            cartCount = ""
                            mainCartDict.removeAllObjects()
                            cartList.removeAllObjects()
                            cartID = ""
                            
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ResultSuccessVC") as! ResultSuccessVC
                            viewController.orderID = self.orderID
                            self.present(viewController, animated: true, completion: nil)
                        } else {
                            
                            if let message = ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as? String {
                                self.present(Tools.alertWithOk(message: message), animated: true, completion: nil)
                            }
                            
                        }
                    }
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                        let message = error.localizedDescription
                        self.present(Tools.alertWithOk(message: message), animated: true, completion: nil)
                    }
            }
        } else {
            self.present(Tools.alertWithOk(message: Texts.NO_INTERNET_CONNECTION), animated: true, completion: nil)
        }
    }
    
    fileprivate func validateCardInfo(_ number: String, expYear: String, expMonth: String, cvv: String) -> Bool {
        var err: Bool = false
        resetFieldsColor()
        if (!CardValidator.validateCardNumber(number)) {
            err = true
            txtCardNumber.backgroundColor = errorColor
        }
        if (!CardValidator.validateExpiryDate(month, year: year)) {
            err = true
            txtExpiryDate.backgroundColor = errorColor
        }
        if (cvv == "") {
            err = true
            txtCvv.backgroundColor = errorColor
        }
        return !err
    }
    
    fileprivate func resetFieldsColor() {
        txtCardNumber.backgroundColor = UIColor.white
        txtExpiryDate.backgroundColor = UIColor.white
        txtCvv.backgroundColor = UIColor.white
    }
    
    fileprivate func updateDate() {
        txtExpiryDate.text = "\(month) / \(year)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
}

extension CheckoutDotComVC : UIPickerViewDelegate, UIPickerViewDataSource {
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return pickerContent.count
        }
        
        // returns the # of rows in each component..
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
        {
            return pickerContent[component].count
        }
        
        func pickerView(_ bigPicker: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
        {
            return pickerContent[component][row]
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
        {
            if component == 0
            {
                month = pickerContent[0][row]
            }
            else
            {
                year = pickerContent[1][row]
            }
            updateDate()
        }
        
}

extension CheckoutDotComVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtExpiryDate { return false }
        return true
    }
}

extension CheckoutDotComVC: SecurityAuth {
    func Success(orderId: String) {
        self.orderID = orderId
        self.deleteCartID()
    }
    
    func Failure() {
        SharedManager.dismissHUD(viewController: self)
        self.present(Tools.alertWithOk(message: Texts.AuthFail), animated: true, completion: nil)
    }
}
