//
//  ShippingAddressVC.swift
//  EshopMagento
//
//  Created by Apple on 19/04/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Alamofire

class ShippingAddressVC: ParentViewController, CLLocationManagerDelegate, GMSMapViewDelegate
{
    @IBOutlet weak var tblAddressList: UITableView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var newAddressView: UIView!
    @IBOutlet weak var addressListView: UIView!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNum: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var layerView: UIView!
    @IBOutlet weak var btnAddToAddress: UIButton!
    @IBOutlet weak var btnSelectFromList: UIButton!
    @IBOutlet weak var checkBoxView: UIView!
    
    var picker : UIPickerView!
    
    var selectedIndex = Int()
    var selectedAddress = Int()
    
    var addToAddressBook = false
    var pickerType = String()
    
    var selectedLat = Double()
    var selectedLong = Double()
    var formattedAddress = String()
    var countryNames = [String]()
    var regionCode = ""
    
    var userDetails = NSDictionary()
    var allAddress:NSMutableArray = NSMutableArray()
    var addressArr = NSMutableArray()
    var cityArr = NSArray()
    
    //Maps
    var locationManager = CLLocationManager()
    
    @IBOutlet var stepView1: UIView!
    @IBOutlet var stepView2: UIView!
    @IBOutlet var stepView3: UIView!
    @IBOutlet var stepView4: UIView!
    
    @IBOutlet var img1: UIImageView!
    @IBOutlet var line: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var headerImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        
        headerImage.image = Tools.headerImage()
        
        self.txtCity.inputView = picker
        self.txtCity.delegate = self
        
        getCountryListNew()
        btnTitle.addTarget(self, action: #selector(self.clickTitle), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(self.clickClose(sender:)), for: .touchUpInside)
        
        if languageID == "2" {
            imgBack.transform = CGAffineTransform(scaleX: -1, y: 1)
            txtFirstName.textAlignment = .right
            txtLastName.textAlignment = .right
            txtEmail.textAlignment = .right
            txtPhoneNum.textAlignment = .right
            txtStreet.textAlignment = .right
            txtCity.textAlignment = .right
            txtState.textAlignment = .right
            txtPostalCode.textAlignment = .right
            txtCountry.textAlignment = .right
        } else {
            txtFirstName.textAlignment = .left
            txtLastName.textAlignment = .left
            txtEmail.textAlignment = .left
            txtPhoneNum.textAlignment = .left
            txtStreet.textAlignment = .left
            txtCity.textAlignment = .left
            txtState.textAlignment = .left
            txtPostalCode.textAlignment = .left
            txtCountry.textAlignment = .left
        }
        
        var value = CGFloat ()
        if SharedManager.DeviceType.IS_IPHONE_5 || SharedManager.DeviceType.IS_IPHONE_4_OR_LESS {
            value = 2.35
        } else {
            value = 2
        }
        
        stepView1.layer.cornerRadius = stepView1.frame.size.height/value
        stepView2.layer.cornerRadius = stepView1.frame.size.height/value
        stepView3.layer.cornerRadius = stepView1.frame.size.height/value
        stepView4.layer.cornerRadius = stepView1.frame.size.height/value
        
        stepView1.layer.borderColor = UIColor.gray.cgColor
        stepView2.layer.borderColor = UIColor.gray.cgColor
        stepView3.layer.borderColor = UIColor.gray.cgColor
        stepView4.layer.borderColor = UIColor.gray.cgColor
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
            self.mapView.delegate = self
            self.mapView.isMyLocationEnabled = true
        }
        
        if isGuest
        {
            self.newAddressView.isHidden = false
            self.checkBoxView.isHidden = true
            self.layerView.frame.origin.y = self.txtEmail.frame.origin.y + self.txtEmail.frame.size.height + 15
            mainScrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: 1200)
            btnSelectFromList.isHidden = true
            self.mapView.frame.origin.y = btnSelectFromList.frame.origin.y
            self.mapView.frame.size.height = 324
            
        }
        else
        {
            self.newAddressView.isHidden = true
            self.checkBoxView.isHidden = false
            self.layerView.frame.origin.y = self.txtLastName.frame.origin.y + self.txtLastName.frame.size.height + 15
            mainScrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: 1150)
            btnSelectFromList.isHidden = false
            self.mapView.frame.origin.y = 82
            self.mapView.frame.size.height = 274
        }
        
        self.tblAddressList.tableFooterView = UIView()
        
        if let id = UserDefaults.standard.string(forKey: "USER_ID")
        {
            if id != ""
            {
                userIDStr = id
                let urlStr = "\(ConfigUrl.baseUrl)customers/me"
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.get.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON
                        { (responseObject) -> Void in
                            SharedManager.dismissHUD(viewController: self)
                            
                            if responseObject.result.isSuccess
                            {
                                if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                {
                                    // print(responseObject.result.value!)
                                    
                                    if "\(responseObject.result.value!)" != "<null>"
                                    {
                                        self.userDetails = (responseObject.result.value!) as! NSDictionary
                                        
                                        let address = self.userDetails.value(forKey: "addresses") as! NSArray
                                        
                                        self.addressArr = address.mutableCopy() as! NSMutableArray
                                        print(self.addressArr)
                                        self.tblAddressList.reloadData()
                                    }
                                    else
                                    {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No User Information Available", comment: ""))", viewController: self)
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                }
                            }
                            if responseObject.result.isFailure
                            {
                                let error : Error = responseObject.result.error!
                                print(error.localizedDescription)
                            }
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickAddtoAddressBook(_ sender: AnyObject)
    {
        if addToAddressBook
        {
            addToAddressBook = false
            self.btnAddToAddress.setBackgroundImage(UIImage (named: "ic_uncheck_box"), for: .normal)
        }
        else
        {
            addToAddressBook = true
            self.btnAddToAddress.setBackgroundImage(UIImage (named: "ic_check_box"), for: .normal)
        }
    }
    
    @IBAction func clickContinue(_ sender: AnyObject)
    {
        let address = NSMutableDictionary()
        address.setObject("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "firstname")!)", forKey: "firstname" as NSCopying)
        address.setObject("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "lastname")!)", forKey: "lastname" as NSCopying)
        address.setObject("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "city")!)", forKey: "city" as NSCopying)
        print("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "country_id")!)")
        address.setObject("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "country_id")!)", forKey: "country_id" as NSCopying)
        address.setObject("\(((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "region") as AnyObject).value(forKey: "region_id")!)", forKey: "region_id" as NSCopying)
        address.setObject("\(((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "region") as AnyObject).value(forKey: "region_code")!)", forKey: "region_code" as NSCopying)
        address.setObject("\(((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "region") as AnyObject).value(forKey: "region")!)", forKey: "region" as NSCopying)
        address.setObject(["\(((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "street") as AnyObject).object(at: 0))"], forKey: "street" as NSCopying)
        address.setObject("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "postcode")!)", forKey: "postcode" as NSCopying)
        address.setObject(userDetails.value(forKey: "email")!, forKey: "email" as NSCopying)
        address.setObject("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "telephone")!)", forKey: "telephone" as NSCopying)
        address.setObject("1", forKey: "same_as_billing" as NSCopying)
        address.setObject("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "customer_id")!)", forKey: "customer_id" as NSCopying)
        
        // let info = NSMutableDictionary()
        // info.setObject(address, forKey: "shipping_address" as NSCopying)
        // info.setObject(address, forKey: "shipping_address" as NSCopying)
        
        let addStr = "\("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "firstname")!)") \("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "lastname")!)"), \("\(((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "street") as AnyObject).object(at: 0))"), \("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "city")!)"), \("\(((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "region") as AnyObject).value(forKey: "region")!)"), \("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "country_id")!)"), \("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "postcode")!)"), \("\((addressArr.object(at: selectedAddress) as AnyObject).value(forKey: "telephone")!)")."
        
        mainCartDict.setObject(addStr, forKey: "shipping_address" as NSCopying)
        mainCartDict.setObject(userDetails.value(forKey: "email")!, forKey: "email" as NSCopying)
        shippingAddressDict.setObject(address, forKey: "address" as NSCopying)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ShippingMethodVC") as! ShippingMethodVC
        self.present(viewController, animated: false, completion: {})
    }
    
    @IBAction func clickAddNewAddress(_ sender: AnyObject)
    {
        self.newAddressView.isHidden = false
        getCountryListNew()
    }
    
    @IBAction func clickNewAddressSubmit(_ sender: AnyObject)
    {
        if isGuest
        {
            if !txtFirstName.hasText || !txtLastName.hasText || !txtEmail.hasText || !txtPhoneNum.hasText || !txtStreet.hasText || !txtCity.hasText ||
                !txtPostalCode.hasText || !txtCountry.hasText || !txtState.hasText
            {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Please Enter all the details marked with", comment: "")) '*'", viewController: self)
            }
            else if self.txtPhoneNum.text?.count != 10
            {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "Mobile number must be 10 characters", viewController: self)
            }
            else
            {
                let address = NSMutableDictionary()
                address.setObject(self.txtFirstName.text!, forKey: "firstname" as NSCopying)
                address.setObject(self.txtLastName.text!, forKey: "lastname" as NSCopying)
                address.setObject(self.txtCity.text!, forKey: "city" as NSCopying)
                address.setObject(countryID, forKey: "country_id" as NSCopying)
                address.setObject(stateID, forKey: "region_id" as NSCopying)
                address.setObject(regionCode, forKey: "region_code" as NSCopying)
                address.setObject(stateName, forKey: "region" as NSCopying)
                address.setObject([self.txtStreet.text!], forKey: "street" as NSCopying)
                address.setObject(self.txtPostalCode.text!, forKey: "postcode" as NSCopying)
                address.setObject(self.txtEmail.text!, forKey: "email" as NSCopying)
                address.setObject(self.txtPhoneNum.text!, forKey: "telephone" as NSCopying)
                address.setObject("1", forKey: "same_as_billing" as NSCopying)
                
                shippingAddressDict.setObject(address, forKey: "address" as NSCopying)
                
                let addStr = "\(self.txtFirstName.text!) \(self.txtLastName.text!), \(self.txtStreet.text!), \(self.txtCity.text!), \(stateName), \(countryID), \(self.txtPostalCode.text!), \(self.txtPhoneNum.text!)."
                
                mainCartDict.setObject(addStr, forKey: "shipping_address" as NSCopying)
                mainCartDict.setObject(self.txtEmail.text!, forKey: "email" as NSCopying)
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ShippingMethodVC") as! ShippingMethodVC
                self.present(viewController, animated: true, completion: nil)
            }
        }
        else
        {
            if !txtFirstName.hasText || !txtLastName.hasText || !txtPhoneNum.hasText || !txtStreet.hasText || !txtCity.hasText || !txtPostalCode.hasText || !txtCountry.hasText || !txtState.hasText
            {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Please Enter all the details marked with", comment: "")) '*'", viewController: self)
            }
            else if self.txtPhoneNum.text?.count != 10
            {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "Mobile number must be 10 characters", viewController: self)
            }
            else
            {
                if allAddress.count == 0
                {
                    addToAddressBook = true
                }
                
                if addToAddressBook
                {
                    let address = NSMutableDictionary()
                    address.setObject(self.txtFirstName.text!, forKey: "firstname" as NSCopying)
                    address.setObject(self.txtLastName.text!, forKey: "lastname" as NSCopying)
                    address.setObject(self.txtCity.text!, forKey: "city" as NSCopying)
                    address.setObject(countryID, forKey: "country_id" as NSCopying)
                    address.setObject(stateID, forKey: "region_id" as NSCopying)
                    address.setObject(regionCode, forKey: "region_code" as NSCopying)
                    address.setObject(stateName, forKey: "region" as NSCopying)
                    address.setObject([self.txtStreet.text!], forKey: "street" as NSCopying)
                    address.setObject(self.txtPostalCode.text!, forKey: "postcode" as NSCopying)
                    address.setObject(userDetails.value(forKey: "email")!, forKey: "email" as NSCopying)
                    address.setObject(self.txtPhoneNum.text!, forKey: "telephone" as NSCopying)
                    address.setObject("1", forKey: "same_as_billing" as NSCopying)
                    let id = userDetails.value(forKey: "id") as AnyObject
                    address.setObject(id, forKey: "customer_id" as NSCopying)
                    
                    // let info = NSMutableDictionary()
                    // info.setObject(address, forKey: "shipping_address" as NSCopying)
                    // info.setObject(address, forKey: "shipping_address" as NSCopying)
                    
                    shippingAddressDict.setObject(address, forKey: "address" as NSCopying)
                    self.newAddressView.isHidden = false
                    
                    let addStr = "\(self.txtFirstName.text!) \(self.txtLastName.text!), \(self.txtStreet.text!), \(self.txtCity.text!), \(stateName), \(countryID), \(self.txtPostalCode.text!), \(self.txtPhoneNum.text!)."
                    
                    mainCartDict.setObject(addStr, forKey: "shipping_address" as NSCopying)
                    mainCartDict.setObject(userDetails.value(forKey: "email")!, forKey: "email" as NSCopying)
                    addtoAddressList()
                }
                else
                {
                    let address = NSMutableDictionary()
                    address.setObject(self.txtFirstName.text!, forKey: "firstname" as NSCopying)
                    address.setObject(self.txtLastName.text!, forKey: "lastname" as NSCopying)
                    address.setObject(self.txtCity.text!, forKey: "city" as NSCopying)
                    address.setObject(countryID, forKey: "country_id" as NSCopying)
                    address.setObject(stateID, forKey: "region_id" as NSCopying)
                    address.setObject(regionCode, forKey: "region_code" as NSCopying)
                    address.setObject(stateName, forKey: "region" as NSCopying)
                    address.setObject([self.txtStreet.text!], forKey: "street" as NSCopying)
                    address.setObject(self.txtPostalCode.text!, forKey: "postcode" as NSCopying)
                    address.setObject(userDetails.value(forKey: "email")!, forKey: "email" as NSCopying)
                    address.setObject(self.txtPhoneNum.text!, forKey: "telephone" as NSCopying)
                    address.setObject("1", forKey: "same_as_billing" as NSCopying)
                    let id = userDetails.value(forKey: "id") as AnyObject
                    address.setObject(id, forKey: "customer_id" as NSCopying)
                    
                    // let info = NSMutableDictionary()
                    // info.setObject(address, forKey: "shipping_address" as NSCopying)
                    // info.setObject(address, forKey: "shipping_address" as NSCopying)
                    
                    let addStr = "\(self.txtFirstName.text!) \(self.txtLastName.text!), \(self.txtStreet.text!), \(self.txtCity.text!), \(stateName), \(countryID), \(self.txtPostalCode.text!), \(self.txtPhoneNum.text!)."
                    
                    mainCartDict.setObject(addStr, forKey: "shipping_address" as NSCopying)
                    mainCartDict.setObject(userDetails.value(forKey: "email")!, forKey: "email" as NSCopying)
                    
                    shippingAddressDict.setObject(address, forKey: "address" as NSCopying)
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ShippingMethodVC") as! ShippingMethodVC
                    self.present(viewController, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func clickSelectFromAddList(_ sender: AnyObject)
    {
        self.newAddressView.isHidden = true
    }
    
    func addtoAddressList()
    {
        if(self.txtFirstName.hasText && self.txtLastName.hasText && self.txtStreet.hasText && self.txtCity.hasText && self.txtPhoneNum.hasText &&  self.txtCountry.hasText && self.txtState.hasText && self.txtPostalCode.hasText)
        {
            
           // let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
           // userDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
            
            let region = NSMutableDictionary()
            
            region.setObject("\((stateArr.object(at: selectedIndex) as AnyObject).value(forKey: "id")!)", forKey: "region_id" as NSCopying)
            region.setObject("\((stateArr.object(at: selectedIndex) as AnyObject).value(forKey: "name")!)", forKey: "region" as NSCopying)
            region.setObject("\((stateArr.object(at: selectedIndex) as AnyObject).value(forKey: "code")!)", forKey: "region_code" as NSCopying)
            
            var address : [String : Any] = [:]
            if allAddress.count != 0
            {
                address = ["firstname": self.txtFirstName.text!,
                               "lastname": self.txtLastName.text!,
                               "company": "",
                               "city": self.txtCity.text!,
                               "region": region,
                               "street": [self.txtStreet.text!],
                               "postcode": self.txtPostalCode.text!,
                               "country_id": countryID,
                               "telephone": self.txtPhoneNum.text!,
                               "default_billing":"0",
                               "default_shipping":"0"
                    ] as [String : Any]
            }
            else
            {
                address = ["firstname": self.txtFirstName.text!,
                               "lastname": self.txtLastName.text!,
                               "company": "",
                               "city": self.txtCity.text!,
                               "region": region,
                               "street": [self.txtStreet.text!],
                               "postcode": self.txtPostalCode.text!,
                               "country_id": countryID,
                               "telephone": self.txtPhoneNum.text!,
                               "default_billing":"1",
                               "default_shipping":"1"
                    ] as [String : Any]
            }
            
            allAddress.add(address)
            
            let customer = [
                "firstname": userDetails.value(forKey: "firstname") as! String,
                "lastname": userDetails.value(forKey: "lastname") as! String,
                "website_id": 1,
                "email": userDetails.value(forKey: "email") as! String,
                "addresses": allAddress,
                ] as [String : Any]
            //"state_id": self.stateTxt.text!,
            let params = ["customer": customer] as [String : Any]
            
            let urlStr = "\(ConfigUrl.baseUrl)customers/me"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.put.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)
                print(jsonString! as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            self.newAddressView.isHidden = true
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ShippingMethodVC") as! ShippingMethodVC
                            self.present(viewController, animated: true, completion: nil)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Information", comment: ""))", alertMessage: NSLocalizedString("Please enter all the details", comment: ""), viewController: self)
        }
    }
    
    @IBAction func countryAction(_ sender: AnyObject)
    {
        stateName = ""
        self.view.endEditing(true)
        
        if countryArr.count <= 1
        {
            if countryArr.count == 1
            {
                txtCountry.text = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: 0) as? String
                countryName = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: 0) as! String
                countryID = (countryArr.value(forKey: "id") as AnyObject).object(at: 0) as! NSString
            }
        }
        else
        {
            pickerType = "country"
            picker.reloadAllComponents()
            picker.selectRow(0, inComponent: 0, animated: true)
        }
        
        /*var countryNames = [String]()
         let country_id = NSMutableArray()
         
         for i in 0 ..< self.countryListAry.count
         {
         let name = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: i)
         countryNames.append(name as! String)
         country_id.add((self.countryListAry.value(forKey: "country_id") as! NSArray).object(at: i) as! String)
         }
         pickerType = "country"*/
    }
    
    @IBAction func stateAction(_ sender: AnyObject)
    {
        // self.showStateList()
        self.view.endEditing(true)
        
        for i in 0..<countryArr.count
        {
            let id = "\((countryArr.value(forKey: "id") as AnyObject).object(at: 0))"
            
            if id == "\(countryID)"
            {
                stateArr = ((countryArr.object(at: i) as AnyObject).value(forKey: "available_regions") as! NSArray).mutableCopy() as! NSMutableArray
            }
        }
        
        if stateArr.count <= 1
        {
            if stateArr.count == 1
            {
                txtState.text = (stateArr.value(forKey: "name") as AnyObject).object(at: 0) as? String
                stateName = (stateArr.value(forKey: "name") as AnyObject).object(at: 0) as! String
                stateID = (stateArr.value(forKey: "id") as AnyObject).object(at: 0) as! NSString
                self.regionCode = "\((stateArr.value(forKey: "code") as AnyObject).object(at: 0))"
            }
        }
        else
        {
            pickerType = "state"
            picker.reloadAllComponents()
            picker.selectRow(0, inComponent: 0, animated: true)
        }
    }
    
    @IBAction func clickCity(_ sender: Any)
    {
        pickerType = "city"
        picker.reloadAllComponents()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    @IBAction func clickPickerDone(_ sender: Any)
    {
        if pickerType == "country"
        {
            txtCountry.text = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: selectedIndex) as? String
            countryName = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: selectedIndex) as! String
            countryID = (countryArr.value(forKey: "id") as AnyObject).object(at: selectedIndex) as! NSString
        }
        else if pickerType == "state"
        {
            txtState.text = (stateArr.value(forKey: "name") as AnyObject).object(at: selectedIndex) as? String
            stateName = (stateArr.value(forKey: "name") as AnyObject).object(at: selectedIndex) as! String
            stateID = (stateArr.value(forKey: "id") as AnyObject).object(at: selectedIndex) as! NSString
            regionCode = "\((stateArr.value(forKey: "code") as AnyObject).object(at: selectedIndex))"
            
            self.getCityList()
        }
        else
        {
            if languageID == "1"
            {
                txtCity.text = (cityArr.value(forKey: "city") as AnyObject).object(at: selectedIndex) as? String
            }
            else
            {
                txtCity.text = (cityArr.value(forKey: "city_ar") as AnyObject).object(at: selectedIndex) as? String
            }
        }
    }

    // MARK: TableView Methods
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == tblAddressList
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblAddressList
        {
            return addressArr.count
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let view = UIView()
        return view
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ShipAddressTableViewCell = self.tblAddressList.dequeueReusableCell(withIdentifier: "Cell") as! ShipAddressTableViewCell
        
        if tableView == tblAddressList
        {
            let firstName = "\((addressArr.object(at: indexPath.row) as AnyObject).value(forKey: "firstname")!)"
            let lastname = "\((addressArr.object(at: indexPath.row) as AnyObject).value(forKey: "lastname")!)"
            let street = "\(((addressArr.object(at: indexPath.row) as AnyObject).value(forKey: "street") as AnyObject).object(at: 0))"
            let city = "\((addressArr.object(at: indexPath.row) as AnyObject).value(forKey: "city")!)"
            let region = "\(((addressArr.object(at: indexPath.row) as AnyObject).value(forKey: "region") as AnyObject).value(forKey: "region")!)"
            let country = "\((addressArr.object(at: indexPath.row) as AnyObject).value(forKey: "country_id")!)"
            let postcode = "\((addressArr.object(at: indexPath.row) as AnyObject).value(forKey: "postcode")!)"
            let telephone = "\((addressArr.object(at: indexPath.row) as AnyObject).value(forKey: "telephone")!)"
            
            cell.lblAddress.text = "\(firstName) \(lastname), \(street), \(city), \(region), \(country), \(postcode), \(telephone)."
            
            if selectedAddress == indexPath.row
            {
                cell.radioImage.image = UIImage (named : "ic_radio_selected")
            }
            else
            {
                cell.radioImage.image = UIImage (named : "ic_radio_unselected")
            }
        }
        else
        {
            
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == self.tblAddressList
        {
            return 72
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblAddressList
        {
            print(addressArr.object(at: indexPath.row))
            selectedAddress = indexPath.row
            self.tblAddressList.reloadData()
        }
    }
    
    //MARK: MapView Methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last
        
        if location != nil
        {
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
            
            self.mapView?.animate(to: camera)
            
            // Creates a marker in the center of the map.
             let marker = GMSMarker()
            let coordinate = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
            marker.position = coordinate
             marker.appearAnimation = .pop
             marker.map = mapView
            
            let aGMSGeocoder: GMSGeocoder = GMSGeocoder()
            aGMSGeocoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
                if let gmsAddress: GMSAddress = response?.firstResult() {
                    self.updateAddress(gmsAddress: gmsAddress)
                }
            }
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        print(coordinate.latitude, coordinate.longitude)
        self.selectedLat = coordinate.latitude
        self.selectedLong = coordinate.longitude
        
        self.addressFromLatLong(lat: "\(self.selectedLat)", long: "\(self.selectedLong)")
    }
    
    func addressFromLatLong(lat: String, long: String) {
        let urlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(long)&key=AIzaSyCooZgkYA_PL_GeXd-zymYfAXZu_UHztvY"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
     //   request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      //  request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let result = responseObject.result.value! as AnyObject
                            if let status = result.value(forKey: "status")
                            {
                                if status as! String == "OK"
                                {
                                    let address = (result.value(forKey: "results") as AnyObject).object(at: 0) as AnyObject
                                    let addressComponents = address.value(forKey: "address_components") as AnyObject
                                    
                                    print(addressComponents)
                                    
                                    var isInsideServiceLocation = false
                                    
                                    for i in 0..<addressComponents.count
                                    {
                                        print(addressComponents.object(at: i))
                                        let types = (addressComponents[i] as AnyObject).value(forKey: "types") as! NSArray
                                        
                                        if types.contains("country")
                                        {
                                            let country = (addressComponents[i] as AnyObject).value(forKey: "short_name") as! String
                                            
                                            if self.countryNames.contains(country)
                                            {
                                                isInsideServiceLocation = true
                                            }
                                        }
                                    }
                                    
                                    var isStreet = false
                                    var isArea = false
                                    var isCity = false
                                    
                                    var cityValue = ""
                                    
                                    if isInsideServiceLocation
                                    {
                                        self.txtStreet.text = ""
                                        self.txtState.text = ""
                                        self.txtCity.text = ""
                                        self.txtCountry.text = ""
                                        self.txtPostalCode.text = ""
                                        
                                        for i in 0..<addressComponents.count
                                        {
                                            let types = (addressComponents[i] as AnyObject).value(forKey: "types") as! NSArray
                                            
                                            if types.contains("sublocality_level_2")
                                            {
                                                self.txtStreet.text = (addressComponents[i] as AnyObject).value(forKey: "long_name") as? String
                                                
                                                isStreet = true
                                            }
                                            else if types.contains("sublocality_level_1")
                                            {
                                                cityValue = "\((addressComponents[i] as AnyObject).value(forKey: "long_name")!)"
                                                
                                                isArea = true
                                            }
                                            else if types.contains("administrative_area_level_2")
                                            {
                                                cityValue = "\((addressComponents[i] as AnyObject).value(forKey: "long_name")!)"
                                                
                                                isCity = true
                                            }
                                            else if types.contains("administrative_area_level_1")
                                            {
                                                //  self.txtState.text = (addressComponents[i] as AnyObject).value(forKey: "long_name") as? String
                                                //  stateID = (addressComponents[i] as AnyObject).value(forKey: "short_name") as! NSString
                                            }
                                            else if types.contains("postal_code")
                                            {
                                                self.txtPostalCode.text = (addressComponents[i] as AnyObject).value(forKey: "long_name") as? String
                                            }
                                            else if types.contains("country")
                                            {
                                                // self.txtCountry.text = (addressComponents[i] as AnyObject).value(forKey: "long_name") as? String
                                                //  countryID = (addressComponents[i] as AnyObject).value(forKey: "short_name") as! NSString
                                            }
                                        }
                                        
                                        if !isStreet
                                        {
                                            for i in 0..<addressComponents.count
                                            {
                                                let types = (addressComponents[i] as AnyObject).value(forKey: "types") as! NSArray
                                                
                                                if types.contains("route")
                                                {
                                                    self.txtStreet.text = (addressComponents[i] as AnyObject).value(forKey: "long_name") as? String
                                                }
                                            }
                                        }
                                        
                                        if isArea && isStreet && isCity
                                        {
                                            var area = ""
                                            for i in 0..<addressComponents.count
                                            {
                                                let types = (addressComponents[i] as AnyObject).value(forKey: "types") as! NSArray
                                                
                                                if types.contains("sublocality_level_1")
                                                {
                                                    area = (addressComponents[i] as AnyObject).value(forKey: "long_name") as! String
                                                }
                                                else if types.contains("administrative_area_level_2")
                                                {
                                                    cityValue = "\((addressComponents[i] as AnyObject).value(forKey: "long_name")!)"
                                                }
                                            }
                                            self.txtStreet.text = "\(self.txtStreet.text!), \(area)"
                                        }
                                        if !isCity && isArea
                                        {
                                            for i in 0..<addressComponents.count
                                            {
                                                let types = (addressComponents[i] as AnyObject).value(forKey: "types") as! NSArray
                                                
                                                if types.contains("sublocality_level_1")
                                                {
                                                    cityValue = "\((addressComponents[i] as AnyObject).value(forKey: "long_name")!)"
                                                }
                                            }
                                        }
                                        if !isCity && !isArea
                                        {
                                            for i in 0..<addressComponents.count
                                            {
                                                let types = (addressComponents[i] as AnyObject).value(forKey: "types") as! NSArray
                                                
                                                if types.contains("administrative_area_level_1")
                                                {
                                                    cityValue = "\((addressComponents[i] as AnyObject).value(forKey: "long_name")!)"
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        self.showToast(message: "We are not available still at your selected location, Please select some other location")
                                    }
                                    
                                    for i in 0..<self.cityArr.count
                                    {
                                        let name = "\((self.cityArr.object(at: i) as AnyObject).value(forKey: "city")!)"
                                        
                                        if cityValue.lowercased() == name.lowercased()
                                        {
                                            self.txtCity.text = name
                                            self.selectedIndex = i
                                        }
                                    }
                                    
                                    let formattedAdd = addressComponents.value(forKey: "formatted_address")!
                                    self.mapView.clear()
                                    let coordinate = CLLocationCoordinate2D(latitude: self.selectedLat, longitude: self.selectedLong)
                                    let marker = GMSMarker()
                                    marker.position = coordinate
                                    marker.title = formattedAdd as? String
                                    marker.appearAnimation = .pop
                                    marker.map = self.mapView
                                    
                                    let aGMSGeocoder: GMSGeocoder = GMSGeocoder()
                                    aGMSGeocoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
                                        if let gmsAddress: GMSAddress = response?.firstResult() {
                                            self.updateAddress(gmsAddress: gmsAddress)
                                        }
                                    }
                                    self.locationManager.stopUpdatingLocation()
                                }
                                else
                                {
                                    print(status)
                                }
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func updateAddress(gmsAddress: GMSAddress) {
        if let city = gmsAddress.locality,
            let street = gmsAddress.thoroughfare,
            let postal = gmsAddress.postalCode {
            self.txtCity.text = city
            self.txtStreet.text = street
            self.txtPostalCode.text = postal
        }
        print("subLocality=\(gmsAddress.subLocality)")
        print("administrativeArea=\(gmsAddress.administrativeArea)")
        print("lines=\(gmsAddress.lines)")
    }
    
    // Country List
    func getCountryListNew()
    {
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "\(ConfigUrl.baseUrl)directory/countries"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            let result = responseObject.result.value! as! NSArray
                            countryArr = result.mutableCopy() as! NSMutableArray
                            stateArr = ((countryArr.object(at: 0) as AnyObject).value(forKey: "available_regions") as! NSArray).mutableCopy() as! NSMutableArray
                            self.countryIDStr = (((responseObject.result.value!) as AnyObject).object(at: 0) as AnyObject).value(forKeyPath: "id") as! String
                            countryID = self.countryIDStr as NSString
                            
                            if countryArr.count <= 1
                            {
                                if countryArr.count == 1
                                {
                                    self.txtCountry.text = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: 0) as? String
                                    self.countryName = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: 0) as! String
                                    countryID = (countryArr.value(forKey: "id") as AnyObject).object(at: 0) as! NSString
                                }
                                
                                for i in 0..<countryArr.count
                                {
                                    let id = "\((countryArr.value(forKey: "id") as AnyObject).object(at: 0))"
                                    
                                    if id == "\(countryID)"
                                    {
                                        stateArr = ((countryArr.object(at: i) as AnyObject).value(forKey: "available_regions") as! NSArray).mutableCopy() as! NSMutableArray
                                    }
                                }
                                
                                if stateArr.count <= 1
                                {
                                    if stateArr.count == 1
                                    {
                                        self.txtState.text = (stateArr.value(forKey: "name") as AnyObject).object(at: 0) as? String
                                        stateName = (stateArr.value(forKey: "name") as AnyObject).object(at: 0) as! String
                                        stateID = (stateArr.value(forKey: "id") as AnyObject).object(at: 0) as! NSString
                                        self.regionCode = "\((stateArr.value(forKey: "code") as AnyObject).object(at: 0))"
                                        
                                        self.getCityList()
                                    }
                                }
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func getCityList()
    {
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "https://balleh.com/mobifilter/index/mcity?country_id=\(countryID)&state_id=\(stateID)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            let result = responseObject.result.value! as! NSArray
                            self.cityArr = result
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
}

extension ShippingAddressVC : UIPickerViewDelegate, UIPickerViewDataSource {
    //MARK: PickerView Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if pickerType == "country"
        {
            return countryArr.count
        }
        else if pickerType == "state"
        {
            return stateArr.count
        }
        else
        {
            return cityArr.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerType == "country"
        {
            let name = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: row) as! String
            
            return name
        }
        else if pickerType == "state"
        {
            let name = (stateArr.value(forKey: "name") as AnyObject).object(at: row) as! String
            
            return name
        }
        else
        {
            var name = ""
            if languageID == "1"
            {
                name = (cityArr.value(forKey: "city") as AnyObject).object(at: row) as! String
            }
            else
            {
                name = (cityArr.value(forKey: "city_ar") as AnyObject).object(at: row) as! String
            }
            
            return name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectedIndex = row
        
        if languageID == "1" {
            txtCity.text = (cityArr.value(forKey: "city") as AnyObject).object(at: selectedIndex) as? String
        } else {
            txtCity.text = (cityArr.value(forKey: "city_ar") as AnyObject).object(at: selectedIndex) as? String
        }
    }
}

extension ShippingAddressVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtCity { return false }
        return true
    }
}
