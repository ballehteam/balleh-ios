//
//  CartVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 20/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire

class CartVC: ParentViewController, loginIntimation
{
    @IBOutlet weak var tblCart: UITableView!
    @IBOutlet weak var btnCheckout: UIButton!
    var cartArr = NSMutableArray()
    var cartCount = ""
    var totalArr = NSMutableArray()
    var imagesArr = NSMutableArray()
    var isProceed = false
    
    var isLoginSuccess = false
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var headerImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerImage.image = Tools.headerImage()
        btnTitle.addTarget(self, action: #selector(self.clickTitle), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(self.clickBack(sender:)), for: .touchUpInside)
        
        if languageID == "2" {
            imgBack.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        // Do any additional setup after loading the view.
    }
    
    @objc func clickBack(sender: AnyObject)
    {
        if orderStatus == "canceled"
        {
            self.gotoRootViewController()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: {})
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.btnCheckout.isHidden = true
        self.imagesArr.removeAllObjects()
        self.tabBarController?.tabBar.isHidden = true
        
        orderStatus = ""
        
        if isLoginSuccess
        {
            isLoginSuccess = false
            
            if !isProceed
            {
                userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
                
                let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items"
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.get.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON
                        { (responseObject) -> Void in
                            
                            if responseObject.result.isSuccess
                            {
                                SharedManager.dismissHUD(viewController: self)
                                
                                if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                {
                                    print(responseObject.result.value)
                                    self.cartArr = (responseObject.result.value! as! NSArray).mutableCopy() as! NSMutableArray
                                    
                                    if self.cartArr.count != 0
                                    {
                                        self.btnCheckout.isHidden = false
                                        for i in 0..<self.cartArr.count
                                        {
                                            self.imagesArr.add("")
                                        }
                                        
                                        var count = 0
                                        
                                        for i in 0..<self.cartArr.count
                                        {
                                            let qty = "\((self.cartArr[i] as AnyObject).value(forKey:"qty")!)"
                                            
                                            count = count + Int(qty)!
                                            
                                            self.getProductImages(userId: userIDStr, type: "user", sku: "\((self.cartArr[i] as AnyObject).value(forKey:"sku")!)", index: i)
                                        }
                                        
                                        //   self.setRightNavigationButton(withBadge: "\(count)")
                                        
                                        self.getTotals(userId: userIDStr, type: "user")
                                        self.tblCart.reloadData()
                                    }
                                    else
                                    {
                                        self.btnCheckout.isHidden = true
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your Cart is Empty", comment: ""))!", viewController: self)
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                }
                            }
                            if responseObject.result.isFailure
                            {
                                SharedManager.dismissHUD(viewController: self)
                                let error : Error = responseObject.result.error!
                                print(error.localizedDescription)
                            }
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
            else
            {
                isGuest = false
                
                userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
                cartID = getCartID(cusToken: userIDStr)
                print(self.cartArr)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    
                    for i in 0..<self.cartArr.count
                    {
                        print(self.cartArr[i])
                        
                        let values = [ "quote_id" : "\(cartID)",
                            "sku" : "\((self.cartArr[i] as AnyObject).value(forKey: "sku")!)",
                            "qty" : "\((self.cartArr[i] as AnyObject).value(forKey: "qty")!)"
                        ]
                        
                        let params = ["cart_item" : values] as [String : Any]
                        
                        let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items"
                        
                        SharedManager.showHUD(viewController: self)
                        
                        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                        var request = URLRequest(url: URL(string: setFinalURl)!)
                        request.httpMethod = HTTPMethod.post.rawValue
                        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                        request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                        
                        let setTemp: [String : Any] = params
                        
                        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                            let jsonString = String(data: jsonData , encoding: .utf8)!
                            print(jsonString as Any)
                            request.httpBody = jsonData
                        }
                        
                        if Connectivity.isConnectedToInternet()
                        {
                            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                                
                                SharedManager.dismissHUD(viewController: self)
                                if responseObject.result.isSuccess
                                {
                                    SharedManager.dismissHUD(viewController: self)
                                    if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                    {
                                        print(responseObject.result.value!)
                                        
                                        if (self.cartArr.count - 1) == i
                                        {
                                            self.getCartItems()
                                        }
                                    }
                                    else
                                    {
                                        SharedManager.dismissHUD(viewController: self)
                                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                    }
                                }
                                
                                if responseObject.result.isFailure
                                {
                                    SharedManager.dismissHUD(viewController: self)
                                    let error : Error = responseObject.result.error!
                                    print(error.localizedDescription)
                                }
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                        }
                    }
                }
                
                //   let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //  let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ShippingAddressVC") as! ShippingAddressVC
                //   self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        else
        {
            if let id = UserDefaults.standard.string(forKey: "USER_ID")
            {
                if id != ""
                {
                    if cartID == ""
                    {
                        cartID = self.createCartID(cusToken: id)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2)
                        {
                            userIDStr = id
                            
                            let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items"
                            SharedManager.showHUD(viewController: self)
                            
                            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                            var request = URLRequest(url: URL(string: setFinalURl)!)
                            request.httpMethod = HTTPMethod.get.rawValue
                            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                            
                            if Connectivity.isConnectedToInternet()
                            {
                                Alamofire.request(request).responseJSON
                                    { (responseObject) -> Void in
                                        
                                        if responseObject.result.isSuccess
                                        {
                                            SharedManager.dismissHUD(viewController: self)
                                            
                                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                            {
                                                self.cartArr = (responseObject.result.value! as! NSArray).mutableCopy() as! NSMutableArray
                                                print(self.cartArr)
                                                if self.cartArr.count != 0
                                                {
                                                    self.btnCheckout.isHidden = false
                                                    for i in 0..<self.cartArr.count
                                                    {
                                                        self.imagesArr.add("")
                                                    }
                                                    
                                                    var count = 0
                                                    
                                                    for i in 0..<self.cartArr.count
                                                    {
                                                        let qty = "\((self.cartArr[i] as AnyObject).value(forKey:"qty")!)"
                                                        
                                                        count = count + Int(qty)!
                                                        
                                                        self.getProductImages(userId: userIDStr, type: "user", sku: "\((self.cartArr[i] as AnyObject).value(forKey:"sku")!)", index: i)
                                                    }
                                                    
                                                    //  self.setRightNavigationButton(withBadge: "\(count)")
                                                    self.getTotals(userId: userIDStr, type: "user")
                                                    self.tblCart.reloadData()
                                                }
                                                else
                                                {
                                                    self.btnCheckout.isHidden = true
                                                    self.tblCart.reloadData()
                                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your Cart is Empty", comment: ""))", viewController: self)
                                                }
                                            }
                                            else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                                            {
                                                
                                                self.createGuestCartID(completionHandler: {(result) -> () in
                                                    cartID = result
                                                })
                                            }
                                            else
                                            {
                                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                            }
                                        }
                                        if responseObject.result.isFailure
                                        {
                                            SharedManager.dismissHUD(viewController: self)
                                            let error : Error = responseObject.result.error!
                                            print(error.localizedDescription)
                                        }
                                        
                                        SharedManager.dismissHUD(viewController: self)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                            }
                        }
                    }
                    else
                    {
                        cartID = self.getCartID(cusToken: id)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2)
                        {
                            userIDStr = id
                            
                            let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items"
                            SharedManager.showHUD(viewController: self)
                            
                            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                            var request = URLRequest(url: URL(string: setFinalURl)!)
                            request.httpMethod = HTTPMethod.get.rawValue
                            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                            
                            if Connectivity.isConnectedToInternet()
                            {
                                Alamofire.request(request).responseJSON
                                    { (responseObject) -> Void in
                                        
                                        if responseObject.result.isSuccess {
                                            SharedManager.dismissHUD(viewController: self)
                                            
                                            if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                                                self.cartArr = (responseObject.result.value! as! NSArray).mutableCopy() as! NSMutableArray
                                                print(self.cartArr)
                                                if self.cartArr.count != 0 {
                                                    self.btnCheckout.isHidden = false
                                                    for i in 0..<self.cartArr.count {
                                                        self.imagesArr.add("")
                                                    }
                                                    
                                                    var count = 0
                                                    
                                                    for i in 0..<self.cartArr.count {
                                                        let qty = "\((self.cartArr[i] as AnyObject).value(forKey:"qty")!)"
                                                        
                                                        count = count + Int(qty)!
                                                        
                                                        self.getProductImages(userId: userIDStr, type: "user", sku: "\((self.cartArr[i] as AnyObject).value(forKey:"sku")!)", index: i)
                                                    }
                                                    
                                                    //  self.setRightNavigationButton(withBadge: "\(count)")
                                                    self.getTotals(userId: userIDStr, type: "user")
                                                    self.tblCart.reloadData()
                                                } else {
                                                    self.btnCheckout.isHidden = true
                                                    self.tblCart.reloadData()
                                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your Cart is Empty", comment: ""))", viewController: self)
                                                }
                                            } else if "\(String(describing: responseObject.response!.statusCode))" == "401" {
                                                
                                                self.createGuestCartID(completionHandler: {(result) -> () in
                                                    cartID = result
                                                })
                                            } else {
                                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                            }
                                        }
                                        if responseObject.result.isFailure {
                                            SharedManager.dismissHUD(viewController: self)
                                            let error : Error = responseObject.result.error!
                                            print(error.localizedDescription)
                                        }
                                        
                                        SharedManager.dismissHUD(viewController: self)
                                }
                            } else {
                                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                            }
                        }
                    }
                } else {
                    if cartID == "" {
                        createGuestCartID(completionHandler: {(result) -> () in
                            cartID = result
                            
                            self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                                
                                self.cartArr = result
                                
                                if self.cartArr.count != 0 {
                                    self.btnCheckout.isHidden = false
                                    for i in 0..<self.cartArr.count {
                                        self.imagesArr.add("")
                                    }
                                    
                                    var count = 0
                                    
                                    for i in 0..<self.cartArr.count {
                                        let qty = "\((self.cartArr[i] as AnyObject).value(forKey:"qty")!)"
                                        
                                        count = count + Int(qty)!
                                        
                                        self.getProductImages(userId: userIDStr, type: "user", sku: "\((self.cartArr[i] as AnyObject).value(forKey:"sku")!)", index: i)
                                    }
                                    
                                    //   self.setRightNavigationButton(withBadge: "\(count)")
                                    
                                    self.getTotals(userId: userIDStr, type: "guest")
                                    self.tblCart.reloadData()
                                } else {
                                    self.btnCheckout.isHidden = true
                                    self.tblCart.reloadData()
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your Cart is Empty", comment: ""))!", viewController: self)
                                }
                            })
                        })
                    } else {
                        getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                            
                            self.cartArr = result
                            print(self.cartArr)
                            if self.cartArr.count != 0
                            {
                                self.btnCheckout.isHidden = false
                                for i in 0..<self.cartArr.count
                                {
                                    self.imagesArr.add("")
                                }
                                
                                var count = 0
                                
                                for i in 0..<self.cartArr.count
                                {
                                    let qty = "\((self.cartArr[i] as AnyObject).value(forKey:"qty")!)"
                                    
                                    count = count + Int(qty)!
                                    
                                    self.getProductImages(userId: userIDStr, type: "user", sku: "\((self.cartArr[i] as AnyObject).value(forKey:"sku")!)", index: i)
                                }
                                
                                //   self.setRightNavigationButton(withBadge: "\(count)")
                                
                                self.getTotals(userId: userIDStr, type: "guest")
                                self.tblCart.reloadData()
                            }
                            else
                            {
                                self.btnCheckout.isHidden = true
                                self.tblCart.reloadData()
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your Cart is Empty", comment: ""))!", viewController: self)
                            }
                        })
                    }
                }
            }
            else
            {
                if cartID == ""
                {
                    createGuestCartID(completionHandler: {(result) -> () in
                        cartID = result
                        
                        self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                            
                            self.cartArr = result
                            
                            if self.cartArr.count != 0
                            {
                                self.btnCheckout.isHidden = false
                                for i in 0..<self.cartArr.count
                                {
                                    self.imagesArr.add("")
                                }
                                
                                var count = 0
                                
                                for i in 0..<self.cartArr.count
                                {
                                    let qty = "\((self.cartArr[i] as AnyObject).value(forKey:"qty")!)"
                                    
                                    count = count + Int(qty)!
                                    
                                    self.getProductImages(userId: userIDStr, type: "user", sku: "\((self.cartArr[i] as AnyObject).value(forKey:"sku")!)", index: i)
                                }
                                
                                //   self.setRightNavigationButton(withBadge: "\(count)")
                                
                                self.getTotals(userId: userIDStr, type: "guest")
                                self.tblCart.reloadData()
                            }
                            else
                            {
                                self.btnCheckout.isHidden = true
                                self.tblCart.reloadData()
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your Cart is Empty", comment: ""))!", viewController: self)
                            }
                        })
                    })
                }
                else
                {
                    getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                        
                        self.cartArr = result
                        
                        if self.cartArr.count != 0
                        {
                            self.btnCheckout.isHidden = false
                            for i in 0..<self.cartArr.count
                            {
                                self.imagesArr.add("")
                            }
                            
                            var count = 0
                            
                            for i in 0..<self.cartArr.count
                            {
                                let qty = "\((self.cartArr[i] as AnyObject).value(forKey:"qty")!)"
                                
                                count = count + Int(qty)!
                                
                                self.getProductImages(userId: userIDStr, type: "user", sku: "\((self.cartArr[i] as AnyObject).value(forKey:"sku")!)", index: i)
                            }
                            
                            //   self.setRightNavigationButton(withBadge: "\(count)")
                            
                            self.getTotals(userId: userIDStr, type: "guest")
                            self.tblCart.reloadData()
                        }
                        else
                        {
                            self.btnCheckout.isHidden = true
                            self.tblCart.reloadData()
                            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your Cart is Empty", comment: ""))!", viewController: self)
                        }
                    })
                }
            }
        }
    }
    
    func loginSuccess() {
        isLoginSuccess = true
       // self.viewWillAppear(true)
    }
    
    func loginFailure() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == tblCart
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblCart
        {
            if cartArr.count != 0
            {
                return self.cartArr.count + self.totalArr.count
            }
            else
            {
                return 0
            }
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let view = UIView()
        return view
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblCart {
            if indexPath.row >= self.cartArr.count {
                let cell:CheckOutTableViewCell = self.tblCart.dequeueReusableCell(withIdentifier: "totalCell") as! CheckOutTableViewCell
                let name = "\((totalArr[indexPath.row - (self.cartArr.count)] as AnyObject).value(forKey: "title") as! String)"
                
                cell.lblTotalTitle.text = Tools.cartText(text: name)
                
                /* if name == "Sub Total"
                 {
                    cell.lblTotalTitle.text = NSLocalizedString("Sub_Total", comment: "")
                 }
                 else if name == "Total"
                 {
                    cell.lblTotalTitle.text = NSLocalizedString("Order_Total", comment: "")
                 }*/
                
                if languageID == "1"
                {
                    cell.lblTotalValue.textAlignment = .left
                }
                else {
                    cell.lblTotalValue.textAlignment = .right
                }
                
                let value : String = {
                    if let amount = (totalArr[indexPath.row - (self.cartArr.count)] as AnyObject).value(forKey: "value") as? Double {
                        return "\(String(format: "%.2f", amount))"
                    } else if let value = (totalArr[indexPath.row - (self.cartArr.count)] as AnyObject).value(forKey: "value") as? Int {
                        return "\(value)"
                    } else {
                        return "\((totalArr[indexPath.row - (self.cartArr.count)] as AnyObject).value(forKey: "value")!)"
                    }
                }()
                
                cell.lblTotalValue.text = "\(value) \(Tools.getCurrency())"
                
                return cell
            } else {
                let cell:CheckOutTableViewCell = self.tblCart.dequeueReusableCell(withIdentifier: "cell") as! CheckOutTableViewCell
                cell.titleLbl.text = (self.cartArr.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
                cell.quantityLbl.text = "\((self.cartArr.object(at: indexPath.row) as AnyObject).value(forKey: "qty")!)"
                let price = "\((self.cartArr.object(at: indexPath.row) as AnyObject).value(forKey: "price")!) \(Tools.getCurrency())"
                cell.priceLbl.text = price
                
                if imagesArr.count == cartArr.count {
                    let imageUrl =  "\(ConfigUrl.imageUrl)\(imagesArr.object(at: indexPath.row))"
                    let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                    cell.productImage.sd_setImage(with: URL(string: trimmedUrl))
                }
                
                if (self.cartArr.object(at: indexPath.row) as AnyObject).value(forKey: "product_option") != nil {
                    cell.subText.isHidden = false
                } else {
                    cell.subText.isHidden = true
                }
                cell.increaseQuantityBtn.addTarget(self, action: #selector(increaseQuantity(_:)), for: UIControlEvents.touchUpInside)
                cell.increaseQuantityBtn.tag = indexPath.row;
                
                cell.deleteBtn.addTarget(self, action: #selector(deleteAction(_:)), for: UIControlEvents.touchUpInside)
                cell.deleteBtn.tag = indexPath.row;

                cell.decreaseQuantityBtn.tag = indexPath.row
                
                if cell.quantityLbl.text == "1"
                {
                    cell.decreaseQuantityBtn.addTarget(self, action: #selector(deleteAction(_:)), for: UIControlEvents.touchUpInside)
                }
                else
                {
                    cell.decreaseQuantityBtn.addTarget(self, action: #selector(decreaseQuantity(_:)), for: UIControlEvents.touchUpInside)
                }
                
                /*  let optionArray = (self.cartArr.object(at: indexPath.row) as AnyObject).value(forKey: "option") as! NSArray
                 var y:CGFloat = 42.0
                 
                 for temp in optionArray
                 {
                    if languageID == "1"
                    {
                        cell.nameLbl.frame = CGRect(x: 145, y: y, width: 52, height: 20)
                        cell.colonLbl.frame = CGRect(x: 210, y: y, width: 10, height: 20)
                        cell.textLbl.frame =  CGRect(x: 225, y: y, width: 52, height: 20)
                    }
                    else
                    {
                        cell.nameLbl.frame = CGRect(x: self.view.bounds.size.width - (cell.productImage.frame.size.width + 92), y: y, width: 52, height: 20)
                        cell.colonLbl.frame = CGRect(x: cell.nameLbl.frame.origin.x - 20, y: y, width: 10, height: 20)
                        cell.textLbl.frame =  CGRect(x: cell.colonLbl.frame.origin.x - 72, y: y, width: 52, height: 20)
                    }
                 
                    cell.nameLbl.font = UIFont(name: "Avenir", size: 12.0)
                    cell.nameLbl.text = (temp as AnyObject).object(forKey: "name") as? String
                 
                    cell.colonLbl.font = UIFont(name: "Avenir", size: 12.0)
                    cell.colonLbl.text = ":"
                 
                    cell.textLbl.font = UIFont(name: "Avenir", size: 12.0)
                    cell.textLbl.text = (temp as AnyObject).object(forKey: "value") as? String
                 
                    cell.contentView.addSubview(cell.nameLbl)
                    cell.contentView.addSubview(cell.colonLbl)
                    cell.contentView.addSubview(cell.textLbl)
                 
                    y = y + 20
                 }*/
                
                return cell
            }
        }
        else
        {
            let cell:CheckOutTableViewCell = self.tblCart.dequeueReusableCell(withIdentifier: "totalCell") as! CheckOutTableViewCell
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    @objc func deleteAction(_ sender : UIButton)
    {
        let alert = UIAlertController(title: "\(NSLocalizedString("Are you Sure!", comment: ""))", message: "\(NSLocalizedString("Do you want to delete?", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
            
            if !isGuest
            {
                self.deleteCartItem(cusToken: userIDStr, itemId: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "item_id")!)", type: "user")
            }
            else
            {
                self.deleteCartItem(cusToken: userIDStr, itemId: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "item_id")!)", type: "guest")
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteCartItem(cusToken: String, itemId: String, type: String)
    {
        if type == "user"
        {
            let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items/\(itemId)"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.delete.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
            
            SharedManager.showHUD(viewController: self)
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                print(responseObject.result.value!)
                                self.viewWillAppear(true)
                            }
                            else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                            {
                                self.createGuestCartID(completionHandler: {(result) -> () in
                                    cartID = result
                                })
                                
                                /*if userIDStr == ""
                                 {
                                 
                                 }
                                 else
                                 {
                                 if isGuest == false
                                 {
                                 let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                                 alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                 
                                 let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                 let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                 self.present(viewController, animated: true, completion: nil)
                                 }))
                                 alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
                                 isGuest = true
                                 if cartID == ""
                                 {
                                 self.createGuestCartID(completionHandler: {(result) -> () in
                                 cartID = result
                                 })
                                 }
                                 else
                                 {
                                 self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                                 // print(result)
                                 })
                                 }
                                 }))
                                 self.present(alert, animated: true, completion: nil)
                                 }
                                 }*/
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/items/\(itemId)"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.delete.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            //  request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
            
            SharedManager.showHUD(viewController: self)
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                print(responseObject.result.value!)
                                self.viewWillAppear(true)
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
    }
    
    @objc func increaseQuantity(_ sender : UIButton)
    {
        var options = NSDictionary()
        if let option = (self.cartArr[sender.tag] as AnyObject).value(forKey: "product_option")
        {
            if (option as AnyObject).count != 0
            {
                options = option as! NSDictionary
            }
        }
        
        if !isGuest
        {
            changeCart(cartId: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "quote_id")!)", sku: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "sku")!)", qty: "1", type: "user", options: options)
        }
        else
        {
            changeCart(cartId: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "quote_id")!)", sku: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "sku")!)", qty: "1", type: "guest", options: options)
        }
    }
    
    @objc func decreaseQuantity(_ sender : UIButton)
    {
        if !isGuest
        {
            let qty = "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "qty")!)"
            
            if Int(qty)! > 1
            {
                let itemId = "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "item_id")!)"
                let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items/\(itemId)"
                
                SharedManager.showHUD(viewController: self)
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.delete.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON
                        { (responseObject) -> Void in
                            
                            if responseObject.result.isSuccess
                            {
                                SharedManager.dismissHUD(viewController: self)
                                
                                if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                {
                                    var options = NSDictionary()
                                    if let option = (self.cartArr[sender.tag] as AnyObject).value(forKey: "product_option")
                                    {
                                        if (option as AnyObject).count != 0
                                        {
                                            options = option as! NSDictionary
                                        }
                                    }
                                    
                                    self.changeCart(cartId: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "quote_id")!)", sku: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "sku")!)", qty: "\(Int(qty)! - 1)", type: "user", options: options)
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Something went wrong", comment: ""))", viewController: self)
                                }
                            }
                            if responseObject.result.isFailure
                            {
                                SharedManager.dismissHUD(viewController: self)
                                let error : Error = responseObject.result.error!
                                print(error.localizedDescription)
                            }
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Minimum quantity must be atleast one", comment: ""))", viewController: self)
            }
        }
        else
        {
            let qty = "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "qty")!)"
            
            if Int(qty)! > 1
            {
                let itemId = "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "item_id")!)"
                let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/items/\(itemId)"
                
                SharedManager.showHUD(viewController: self)
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.delete.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
              //  request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON
                        { (responseObject) -> Void in
                            
                            if responseObject.result.isSuccess
                            {
                                SharedManager.dismissHUD(viewController: self)
                                
                                if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                {
                                    var options = NSDictionary()
                                    if let option = (self.cartArr[sender.tag] as AnyObject).value(forKey: "product_option")
                                    {
                                        if (option as AnyObject).count != 0
                                        {
                                            options = option as! NSDictionary
                                        }
                                    }
                                    
                                    self.changeCart(cartId: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "quote_id")!)", sku: "\((self.cartArr[sender.tag] as AnyObject).value(forKey: "sku")!)", qty: "\(Int(qty)! - 1)", type: "guest", options: options)
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Something went wrong", comment: ""))", viewController: self)
                                }
                            }
                            if responseObject.result.isFailure
                            {
                                SharedManager.dismissHUD(viewController: self)
                                let error : Error = responseObject.result.error!
                                print(error.localizedDescription)
                            }
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }

            }
            else
            {
                SharedManager.showAlertWithMessage(title: "Sorry!", alertMessage: "\(NSLocalizedString("Minimum quantity must be atleast one", comment: ""))", viewController: self)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == self.tblCart
        {
            if indexPath.row >= self.cartArr.count
            {
                return 50
            }
            else
            {
                return 134
            }
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row >= self.cartArr.count
        {
            
        }
        else
        {
            print(cartArr)
            let sku = "\(String(describing: ((cartArr[indexPath.row]) as AnyObject).value(forKey: "sku")!))"
            let id = "\(String(describing: ((cartArr[indexPath.row]) as AnyObject).value(forKey: "item_id")!))"
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
            viewController.productSku = sku
            viewController.productID = id
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func clickProceed(_ sender: AnyObject) {
        mainCartDict.setObject(self.cartArr, forKey: "cart_product_details" as NSCopying)
        mainCartDict.setObject(self.imagesArr, forKey: "product_images" as NSCopying)
        
        if let id = UserDefaults.standard.string(forKey: "USER_ID"),
            id != "" {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ShippingAddressVC") as! ShippingAddressVC
            self.present(viewController, animated: true, completion: {})
        } else {
            self.isProceed = true
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            viewController.delegate = self
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    
    /*func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        print("\(buttonIndex)")
        switch (buttonIndex){
            
        case 0:
            print("Cancel")
        case 1:
            print("Save")
        case 2:
            print("Delete")
        default:
            print("Default")
            //Some code here..
            
        }
    }*/
    
    //MARK: API Methods
    
    func getCartItems()
    {
        userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
        
        let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            self.cartArr = (responseObject.result.value! as! NSArray).mutableCopy() as! NSMutableArray
                            print(self.cartArr)
                            
                            if self.cartArr.count != 0
                            {
                                self.btnCheckout.isHidden = false
                                for i in 0..<self.cartArr.count
                                {
                                    self.imagesArr.add("")
                                }
                                
                                var count = 0
                                
                                for i in 0..<self.cartArr.count
                                {
                                    let qty = "\((self.cartArr[i] as AnyObject).value(forKey:"qty")!)"
                                    
                                    count = count + Int(qty)!
                                    
                                    self.getProductImages(userId: userIDStr, type: "user", sku: "\((self.cartArr[i] as AnyObject).value(forKey:"sku")!)", index: i)
                                }
                                
                                //   self.setRightNavigationButton(withBadge: "\(count)")
                                
                                self.getTotals(userId: userIDStr, type: "user")
                                self.tblCart.reloadData()
                            }
                            else
                            {
                                self.btnCheckout.isHidden = true
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your Cart is Empty", comment: ""))!", viewController: self)
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func getTotals(userId: String, type: String)
    {
        if type == "user"
        {
            var result = NSMutableArray()
            let urlStr = "\(ConfigUrl.baseUrl)carts/mine/totals"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userId)", forHTTPHeaderField: "Authorization")
            
            SharedManager.showHUD(viewController: self)
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            
                            self.totalArr.removeAllObjects()
                            
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                let testArr = (responseObject.result.value! as AnyObject).value(forKey: "total_segments") as! NSArray
                                print(responseObject.result.value)
                                
                                for i in 0..<testArr.count
                                {
                                    let value = "\((testArr[i] as AnyObject).value(forKey: "value")!)"
                                    if value != "<null>"
                                    {
                                        if Float(value)! != 0
                                        {
                                            self.totalArr.add(testArr[i])
                                        }
                                    }
                                }
                                
                                self.tblCart.reloadData()
                            }
                            else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                            {
                                
                                self.createGuestCartID(completionHandler: {(result) -> () in
                                    cartID = result
                                })
                                
                                /*if userIDStr == ""
                                 {
                                 
                                 }
                                 else
                                 {
                                 if isGuest == false
                                 {
                                 let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                                 alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                 
                                 let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                 let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                 self.present(viewController, animated: true, completion: nil)
                                 }))
                                 alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
                                 isGuest = true
                                 if cartID == ""
                                 {
                                 self.createGuestCartID(completionHandler: {(result) -> () in
                                 cartID = result
                                 })
                                 }
                                 else
                                 {
                                 self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                                 // print(result)
                                 })
                                 }
                                 }))
                                 self.present(alert, animated: true, completion: nil)
                                 }
                                 }*/
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/totals"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
           // request.setValue("Bearer \(userId)", forHTTPHeaderField: "Authorization")
            
            SharedManager.showHUD(viewController: self)
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            
                            self.totalArr.removeAllObjects()
                            
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                let testArr = (responseObject.result.value! as AnyObject).value(forKey: "total_segments") as! NSArray
                                print(responseObject.result.value)
                                for i in 0..<testArr.count
                                {
                                    let value = "\((testArr[i] as AnyObject).value(forKey: "value")!)"
                                    if value != "<null>"
                                    {
                                        if Float(value)! != 0
                                        {
                                            self.totalArr.add(testArr[i])
                                        }
                                    }
                                }
                                
                                self.tblCart.reloadData()
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
    }
    
    func getProductImages(userId: String, type : String, sku: String, index: Int)
    {
        let urlStr = "\(ConfigUrl.baseUrl)products/\(sku)/media"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        // SharedManager.showHUD(viewController: self)
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        print(responseObject.result.value!)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let result = (responseObject.result.value! as AnyObject).object(at: 0) as AnyObject
                            self.imagesArr.replaceObject(at: index, with: "\(result.value(forKey: "file")!)")
                            
                            print(self.imagesArr)
                            
                            self.tblCart.reloadData()
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func changeCart(cartId: String, sku: String, qty: String, type : String, options: NSDictionary)
    {
        if type == "user"
        {
            var values = [String : Any]()
            
            if options.count != 0
            {
                values = [ "quote_id" : cartId,
                           "sku" : sku,
                           "qty" : qty,
                           "product_type" : "simple",
                           "product_option" : options
                ]
            }
            else
            {
                values = [ "quote_id" : cartId,
                           "sku" : sku,
                           "qty" : qty
                ]
            }
            
            let params = ["cart_item" : values] as [String : Any]
            
            let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)!
                print(jsonString as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    // SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            self.viewWillAppear(true)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                    
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            var values = [String : Any]()
            
            if options.count != 0
            {
                values = [ "quote_id" : cartId,
                           "sku" : sku,
                           "qty" : qty,
                           "product_type" : "simple",
                           "product_option" : options
                ]
            }
            else
            {
                values = [ "quote_id" : cartId,
                           "sku" : sku,
                           "qty" : qty
                ]
            }
            
            let params = ["cart_item" : values] as [String : Any]
            
            let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/items"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
          //  request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)!
                print(jsonString as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    // SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            self.viewWillAppear(true)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
    }
}
