//
//  ConfirmWebView.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 21/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

protocol SecurityAuth {
    func Success(orderId: String)
    func Failure()
}

import UIKit

class ConfirmWebView: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBAction func backAction(_ sender: Any) {
        DispatchQueue.main.async(execute: { () -> Void in
            self.dismiss(animated: false, completion: nil)
            self.delegate?.Failure()
        })
    }
    
    var delegate : SecurityAuth?
    
    var urlToLoad : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if languageID == "1" {
            self.backButton.setImage(UIImage(named: "right-arrow"), for: .normal)
        } else {
            self.backButton.setImage(UIImage(named: "left-arrowback"), for: .normal)
        }
        
        webView.delegate = self
        if let url = self.urlToLoad {
            webView.loadRequest(URLRequest(url: URL(string: url)!))
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SharedManager.dismissHUD(viewController: self)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        SharedManager.showHUD(viewController: self)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(request.url)
        guard let url = request.url?.absoluteString else {
            return true
        }
        if url.contains("checkoutcomVerifySuccess") {
            if let range = url.range(of: "order_id=") {
                let orderId = url[range.upperBound...]
                print(orderId)
                DispatchQueue.main.async(execute: { () -> Void in
                    self.dismiss(animated: false, completion: nil)
                    self.delegate?.Success(orderId: String(orderId))
                })
            }
            return false
        } else if url.contains("checkoutcomVerifyFailure") {
            DispatchQueue.main.async(execute: { () -> Void in
                self.dismiss(animated: false, completion: nil)
                self.delegate?.Failure()
            })
            return false
        } else {
            return true
        }
        
        // Success = https://balleh.com/checkoutcomVerifySuccess/report.php?order_incId=2000004605
        // Failure = (https://balleh.com/checkoutcomVerifyFailure/report.php?order_incId=2000004605)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
