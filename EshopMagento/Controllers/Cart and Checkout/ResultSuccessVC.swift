//
//  ResultSuccessVC.swift
//  EshopMagento
//
//  Created by Apple on 20/09/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire

class ResultSuccessVC: ParentViewController {
    @IBOutlet weak var lblOrderId: UILabel!
    
    var orderID : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblOrderId.text = "\(Texts.YourOrder) \(orderID)"
        
        self.updateDevice(orderId: orderID)
        
        self.orderNotification(orderId: orderID)
        
    }
    
    func updateDevice(orderId: String){
        let request = UpdateOrderDevice(orderId: orderId)
        request.start()
    }
    
    func orderNotification(orderId: String){
        let request = OrderNotification(orderId: orderId)
        request.start()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickContinue(_ sender: Any) {
        self.gotoRootViewController()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
