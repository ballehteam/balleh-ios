//
//  FavoritesVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 20/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Intercom

class FavoritesVC: ParentViewController, loginIntimation {
    
    @IBOutlet weak var tblFavourites: UITableView!
    
    var favArr = [WishList]()
    var addressArr = NSMutableArray()
    
    override func onSuccess(message: String) {
        SharedManager.dismissHUD(viewController: self)
        self.getWishlist()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        expandedSection.removeAllObjects()
        self.title = NSLocalizedString("Wishlist", comment: "")
        setLeftNavigationButtonHome()
        self.setRightNavigationButton()
        updateToken()
//        if UserDefaults.standard.string(forKey: "USER_ID") == nil  {
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            viewController.delegate = self
//            self.present(viewController, animated: true, completion: nil)
//            return
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setRightNavigationButton()
        if let id = UserDefaults.standard.string(forKey: "USER_ID") {
            userIDStr = id
            self.getWishlist()
        } else {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            viewController.delegate = self
            self.present(viewController, animated: true, completion: nil)
            return
        }
        /*
        if userIDStr == "" {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        } else {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        }
        
        if(UserDefaults.standard.object(forKey: kUserDetails) == nil) {
           // let nextVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
           // self.navigationController?.pushViewController(nextVC, animated: true)
            
            if let data = UserDefaults.standard.object(forKey: kAddToWishlist) as? Data {
                                
                if self.favArr.count == 0 {
                    self.tblFavourites.reloadData()
                    self.emptyList()
                } else {
                    self.tblFavourites.reloadData()
                }
            } else {
                self.emptyList()
            }
        } else {
            if let data = UserDefaults.standard.object(forKey: kAddToWishlist) as? Data {
                
                if self.favArr.count == 0
                {
                    self.tblFavourites.reloadData()
                    self.emptyList()
                }
                else
                {
                    self.tblFavourites.reloadData()
                }
            }
            else
            {
                self.emptyList()
            }
            
            // self.addToWishList()
        }
         */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getWishlist() {
        if Connectivity.isConnectedToInternet() {
            SharedManager.showHUD(viewController: self)
            let request = GetWishlist()
            request.response = self
            request.start()
        } else {
            self.showAlertWith(message: Texts.NO_INTERNET_CONNECTION)
        }
    }
    
    func deleteWishlist(productId : String) {
        if Connectivity.isConnectedToInternet() {
            SharedManager.showHUD(viewController: self)
            let request = DeleteWishlist(productId: productId)
            request.mainResponse = self
            request.start()
        } else {
            self.showAlertWith(message: Texts.NO_INTERNET_CONNECTION)
        }
        
    }
    
    func emptyList() {
        let alert = UIAlertController(title: "\(NSLocalizedString("Sorry", comment: ""))", message: "\(NSLocalizedString("Your Favourite List is Empty", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: TableView Methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblFavourites {
            return 1
        } else if tableView == mainTableView {
            return self.mainTblArray.count
        } else {
            if categoryDict.count != 0 {
                return categoryDict.count
            } else {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblFavourites {
            return favArr.count
        } else if tableView == mainTableView {
            return mainTblArray[section].count
        } else {
            if categoryDict.count != 0 {
                return ((categoryDict[section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count
            } else {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tblFavourites {
            
            let cell:FavTableViewCell = self.tblFavourites.dequeueReusableCell(withIdentifier: "FavCell") as! FavTableViewCell
            
            let product = self.favArr[indexPath.row]
            
            cell.lblName.text = product.name
            
            var imageUrl = ""
            
            if let url = product.smallImage {
                imageUrl = url
            }
            
            cell.lblPrice.text = product.price
            
            if let splPrice = product.specialPrice {
                cell.lblSplPrice.isHidden = false
                cell.lblSplPrice.text = splPrice
            } else {
                cell.lblSplPrice.isHidden = true
            }
            
            cell.lblName.text = product.name
            
            cell.lblPrice.sizeToFit()
            cell.lblSplPrice.frame.origin.x = cell.lblPrice.frame.origin.x + cell.lblPrice.frame.width + 5
            cell.lblSplPrice.sizeToFit()
            
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20")
            
            var activityLoader = UIActivityIndicatorView()
            activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityLoader.center = cell.imgProduct.center
            activityLoader.startAnimating()
            cell.imgProduct.addSubview(activityLoader)
            
            cell.imgProduct.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
                
                if image != nil {
                    activityLoader.stopAnimating()
                } else {
                    print("image not found")
                    cell.imgProduct.image = UIImage(named: "no_image")
                    activityLoader.stopAnimating()
                }
            })
            
            cell.imgProduct.contentMode = UIViewContentMode.scaleAspectFit
            
            cell.btnRemove.addTarget(self, action: #selector(self.deleteAction(_:)), for: .touchUpInside)
            cell.btnRemove.tag = indexPath.row
            
            return cell
        } else if tableView == mainTableView {
            if indexPath.section != 1 {
                if indexPath.section == 0 {
                    if indexPath.row != 0 {
                        
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                        
                        /* if cell1 == nil {
                         var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                         for view: UIView in views {
                         if (view is UITableViewCell) {
                         cell1 = view as? DetailCell
                         }
                         }
                         }*/
                        
                        let subArr = mainTblArray[indexPath.section]
                        cell1.title.text = subArr[indexPath.row]
                        
                        let subArr2 = iconsArr[indexPath.section]
                        cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                        if languageID == "1" {
                            cell1.imgArrow.image = UIImage(named: "left-arrow")
                        } else {
                            cell1.imgArrow.image = UIImage(named: "right-arrow1")
                        }
                        cell1.imgArrow.contentMode = .scaleAspectFit
                        cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                        cell1.imgIcon.tintColor = UIColor.gray
                        cell1.selectionStyle = .none
                        
                        return cell1
                    } else {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as! LanguageCell
                        
                        if languageID == "1" {
                            //selectedSegmentIndex = 0
                            cell1.segmentView.selectedSegmentIndex = 0
                        } else {
                            cell1.segmentView.selectedSegmentIndex = 1
                        }
                        
                        cell1.segmentView.addTarget(self, action: #selector(ParentViewController.clickLang(_:)), for: UIControlEvents.valueChanged)
                        
                        return cell1
                    }
                } else {
                    let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                    
                    /* if cell1 == nil {
                     var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                     for view: UIView in views {
                     if (view is UITableViewCell) {
                     cell1 = view as? DetailCell
                     }
                     }
                     }*/
                    
                    let subArr = mainTblArray[indexPath.section]
                    cell1.title.text = subArr[indexPath.row]
                    
                    let subArr2 = iconsArr[indexPath.section]
                    cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                    if languageID == "1" {
                        cell1.imgArrow.image = UIImage(named: "left-arrow")
                    } else {
                        cell1.imgArrow.image = UIImage(named: "right-arrow1")
                    }
                    cell1.imgArrow.contentMode = .scaleAspectFit
                    cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                    cell1.imgIcon.tintColor = UIColor.gray
                    cell1.selectionStyle = .none
                    
                    return cell1
                }
            } else {
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "CatCell") as! categoryTCell
                cell1.insideTableView.isScrollEnabled = false
                cell1.insideTableView.dataSource = self
                cell1.insideTableView.delegate = self
                cell1.insideTableView.reloadData()
                cell1.selectionStyle = .none
                
                return cell1
            }
        } else {
            var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            }
            
            cell?.textLabel?.font = .appFontWith(size: 15)
            if languageID == "2" {
                cell?.textLabel?.textAlignment = .right
            }
            cell?.textLabel?.text = (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "name") as? String
            cell?.selectionStyle = .none
            
            return cell!
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tblFavourites {
            return 0
        } else if tableView == mainTableView {
            if section == 0 {
                return 0
            } else {
                return 35
            }
        } else {
            return 44
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblFavourites {
            return 120
        } else if tableView == mainTableView {
            if indexPath.section != 1 {
                return 44
            } else {
                if categoryDict.count != 0 {
                    var collapsed = false
                    
                    var expandedRowHeight = 0
                    
                    for i in 0..<expandedSection.count {
                        let index = Int("\(expandedSection[i])")!
                        
                        expandedRowHeight = expandedRowHeight + (((categoryDict[index] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count * 40)
                    }
                    
                    return CGFloat(expandedRowHeight) + CGFloat(categoryDict.count * 44)
                } else {
                    return UITableViewAutomaticDimension
                }
            }
        } else {
            let collapsed = ((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool)
            
            if collapsed {
                return 0
            } else {
                return 40
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblFavourites {
            let product = self.favArr[indexPath.row]
            
            let nextVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
            nextVC.productSku = product.sku
            nextVC.productID = product.productId
            present(nextVC, animated: true, completion: nil)
        } else if tableView == mainTableView {
            let subArr = mainTblArray[indexPath.section]
            
            let action = subArr[indexPath.row]
            
            if action == NSLocalizedString("Home", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Login", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.present(viewController, animated: true, completion: nil)
            }
            else if action == NSLocalizedString("Deals", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Live Chat", comment: "")
            {
                Intercom.presentMessenger()
            }
            else if action == NSLocalizedString("Track Order", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Share", comment: "")
            {
                let textToShare = [ Tools.shareText ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                
                self.present(activityViewController, animated: true, completion: nil)
            }
        } else  {
            let id = "\(String(describing: (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "id")!))"
            print("Category ID: \(id)")
            categoryID = id
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            viewController.subIds = id
            viewController.isFromHome = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func deleteAction(_ sender:AnyObject!) {
        let product = self.favArr[sender.tag]
        
        guard let name = product.name else {return}
        
        let deleteAlert = UIAlertController(title: nil, message: "\(NSLocalizedString("Do you want to remove", comment: "")) \(name) \(NSLocalizedString("from your wishlist?", comment: ""))", preferredStyle: .alert)
        
        deleteAlert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: { (action) in
            self.deleteWishlist(productId: "\(product.productId)")
            
            let isHave:Bool = self.checkProductInWishList(id: sender.tag)
            
            if UserDefaults.standard.object(forKey: kAddToWishlist) != nil {
                let data = UserDefaults.standard.object(forKey: kAddToWishlist) as! Data
                let tempArr:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                
                if isHave {
                    let str2 = product.sku
                    
                    for i in 0..<self.favArr.count {
                        let str1 = self.favArr[i].sku
                        
                        if str1 == str2 {
                            tempArr.removeObject(at: i)
                        }
                    }
                }
                let tempData = NSKeyedArchiver.archivedData(withRootObject: tempArr)
                UserDefaults.standard.set(tempData, forKey: kAddToWishlist)
            }
            self.showToast(message: NSLocalizedString("Successfully removed from your wishlist", comment: ""))
        }))
        
        deleteAlert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .destructive, handler: nil))
        
        self.present(deleteAlert, animated: true, completion: nil)
        
    }
    
    func checkProductInWishList(id: Int) -> Bool {
        let product = self.favArr[id]
        
        var isAleadyHave:Bool = false
        
        if UserDefaults.standard.object(forKey: kAddToWishlist) != nil {
            
            let str2 =  product.sku
            
            for i in 0..<self.favArr.count {
                let str1 = self.favArr[i].sku
                
                if str1 == str2 {
                    isAleadyHave = true
                }
            }
        }
        return isAleadyHave
    }
    
    
    func loginSuccess() {
        self.getWishlist()
    }
    
    func loginFailure() {
        
        self.tabBarController?.selectedIndex = 0
    }
}

extension FavoritesVC: FetchWishlistResponse {
    func getAll(wishlist: [WishList]) {
        SharedManager.dismissHUD(viewController: self)
        SharedManager.dismissHUD(viewController: self)
        self.favArr = wishlist
        self.tblFavourites.reloadData()
        if wishlist.count == 0 {
            self.emptyList()
        }
    }
}
