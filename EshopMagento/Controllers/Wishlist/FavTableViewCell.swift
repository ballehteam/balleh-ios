//
//  FavTableViewCell.swift
//  Restaurant
//
//  Created by Adyas Iinfotech on 23/06/17.
//  Copyright © 2017 Adyas Iinfotech. All rights reserved.
//

import UIKit

class FavTableViewCell: UITableViewCell {
    @IBOutlet weak var lblAddress: UILabel!

    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblSplPrice: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
