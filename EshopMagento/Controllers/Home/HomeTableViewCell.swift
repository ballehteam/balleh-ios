//
//  HomeTableViewCell.swift
//  EshopMagento
//
//  Created by Apple on 05/05/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import FSPagerView

class HomeTableViewCell: UITableViewCell
{
    @IBOutlet weak var lblProductListType: UILabel!
    @IBOutlet weak var productListCollView: UICollectionView!
    @IBOutlet weak var categoryCollView: UICollectionView!
   // @IBOutlet weak var pagerView: FSPagerView!
   // @IBOutlet weak var pagerControl: FSPageControl!
    @IBOutlet weak var btnViewAll: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
