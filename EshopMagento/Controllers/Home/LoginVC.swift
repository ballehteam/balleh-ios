//
//  LoginVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 20/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import TwitterKit
import OAuthSwift

protocol loginIntimation {
    func loginSuccess()
    func loginFailure()
}

class LoginVC: ParentViewController {
    
    var delegate:loginIntimation?
    
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtPwdlg: UITextField!
    @IBOutlet var btnShowPwd: UIButton!
    @IBOutlet weak var viewForgotPwd: UIView!
    @IBOutlet weak var txtEmailForgot: UITextField!
    
    @IBOutlet weak var btnForgotPwd: UIButton!
    
    
    @IBOutlet var viewLogin: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPwdReg: UITextField!
    @IBOutlet var txtConfirmPwd: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var btnLoginTwitter: UIButton!
    
    @IBOutlet weak var txtSocialEmail: UITextField!
    @IBOutlet weak var viewSocialLogin: UIView!
    
    @IBOutlet weak var lblFname: UILabel!
    @IBOutlet weak var lblLname: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblRegPasswd: UILabel!
    @IBOutlet weak var lblConfirmPasswd: UILabel!
    
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignupNow: UIButton!
    
    var socialEmail = ""
    var socialID = ""
    var socialFname = ""
    var socialLname = ""
    var socialType = ""
    
    var oauthswift: OAuth1Swift?
    
    let loginManager = FBSDKLoginManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateToken()
        
        if languageID == "1"
        {
            txtUserName.textAlignment = .left
            txtPwdlg.textAlignment = .left
            txtEmailForgot.textAlignment = .left
            btnForgotPwd.contentHorizontalAlignment = .right
            lblFname.textAlignment = .left
            lblLname.textAlignment = .left
            lblEmail.textAlignment = .left
            lblRegPasswd.textAlignment = .left
            lblConfirmPasswd.textAlignment = .left
            
        }
        else
        {
            txtUserName.textAlignment = .right
            txtPwdlg.textAlignment = .right
            txtEmailForgot.textAlignment = .right
            btnForgotPwd.contentHorizontalAlignment = .left
            lblFname.textAlignment = .right
            lblLname.textAlignment = .right
            lblEmail.textAlignment = .right
            lblRegPasswd.textAlignment = .right
            lblConfirmPasswd.textAlignment = .right
            
        }
        
        //Login
        viewForgotPwd.isHidden = true
        viewLogin.isHidden = false
        
        self.mainScrollView.autoresizesSubviews = true
        
        let oauthswift = OAuth1Swift(
            consumerKey:    consumerKey,
            consumerSecret: consumerSecret
        )
        // authorize
        let handle = oauthswift.authorize(
            withCallbackURL: URL(string: "https://balleh.com/rest/V1/social/customer_token")!,
            success: { credential, response, parameters in
                print(credential.oauthToken)
                print(credential.oauthTokenSecret)
                print(parameters["user_id"])
                // Do your request
        },
            failure: { error in
                print(error.localizedDescription)
        }
        )
        
        self.mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width - 20, height: self.mainScrollView.frame.height)
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func closePopups() {
        view.endEditing(true)
    }
    
    @IBAction func clickShowPwd(_ sender: AnyObject) {
        if btnShowPwd.isSelected == true {
            btnShowPwd.setImage(UIImage(named: "ic_uncheck_box"), for: .normal)
            btnShowPwd.isSelected = false
            self.txtPwdlg.isSecureTextEntry = true
        } else {
            btnShowPwd.setImage(UIImage(named: "ic_check_box"), for: .selected)
            btnShowPwd.isSelected = true
            self.txtPwdlg.isSecureTextEntry = false
        }
    }
    
    @IBAction func clickForgotPwd(_ sender: AnyObject) {
        viewForgotPwd.isHidden = false
        viewForgotPwd.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    @IBAction func clickForgotSend(_ sender: AnyObject) {
        if self.isValidEmail(txtEmailForgot.text!) {
            self.view.endEditing(true)
            SharedManager.showHUD(viewController: self)
            
            let params = [
                "email": self.txtEmailForgot.text!,
                "template": "email_reset",
                 "websiteId": 1
                ] as [String : Any]
            
            let urlStr = "\(ConfigUrl.baseUrl2)customers/password"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.put.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            //  request.setValue((Defaults.object(forKey: XTRM_UserDefautKeys.oAuthTokenKey) as! String), forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
             //   let jsonString = String(data: jsonData , encoding: .utf8)
               // print(jsonData as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet() {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: "\(NSLocalizedString("A link to reset your password has been sent to your Email Address", comment: ""))", viewController: self)
                            self.viewForgotPwd.isHidden = true
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            self.viewForgotPwd.isHidden = true
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        
                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: error.localizedDescription , viewController: self)
                    }
                }
            } else {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        } else {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Please enter a valid email address", comment: ""))", viewController: self)
        }
    }
    
    @IBAction func clickForgotCancel(_ sender: AnyObject) {
        viewForgotPwd.isHidden = true
        self.view.endEditing(true)
        txtEmailForgot.text = ""
    }
    
    @IBAction func clickCloseLogin(_ sender: AnyObject) {
        DispatchQueue.main.async(execute: { () -> Void in
            self.dismiss(animated: false, completion: nil)
            self.delegate?.loginFailure()
        })
    }
    
    func login() {
        self.view.endEditing(true)
        SharedManager.showHUD(viewController: self)
        
        let params = [
            "username": self.txtUserName.text!,
            "password": self.txtPwdlg.text!
        ]
        
        let urlStr = "\(ConfigUrl.baseUrl)integration/customer/token"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let setTemp: [String : Any] = params
        
        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
            let jsonString = String(data: jsonData , encoding: .utf8)
            print(jsonString as Any)
            request.httpBody = jsonData
        }
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                
                SharedManager.dismissHUD(viewController: self)
                if responseObject.result.isSuccess
                {
                    if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                        userIDStr = responseObject.result.value as! String
                        UserDefaults.standard.set(userIDStr, forKey: "USER_ID")
                        
                        if userIDStr != ""
                        {
                            cartID = self.getCartID(cusToken: userIDStr)
                        }
                        
                        self.delegate?.loginSuccess()
                        
                        self.dismiss(animated: true, completion: { () -> Void in
                            // self.delegate?.loginSuccess()
                        })
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                    }
                }
                
                if responseObject.result.isFailure
                {
                    let error : Error = responseObject.result.error!
                    print(error.localizedDescription)
                }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    @IBAction func clickLogin(_ sender: AnyObject)
    {
        if !txtUserName.hasText
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Enter your Email Address", comment: ""))", viewController: self)
        }
        else if !self.isValidEmail(txtUserName.text!)
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please enter a valid email address", comment: ""))", viewController: self)
        }
        else if !txtPwdlg.hasText
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Enter Password", comment: ""))", viewController: self)
        }
        else
        {
            self.login()
        }
    }
    
    @IBAction func clickSignUp(_ sender: AnyObject)
    {
        viewLogin.isHidden = true
    }
    
    @IBAction func clickBack(_ sender: AnyObject)
    {
        viewLogin.isHidden = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        mainScrollView.setContentOffset(CGPoint(x: 0, y: textField.frame.origin.y-85), animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        mainScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func clickSignupNow(_ sender: AnyObject)
    {
        self.view.endEditing(true)
        
        if !txtFirstName.text!.isEmpty && !txtLastName.text!.isEmpty && !txtEmail.text!.isEmpty  && txtPwdReg.text == txtConfirmPwd.text && isValidEmail(txtEmail.text!) {
            
            self.view.endEditing(true)
            SharedManager.showHUD(viewController: self)
            
            
            let customer = [
                "firstname": self.txtFirstName.text!,
                "lastname": self.txtLastName.text!,
                "email": self.txtEmail.text!
                ] as [String : Any]
            
            
            let params = ["customer" : customer,
                          "password": self.txtPwdReg.text!
                ] as [String : Any]
            
            print(params)
            
            let urlStr = "\(ConfigUrl.baseUrl)customers"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)
                print(jsonString as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if responseObject.result.isSuccess {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            let alert = UIAlertController(title: "\(NSLocalizedString("Thanks", comment: ""))", message: "\(NSLocalizedString("Customer account created successfully!", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { action in
                                
                                self.txtUserName.text = self.txtEmail.text!
                                self.txtPwdlg.text = self.txtPwdReg.text!
                                
                                self.login()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else if txtPwdReg.text != txtConfirmPwd.text
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: kPasswordMismatchAlertMessage, viewController: self)
        }
        else
        {
            if txtLastName.text!.isEmpty
            {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Enter your First Name", comment: ""))", viewController: self)
            }
            else
            {
                if txtLastName.text!.isEmpty
                {
                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Enter your Last Name", comment: ""))", viewController: self)
                }
                else
                {
                    if txtEmail.text!.isEmpty
                    {
                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Enter your Email Address", comment: ""))", viewController: self)
                    }
                    else
                    {
                        if !isValidEmail(txtEmail.text!)
                        {
                            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please enter a valid email address", comment: ""))", viewController: self)
                        } else {
                            
                            if txtPwdReg.text!.isEmpty
                            {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Enter Password", comment: ""))", viewController: self)
                            }
                            else
                            {
                                if txtConfirmPwd.text!.isEmpty
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Re-enter your Password", comment: ""))", viewController: self)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Social Login
    
    @IBAction func clickSocialLoginContinue(_ sender: AnyObject) {
        if !txtSocialEmail.hasText {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Enter your Email Address", comment: ""))", viewController: self)
        } else {
            if !isValidEmail(self.txtSocialEmail.text!) {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please enter a valid email address", comment: ""))", viewController: self)
            } else {
                self.socialEmail = self.txtSocialEmail.text!
                
                self.socialLoginAPI()
            }
        }
    }
    
    
    //MARK: Twitter Login
    @IBAction func clickTwitterLogin(_ sender: Any) {
        /*
        // Saved for when twitter kit is deprecated, uses OAuth Swift SDK
         
        guard let rwURL = URL(string: "twitterkit-l5bSOV1wLEt9DvRR9YLW9CB3p://oauth-callback/twitter") else { return }

        self.oauthswift = OAuth1Swift(
            consumerKey:    consumerKey,
            consumerSecret: consumerSecret,
            requestTokenUrl: "https://api.twitter.com/oauth/request_token",
            authorizeUrl:    "https://api.twitter.com/oauth/authorize",
            accessTokenUrl:  "https://api.twitter.com/oauth/access_token"
        )
        
        self.oauthswift!.authorizeURLHandler = SafariURLHandler(viewController: self, oauthSwift: self.oauthswift!)

        self.oauthswift!.authorize(withCallbackURL: rwURL, success: { (credential, response, parameters) in
            print(credential.oauthToken)
            print(parameters)
            print(response?.data)

            if let name = parameters["screen_name"] as? String {
                self.socialFname = name
            }
            if let userId = parameters["user_id"] as? Int {
                self.socialID = "\(userId)"
            }

            self.socialType = "Twitter"
            twitLogin()
            
            self.socialLoginAPI()
        }) { (error) in
            print(error)
        }
        */
        
        func twitLogin() {
            TWTRTwitter.sharedInstance().logIn {(session, error) in
                if let err = error {
                    print(error)
                    return
                }
                
                if let sss = session {
                    print("logged in user with id \(String(describing: sss.userID))")
                    
                    let client = TWTRAPIClient.withCurrentUser()
                    
                    client.requestEmail { email, error in
                        if let err = error {
                            print(error?.localizedDescription)
                            self.socialType = "Twitter"
                            self.socialID = sss.userID
                            self.socialFname = sss.userName
                            self.socialLname = sss.userName
                            self.viewSocialLogin.isHidden = false
                            return
                        }
                        
                        if let mail = email {
                            self.viewSocialLogin.isHidden = true
                            print("signed in as \(String(describing: sss.userName))");
                            
                            let store = TWTRTwitter.sharedInstance().sessionStore
                            print(mail)
                            
                            self.socialType = "Twitter"
                            self.socialID = sss.userID
                            self.socialEmail = mail
                            self.socialFname = sss.userName
                            self.socialLname = sss.userName
                            
                            self.socialLoginAPI()
                            
                            // let lastSession = store.session()
                            //  let sessions = store.existingUserSessions()
                            //  let specificSession = store.sessionForUserID("\(session?.userID)")
                            
                            if let userID = store.session()?.userID {
                                store.logOutUserID(userID)
                            }
                            
                        }
                    }
                }
            }
        }
        
        twitLogin()
    }
    
    //MARK: Facebook Login
    
    @IBAction func signUpWithFacebookBtn(_ sender: AnyObject) {
        
        loginManager.loginBehavior = FBSDKLoginBehavior.web
        
        loginManager.logIn(withReadPermissions: ["public_profile","email"], from: self, handler: { (result, error) -> Void in
            
            if error != nil {
                print(error?.localizedDescription as Any)
            } else {
                
                if (result! as FBSDKLoginManagerLoginResult).isCancelled == true {
                    
                } else {
                    if(result?.grantedPermissions.contains("email"))! {
                        self.viewSocialLogin.isHidden = true
                        self.fetchUserInfo()
                    } else {
                        print("email Not Available", terminator: "")
                        
                        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"name, first_name, last_name"])
                        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                            
                            if ((error) != nil) {
                                // Process error
                                print("Error: \(String(describing: error))")
                            } else {
                                print("fetched user: \(String(describing: result))")
                                self.socialFname = "\((result as AnyObject).value(forKey: "first_name")!)"
                                self.socialLname = "\((result as AnyObject).value(forKey: "last_name")!)"
                                self.socialID = "\((result as AnyObject).value(forKey: "id")!)"
                                
                                self.socialType = "Facebook"
                                
                                self.socialLoginAPI()
                            }
                        })
                        
                        self.viewSocialLogin .isHidden = false
//                        let nextViewController = ViewController(nibName: "ViewController", bundle: nil)
//                        self.navigationController!.pushViewController(nextViewController, animated: false)
                    }
                }
            }
        })
    }
    
    func fetchUserInfo()
    {
        if (FBSDKAccessToken.current() != nil)
        {
            let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name, first_name, last_name"])
            graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                
                if ((error) != nil)
                {
                    // Process error
                    print("Error: \(String(describing: error))")
                }
                else
                {
                    print("fetched user: \(String(describing: result))")
                    self.socialFname = "\((result as AnyObject).value(forKey: "first_name")!)"
                    self.socialLname = "\((result as AnyObject).value(forKey: "last_name")!)"
                    self.socialEmail = "\((result as AnyObject).value(forKey: "email")!)"
                    self.socialID = "\((result as AnyObject).value(forKey: "id")!)"
                    
                    self.socialType = "Facebook"
                    
                    self.socialLoginAPI()
                }
            })
        }
    }
    
    func socialLoginAPI()
    {
        self.view.endEditing(true)
        SharedManager.showHUD(viewController: self)
        
        let params = [
            "firstname": self.socialFname,
            "lastname": self.socialLname,
            "email": self.socialEmail,
            "socialId": self.socialID,
            "socialType":self.socialType
            ] as [String : Any]
        
        print(params)
        
        let urlStr = "\(ConfigUrl.baseUrl)social/customer_token"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //  request.setValue((Defaults.object(forKey: XTRM_UserDefautKeys.oAuthTokenKey) as! String), forHTTPHeaderField: "Authorization")
        
        let setTemp: [String : Any] = params
        
        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
            let jsonString = String(data: jsonData , encoding: .utf8)
            print(jsonString as Any)
            request.httpBody = jsonData
        }
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                SharedManager.dismissHUD(viewController: self)
                
                print(responseObject.result.value!)
                
                if responseObject.result.isSuccess {
                    if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                        let result = (responseObject.result.value! as AnyObject).object(at: 0) as AnyObject
                        userIDStr =  "\(result.value(forKey: "customer_token")!)"
                        
                        if userIDStr != "" {
                            cartID = self.getCartID(cusToken: userIDStr)
                        }
                        
                        let isMailRequired = "\(result.value(forKey: "mail_required")!)"
                        
                        if isMailRequired == "1" {
                            self.resetPasswordAPI(email: self.socialEmail)
                        } else {
                            UserDefaults.standard.set(userIDStr, forKey: "USER_ID")
                            self.delegate?.loginSuccess()
                            
                            self.dismiss(animated: true, completion: { () -> Void in
                                self.delegate?.loginSuccess()
                            })
                            self.navigationController?.popViewController(animated: true)
                        }
                    } else {
                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                    }
                }
                if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    print(error.localizedDescription)
                }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func resetPasswordAPI(email: String)
    {
        self.view.endEditing(true)
        SharedManager.showHUD(viewController: self)
        
        let params = [
            "email": self.socialEmail,
            "template": "email_reset",
            "websiteId": "1",
            ]
        
        print(params)
        
        let urlStr = "\(ConfigUrl.baseUrl)customers/password"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.put.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      //  request.setValue(userIDStr, forHTTPHeaderField: "Authorization")
        
        let setTemp: [String : Any] = params
        
        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
            let jsonString = String(data: jsonData , encoding: .utf8)
            print(jsonString as Any)
            request.httpBody = jsonData
        }
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                SharedManager.dismissHUD(viewController: self)
                
                print(responseObject.result.value!)
                
                if responseObject.result.isSuccess
                {
                    if "\(String(describing: responseObject.response!.statusCode))" == "200"
                    {
                       // let result = responseObject.result.value! as AnyObject
                        
                        UserDefaults.standard.set(userIDStr, forKey: "USER_ID")
                        self.delegate?.loginSuccess()
                        
                        self.dismiss(animated: true, completion: { () -> Void in
                            self.delegate?.loginSuccess()
                        });
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                    }
                }
                if responseObject.result.isFailure
                {
                    let error : Error = responseObject.result.error!
                    print(error.localizedDescription)
                }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
}
