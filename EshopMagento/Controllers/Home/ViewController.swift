//
//  ViewController.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 19/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Intercom
import FSPagerView
import Alamofire
import Crashlytics
import CheckoutKit

class ViewController: ParentViewController, FSPagerViewDataSource, FSPagerViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate
{
    var bannerImageArr = NSMutableArray()
    var homeCategoryArr = NSMutableArray()
    var collectionsArr = NSMutableArray()
    
    @IBOutlet weak var mainTableVw: UITableView!
    @IBOutlet weak var homeBannersVw: UIView!
    
    var productListAry1 = [Product]()
    var productListAry2 = [Product]()
    var categoryImgArray = NSMutableArray()
    var featuredArray = NSMutableArray()
    var latestArray = NSMutableArray()
    
    var numberOfRowsinTable = Int()
    
    var bannersView = UIView()
    
    @IBOutlet weak  var pagerView: FSPagerView! {
        didSet {
            pagerView.dataSource = self
            pagerView.delegate = self
            pagerView.automaticSlidingInterval = 3
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = .zero
        }
    }
    
    @IBOutlet weak var pagerControl: FSPageControl! {
        didSet {
            self.pagerControl.numberOfPages = self.bannerImageArr.count
            self.pagerControl.contentHorizontalAlignment = .center
            // self.pagerControl.backgroundColor = .clear
            self.pagerControl.setFillColor(positiveBtnColor, for: .selected) //= [.normal : themeColor]
            self.pagerControl.currentPage = 0
            self.pagerControl.setStrokeColor(positiveBtnColor, for: .normal)
            self.pagerControl.setStrokeColor(.white, for: .selected)
            self.pagerControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }
    
    // MARK: PAGER
    
    // MARK:- FSPagerView DataSource
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return bannerImageArr.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell
    {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        //  cell.imageView?.image = UIImage(named: self.myImages[index] as! String)
        //  cell.imageView?.contentMode = .scaleAspectFill
        //  cell.imageView?.clipsToBounds = true
        // cell.textLabel?.text = index.description+index.description

        let imgUrl : String = "\((bannerImageArr.object(at: index) as AnyObject).value(forKey: "image_url")!)"
        let trimmedUrl = imgUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
        cell.imageView?.sd_setImage(with: URL(string: trimmedUrl))
        cell.imageView?.contentMode = UIViewContentMode.scaleToFill
        cell.imageView?.backgroundColor = .white
        
        var activityLoader = UIActivityIndicatorView()
        activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityLoader.center = (cell.imageView?.center)!
        activityLoader.startAnimating()
        cell.imageView?.addSubview(activityLoader)
        
        cell.imageView?.sd_setImage(with: URL(string: trimmedUrl), completed:
            { (image, error, imageCacheType, imageUrl) in
                if image != nil
                {
                    activityLoader.stopAnimating()
                }
                else
                {
                    print("image not found")
                    activityLoader.stopAnimating()
                }
        })
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int)
    {
      //  let cell:HomeTableViewCell = (self.mainTableVw.dequeueReusableCell(withIdentifier: "bannerCell") as! HomeTableViewCell?)!
        
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        
        let id = "\((bannerImageArr.object(at: index) as AnyObject).value(forKey: "category_id")!)"
        
        if id != "0" {
            categoryID = id
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            viewController.subIds = id
            viewController.isFromHome = true
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
            viewController.productSku = "\((bannerImageArr.object(at: index) as AnyObject).value(forKey: "product_id")!)"
//            viewController.productID = "\((bannerImageArr.object(at: index) as AnyObject).value(forKey: "category_id")!)"
//            self.navigationController?.pushViewController(viewController, animated: true)
            self.present(viewController, animated: true, completion: nil)
        }
        self.pagerControl.currentPage = index
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView)
    {
       // let cell:HomeTableViewCell = (self.mainTableVw.dequeueReusableCell(withIdentifier: "bannerCell") as! HomeTableViewCell?)!
        guard self.pagerControl.currentPage != self.pagerView.currentIndex else { return }
        self.pagerControl.currentPage = self.pagerView.currentIndex
      //  self.mainTableVw.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateToken()
        self.title = NSLocalizedString("Home", comment: "")
        self.bannersView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 178)
        
      //  self.bannersView.addSubview(pagerView)
      //  self.bannersView.addSubview(pagerControl)
        
       /* let button = UIButton(type: .roundedRect)
        button.frame = CGRect(x: 20, y: 50, width: 100, height: 30)
        button.setTitle("Crash", for: [])
        button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(button)*/
        
        //banner Images
        SharedManager.showHUD(viewController: self)
        
      //  let cell:HomeTableViewCell = self.mainTableVw.dequeueReusableCell(withIdentifier: "productListCell") as! HomeTableViewCell
                
      //  let nib = UINib(nibName: "HomeProductCollecCell", bundle: nil)
      //  cell.productListCollView.register(nib, forCellWithReuseIdentifier: "productCell")
        
     //   let nib1 = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
     //   cell.categoryCollView.register(nib1, forCellWithReuseIdentifier: "categoryCell")
        
        let urlStr : String = {
            if languageID == "1" {
                return "https://balleh.com/rest/en/V1/bannerimages"
            } else {
                return "https://balleh.com/rest/ar/V1/bannerimages"
            }
        }()
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
      //  request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      //  request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            let result = responseObject.result.value! as AnyObject
                            
                            for i in 0..<result.count {
                                if "\((result.object(at: i) as AnyObject).value(forKey: "banner_type")!)" == "0" {
                                    self.bannerImageArr.add(result.object(at: i))
                                    self.pagerControl.numberOfPages = self.bannerImageArr.count
                                    self.pagerView.reloadData()
                                } else if "\((result.object(at: i) as AnyObject).value(forKey: "banner_type")!)" == "1" {
                                    self.homeCategoryArr.add(result.object(at: i))
                                }
                            }
                            //   let indexPath = NSIndexPath(row: 0, section: 0)
                            self.mainTableVw.reloadData()
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                    self.getProductCategories()
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
   /* @IBAction func crashButtonTapped(_ sender: AnyObject) {
        Crashlytics.sharedInstance().crash()
    }*/
    
    func getProductCategories()
    {
        SharedManager.showHUD(viewController: self)
        
        let urlStr : String = {
            if languageID == "1" {
                return "https://balleh.com/rest/en/V1/categoryslider"
            } else {
                return "https://balleh.com/rest/ar/V1/categoryslider"
            }
        }()
        
        let setFinalURl = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        //  request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //  request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            let result = (responseObject.result.value!) as! NSArray
                            self.collectionsArr = result.mutableCopy() as! NSMutableArray
                            print(self.collectionsArr)
                            for i in 0..<result.count {
                                if i == 0 {
                                    let catId = "\((result.object(at: i) as AnyObject).value(forKey: "category_id")!)"
                                    self.getProductList(catId: catId, index: i)
                                    if i == result.count - 1 {
                                        self.mainTableVw.reloadData()
                                    }
                                } else {
                                    self.delay(2, closure: {
                                        let catId = "\((result.object(at: i) as AnyObject).value(forKey: "category_id")!)"
                                        self.getProductList(catId: catId, index: i)
                                        
                                        self.delay(2, closure: {
                                            if i == result.count - 1 {
                                                self.mainTableVw.reloadData()
                                            }
                                        })
                                    })
                                }
                            }
                            // self.mainTableVw.reloadData()
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func getProductList(catId : String, index: Int) {
        SharedManager.showHUD(viewController: self)
        print(index)
        let urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(catId)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=created_at&searchCriteria[sortOrders][0][direction]=DESC&searchCriteria[pageSize]=10&searchCriteria[currentPage]=1"
        print(urlStr)
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        //  request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //  request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        print(responseObject.result.value)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items"),
                                "\(result)" != "<null>" {
                                var products = [Product]()
                                if let dataResponse = result as? NSArray {
                                    for data in dataResponse {
                                        if let dataDic = data as? NSDictionary {
                                            products.append(Product(data: dataDic))
                                        }
                                    }
                                }
                                for i in products {
                                    print(i.name, i.price)
                                }
                                
                                if index == 0 {
                                    self.productListAry1 = products
                                } else {
                                    self.productListAry2 = products
                                }
//                                self.productListAry.insert(products, at: index)
                                print(result)
                                
//                                let values = (self.collectionsArr.object(at: index) as! NSDictionary).mutableCopy() as! NSMutableDictionary
//                                values.setObject(result.value(forKey: "items")!, forKey: "product_list" as NSCopying)
//                                self.collectionsArr.replaceObject(at: index, with: values)
                            }
                            
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        if let id = UserDefaults.standard.string(forKey: "USER_ID") {
            userIDStr = id
            
            if userIDStr != "" {
                mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: "")],
                                [""],
                                [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
                ]
                
                isGuest = false
                cartID = getCartID(cusToken: userIDStr)
                getCartDetails(cusToken: userIDStr, completionHandler: {(result) -> () in
                    // print(result)
                })
            }
            else
            {
                mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")],
                                [""],
                                [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
                ]
                
                isGuest = true
                if cartID == ""
                {
                    createGuestCartID(completionHandler: {(result) -> () in
                        cartID = result
                    })
                }
                else
                {
                    getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                        // print(result)
                    })
                }
                
            }
        }
        else
        {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
            
            isGuest = true
            if cartID == "" {
                createGuestCartID(completionHandler: {(result) -> () in
                    cartID = result
                })
            } else {
                getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                    // print(result)
                })
            }
        }
        
        //self.setRightNavigationButton()
        self.setLeftNavigationButtonHome()
        
      //  UserDefaults.standard.removeObject(forKey: kAddToWishlist)
    }
    
    //MARK: CollectionView Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
            return self.homeCategoryArr.count
        } else {
            print(section)
            if section == 0 {
                return self.productListAry1.count
            } else {
                return self.productListAry2.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(indexPath.section, collectionView.tag)
        if collectionView.backgroundColor != .groupTableViewBackground {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCollCell", for: indexPath) as! HomeCollectionViewCell
            
            cell.lblTitle.text = "\((self.homeCategoryArr.object(at: indexPath.row) as AnyObject).value(forKey: "title")!)"
            
            let imageUrl = "\((self.homeCategoryArr.object(at: indexPath.row) as AnyObject).value(forKey: "image_url")!)"
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20")
            
            var activityLoader = UIActivityIndicatorView()
            activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityLoader.center = cell.imgProduct.center
            activityLoader.startAnimating()
            cell.imgProduct.addSubview(activityLoader)
            
            cell.imgProduct.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                    activityLoader.stopAnimating()
                } else {
                    print("image not found")
                    cell.imgProduct.image = UIImage(named: "no_image")
                    activityLoader.stopAnimating()
                }
            })
            
            cell.imgProduct.contentMode = UIViewContentMode.scaleAspectFit
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as!  HomeProductCollecCell
            
            if self.productListAry1.isEmpty || self.productListAry2.isEmpty {
                return cell
            }
            let product : Product = {
                if collectionView.tag == 0 {
                    return productListAry1[indexPath.row]
                } else {
                    return productListAry2[indexPath.row]
                }
            }()
            
            cell.lblTitle.text = product.name
            
            cell.lblSplPrice.text = product.price
            
            var imageUrl = ""
            
            if let url = product.smallImage {
                imageUrl = url
            }
            
            if let title = product.title {
                cell.lblTitle.text = title
            }
            
            cell.lblSplPrice.text = product.price
            
            if let splPrice = product.specialPrice {
                cell.lblTitle.frame.origin.y = 155
                cell.lblSplPrice.frame.origin.y = 195
                cell.lblSplPrice.isHidden = false
                cell.lblSplPrice.text = splPrice
            } else {
                cell.lblPrice.isHidden = true
                cell.lblTitle.frame.origin.y = 160
                cell.lblSplPrice.frame.origin.y = 203
            }
            
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20")
            
            var activityLoader = UIActivityIndicatorView()
            activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityLoader.center = cell.imgProduct.center
            activityLoader.startAnimating()
            cell.imgProduct.addSubview(activityLoader)
            
            cell.imgProduct.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                    activityLoader.stopAnimating()
                } else {
                    print("image not found")
                    cell.imgProduct.image = UIImage(named: "no_image")
                    activityLoader.stopAnimating()
                }
            })
            
            cell.imgProduct.contentMode = UIViewContentMode.scaleAspectFit
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.backgroundColor != .groupTableViewBackground {
            let catId = "\((self.homeCategoryArr.object(at: indexPath.row) as AnyObject).value(forKey: "category_id")!)"

            //  categoryID = catId
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            viewController.categoryID = catId
            viewController.isFromHome = true
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            let product : Product = {
                if collectionView.tag == 0 {
                    return productListAry1[indexPath.row]
                } else {
                    return productListAry2[indexPath.row]
                }
            }()
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
            viewController.productSku = product.sku
            viewController.productID = "\(product.id)"
            self.present(viewController, animated: true, completion: nil)
           // self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    //MARK: TableView methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == mainTableVw {
            return 1
        } else if tableView == mainTableView {
            return self.mainTblArray.count
        } else {
            if categoryDict.count != 0 {
                return categoryDict.count
            } else {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mainTableVw {
            numberOfRowsinTable = collectionsArr.count + 1
            if bannerImageArr.count != 0 {
                numberOfRowsinTable = numberOfRowsinTable + 1
            }
            if homeCategoryArr.count != 0 {
                numberOfRowsinTable = numberOfRowsinTable + 1
            }
            return numberOfRowsinTable
        } else if tableView == mainTableView {
            return mainTblArray[section].count
        } else {
            if categoryDict.count != 0 {
                var categoryArr = NSArray()
                if let arr = (categoryDict[section] as AnyObject).value(forKeyPath: "children_data") {
                    categoryArr = arr as! NSArray
                }
                return categoryArr.count
            } else {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == mainTableVw {
            let lastRowIndex = tableView.numberOfRows(inSection: indexPath.section) - 1
            
            if indexPath.row == 0 {
                let cell:HomeTableViewCell = self.mainTableVw.dequeueReusableCell(withIdentifier: "bannerCell") as! HomeTableViewCell
                
                if bannerImageArr.count != 0 {
                    homeBannersVw.frame.origin.x = 0
                    homeBannersVw.frame.origin.y = 0
//                    CGSize(x: 0, y: 0, width: cell.contentView.frame.size.width, height: cell.contentView.frame.size.height)
                    cell.contentView.addSubview(self.homeBannersVw)
                    
//                    let cell:HomeTableViewCell = self.mainTableVw.dequeueReusableCell(withIdentifier: "bannerCell") as! HomeTableViewCell
//
//                    cell.pagerView.dataSource = self
//                    cell.pagerView.delegate = self
//                    cell.pagerView.reloadData()
//                    cell.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
//                    cell.pagerView.itemSize = .zero
//
//                    cell.pagerControl.numberOfPages = self.bannerImageArr.count
//                    cell.pagerControl.contentHorizontalAlignment = .center
//                    // cell.pagerControl.backgroundColor = .clear
//                    // cell.pagerControl.fillColors = [.normal : themeColor]
//                    cell.pagerControl.currentPage = 0
//                    cell.pagerControl.setStrokeColor(themeColor, for: .normal)
//                    cell.pagerControl.setStrokeColor(.white, for: .selected)
//                    cell.pagerControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
//                    cell.pagerView.reloadData()
                }
                return cell
            } else if indexPath.row == 1 {
                let cell:HomeTableViewCell = self.mainTableVw.dequeueReusableCell(withIdentifier: "categoryCell") as! HomeTableViewCell

//                let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
//                cell.categoryCollView.register(nib, forCellWithReuseIdentifier: "categoryCollCell")
                
                cell.categoryCollView.backgroundColor = UIColor(red: 235.0/255.0, green: 237.0/255.0, blue: 241.0/255.0, alpha: 1)
                cell.categoryCollView.tag = indexPath.row
                cell.categoryCollView.dataSource = self
                cell.categoryCollView.delegate = self
                cell.categoryCollView.reloadData()
                
                return cell
            } else if indexPath.row == lastRowIndex {
                let cell:HomeTableViewCell = self.mainTableVw.dequeueReusableCell(withIdentifier: "infoCell") as! HomeTableViewCell
                return cell
            } else {
                let cell:HomeTableViewCell = self.mainTableVw.dequeueReusableCell(withIdentifier: "productListCell") as! HomeTableViewCell
                
                var catId = ""
                
                if bannerImageArr.count != 0 && homeCategoryArr.count != 0 {
                    cell.productListCollView.tag = indexPath.row - 2
                    
                    if languageID == "1" {
                        cell.lblProductListType.text = "\((collectionsArr.object(at: indexPath.row - 2) as AnyObject).value(forKey: "title")!)"
                    } else {
                        cell.lblProductListType.text = "\((collectionsArr.object(at: indexPath.row - 2) as AnyObject).value(forKey: "title_ar")!)"
                    }
                    
                    catId = "\((collectionsArr.object(at: indexPath.row - 2) as AnyObject).value(forKey: "category_id")!)"
                } else if bannerImageArr.count == 0 && homeCategoryArr.count == 0 {
                    cell.productListCollView.tag = indexPath.row
                    cell.lblProductListType.text = "\((collectionsArr.object(at: indexPath.row) as AnyObject).value(forKey: "title")!)"
                    
                    catId = "\((collectionsArr.object(at: indexPath.row) as AnyObject).value(forKey: "category_id")!)"
                } else {
                    cell.productListCollView.tag = indexPath.row - 1
                    cell.lblProductListType.text = "\((collectionsArr.object(at: indexPath.row - 1) as AnyObject).value(forKey: "title")!)"
                    
                    catId = "\((collectionsArr.object(at: indexPath.row - 1) as AnyObject).value(forKey: "category_id")!)"
                }
                
              //  let nib = UINib(nibName: "TestCollectionViewCell", bundle: nil)
              //  cell.productListCollView.register(nib, forCellWithReuseIdentifier: "testCell")
               /* if languageID == "1"
                {
                    cell.btnViewAll.contentHorizontalAlignment = .right
                }
                else
                {
                    cell.btnViewAll.contentHorizontalAlignment = .left
                }*/
                
                cell.btnViewAll.layer.borderColor = positiveBtnColor.cgColor
                cell.btnViewAll.setTitle(NSLocalizedString("View All", comment: ""), for: .normal)
                cell.btnViewAll.addTarget(self, action: #selector(self.viewAllAction(_:)), for: .touchUpInside)
                cell.btnViewAll.tag = Int(catId)!
                
                cell.productListCollView.backgroundColor = .groupTableViewBackground
                cell.productListCollView.dataSource = self
                cell.productListCollView.delegate = self
                cell.productListCollView.reloadData()
                
                return cell
            }
        } else if tableView == mainTableView {
            if indexPath.section != 1 {
                if indexPath.section == 0 {
                    if indexPath.row != 0 {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                        
                        let subArr = mainTblArray[indexPath.section]
                        cell1.title.text = subArr[indexPath.row]
                        
                        let subArr2 = iconsArr[indexPath.section]
                        cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                        if languageID == "1" {
                            cell1.imgArrow.image = UIImage(named: "left-arrow")
                        } else {
                            cell1.imgArrow.image = UIImage(named: "right-arrow1")
                        }
                        cell1.imgArrow.contentMode = .scaleAspectFit
                        cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                        cell1.imgIcon.tintColor = UIColor.gray
                        cell1.selectionStyle = .none
                        
                        return cell1
                    } else {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as! LanguageCell
                        
                        if languageID == "1" {
                            //selectedSegmentIndex = 0
                            cell1.segmentView.selectedSegmentIndex = 0
                        } else {
                            cell1.segmentView.selectedSegmentIndex = 1
                        }
                        
                        cell1.segmentView.addTarget(self, action: #selector(ParentViewController.clickLang(_:)), for: UIControlEvents.valueChanged)
                        
                        return cell1
                    }
                } else {
                    let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                    
                    let subArr = mainTblArray[indexPath.section]
                    cell1.title.text = subArr[indexPath.row]
                    
                    let subArr2 = iconsArr[indexPath.section]
                    cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                    if languageID == "1" {
                        cell1.imgArrow.image = UIImage(named: "left-arrow")
                    } else {
                        cell1.imgArrow.image = UIImage(named: "right-arrow1")
                    }
                    cell1.imgArrow.contentMode = .scaleAspectFit
                    cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                    cell1.imgIcon.tintColor = UIColor.gray
                    cell1.selectionStyle = .none
                    
                    return cell1
                }
            }
            else
            {
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "CatCell") as! categoryTCell
                
                cell1.insideTableView.isScrollEnabled = false
                cell1.insideTableView.dataSource = self
                cell1.insideTableView.delegate = self
                cell1.insideTableView.reloadData()
                cell1.selectionStyle = .none
                
                return cell1
            }
        }

        else
        {
            var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if cell == nil
            {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            }
            
            cell?.textLabel?.font = UIFont.appFontWith(size: 15)
            
            if languageID == "2"
            {
                cell?.textLabel?.textAlignment = .right
            }
            cell?.textLabel?.text = (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "name") as? String
            cell?.selectionStyle = .none
            
            return cell!
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == mainTableView {
            let subArr = mainTblArray[indexPath.section]
            
            let action = subArr[indexPath.row] 
            
            if action == NSLocalizedString("Home", comment: "") {
                self.closeMenu()
            } else if action == NSLocalizedString("Login", comment: "") {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.present(viewController, animated: true, completion: nil)
            } else if action == NSLocalizedString("Deals", comment: "") {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
                self.navigationController?.pushViewController(viewController, animated: true)
            } else if action == NSLocalizedString("Live Chat", comment: "") {
                Intercom.presentMessenger()
            } else if action == NSLocalizedString("Track Order", comment: "") {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                self.navigationController?.pushViewController(viewController, animated: true)
            } else if action == NSLocalizedString("Share", comment: "") {
                let textToShare = [ Tools.shareText ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
            }
        } else if tableView == mainTableVw {
            
        } else {
            let id = "\(String(describing: (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "id")!))"
            
            categoryID = id
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            viewController.subIds = id
            viewController.isFromHome = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let lastRowIndex = tableView.numberOfRows(inSection: indexPath.section) - 1
        if tableView == mainTableVw {
            if indexPath.row == 0 {
                var height = CGFloat ()
                if self.bannerImageArr.count == 0 {
                    height = 0
                } else {
                    height = 215
                }
                return height
            }
            if indexPath.row == 1 {
                var height = CGFloat ()
                if homeCategoryArr.count == 0 {
                    height = 0
                } else {
                    height = 212
                }
                return height
            }
            if indexPath.row == lastRowIndex {
                return 103
            } else {
                var productsArr = self.productListAry1
                
                if productsArr.count != 0 {
                    return 289
                } else {
                    return 0
                }
            }
        } else if tableView == mainTableView {
            if indexPath.section != 1 {
                return 44
            } else {
                if categoryDict.count != 0 {
                    var expandedRowHeight = 0
                    
                    for i in 0..<expandedSection.count {
                        let index = Int("\(expandedSection[i])")!
                        
                        expandedRowHeight = expandedRowHeight + (((categoryDict[index] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count * 40)
                    }
                    
                    return CGFloat(expandedRowHeight) + CGFloat(categoryDict.count * 44)
                } else {
                    return UITableViewAutomaticDimension
                }
            }
        } else {
            let collapsed = ((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool)
            
            if collapsed {
                return 0
            } else {
                return 40
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == mainTableVw {
            if section == 0 {
                return 0
            } else {
                return 63
            }
        } else if tableView == mainTableView {
            if section == 0 {
                return 0
            } else {
                return 35
            }
        } else {
            return 44
        }
    }
    
    
    @objc func viewAllAction(_ sender:AnyObject!)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
        viewController.categoryID = "\(sender.tag!)"
        viewController.isFromHome = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

