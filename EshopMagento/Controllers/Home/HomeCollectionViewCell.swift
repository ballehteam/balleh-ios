//
//  HomeCollectionViewCell.swift
//  EshopMagento
//
//  Created by Apple on 10/05/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
}
