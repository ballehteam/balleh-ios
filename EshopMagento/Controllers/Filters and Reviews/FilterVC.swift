//
//  FilterVC.swift
//  Exlcart
//
//  Created by Adyas on 31/12/16.
//  Copyright © 2016 adyas. All rights reserved.
//

import UIKit
import Alamofire

class FilterVC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet var filterTableView: UITableView!
    @IBOutlet var filterTableView1: UITableView!
    
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblFilter: UILabel!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnApply: UIButton!
    
    var filterAry:NSMutableArray = []
    var filterID:String = String()
    var filterValues:NSArray = []
    
    var catgryID:String = ""
    var selectedType = Int()
    var selectedFilters = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // setLeftNavigationButton()
       // self.setRightNavigationButton()
        
        /*lblFilter.text = NSLocalizedString("Filter", comment: "")
        btnCancel.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
        btnReset.setTitle(NSLocalizedString("Reset", comment: ""), for: .normal)
        btnApply.setTitle(NSLocalizedString("Apply", comment: ""), for: .normal)
        */
        
                
        filterTableView.tableFooterView = UIView()
        filterTableView1.tableFooterView = UIView()
        self.navigationController?.isNavigationBarHidden = true
        
        let urlStr = "https://balleh.com/mobifilter/index/mfilter?category_id=\(catgryID)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if responseObject.result.isSuccess {
                        print(responseObject)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            let result = responseObject.result.value! as AnyObject
                            
                            for i in 0..<result.count {
                                let name = "\((result.object(at: i) as AnyObject).value(forKey: "label")!)"
                                
                                if !name.lowercased().contains("category") {
                                    self.filterAry.add(result.object(at: i))
                                }
                            }
                            
                            self.filterTableView.reloadData()
                            self.filterTableView1.reloadData()
                            
                            if self.filterAry.count != 0 {
                                let indexPath = IndexPath(row: 0, section: 0)
                                self.filterTableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
                                self.tableView(self.filterTableView, didSelectRowAt: indexPath)
                            }
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.filterTableView {
            return self.filterAry.count
        } else {
            return filterValues.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.filterTableView {
            let cell = filterTableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as UITableViewCell
            cell.textLabel?.text = (filterAry.object(at: indexPath.row) as AnyObject).value(forKey: "label") as? String
            cell.textLabel?.font = UIFont.appFontWith(size: 14)
            
            return cell
        } else {
            let cell = filterTableView1.dequeueReusableCell(withIdentifier: "FilterCell1", for: indexPath) as UITableViewCell
            cell.textLabel?.text = (filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "display") as? String
            cell.textLabel?.font = UIFont.appFontWith(size: 12)
            cell.accessoryType = .checkmark
            if filterID.contains((filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "display") as! String) {
                cell.tintColor = positiveBtnColor
            } else {
                cell.tintColor = .lightGray
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.filterTableView {
            filterValues = (filterAry.object(at: indexPath.row) as AnyObject).value(forKey: "values") as! NSArray
            selectedType = indexPath.row
            self.filterTableView1.reloadData()
        } else {
            let display = "\((filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "display")!)"
            let str = display +  (",")
            filterID.append(str)
            print(filterID)
            
            let cell = tableView.cellForRow(at: indexPath)
            
            if cell!.isSelected == true {
                cell!.tintColor = positiveBtnColor
            }
            
            let field = "\((filterAry.object(at: selectedType) as AnyObject).value(forKey: "code")!)"
            let value = "\((filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "value")!)"
            
            let tempDict = NSMutableDictionary()
            let tempDict1 = NSMutableDictionary()
            var condition = "eq"
            
            if field.lowercased().contains("price") {
                let arr = value.components(separatedBy: "-")
                
                print(arr)
                for i in 0..<arr.count {
                    if "\(arr[0])" == "" {
                        condition = "lt"
                        
                        tempDict.setObject(field, forKey: "field" as NSCopying)
                        tempDict.setObject(arr[1], forKey: "value" as NSCopying)
                        tempDict.setObject(display, forKey: "display" as NSCopying)
                        tempDict.setObject(condition, forKey: "condition" as NSCopying)
                        
                        selectedFilters.add(tempDict)
                        
                        break
                    } else if "\(arr[1])" == "" {
                        condition = "gt"
                        
                        tempDict.setObject(field, forKey: "field" as NSCopying)
                        tempDict.setObject(arr[0], forKey: "value" as NSCopying)
                        tempDict.setObject(display, forKey: "display" as NSCopying)
                        tempDict.setObject(condition, forKey: "condition" as NSCopying)
                        
                        selectedFilters.add(tempDict)
                        
                        break
                    } else {
                        if i == 0 {
                            condition = "from"
                            
                            tempDict.setObject(field, forKey: "field" as NSCopying)
                            tempDict.setObject(arr[i], forKey: "value" as NSCopying)
                            tempDict.setObject(display, forKey: "display" as NSCopying)
                            tempDict.setObject(condition, forKey: "condition" as NSCopying)
                            
                            selectedFilters.add(tempDict)
                        } else {
                            condition = "to"
                            
                            tempDict1.setObject(field, forKey: "field" as NSCopying)
                            tempDict1.setObject(arr[i], forKey: "value" as NSCopying)
                            tempDict1.setObject(display, forKey: "display" as NSCopying)
                            tempDict1.setObject(condition, forKey: "condition" as NSCopying)
                            
                            selectedFilters.add(tempDict1)
                        }
                    }
                }
            } else {
                tempDict.setObject(field, forKey: "field" as NSCopying)
                tempDict.setObject(value, forKey: "value" as NSCopying)
                tempDict.setObject(display, forKey: "display" as NSCopying)
                tempDict.setObject(condition, forKey: "condition" as NSCopying)
                
                selectedFilters.add(tempDict)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == self.filterTableView1 {
            let display = (filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "display") as! String
            
            filterID = filterID.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: display + (","), with: "") as String
            print(filterID)
            
            let cell = tableView.cellForRow(at: indexPath)
            
            if cell!.isSelected == false {
                cell!.tintColor = .lightGray
            }
            
            let field = "\((filterAry.object(at: selectedType) as AnyObject).value(forKey: "code")!)"
            let value = "\((filterValues.object(at: indexPath.row) as AnyObject).value(forKey: "value")!)"
            
            let tempDict = NSMutableDictionary()
            let tempDict1 = NSMutableDictionary()
            var condition = "eq"
            
            if field.lowercased().contains("price") {
                let arr = value.components(separatedBy: "-")
                
                print(arr)
                for i in 0..<arr.count {
                    if i == 0 {
                        condition = "from"
                        
                        tempDict.setObject(field, forKey: "field" as NSCopying)
                        tempDict.setObject(arr[i], forKey: "value" as NSCopying)
                        tempDict.setObject(display, forKey: "display" as NSCopying)
                        tempDict.setObject(condition, forKey: "condition" as NSCopying)
                        
                        selectedFilters.remove(tempDict)
                    } else {
                        condition = "to"
                        
                        tempDict1.setObject(field, forKey: "field" as NSCopying)
                        tempDict1.setObject(arr[i], forKey: "value" as NSCopying)
                        tempDict1.setObject(display, forKey: "display" as NSCopying)
                        tempDict1.setObject(condition, forKey: "condition" as NSCopying)
                        
                        selectedFilters.remove(tempDict1)
                    }
                }
            } else {
                tempDict.setObject(field, forKey: "field" as NSCopying)
                tempDict.setObject(value, forKey: "value" as NSCopying)
                tempDict.setObject(display, forKey: "display" as NSCopying)
                tempDict.setObject(condition, forKey: "condition" as NSCopying)
                
                selectedFilters.remove(tempDict)
            }
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetFilter(_ sender: Any)
    {
        self.filterID = ""
    }
    
    @IBAction func applyFilter(_ sender: Any)
    {
        print(selectedFilters)
        let urlArr = NSMutableArray()
        
        for i in 0..<selectedFilters.count
        {
            var name = "\((selectedFilters.object(at: i) as AnyObject).value(forKey: "field")!)"
            let value = "\((selectedFilters.object(at: i) as AnyObject).value(forKey: "value")!)"
            let condition = "\((selectedFilters.object(at: i) as AnyObject).value(forKey: "condition")!)"
            
            print(name)
            if name == "state"
            {
                name = "onsale_enabled"
            }
            
            let urlStr = "&searchCriteria[filter_groups][1][filters][0][field]=\(name)&searchCriteria[filter_groups][1][filters][0][value]=\(value)&searchCriteria[filter_groups][1][filters][0][condition_type]=\(condition)"
            urlArr.add(urlStr)
        }
        
        print(urlArr)
        
        var urlStr = ""
        
        for i in 0..<urlArr.count
        {
            urlStr = "\(urlStr)\(urlArr[i])"
        }
        print(urlStr)
        
        self.dismiss(animated: true, completion: { () -> Void in

            UserDefaults.standard.set("1", forKey: "filter")
            isFiltered = true
            filterUrlStr = urlStr
        });
    }
}






