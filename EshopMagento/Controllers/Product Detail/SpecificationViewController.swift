//
//  SpecificationViewController.swift
//  N-Gal
//
//  Created by Adyas Iinfotech on 15/09/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class SpecificationViewController: ParentViewController
{
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var tblOptions: UITableView!
    
    var gDetailsArr = NSMutableArray()
    var optionArr = NSMutableArray()
    var detailsArr = NSMutableArray()
    var productName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLeftNavigationButton()
        self.setRightNavigationButton()
       // print(detailsArr)
        lblProductName.text = productName
        
        let tempDict = NSMutableDictionary()
        tempDict.setObject(gDetailsArr[0], forKey: "text" as NSCopying)
        tempDict.setObject("Brand", forKey: "name" as NSCopying)
        
        let tempDict1 = NSMutableDictionary()
        tempDict1.setObject(gDetailsArr[1], forKey: "text" as NSCopying)
        tempDict1.setObject("Stock", forKey: "name" as NSCopying)
        
        let tempArr = NSMutableArray ()
        tempArr.add(tempDict)
        tempArr.add(tempDict1)
        
        let newDict = NSMutableDictionary()
        newDict.setObject(tempArr, forKey: "attribute" as NSCopying)
        newDict.setObject("", forKey: "name" as NSCopying)
        
        detailsArr.insert(newDict, at: 0)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        detailsArr.removeAllObjects()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return detailsArr.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       // print((((detailsArr[0] as AnyObject).value(forKey: "attribute")!) as AnyObject).count)
        return (((detailsArr[section] as AnyObject).value(forKey: "attribute")!) as AnyObject).count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:SpecificationTableViewCell = self.tblOptions.dequeueReusableCell(withIdentifier: "optionCell") as! SpecificationTableViewCell
        
        cell.lblName.text = "\((((detailsArr.object(at: indexPath.section) as AnyObject).value(forKey: "attribute") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "name") as! String)"
        
        if let value = (((detailsArr.object(at: indexPath.section) as AnyObject).value(forKey: "attribute") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "text")
        {
            if "\(value)" != "<null>"
            {
                cell.lblValue.text = "\((((detailsArr.object(at: indexPath.section) as AnyObject).value(forKey: "attribute") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "text") as! String)"
            }
            else
            {
                cell.lblValue.text = "--"
            }
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 54
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section != 0
        {
            return 44
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0.00001
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let CellIdentifier: String = "sectionHeader"
        let headerView: SpecificationTableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as! SpecificationTableViewCell?

        headerView?.lblTitle.text = (detailsArr.object(at: section) as AnyObject).value(forKeyPath: "name") as! String?
        
        if headerView == nil
        {
            print("No cells with matching CellIdentifier loaded from your storyboard")
            
            //   NSException.raise("headerView == nil..", format: "No cells with matching CellIdentifier loaded from your storyboard")
        }
        return headerView!
    }
}
