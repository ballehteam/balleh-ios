//
//  OptionTableViewCell.swift
//  SouqTime
//
//  Created by Apple on 19/07/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit

class OptionTableViewCell: UITableViewCell {

    @IBOutlet weak var collOptions: UICollectionView!
    @IBOutlet weak var lblOptionTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
