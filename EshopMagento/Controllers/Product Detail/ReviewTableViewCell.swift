//
//  ReviewTableViewCell.swift
//  Exlcart
//
//  Created by Tech Basics on 29/12/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit
import FloatRatingView

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTtile: UILabel!
    @IBOutlet weak var lblReviewDescription: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet weak var ratingStarView: FloatRatingView!
    
    @IBOutlet weak var ratingCodeStarView: FloatRatingView!
    @IBOutlet weak var ratingCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
