//
//  ZoomViewController.swift
//  Exlcart
//
//  Created by Tech Basics on 10/02/16.
//  Copyright (c) 2016 iPhone. All rights reserved.
//

import UIKit

class ZoomViewController: ParentViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var productImage:NSArray = []
    var selectedTag = Int ()
    
    @IBOutlet weak var imageCollView: UICollectionView!
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var zoomImgScrollView: UIScrollView!
    
    var isSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLeftNavigationButton()
        self.setRightNavigationButton()
        self.zoomImgScrollView.minimumZoomScale = 1.0;
        self.zoomImgScrollView.maximumZoomScale = 6.0;
        let tap = UITapGestureRecognizer(target: self, action: #selector(ZoomViewController.tappedMe))
        self.mainImage.addGestureRecognizer(tap)
        self.mainImage.isUserInteractionEnabled = true
        
        zoomImgScrollView.autoresizesSubviews = true
        
        if productImage.count != 0
        {
            let imageUrl = self.productImage[0] as! String
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            self.mainImage.sd_setImage(with: URL(string: trimmedUrl))
            
            self.imageCollView.reloadData()
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "No images Found", viewController: self)
        }
    }
    
    
    @IBAction func closeButtonPressed(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: { () -> Void in
        })
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.mainImage
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        
    }
    
    @objc func tappedMe()
    {
        self.zoomImgScrollView.zoomScale = 2.0;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return productImage.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "zoomCell", for: indexPath) as! ColorCollectionViewCell
        
        let imageURL = self.productImage[indexPath.row] as! String
        
        if (imageURL as AnyObject).lowercased.range(of: "no_image") != nil
        {
            
        }
        else
        {
            let trimmedUrl = imageURL.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            cell.productImg.sd_setImage(with: URL(string: trimmedUrl))
        }
        
        if selectedTag == indexPath.row
        {
            cell.productImg.layer.borderColor = themeColor.cgColor
            cell.productImg.layer.borderWidth = 2.0
            cell.productImg.layer.cornerRadius = 7.0
            cell.productImg.clipsToBounds = true
        }
        else
        {
            cell.productImg.layer.borderColor = UIColor.white.cgColor
            cell.productImg.layer.borderWidth = 0
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.zoomImgScrollView.zoomScale = 1.0
        let imgUrl : String = self.productImage[indexPath.row] as! String
        let trimmedUrl = imgUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
        self.mainImage.sd_setImage(with: URL(string: trimmedUrl))
        
        selectedTag = indexPath.row
        
        self.imageCollView.reloadData()
    }
    
}
