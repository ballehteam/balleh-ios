//
//  ProductDetailVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 20/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import FloatRatingView
import FSPagerView
import Alamofire
import FTPopOverMenu_Swift

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

class ProductDetailVC: ParentViewController,loginIntimation, FSPagerViewDataSource, FSPagerViewDelegate, UIWebViewDelegate, UIPopoverPresentationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var txtQuantity: UIButton!
    
    @IBOutlet weak var subButton: CustomButtonUI!
    
    var productID:String = ""
    var productDetailsDic : NSMutableDictionary = [:]
    var myImages:NSMutableArray = []
    var optionsDic:NSMutableDictionary = [:]
    var paragraphStyle = NSMutableParagraphStyle()
    let attributes : NSDictionary = [:]
    var descriptionWebView:UIWebView = UIWebView()
    let descView:UIView = UIView()
    var indexList = NSInteger()
    var index1 = NSInteger()
    var quantity1 = NSString()
    var scrollHeight:CGFloat = 0.0
    var productSku = ""
    var stockStatus = Bool()
    
    var brandsArr = NSArray()
    var sizeArr = NSArray()
    var countArr = NSArray()
    
    var brandValue = ""
    var sizeValue = ""
    var countValue = ""
    var optionsY = CGFloat()
    var isOptionRequired = false
    var subscriptionOptionArr = NSMutableArray()
    
    // @IBOutlet weak var favImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
   
    // @IBOutlet weak var favImgView: UIImageView!
    @IBOutlet weak var productImageView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var viewRelatedProducts: UIView!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var discribtionLbl: UILabel!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var imgWishList: UIButton!
    
    @IBOutlet weak var collRelatedProducts: UICollectionView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet var splPriceLbl: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnTitle: UIButton!
    
    @IBOutlet weak var imgReviewIc: UIImageView!
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var headerImage: UIImageView!
    
    var relatedProductsArr = NSMutableArray()
    var stockQuantity = 0
    var quantity = "1"
    
    // Options
    
    var optionFilteredArr = NSMutableArray()
    var selectedOptionsArr = NSMutableArray()
    var optionsArr = NSMutableArray()
    let optionCountArr = NSMutableArray()
    var optionValuesArr = NSMutableArray()
    @IBOutlet weak var tblOptions: UITableView!
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            pagerView.dataSource = self
            pagerView.delegate = self
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = .zero
        }
    }
    
    @IBOutlet weak var pagerControl: FSPageControl! {
        didSet {
            self.pagerControl.numberOfPages = self.myImages.count
            self.pagerControl.contentHorizontalAlignment = .center
            self.pagerControl.backgroundColor = .white
           // self.pagerControl.fillColors = [.normal : themeColor]
            self.pagerControl.setFillColor(positiveBtnColor, for: .selected)
            self.pagerControl.currentPage = 0
            self.pagerControl.setStrokeColor(positiveBtnColor, for: .normal)
            self.pagerControl.setStrokeColor(.white, for: .selected)
            self.pagerControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }
    
    var ratingArr = NSArray()
    var ratingCodesArr = NSArray()
    var postRatingData = NSMutableArray()
    
    //MARK: Product Banner
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int
    {
        return myImages.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell
    {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        //  cell.imageView?.image = UIImage(named: self.myImages[index] as! String)
        //  cell.imageView?.contentMode = .scaleAspectFill
        //  cell.imageView?.clipsToBounds = true
        // cell.textLabel?.text = index.description+index.description
        
        let imgUrl : String = self.myImages[index] as! String
        let trimmedUrl = imgUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
        cell.imageView?.sd_setImage(with: URL(string: trimmedUrl))
        cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        cell.imageView?.backgroundColor = .white
        
        var activityLoader = UIActivityIndicatorView()
        activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityLoader.center = (cell.imageView?.center)!
        activityLoader.startAnimating()
        cell.imageView?.addSubview(activityLoader)
        
        cell.imageView?.sd_setImage(with: URL(string: trimmedUrl), completed:
            { (image, error, imageCacheType, imageUrl) in
                if image != nil
                {
                    activityLoader.stopAnimating()
                    
                }
                else
                {
                    print("image not found")
                    cell.imageView?.image = UIImage(named: "no_image")
                    activityLoader.stopAnimating()
                }
        })
        
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int)
    {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        self.pagerControl.currentPage = index
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        // instantiate your desired ViewController
        let viewController = storyboard.instantiateViewController(withIdentifier: "ZoomViewController") as! ZoomViewController
        viewController.productImage = self.myImages
        self.present(viewController, animated: true, completion: { () -> Void in
            
        })
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView)
    {
        guard self.pagerControl.currentPage != pagerView.currentIndex else { return }
        self.pagerControl.currentPage = pagerView.currentIndex
        self.pagerControl.currentPage = pagerView.currentIndex // Or Use KVO with property "currentIndex"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Product Detail", comment: "")
        
        btnTitle.addTarget(self, action: #selector(self.clickTitle), for: .touchUpInside)
        btnCart.addTarget(self, action: #selector(self.clickCart(sender:)), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(self.clickClose(sender:)), for: .touchUpInside)
        btnMenu.addTarget(self, action: #selector(self.clickMenu(sender:)), for: .touchUpInside)
        
        headerImage.image = Tools.headerImage()
        
        if languageID == "2" {
            imgBack.transform = CGAffineTransform(scaleX: -1, y: 1)
            imgReviewIc.image = UIImage(named: "right-arrow1")
            splPriceLbl.textAlignment = .right
            priceLbl.textAlignment = .right
        }
        
        let nib = UINib(nibName: "GridCollectionViewCell", bundle: nil)
        self.collRelatedProducts.register(nib, forCellWithReuseIdentifier: "gridCell")
        
        self.txtQuantity.layer.borderColor = positiveBtnColor.cgColor
        
        if let brands = UserDefaults.standard.value(forKey: "BRANDS") {
            if "\(brands)" == "" {
                self.getBrands()
            } else {
                self.brandsArr = brands as! NSArray
            }
        } else {
            self.getBrands()
        }
        
        if let size = UserDefaults.standard.value(forKey: "SIZES") {
            if "\(size)" == "" {
                self.getSize()
            } else {
                self.sizeArr = size as! NSArray
            }
        } else {
            self.getSize()
        }
        
        if let count = UserDefaults.standard.value(forKey: "COUNTS") {
            if "\(count)" == "" {
                self.getCount()
            } else {
                self.countArr = count as! NSArray
            }
        } else {
            self.getCount()
        }
        
        updateToken()
        self.btnAddToCart.setTitle(NSLocalizedString("Add to Cart", comment: ""), for: UIControlState())
        
      //  self.imgWishList.frame.size.width = self.view.bounds.width/2
      //  self.btnAddToCart.frame.size.width = self.view.bounds.width/2
      //  self.btnAddToCart.frame.origin.x = self.imgWishList.frame.size.width
        
        self.titleLbl.text = ""
        self.priceLbl.text = ""
        self.splPriceLbl.text = ""
        self.discribtionLbl.text = ""
        self.priceLbl.textColor = .lightGray
        self.splPriceLbl.textColor = .black
        
        var img = UIImage (named: "ic_share")
        img = img?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.share))
        
        self.mainScrollView.autoresizesSubviews = true
        // self.imageScrollView.autoresizesSubviews = true
        //  self.zoomImgScrollView.autoresizesSubviews = true
        
        //products/24-WG086
        self.getRelatedProducts()
        
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "\(ConfigUrl.baseUrl)products/\(productSku)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            let result = (responseObject.result.value!) as! NSDictionary
                            print(result)
                            let userQuantity : NSMutableDictionary  = result.mutableCopy() as! NSMutableDictionary
                            userQuantity.setValue("1", forKey: "userQuantity")
                            
                            self.productDetailsDic = userQuantity
                            print(self.productDetailsDic)
                            self.updateCartCount()
                            
                            if self.productDetailsDic.count != 0 {
                                self.setFrames()
                            }
                            let attribute_code = self.productDetailsDic.value(forKey: "custom_attributes") as AnyObject
                            for i in 0..<attribute_code.count {
                                if let code = (attribute_code.object(at: i) as AnyObject).value(forKey: "attribute_code") as? String {
                                    print(code)
                                    let value = "\((attribute_code.object(at: i) as AnyObject).value(forKey: "value")!)"
                                    if code == "subscription_active",
                                        value == "1" {
                                        self.subButton.isHidden = false
                                        break
                                    }
                                }
                                
                            }
//                            if let sub = atrributesz.value(forKeyPath: "attribute_code") as? String,
//                                sub == "subscription_active"{
//                                print(sub)
//                            }
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        // Do any additional setup after loading the view.
    }
    
    func updateCartCount() {
       // let cartCount = "\(String(describing: UserDefaults.standard.string(forKey: kAddToCart)))"
        
        if cartCount == "" || cartCount == "nil" || cartCount == "0" {
            for subview in (btnCart.subviews) {
                if subview.tag == 321 {
                    subview.removeFromSuperview()
                }
            }
        } else {
            // badge label
            let label = UILabel(frame: CGRect(x: 15, y: -4, width: 20, height: 20))
            label.layer.borderColor = UIColor.clear.cgColor
            label.layer.borderWidth = 2
            label.layer.cornerRadius = label.bounds.size.height / 2
            label.textAlignment = .center
            label.layer.masksToBounds = true
            label.textColor = .white
            label.font = UIFont.appFontWith(size: 12)
            label.backgroundColor = positiveBtnColor
            label.text = cartCount
            label.tag = 1000
            var image = UIImage(named: "cart")
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            btnCart.addSubview(label)
        }
    }
    
    func setFrames() {
        self.mainScrollView.isScrollEnabled = true
        
        var brandId = ""
        var sizeId = ""
        var countId = ""
        print(self.productDetailsDic)
        self.titleLbl.text = "\(self.productDetailsDic.value(forKey: "name")!)"
        
      //  self.votesAndReviewLbl.text = NSString(format: "%@ REVIEWS", self.productDetailsDic.value(forKeyPath: "product.reviews") as! String) as String
        
        let atrributes = self.productDetailsDic.value(forKey: "custom_attributes") as AnyObject
        
        for i in 0..<atrributes.count
        {
            let code = (atrributes.object(at: i) as AnyObject).value(forKey: "attribute_code") as! String
            let value = "\((atrributes.object(at: i) as AnyObject).value(forKey: "value")!)"
            
            var title = ""
            if languageID == "1"
            {
                title = "full_name_en"
            }
            else
            {
                title = "full_name_ar"
            }
            
            if code == title
            {
                self.titleLbl.text = value
            }
            else if code == "description"
            {
              //  self.descriptionWebView.text = value as! String
                let htmlCode = value
                descriptionWebView.loadHTMLString(htmlCode, baseURL: nil)
            }
            else if code == "special_price"
            {
                self.priceLbl.text = "\(Tools.getCurrency()) \(value)"
            }
            else if code == "size"
            {
                sizeId = value
                
                for i in 0..<self.sizeArr.count
                {
                    let id = "\((self.sizeArr.object(at: i) as AnyObject).value(forKey: "value")!)"
                    
                    if sizeId == id
                    {
                        self.sizeValue = "\((self.sizeArr.object(at: i) as AnyObject).value(forKey: "label")!)"
                    }
                }
            }
            else if code == "count"
            {
                countId = value
                
                for i in 0..<self.countArr.count
                {
                    let id = "\((self.countArr.object(at: i) as AnyObject).value(forKey: "value")!)"
                    
                    if countId == id
                    {
                        self.countValue = "\((self.countArr.object(at: i) as AnyObject).value(forKey: "label")!)"
                    }
                }
            }
            else if code == "brand"
            {
                brandId = value
                
                for i in 0..<self.brandsArr.count
                {
                    let id = "\((self.brandsArr.object(at: i) as AnyObject).value(forKey: "value")!)"
                    
                    if brandId == id
                    {
                        self.brandValue = "\((self.brandsArr.object(at: i) as AnyObject).value(forKey: "label")!)"
                    }
                }
            }
        }
        
        var price = "0"
        
        if let value = self.productDetailsDic.value(forKey: "price")
        {
            if "\(value)" != "nil" && "\(value)" != ""
            {
                price = "\(value)"
            }
        }
        
        self.splPriceLbl.text = "SAR \(price)"
        
        if self.priceLbl.text! != "SAR 0" && self.priceLbl.text! != "SAR " && self.priceLbl.text! != ""
        {
            self.priceLbl.isHidden = false
            self.splPriceLbl.text = self.priceLbl.text
            self.priceLbl.text = "SAR \(price)"
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self.priceLbl.text!)
            attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            self.priceLbl.attributedText = attributeString
            self.splPriceLbl.textColor = positiveBtnColor
            self.priceLbl.textColor = .darkGray
        }
        else
        {
            self.priceLbl.isHidden = true
            self.splPriceLbl.textColor = positiveBtnColor
        }
        
        self.splPriceLbl.sizeToFit()
        self.priceLbl.sizeToFit()
        
        //self.priceLbl.frame.origin.x = self.splPriceLbl.frame.origin.x + self.splPriceLbl.frame.width + 5
        
        //self.productImageView.frame = CGRect(x: self.productImageView.frame.origin.x, y: 10, width: self.productImageView.frame.size.width, height: self.productImageView.frame.size.height)
        
        let font = UIFont.appFontWith(size: 16)
        self.titleLbl.font = font
        self.titleLbl.numberOfLines = 0
        
        let height = ProductDetailVC.heightForView(self.titleLbl.text!, font: font, width: UIScreen.main.bounds.size.width-20)
        //self.titleLbl.frame = CGRect(x: 10, y: self.productImageView.frame.origin.y+self.productImageView.frame.size.height+10, width: UIScreen.main.bounds.size.width-20, height: height)
        //self.mainScrollView.addSubview(self.titleLbl)
        
        //self.ratingView.frame = CGRect(x:self.productImageView.frame.origin.x, y:self.titleLbl.frame.origin.y+height+5,width: self.ratingView.frame.size.width, height:self.ratingView.frame.size.height)
        
        let temp = self.productDetailsDic.value(forKeyPath: "options") as! NSArray
        optionsArr.removeAllObjects()
        
        for i in 0..<temp.count
        {
            self.optionValuesArr = ((temp.object(at: i) as AnyObject).value(forKey: "values") as! NSArray).mutableCopy() as! NSMutableArray
            
            let optionTitle = "\((self.optionValuesArr.object(at : 0) as AnyObject).value(forKey: "title")!)"
            
            if optionTitle.lowercased() != "one time" && optionTitle.lowercased() != "every month"
            {
                self.optionsArr.add(temp.object(at: i))
            }
            else
            {
                if optionTitle.lowercased() == "one time" && optionTitle.lowercased() != "every month"
                {
                    self.subscriptionOptionArr.add(temp.object(at: i))
                }
            }
        }
        
       // self.optionsArr = (self.productDetailsDic.value(forKeyPath: "options") as! NSArray).mutableCopy() as! NSMutableArray
        print(optionsArr)
        for i in 0..<optionsArr.count
        {
            let bool = "\((optionsArr.object(at: i) as AnyObject).value(forKey: "is_require")!)"
            
            if bool == "1" || bool == "true"
            {
                isOptionRequired = true
            }
        }
        
        self.tblOptions.reloadData()
        
        self.tblOptions.frame = CGRect(x:0, y:self.ratingView.frame.origin.y + self.ratingView.frame.size.height + 10,width: self.view.bounds.width, height: CGFloat(self.optionsArr.count * 100))
        self.mainScrollView.addSubview(self.tblOptions)
         self.tblOptions.translatesAutoresizingMaskIntoConstraints = true
        
        optionsY = self.tblOptions.frame.origin.y + self.tblOptions.frame.size.height + 10
        
        if let images = self.productDetailsDic.value(forKey: "media_gallery_entries")
        {
            for i in 0..<(images as AnyObject).count
            {
                let imageFile = ((images as AnyObject).object(at: i) as AnyObject).value(forKey: "file") as! String
                
                self.myImages.add("\(ConfigUrl.imageUrl)\(imageFile)")
            }
        }
        
      //  self.myImages = (self.productDetailsDic.value(forKeyPath: "product.images") as? NSMutableArray)!
        
      //  let mainImgDic:NSDictionary = ["image":self.productDetailsDic.value(forKeyPath: "product.image") as! String]
        
      //  self.myImages.insert(mainImgDic, at: 0)
        self.pagerControl.numberOfPages = self.myImages.count
        self.pagerView.reloadData()

          self.descriptionWebView.delegate = self
        self.descriptionWebView.sizeToFit()
        //   self.descriptionWebView.stringByEvaluatingJavaScript( from: String(format: "document.body.style.font-size = '8px'"))
        //   self.descriptionWebView.loadRequest(NSURLRequest(url: NSURL(string: url as String)! as URL) as URLRequest)
        
        var coordY:CGFloat = 40.0
        let descLbl:UILabel = UILabel()
        
        descLbl.frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.size.width - 20, height: 25)
        descLbl.text = NSString(format: "Description", locale: nil) as String
        descLbl.font = UIFont.appFontWith(size: 16)
        descLbl.numberOfLines = 0
        descLbl.textAlignment = .natural
        
        self.descriptionWebView.frame = CGRect(x:0, y: 25, width:UIScreen.main.bounds.size.width, height:height)
     //   self.descriptionWebView.sizeToFit()
        descriptionWebView.scrollView.bounces = false
        
      //  self.descView.addSubview(descLbl)
        self.descView.addSubview(self.descriptionWebView)
        
        coordY = coordY + max(descLbl.frame.size.height, self.descriptionWebView.frame.size.height) + 5
        
        self.descView.frame = CGRect(x:0, y: optionsY, width:UIScreen.main.bounds.size.width, height:coordY)
        self.descView.backgroundColor = UIColor.white
        self.mainScrollView.addSubview(self.descView)
        
        let scHeight = CGFloat(self.optionsArr.count * 100) + self.descriptionWebView.scrollView.contentSize.height + 65
        
        self.scrollHeight = self.productImageView.frame.size.height + height + self.ratingView.frame.size.height + scHeight
        
        if relatedProductsArr.count != 0 {
            self.viewRelatedProducts.isHidden = false
            self.viewRelatedProducts.frame = CGRect(x:0, y: descView.frame.origin.y + descView.frame.size.height + 20, width:UIScreen.main.bounds.size.width, height:289)
            self.mainScrollView.addSubview(self.viewRelatedProducts)
            
            scrollHeight = scrollHeight + 289
        }
        
        self.mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.scrollHeight)
        //  self.mainScrollView.addSubview(self.descriptionWebView)
        

        
      //  self.mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.productImageView.frame.size.height + self.ratingView.frame.size.height + self.descriptionWebView.frame.size.height + self.viewRelatedProducts.frame.size.height + 100)
        
       // self.mainScrollView.translatesAutoresizingMaskIntoConstraints = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.checkStock()
        updateCartCount()
        
        self.tabBarController?.tabBar.isHidden = false
        
      //  setLeftNavigationButton()
        
       // self.setRightNavigationButton()
        
        let isAleadyFavorited:Bool = self.checkProductInWishList(self.productSku)
        if isAleadyFavorited {
            self.imgWishList.setImage(UIImage (named: "ic_favourite"), for: .normal)
        } else {
            self.imgWishList.setImage(UIImage (named: "ic_unfavourite"), for: .normal)
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SharedManager.dismissHUD(viewController: self)
        
        if webView.isLoading {
            return
        } else {
            let height = webView.scrollView.contentSize.height
            
            let descLbl:UILabel = UILabel()
            
            if height > 50 {
                var coordY:CGFloat = 40.0
                
                descLbl.frame = CGRect(x: 10, y: 0, width: descView.frame.width - 20, height: 25)
                descLbl.text = NSLocalizedString("Description", comment: "Product description")
                descLbl.font = UIFont.appFontWith(size: 15)
                descLbl.numberOfLines = 0
                descLbl.textAlignment = .natural
                
                self.tblOptions.frame = CGRect(x:0, y:self.ratingView.frame.origin.y + self.ratingView.frame.size.height + 10,width: self.view.bounds.width, height: CGFloat(self.optionsArr.count * 100))
                self.tblOptions.translatesAutoresizingMaskIntoConstraints = true
                
                descriptionWebView.frame = CGRect(x:0, y:25, width:UIScreen.main.bounds.size.width, height:height)
              //  descriptionWebView.scrollView.bounces = false
                
                descView.addSubview(descLbl)
                descView.addSubview(descriptionWebView)
                
                coordY = coordY + max(descLbl.frame.size.height, descriptionWebView.frame.size.height) + 5
                
                descView.frame = CGRect(x:0, y: self.tblOptions.frame.origin.y + self.tblOptions.frame.size.height + 10 , width:UIScreen.main.bounds.size.width, height:coordY)
                descView.backgroundColor = UIColor.white
                
                self.mainScrollView.addSubview(descView)
                
                self.scrollHeight = self.productImageView.frame.size.height + self.ratingView.frame.size.height + self.tblOptions.frame.size.height + coordY + 65
            
                if relatedProductsArr.count != 0 {
                    self.viewRelatedProducts.isHidden = false
                    self.viewRelatedProducts.frame = CGRect(x:0, y: descView.frame.origin.y + descView.frame.size.height + 10, width:UIScreen.main.bounds.size.width, height:289)
                    self.mainScrollView.addSubview(self.viewRelatedProducts)
                    
                    scrollHeight = scrollHeight + 289
                }
                
                self.mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: scrollHeight)
            }
        }
    }
    
    @IBAction func clickSpecification(_ sender: Any)
    {
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let VC = mainStoryboard.instantiateViewController(withIdentifier: "SpecificationViewController") as! SpecificationViewController
//
//        VC.detailsArr = (self.productDetailsDic.value(forKeyPath: "custom_attributes") as? NSMutableArray)!
//        VC.productName = (self.productDetailsDic.value(forKeyPath: "name") as? String)!
//        let tempArr = NSMutableArray()
//      //  tempArr.add(self.productDetailsDic.value(forKeyPath: "product.manufacturer")!)
//      //  tempArr.add(self.productDetailsDic.value(forKeyPath: "product.quantity")!)
//        VC.gDetailsArr = tempArr
//
//        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func heightForViewSpecfic(_ text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        if label.frame.height > 25
        {
            return label.frame.height
        }
        else
        {
            return 25
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonImageTapped(_ sender: UIImageView) {
        for index : Int in 0 ..< self.myImages.count {
            let imgUrl : String = (self.myImages[index] as AnyObject).value(forKey: "image") as! String
            let trimmedUrl = imgUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            
            /*  var activityLoader = UIActivityIndicatorView()
             activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
             activityLoader.center = self.mainImage.center
             activityLoader.startAnimating()
             self.mainImage.addSubview(activityLoader)
             
             self.mainImage.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
             
             if image != nil
             {
             
             activityLoader.stopAnimating()
             }else
             {
             print("image not found")
             activityLoader.stopAnimating()
             }
             })*/
        }
    }
    
    @objc func optionSelection(_ sender:CustomButton) {
        for scrollSubview in self.mainScrollView.subviews {
            if(scrollSubview.isKind(of: UIView.self) && scrollSubview.tag == 2000) {
                let buttonView:UIView = scrollSubview
                
                for tempView in buttonView.subviews {
                    if tempView.isKind(of: CustomButton.self) as Bool {
                        let button = tempView as? CustomButton
                        
                        if button?.type == sender.type {
                            if sender.tagValue == button?.tagValue {
                                if button?.backgroundColor == themeColor {
                                    button!.backgroundColor = UIColor.clear
                                    button!.setTitleColor(UIColor.black, for: UIControlState())
                                } else {
                                    let filterPredicate:NSPredicate = NSPredicate(format: "option_id =%@",button!.type)
                                    let filterResult = (self.productDetailsDic.value(forKeyPath: "options") as! NSArray).filtered(using: filterPredicate)
                                    
                                    let tempAry:NSArray = filterResult as NSArray
                                    
                                    if tempAry.count > 0 {
                                        let value:String = ((((tempAry.object(at: 0) as AnyObject).object(forKey: "values") as! NSArray).object(at: button!.tagValue) as AnyObject).object(forKey: "option_type_id") as? String)!
                                        
                                        let key:String = ((tempAry.object(at: 0) as AnyObject).object(forKey: "option_id") as?String)!
                                        
                                        optionsDic.setValue(value, forKey:key)
                                    }
                                    button!.backgroundColor = themeColor
                                    button!.setTitleColor(UIColor.white, for: UIControlState())
                                }
                            } else {
                                button!.backgroundColor = UIColor.clear
                                button!.setTitleColor(UIColor.black, for: UIControlState())
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func checkProductOptionInCart() -> Bool {
        var isCartOptionHave:Bool = false
        
        if  UserDefaults.standard.object(forKey: kAddToCart) != nil {
            let cartData = UserDefaults.standard.object(forKey: kAddToCart) as! Data
            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: cartData) as! NSMutableArray
            
            for cartTemp in cartAry {
                let tempDic:NSMutableDictionary = cartTemp as! NSMutableDictionary
                let options = tempDic.value(forKey: "options") as! NSArray
                
                if options.count == 0 {
                    isCartOptionHave = false
                } else {
                    let optionValues = tempDic.value(forKey: "values") as! NSMutableDictionary
                    if optionValues == self.optionsDic {
                        isCartOptionHave = true
                        index1 = cartAry.index(of: cartTemp)
                    }
                }
            }
        }
        return isCartOptionHave
    }
    
    class func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    @objc func share() {
        // text to share
        let text = "http://eshop.exlcart.in/index.php?route=product/product&product_id=\(productID)"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        // activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @available(iOS 12.0, *)
    @IBAction func clickQuantity(_ sender: AnyObject) {
        if self.stockQuantity != 0 {
            let configuration = FTConfiguration.shared
            // configuration.menuRowHeight = ...
            //   configuration.menuWidth = ...
            configuration.textColor = .black
            configuration.textFont = UIFont.appFontWith(size: 15)
            configuration.backgoundTintColor = .white
            configuration.borderColor = positiveBtnColor
            configuration.borderWidth = 2
            configuration.textAlignment = .center
            
            var array : [String] = []
            
            if self.stockQuantity <= 10 {
                for i in 0..<self.stockQuantity {
                    let str = "\(i + 1)"
                    array.append(str)
                }
                print(array)
            } else {
                array = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
            }
            
            FTPopOverMenu.showFromSenderFrame(senderFrame: sender.frame, with: array, done: { (selectedIndex) -> () in
                self.quantity = array[selectedIndex]
                self.txtQuantity.setTitle("\(array[selectedIndex])", for: .normal)
            }) {
                
            }
        } else {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("This Product is currently out of Stock", comment: ""))", viewController: self)
        }
    }
    
    @IBAction func addToCartAction(_ sender: AnyObject) {
        self.addToCart(sub: false)
    }
    
    @IBAction func subscribeAction(_ sender: AnyObject) {
        self.addToCart(sub: true)
    }
    
    func addToCart(sub: Bool) {
        if stockStatus
        {
            if isOptionRequired && selectedOptionsArr.count == 0
            {
                SharedManager.showAlertWithMessage(title: "Sorry", alertMessage: "Please select any Option", viewController: self)
            }
            else
            {
                if let id = UserDefaults.standard.string(forKey: "USER_ID")
                {
                    userIDStr = id
                    
                    if userIDStr != ""
                    {
                        cartID = getCartID(cusToken: userIDStr)
                        print("cartID : \(cartID)")
                        
                        var values : [String : Any] = [:]
                        print(self.subscriptionOptionArr.count)
                        if self.subscriptionOptionArr.count != 0
                        {
                            let optionValues = (self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "values") as AnyObject
                            
                            let tempDict = NSMutableDictionary()
                            
                            for i in 0..<optionValues.count
                            {
                                let optionTitle = "\((optionValues.object(at : i) as AnyObject).value(forKey: "title")!)"
                                
                                if optionTitle.lowercased() == "every month"
                                {
                                    let selectedID = "\((optionValues.object(at : i) as AnyObject).value(forKey: "option_type_id")!)"
                                    
                                    let attrID = "\((self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "option_id")!)"
                                    
                                    tempDict.setObject(attrID, forKey: "option_id" as NSCopying)
                                    tempDict.setObject(selectedID, forKey: "option_value" as NSCopying)
                                    
                                    self.selectedOptionsArr.add(tempDict)
                                }
                            }
                        }
                        
                        if selectedOptionsArr.count != 0 {
                            let options = NSMutableDictionary()
                            options.setObject(selectedOptionsArr, forKey: "custom_options" as NSCopying)
                            
                            let temp = NSMutableDictionary()
                            temp.setObject(options, forKey: "extension_attributes" as NSCopying)
                            
                            if sub {
                                values = [ "quote_id" : cartID,
                                           "sku" : productSku,
                                           "qty" : self.quantity,
                                           "product_option" : temp
                                ]
                                
                            } else {
                                values = [ "quote_id" : cartID,
                                           "sku" : productSku,
                                           "qty" : self.quantity
                                ]
                                
                            }
                            
                            
                        } else {
                            values = [ "quote_id" : cartID,
                                       "sku" : productSku,
                                       "qty" : self.quantity
                            ]
                        }
                        
                        let params = ["cart_item" : values] as [String : Any]
                        
                        let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items"
                        
                        SharedManager.showHUD(viewController: self)
                        
                        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                        var request = URLRequest(url: URL(string: setFinalURl)!)
                        request.httpMethod = HTTPMethod.post.rawValue
                        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                        request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                        
                        let setTemp: [String : Any] = params
                        
                        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                            let jsonString = String(data: jsonData , encoding: .utf8)!
                            print(jsonString as Any)
                            request.httpBody = jsonData
                        }
                        
                        if Connectivity.isConnectedToInternet() {
                            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                                
                                if responseObject.result.isSuccess
                                {
                                    SharedManager.dismissHUD(viewController: self)
                                    if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                    {
                                        print(responseObject.result.value!)
                                        self.getCartDetails(cusToken: userIDStr, completionHandler: {(result) -> () in
                                            // print(result)
                                            self.delay(1, closure: {
                                                self.updateCartCount()
                                            })
                                        })
                                        self.cartItemNotification(quoteId: cartID)
                                        self.showToast(message: NSLocalizedString("Successfully added to your cart", comment: ""))
                                    }
                                    else
                                    {
                                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                    }
                                }
                                
                                if responseObject.result.isFailure
                                {
                                    SharedManager.dismissHUD(viewController: self)
                                    let error : Error = responseObject.result.error!
                                    print(error.localizedDescription)
                                }
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                        }
                    }
                    else
                    {
                        if cartID == ""
                        {
                            createGuestCartID(completionHandler: {(result) -> () in
                                cartID = result
                                
                                print("Guest cartID : \(cartID)")
                                
                                var values : [String : Any] = [:]
                                
                                
                                if self.subscriptionOptionArr.count != 0
                                {
                                    let optionValues = (self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "values") as AnyObject
                                    
                                    let tempDict = NSMutableDictionary()
                                    
                                    for i in 0..<optionValues.count
                                    {
                                        let optionTitle = "\((optionValues.object(at : i) as AnyObject).value(forKey: "title")!)"
                                        
                                        if optionTitle.lowercased() == "one time"
                                        {
                                            let selectedID = "\((optionValues.object(at : i) as AnyObject).value(forKey: "option_type_id")!)"
                                            
                                            let attrID = "\((self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "option_id")!)"
                                            
                                            tempDict.setObject(attrID, forKey: "option_id" as NSCopying)
                                            tempDict.setObject(selectedID, forKey: "option_value" as NSCopying)
                                            
                                            self.selectedOptionsArr.add(tempDict)
                                        }
                                    }
                                }
                                
                                if self.selectedOptionsArr.count != 0
                                {
                                    let options = NSMutableDictionary()
                                    options.setObject(self.selectedOptionsArr, forKey: "custom_options" as NSCopying)
                                    
                                    let temp = NSMutableDictionary()
                                    temp.setObject(options, forKey: "extension_attributes" as NSCopying)
                                    
                                    if sub {
                                        values = [ "quote_id" : cartID,
                                                   "sku" : self.productSku,
                                                   "qty" : self.quantity,
                                                   "product_option" : temp
                                        ]
                                        
                                    } else {
                                        values = [ "quote_id" : cartID,
                                                   "sku" : self.productSku,
                                                   "qty" : self.quantity
                                        ]
                                        
                                    }
                                }
                                else
                                {
                                    values = [ "quote_id" : cartID,
                                               "sku" : self.productSku,
                                               "qty" : self.quantity
                                    ]
                                }
                                
                                let params = ["cart_item" : values] as [String : Any]
                                
                                let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/items"
                                
                                SharedManager.showHUD(viewController: self)
                                
                                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                                var request = URLRequest(url: URL(string: setFinalURl)!)
                                request.httpMethod = HTTPMethod.post.rawValue
                                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                                // request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                                
                                let setTemp: [String : Any] = params
                                
                                if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                                    let jsonString = String(data: jsonData , encoding: .utf8)!
                                    print(jsonString as Any)
                                    request.httpBody = jsonData
                                }
                                
                                if Connectivity.isConnectedToInternet()
                                {
                                    Alamofire.request(request).responseJSON { (responseObject) -> Void in
                                        
                                        if responseObject.result.isSuccess
                                        {
                                            SharedManager.dismissHUD(viewController: self)
                                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                            {
                                                print(responseObject.result.value!)
                                                self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                                                    // print(result)
                                                    self.delay(1, closure: {
                                                        self.updateCartCount()
                                                    })
                                                })
                                                self.cartItemNotification(quoteId: cartID)
                                                self.showToast(message: NSLocalizedString("Successfully added to your cart", comment: ""))
                                            } else {
                                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                            }
                                        }
                                        
                                        if responseObject.result.isFailure {
                                            SharedManager.dismissHUD(viewController: self)
                                            let error : Error = responseObject.result.error!
                                            print(error.localizedDescription)
                                        }
                                    }
                                } else {
                                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                                }
                            })
                        } else {
                            print("Guest cartID : \(cartID)")
                            
                            var values : [String : Any] = [:]
                            
                            if self.subscriptionOptionArr.count != 0 {
                                let optionValues = (self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "values") as AnyObject
                                
                                let tempDict = NSMutableDictionary()
                                
                                for i in 0..<optionValues.count {
                                    let optionTitle = "\((optionValues.object(at : i) as AnyObject).value(forKey: "title")!)"
                                    
                                    if optionTitle.lowercased() == "one time" {
                                        let selectedID = "\((optionValues.object(at : i) as AnyObject).value(forKey: "option_type_id")!)"
                                        
                                        let attrID = "\((self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "option_id")!)"
                                        
                                        tempDict.setObject(attrID, forKey: "option_id" as NSCopying)
                                        tempDict.setObject(selectedID, forKey: "option_value" as NSCopying)
                                        
                                        self.selectedOptionsArr.add(tempDict)
                                    }
                                }
                            }
                            
                            if selectedOptionsArr.count != 0 {
                                let options = NSMutableDictionary()
                                options.setObject(selectedOptionsArr, forKey: "custom_options" as NSCopying)
                                
                                let temp = NSMutableDictionary()
                                temp.setObject(options, forKey: "extension_attributes" as NSCopying)
                                
                                if sub {
                                    values = [ "quote_id" : cartID,
                                               "sku" : productSku,
                                               "qty" : self.quantity,
                                               "product_option" : temp
                                    ]
                                    
                                } else {
                                    values = [ "quote_id" : cartID,
                                               "sku" : productSku,
                                               "qty" : self.quantity
                                    ]
                                    
                                }
                            }
                            else
                            {
                                values = [ "quote_id" : cartID,
                                           "sku" : productSku,
                                           "qty" : self.quantity
                                ]
                            }
                            
                            let params = ["cart_item" : values] as [String : Any]
                            
                            let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/items"
                            
                            SharedManager.showHUD(viewController: self)
                            
                            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                            var request = URLRequest(url: URL(string: setFinalURl)!)
                            request.httpMethod = HTTPMethod.post.rawValue
                            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                            // request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                            
                            let setTemp: [String : Any] = params
                            
                            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                                let jsonString = String(data: jsonData , encoding: .utf8)!
                                print(jsonString as Any)
                                request.httpBody = jsonData
                            }
                            
                            if Connectivity.isConnectedToInternet()
                            {
                                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                                    
                                    if responseObject.result.isSuccess
                                    {
                                        SharedManager.dismissHUD(viewController: self)
                                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                        {
                                            print(responseObject.result.value!)
                                            self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                                                // print(result)
                                                self.delay(1, closure: {
                                                    self.updateCartCount()
                                                })
                                            })
                                            self.cartItemNotification(quoteId: cartID)
                                            self.showToast(message: NSLocalizedString("Successfully added to your cart", comment: ""))
                                        }
                                        else
                                        {
                                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                        }
                                    }
                                    
                                    if responseObject.result.isFailure
                                    {
                                        SharedManager.dismissHUD(viewController: self)
                                        let error : Error = responseObject.result.error!
                                        print(error.localizedDescription)
                                    }
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                            }
                        }
                    }
                }
                else
                {
                    if cartID == ""
                    {
                        createGuestCartID(completionHandler: {(result) -> () in
                            cartID = result
                            
                            print("Guest cartID : \(cartID)")
                            
                            var values : [String : Any] = [:]
                            
                            if self.subscriptionOptionArr.count != 0
                            {
                                let optionValues = (self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "values") as AnyObject
                                
                                let tempDict = NSMutableDictionary()
                                
                                for i in 0..<optionValues.count
                                {
                                    let optionTitle = "\((optionValues.object(at : i) as AnyObject).value(forKey: "title")!)"
                                    
                                    if optionTitle.lowercased() == "one time"
                                    {
                                        let selectedID = "\((optionValues.object(at : i) as AnyObject).value(forKey: "option_type_id")!)"
                                        
                                        let attrID = "\((self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "option_id")!)"
                                        
                                        tempDict.setObject(attrID, forKey: "option_id" as NSCopying)
                                        tempDict.setObject(selectedID, forKey: "option_value" as NSCopying)
                                        
                                        self.selectedOptionsArr.add(tempDict)
                                    }
                                }
                            }
                            
                            if self.selectedOptionsArr.count != 0
                            {
                                let options = NSMutableDictionary()
                                options.setObject(self.selectedOptionsArr, forKey: "custom_options" as NSCopying)
                                
                                let temp = NSMutableDictionary()
                                temp.setObject(options, forKey: "extension_attributes" as NSCopying)
                                
                                if sub {
                                    values = [ "quote_id" : cartID,
                                               "sku" : self.productSku,
                                               "qty" : self.quantity,
                                               "product_option" : temp
                                    ]
                                    
                                } else {
                                    values = [ "quote_id" : cartID,
                                               "sku" : self.productSku,
                                               "qty" : self.quantity
                                    ]
                                    
                                }
                            }
                            else
                            {
                                values = [ "quote_id" : cartID,
                                           "sku" : self.productSku,
                                           "qty" : self.quantity
                                ]
                            }
                            
                            let params = ["cart_item" : values] as [String : Any]
                            
                            let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/items"
                            SharedManager.showHUD(viewController: self)
                            
                            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                            var request = URLRequest(url: URL(string: setFinalURl)!)
                            request.httpMethod = HTTPMethod.post.rawValue
                            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                            // request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                            
                            let setTemp: [String : Any] = params
                            
                            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                                let jsonString = String(data: jsonData , encoding: .utf8)!
                                print(jsonString as Any)
                                request.httpBody = jsonData
                            }
                            
                            if Connectivity.isConnectedToInternet()
                            {
                                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                                    
                                    if responseObject.result.isSuccess
                                    {
                                        SharedManager.dismissHUD(viewController: self)
                                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                        {
                                            print(responseObject.result.value!)
                                            self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                                                // print(result)
                                                self.delay(1, closure: {
                                                    self.updateCartCount()
                                                })
                                            })
                                            self.cartItemNotification(quoteId: cartID)
                                            self.showToast(message: NSLocalizedString("Successfully added to your cart", comment: ""))
                                        }
                                        else
                                        {
                                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                        }
                                    }
                                    
                                    if responseObject.result.isFailure
                                    {
                                        SharedManager.dismissHUD(viewController: self)
                                        let error : Error = responseObject.result.error!
                                        print(error.localizedDescription)
                                    }
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                            }
                        })
                    } else {
                        print("Guest cartID : \(cartID)")
                        
                        var values : [String : Any] = [:]
                        
                        if self.subscriptionOptionArr.count != 0
                        {
                            let optionValues = (self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "values") as AnyObject
                            
                            let tempDict = NSMutableDictionary()
                            
                            for i in 0..<optionValues.count
                            {
                                let optionTitle = "\((optionValues.object(at : i) as AnyObject).value(forKey: "title")!)"
                                
                                if optionTitle.lowercased() == "one time"
                                {
                                    let selectedID = "\((optionValues.object(at : i) as AnyObject).value(forKey: "option_type_id")!)"
                                    
                                    let attrID = "\((self.subscriptionOptionArr.object(at: 0) as AnyObject).value(forKey: "option_id")!)"
                                    
                                    tempDict.setObject(attrID, forKey: "option_id" as NSCopying)
                                    tempDict.setObject(selectedID, forKey: "option_value" as NSCopying)
                                    
                                    self.selectedOptionsArr.add(tempDict)
                                }
                            }
                        }
                        
                        if selectedOptionsArr.count != 0
                        {
                            let options = NSMutableDictionary()
                            options.setObject(selectedOptionsArr, forKey: "custom_options" as NSCopying)
                            
                            let temp = NSMutableDictionary()
                            temp.setObject(options, forKey: "extension_attributes" as NSCopying)
                            
                            if sub {
                                values = [ "quote_id" : cartID,
                                           "sku" : productSku,
                                           "qty" : self.quantity,
                                           "product_option" : temp
                                ]
                                
                            } else {
                                values = [ "quote_id" : cartID,
                                           "sku" : productSku,
                                           "qty" : self.quantity
                                ]
                                
                            }
                        } else {
                            values = [ "quote_id" : cartID,
                                       "sku" : productSku,
                                       "qty" : self.quantity
                            ]
                        }
                        
                        let params = ["cart_item" : values] as [String : Any]
                        
                        let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/items"
                        
                        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                        var request = URLRequest(url: URL(string: setFinalURl)!)
                        request.httpMethod = HTTPMethod.post.rawValue
                        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                        // request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                        
                        let setTemp: [String : Any] = params
                        
                        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                            let jsonString = String(data: jsonData , encoding: .utf8)!
                            print(jsonString as Any)
                            request.httpBody = jsonData
                        }
                        
                        if Connectivity.isConnectedToInternet()
                        {
                            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                                
                                SharedManager.dismissHUD(viewController: self)
                                if responseObject.result.isSuccess
                                {
                                    if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                    {
                                        print(responseObject.result.value!)
                                        self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                                            // print(result)
                                            self.delay(1, closure: {
                                                self.updateCartCount()
                                            })
                                        })
                                        self.cartItemNotification(quoteId: cartID)
                                        self.showToast(message: NSLocalizedString("Successfully added to your cart", comment: ""))
                                    } else {
                                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                    }
                                }
                                
                                if responseObject.result.isFailure
                                {
                                    let error : Error = responseObject.result.error!
                                    print(error.localizedDescription)
                                }
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                        }
                    }
                }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("This Product is currently out of Stock", comment: ""))", viewController: self)
        }
    }
    
    
    
    func loginSuccess() {
        self.wishlist()
    }
    
    func loginFailure() {
        
    }
    
    //MARK: CollectionView Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collRelatedProducts {
            return self.relatedProductsArr.count
        } else {
            if optionsArr.count != 0 {
                self.optionValuesArr = ((optionsArr.object(at: collectionView.tag) as AnyObject).value(forKey: "values") as! NSArray).mutableCopy() as! NSMutableArray
                
                return self.optionValuesArr.count
            } else {
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collRelatedProducts {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
            cell.title.text = (self.relatedProductsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
            
            var price = "0"
            if let value = (relatedProductsArr.object(at: indexPath.row) as AnyObject).value(forKey: "price") {
                if "\(value)" != "nil" && "\(value)" != "" {
                    price = "\(value)"
                }
            }
            
            cell.splPriceLbl.text = "SAR \(price)"
            
            let customAttributes = (self.relatedProductsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "custom_attributes") as AnyObject
            
            var imageUrl = ""
            
            for i in 0..<customAttributes.count {
                let name = customAttributes[i].value(forKey: "attribute_code") as! String
                let value = "\(customAttributes[i].value(forKey: "value")!)"
                
                var title = ""
                if languageID == "1" {
                    title = "full_name_en"
                } else {
                    title = "full_name_ar"
                }
                
                if name == title {
                    cell.title.text = "\(customAttributes[i].value(forKey: "value")!)"
                }
                
                if name == "small_image" {
                    imageUrl = "\(ConfigUrl.imageUrl)\(value)"
                } else if name == "special_price" {
                    cell.priceLbl.text = "\(Tools.getCurrency()) \(value)"
                }
            }
            
            if cell.priceLbl.text! != "\(Tools.getCurrency()) 0" && cell.priceLbl.text! != "\(Tools.getCurrency()) " && cell.priceLbl.text! != "" && cell.priceLbl.text! != "\(Tools.getCurrency())"
            {
                cell.title.frame.origin.y = 155
                cell.splPriceLbl.frame.origin.y = 195
                cell.priceLbl.isHidden = false
                cell.splPriceLbl.text = cell.priceLbl.text
                cell.priceLbl.text = "\(Tools.getCurrency()) \(price)"
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: cell.priceLbl.text!)
                attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                cell.priceLbl.attributedText = attributeString
            }
            else
            {
                cell.priceLbl.isHidden = true
                cell.title.frame.origin.y = 160
                cell.splPriceLbl.frame.origin.y = 203
            }
            
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20")
            
            var activityLoader = UIActivityIndicatorView()
            activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityLoader.center = cell.pinImage.center
            activityLoader.startAnimating()
            cell.pinImage.addSubview(activityLoader)
            
            cell.pinImage.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
                if image != nil {
                    activityLoader.stopAnimating()
                } else {
                    print("image not found")
                    cell.pinImage.image = UIImage(named: "no_image")
                    activityLoader.stopAnimating()
                }
            })
            
            cell.pinImage.contentMode = UIViewContentMode.scaleAspectFit
            
            let str2 = "\(String(describing: (self.relatedProductsArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "sku")!))"
            
            if self.checkProductInWishList(str2) == true {
                cell.imgFav.image = UIImage(named: "ic_favourite")
            }
            else
            {
                cell.imgFav.image = UIImage(named: "ic_unfavourite")
            }
            
            cell.addToFav.addTarget(self, action: #selector(ProductListVC.addToFav(_:)), for: UIControlEvents.touchUpInside)
            cell.addToFav.tag = (indexPath as NSIndexPath).row
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "optionValueCell", for: indexPath) as! OptionCollectionViewCell
            
            self.optionValuesArr = ((optionsArr.object(at: collectionView.tag) as AnyObject).value(forKey: "values") as! NSArray).mutableCopy() as! NSMutableArray
            
            cell.lblOption.layer.borderColor = UIColor.lightGray.cgColor
            
            cell.lblOption.text = "\((self.optionValuesArr.object(at : indexPath.row) as AnyObject).value(forKey: "title")!)"
            
            let id = "\((self.optionValuesArr.object(at : indexPath.row) as AnyObject).value(forKey: "option_type_id")!)"
            
            let attrID = "\((optionsArr.object(at: collectionView.tag) as AnyObject).value(forKey: "option_id")!)"
            
            for i in 0..<selectedOptionsArr.count
            {
                let optionId = "\((selectedOptionsArr.object(at: i) as AnyObject).value(forKey: "option_id")!)"
                
                if attrID == optionId
                {
                    let tempid =  "\((selectedOptionsArr.object(at: i) as AnyObject).value(forKey: "option_value")!)"
                    
                    if tempid == id
                    {
                        cell.lblOption.backgroundColor = themeColor
                        cell.lblOption.textColor = .white
                    }
                    else
                    {
                        cell.lblOption.backgroundColor = .white
                        cell.lblOption.textColor = .black
                    }
                }
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == self.collRelatedProducts
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
            viewController.productSku = "\(String(describing: (self.relatedProductsArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "sku")!))"
            viewController.productID = "\(String(describing: (self.relatedProductsArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "id")!))"
            self.present(viewController, animated: true, completion: nil)
        }
        else
        {
            let optionValues = (optionsArr.object(at: collectionView.tag) as AnyObject).value(forKey: "values") as AnyObject
            
            let tempDict = NSMutableDictionary()
            
            let selectedID = "\((optionValues.object(at : indexPath.row) as AnyObject).value(forKey: "option_type_id")!)"
            if selectedOptionsArr.count == 0
            {
                let attrID = "\((optionsArr.object(at: collectionView.tag) as AnyObject).value(forKey: "option_id")!)"
                
                tempDict.setObject(attrID, forKey: "option_id" as NSCopying)
                tempDict.setObject(selectedID, forKey: "option_value" as NSCopying)
                
                self.selectedOptionsArr.add(tempDict)
            }
            else
            {
                let attrID = "\((optionsArr.object(at: collectionView.tag) as AnyObject).value(forKey: "option_id")!)"
                
                var contains = false
                
                for i in 0..<selectedOptionsArr.count
                {
                    let optionId = "\((selectedOptionsArr.object(at: i) as AnyObject).value(forKey: "option_id")!)"
                    
                    if attrID == optionId
                    {
                        contains = true
                        
                        tempDict.setValue(attrID, forKey: "option_id")
                        tempDict.setValue(selectedID, forKey: "option_value")
                        
                        self.selectedOptionsArr.replaceObject(at: i, with: tempDict)
                    }
                }
                
                if !contains
                {
                    tempDict.setObject(attrID, forKey: "option_id" as NSCopying)
                    tempDict.setObject(selectedID, forKey: "option_value" as NSCopying)
                    
                    self.selectedOptionsArr.add(tempDict)
                }
            }
            collectionView.reloadData()
            
            print("selected : \(selectedOptionsArr)")
        }
    }
    
    @IBAction func addToWishListAction(_ sender: AnyObject) {
        
        if (UserDefaults.standard.object(forKey: "USER_ID") != nil) {
            self.wishlist()
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            viewController.delegate = self
            self.present(viewController, animated: true, completion: { () -> Void in
                //
            })
        }
    }
    
    func wishlist() {
        let isHave:Bool = self.checkProductInWishList("\(productDetailsDic.value(forKey: "sku")!)")
        if isHave {
            self.deleteWishlist(productId: self.productID)
            if UserDefaults.standard.object(forKey: kAddToWishlist) != nil {
                let data = UserDefaults.standard.object(forKey: kAddToWishlist) as! Data
                let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                
                let tempArr = NSMutableArray()
                tempArr.addObjects(from: favAry as! [Any])
                
                if isHave {
                    let str2 = productDetailsDic.value(forKey: "sku") as! String
                    
                    for i in 0..<tempArr.count {
                        let str1 = (tempArr[i] as AnyObject).value(forKey: "sku") as! String
                        
                        if str1 == str2 {
                            favAry.removeObject(at: i)
                            self.imgWishList.setImage(UIImage (named: "ic_unfavourite"), for: .normal)
                        }
                    }
                }
                let tempData = NSKeyedArchiver.archivedData(withRootObject: favAry)
                UserDefaults.standard.set(tempData, forKey: kAddToWishlist)
            }
            self.showToast(message: NSLocalizedString("Successfully removed from your wishlist", comment: ""))
        } else {
            self.addToWishlist(productId: self.productID)
            if UserDefaults.standard.object(forKey: kAddToWishlist) != nil {
                let data = UserDefaults.standard.object(forKey: kAddToWishlist) as! Data
                let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                
                let currentArr : NSDictionary  = self.productDetailsDic
                favAry.add(currentArr)
                
                let tempData = NSKeyedArchiver.archivedData(withRootObject: favAry)
                UserDefaults.standard.set(tempData, forKey: kAddToWishlist)
            } else {
                let currentArr : NSDictionary  = self.productDetailsDic
                let favAry = NSMutableArray()
                favAry.add(currentArr)
                
                let data1 = NSKeyedArchiver.archivedData(withRootObject: favAry)
                UserDefaults.standard.set(data1, forKey: kAddToWishlist)
            }
            self.imgWishList.setImage(UIImage (named: "ic_favourite"), for: .normal)
            self.showToast(message: NSLocalizedString("Successfully added to your wishlist", comment: ""))
        }
    }
    
    @IBAction func clickReviews(_ sender: AnyObject) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ReviewsVC") as! ReviewsVC
        viewController.revProductSku = productSku
        viewController.revProductID = productID
        print(productID)
        self.present(viewController, animated: true, completion: nil)
    }
    
    func addToWishlist(productId : String) {
        let request = AddWishlist(productId: productId)
        request.start()
    }
    
    func deleteWishlist(productId : String) {
        let request = DeleteWishlist(productId: productId)
        request.start()
    }
    
    func cartItemNotification(quoteId : String) {
        let request = CartItemNotification(quoteId: quoteId)
        request.start()
    }
    
    func openZoom() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ZoomViewController") as! ZoomViewController
        self.present(viewController, animated: true, completion:nil)
    }
    
    func checkStock() {
        let urlStr = "\(ConfigUrl.baseUrl)stockStatuses/\(productSku)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            let result = "\(String(describing: (responseObject.result.value! as AnyObject).value(forKey: "stock_status")!))"
                            print(responseObject.result.value!)
                            
                            if let value = (responseObject.result.value! as AnyObject).value(forKey: "qty") {
                                self.stockQuantity = Int("\(value)")!
                            }
                            
                            if result == "1" || result == "true" {
                                self.stockStatus = true
                                self.btnAddToCart.setTitle(NSLocalizedString("Add to Cart", comment: ""), for: .normal)
                            } else {
                                self.stockStatus = false
                                self.btnAddToCart.setTitle(NSLocalizedString("Out of Stock", comment: ""), for: .normal)
                            }
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func getRelatedProducts() {
        let urlStr = "\(ConfigUrl.baseUrl)products/\(productSku)/links/related"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer ljeygdxd6krq1uqxos1gk33568ivrgcx", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            print(responseObject.result.value!)
                            if (responseObject.result.value! as AnyObject).count != 0 {
                                let result = responseObject.result.value! as AnyObject
                                
                                var relatedSkuString = ""
                                
                                for i in 0..<result.count {
                                    let sku = "\((result.object(at: i) as AnyObject).value(forKey: "linked_product_sku")!)"
                                    relatedSkuString = "\(relatedSkuString),\(sku)"
                                }
                                let str = relatedSkuString.dropFirst()
                                print(str)
                                self.getRelatedProductsDetail(skuString: "\(str)")
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func getRelatedProductsDetail(skuString : String)
    {
        let urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=sku&searchCriteria[filterGroups][0][filters][0][value]=\(skuString)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=created_at& searchCriteria[sortOrders][0][direction]=ASC&searchCriteria[pageSize]=10&searchCriteria[currentPage]=1"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            if (responseObject.result.value! as AnyObject).count != 0
                            {
                                let result = responseObject.result.value! as AnyObject
                                
                                self.relatedProductsArr = (result.value(forKey: "items") as! NSArray).mutableCopy() as! NSMutableArray
                                
                                if self.relatedProductsArr.count != 0
                                {
                                    self.viewRelatedProducts.isHidden = false
                                    if self.productDetailsDic.count != 0 {
                                        self.setFrames()
                                    }
                                }
                                else
                                {
                                    self.viewRelatedProducts.isHidden = true
                                }
                                
                                print(self.relatedProductsArr)
                                
                                self.collRelatedProducts.reloadData()
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func getBrands() {
        let urlStr = "\(ConfigUrl.baseUrl)products/attributes/brand"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let result = responseObject.result.value! as AnyObject
                            
                            self.brandsArr = result.value(forKey: "options") as! NSArray
                            UserDefaults.standard.set(self.brandsArr, forKey: "BRANDS")
                            
                            /*let brandId = "(\(self.productDetailsDic.value(forKey: "brand")!))"
                             
                             for i in 0..<self.brandsArr.count
                             {
                             let id = "\((self.brandsArr.object(at: i) as AnyObject).value(forKey: "value")!)"
                             
                             if brandId == id
                             {
                             self.brandValue = "\((self.brandsArr.object(at: i) as AnyObject).value(forKey: "label")!)"
                             }
                             }
                             
                             self.titleLbl.text = "\(self.brandValue) \(self.productDetailsDic.value(forKey: "name")!) \(self.sizeValue) \(self.countValue)"*/
                        }
                        else
                        {
                            print(((responseObject.result.value) as AnyObject).value(forKeyPath: "message")!)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func getSize() {
        let urlStr = "\(ConfigUrl.baseUrl)products/attributes/size"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let result = responseObject.result.value! as AnyObject
                            
                            self.sizeArr = result.value(forKey: "options") as! NSArray
                            UserDefaults.standard.set(self.sizeArr, forKey: "SIZES")
                            
                            /*let sizeId = "(\(self.productDetailsDic.value(forKey: "size")!))"
                             
                             for i in 0..<self.sizeArr.count
                             {
                             let id = "\((self.sizeArr.object(at: i) as AnyObject).value(forKey: "value")!)"
                             
                             if sizeId == id
                             {
                             self.sizeValue = "\((self.sizeArr.object(at: i) as AnyObject).value(forKey: "label")!)"
                             }
                             }
                             
                             self.titleLbl.text = "\(self.brandValue) \(self.productDetailsDic.value(forKey: "name")!) \(self.sizeValue) \(self.countValue)"*/
                        } else {
                            print(((responseObject.result.value) as AnyObject).value(forKeyPath: "message")!)
                        }
                    }
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func getCount() {
        let urlStr = "\(ConfigUrl.baseUrl)products/attributes/count"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            let result = responseObject.result.value! as AnyObject
                            
                            self.countArr = result.value(forKey: "options") as! NSArray
                            UserDefaults.standard.set(self.countArr, forKey: "COUNTS")
                            
                            /* let countId = "(\(self.productDetailsDic.value(forKey: "count")!))"
                             
                             for i in 0..<self.countArr.count
                             {
                             let id = "\((self.countArr.object(at: i) as AnyObject).value(forKey: "value")!)"
                             
                             if countId == id
                             {
                             self.countValue = "\((self.countArr.object(at: i) as AnyObject).value(forKey: "label")!)"
                             }
                             }
                             
                             self.titleLbl.text = "\(self.brandValue) \(self.productDetailsDic.value(forKey: "name")!) \(self.sizeValue) \(self.countValue)"*/
                        } else {
                            print(((responseObject.result.value) as AnyObject).value(forKeyPath: "message")!)
                        }
                    }
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }

    }
    
    // MARK: TableView Delegates
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblOptions {
            return 1
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOptions {
            return optionsArr.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOptions {
            let cell:OptionTableViewCell = self.tblOptions.dequeueReusableCell(withIdentifier: "optionCell") as! OptionTableViewCell
            
            cell.lblOptionTitle.text = "\((self.optionsArr.object(at: indexPath.row) as AnyObject).value(forKey: "title")!)"
            
            cell.collOptions.dataSource = self
            cell.collOptions.delegate = self
            cell.collOptions.tag = indexPath.row
            cell.collOptions.reloadData()
            
            return cell
        } else {
            let cell:OptionTableViewCell = self.tblOptions.dequeueReusableCell(withIdentifier: "optionCell") as! OptionTableViewCell
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.tblOptions {
            return 0
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if tableView == self.tblOptions
        {
            return 99
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}
