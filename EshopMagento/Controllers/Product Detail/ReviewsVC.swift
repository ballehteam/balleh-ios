//
//  ReviewsVC.swift
//  Exlcart
//
//  Created by Tech Basics on 28/12/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

import UIKit
import FloatRatingView
import Alamofire

class ReviewsVC: UIViewController,loginIntimation, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var ratingStar: FloatRatingView!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var vwPostReview: UIView!
    @IBOutlet weak var txtNickName: UITextField!
    @IBOutlet weak var txtReview: UITextView!
    @IBOutlet weak var txtSummary: UITextField!
    @IBOutlet weak var tblReviews: UITableView!
    @IBOutlet weak var tblRatingStar: UITableView!
    @IBOutlet weak var lblReviewsCount: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var headerImage: UIImageView!
    
    var ratingTypeArr:NSArray = []
    var reviewsArr:NSArray = []
    var productName:String = ""
    var revProductID:String = ""
    var revProductSku:String = ""
    
    var postRatingData : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerImage.image = Tools.headerImage()
        btnTitle.addTarget(self, action: #selector(self.clickTitle), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(self.clickClose(sender:)), for: .touchUpInside)
        
        if languageID == "2"
        {
            imgBack.transform = CGAffineTransform(scaleX: -1, y: 1)
            txtReview.textAlignment = .right
            txtSummary.textAlignment = .right
            txtNickName.textAlignment = .right
        }
        else
        {
            txtReview.textAlignment = .left
            txtSummary.textAlignment = .left
            txtNickName.textAlignment = .left
        }
        getProductReviews()
        getReviewCodes()
        
        self.tblReviews.tableFooterView = UIView()
        self.tblRatingStar.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    @objc func clickClose(sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: {})
    }
    
    @objc func clickTitle()
    {
        
    }
    
    @IBAction func clickWriteReview(_ sender: Any)
    {
        self.vwPostReview.isHidden = false
    }
    
    @IBAction func clickPostReview(_ sender: Any) {
        vwPostReview.isHidden = true
        postRatingData.removeAllObjects()
        
        for i in 0..<self.ratingTypeArr.count {
            let data = NSMutableDictionary()
            
            data.setObject("4", forKey: "ratingValue" as NSCopying)
            data.setObject("\((ratingTypeArr.object(at: i) as AnyObject).value(forKey: "rating_code")!)", forKey: "ratingCode" as NSCopying)
            data.setObject("\((ratingTypeArr.object(at: i) as AnyObject).value(forKey: "rating_id")!)", forKey: "rating_id" as NSCopying)
            
            postRatingData.add(data)
        }
        
        if let id = UserDefaults.standard.string(forKey: "USER_ID"),
            id != "" {
            if let nickname = self.txtNickName.text?.trimmingCharacters(in: .whitespaces),
                !nickname.isEmpty,
                let summary = self.txtSummary.text?.trimmingCharacters(in: .whitespaces),
                !summary.isEmpty,
                let review = self.txtReview.text?.trimmingCharacters(in: .whitespaces),
                !review.isEmpty{
                
                let storeId: String = {
                    if languageID == "2" {
                        return "2"
                    } else {
                        return "3"
                    }
                }()
                
                let params = [
                    "productId" : revProductID,
                    "nickname" : self.txtNickName.text!,
                    "title" : self.txtSummary.text!,
                    "detail" : self.txtReview.text!,
                    "storeId" : storeId,
                    "ratingData" : postRatingData,
                    "deviceId": deviceId
                    ] as [String : Any]
                
                let urlStr = "\(ConfigUrl.baseUrl)review/mine/post"
                
                SharedManager.showHUD(viewController: self)
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.post.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                let setTemp: [String : Any] = params
                
                if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                    let jsonString = String(data: jsonData , encoding: .utf8)!
                    print(jsonString as Any)
                    request.httpBody = jsonData
                }
                
                self.makeReview(request: request)
                
            } else {
                self.present(Tools.alertWithOk(message: Texts.REQUIRED_FILED), animated: true, completion: nil)
            }
            
            
        } else {
            let alert = UIAlertController(title: "\(NSLocalizedString("Sorry", comment: ""))", message: "Only registered users can write review. Please Login or create an account", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                // instantiate your desired ViewController
                let viewController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                viewController.delegate = self
                self.present(viewController, animated: true, completion: { () -> Void in
                    //
                })
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func makeReview(request: URLRequest) {
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON { (responseObject) -> Void in
                
                if responseObject.result.isSuccess
                {
                    SharedManager.dismissHUD(viewController: self)
                    if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                        print(responseObject.result.value!)
                        
                        self.showToast(message: "\(NSLocalizedString("Thankyou for your Feedback", comment: ""))...!")
                    } else {
                        print(responseObject.result.value!)
                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                    }
                }
                
                if responseObject.result.isFailure {
                    SharedManager.dismissHUD(viewController: self)
                    let error : Error = responseObject.result.error!
                    print(error.localizedDescription)
                }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    @IBAction func clickCancelReview(_ sender: Any) {
        vwPostReview.isHidden = true
    }
    
    func loginSuccess()
    {
       // self.postReview()
    }
    
    func loginFailure()
    {
        //
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblReviews {
            return  reviewsArr.count
        } else {
            return  ratingTypeArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblReviews
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell") as! ReviewTableViewCell
            
            cell.lblName.text = "\((reviewsArr.object(at: indexPath.row) as AnyObject).value(forKey: "title")!) by \((reviewsArr.object(at: indexPath.row) as AnyObject).value(forKey: "nickname")!) on \((reviewsArr.object(at: indexPath.row) as AnyObject).value(forKey: "created_at")!)"
            cell.ratingStarView.rating = 4
            cell.lblReviewDescription.text = "\((reviewsArr.object(at: indexPath.row) as AnyObject).value(forKey: "detail")!)"
            
            let font = UIFont.appFontWith(size: 16)
            cell.lblReviewDescription.font = font
            cell.lblReviewDescription.numberOfLines = 0;
            // let text = cell.reviewCommentLbl.text! as NSString
            // let size = text.sizeWithAttributes([NSFontAttributeName:font])
            let height = ReviewsVC.heightForView(cell.lblReviewDescription.text!, font: font, width: cell.lblReviewDescription.frame.width)
            
            cell.lblReviewDescription.frame = CGRect(x: cell.lblReviewDescription.frame.origin.x, y: cell.lblReviewDescription.frame.origin.y, width: cell.lblReviewDescription.frame.size.width, height: height)
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ratingCell") as! ReviewTableViewCell
            
            cell.ratingCode.text = "\((ratingTypeArr.object(at: indexPath.row) as AnyObject).value(forKey: "rating_code")!)"
            cell.ratingCodeStarView.rating = 0
            cell.ratingCodeStarView.tag = indexPath.row
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       // print("didsele", terminator: "")
    }

    class func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblRatingStar
        {
            return 58
        }
        else
        {
            let commentStr = "\((reviewsArr.object(at: indexPath.row) as AnyObject).value(forKey: "detail")!)"
            
            let font = UIFont.appFontWith(size: 16)
            //  let size = commentStr!.sizeWithAttributes([NSFontAttributeName:font])
            let height = ReviewsVC.heightForView(commentStr, font: font, width: 304)
            
            return max(92, 92 + height)
        }
    }
    
    func getReviewCodes()
    {
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "\(ConfigUrl.baseUrl)rating/ratings/\(revProductID)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        //  request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let result = responseObject.result.value! as AnyObject
                            print(result)
                            
                            self.ratingTypeArr = result as! NSArray
                            self.tblRatingStar.reloadData()
                            
                            self.tblRatingStar.frame.size.height = self.tblRatingStar.contentSize.height
                            
                            var frame = self.tblRatingStar.frame
                            frame.size.height = CGFloat(self.ratingTypeArr.count * 58)
                            self.tblRatingStar.frame = frame
                            self.tblRatingStar.translatesAutoresizingMaskIntoConstraints = true
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                    SharedManager.dismissHUD(viewController: self)
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        
    }
    
    func getProductReviews()
    {
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "\(ConfigUrl.baseUrl)review/reviews/\(revProductID)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        //  request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let result = (responseObject.result.value! as AnyObject).object(at: 0) as AnyObject
                            print(result)
                            
                            let count = "\(result.value(forKey: "count")!)"
                            
                            if Int(count) != 0
                            {
                                
                                self.lblReviewsCount.text = "\(count) \(NSLocalizedString("Customer Reviews", comment: ""))"
                            }
                            else
                            {
                                self.lblReviewsCount.text = NSLocalizedString("Be the first to review this product", comment: "")
                            }
                            
                            self.reviewsArr = (result.value(forKey: "reviews") as! NSArray).mutableCopy() as! NSMutableArray
                            print(self.reviewsArr.count)
                            self.tblReviews.reloadData()
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                    SharedManager.dismissHUD(viewController: self)
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
}
