//
//  SubCategoryTableViewCell.swift
//  Exlcart
//
//  Created by Adyas Iinfotech on 13/06/17.
//  Copyright © 2017 adyas. All rights reserved.
//

import UIKit

class SubCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var productListCollectVw: UICollectionView!
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var subCategryTitle: UILabel!
    @IBOutlet weak var lblViewType: UILabel!
    @IBOutlet weak var lblExplore: UILabel!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var imgViewType: UIImageView!
    
    @IBOutlet weak var lblFilter: UILabel!
    @IBOutlet weak var lblSortby: UILabel!
    @IBOutlet weak var lblCategories: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
