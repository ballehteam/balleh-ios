//
//  CollectionViewCell.swift
//  PinterestLayout
//
//  Created by Shrikar Archak on 12/21/14.
//  Copyright (c) 2014 Shrikar Archak. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var pinImage: UIImageView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet var splPriceLbl: UILabel!
    
    @IBOutlet weak var addToFav: UIButton!
    
    @IBOutlet weak var removeBtn: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.contentView.autoresizingMask = [UIViewAutoresizing.flexibleRightMargin, UIViewAutoresizing.flexibleLeftMargin, UIViewAutoresizing.flexibleBottomMargin, UIViewAutoresizing.flexibleTopMargin]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true
        
    }
}



