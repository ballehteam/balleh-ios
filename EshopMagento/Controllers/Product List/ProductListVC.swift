//
//  ProductListVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 19/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire

var reuseIdentifier = "gridCell"

class ProductListVC: ParentViewController,UICollectionViewDataSource,UICollectionViewDelegate,loginIntimation, UICollectionViewDelegateFlowLayout
{

    var categoryID : String = ""
    
    @IBOutlet weak var productListCollectionView: UICollectionView!
    @IBOutlet var buttonsView: UIView!
    @IBOutlet var imgViewType: UIImageView!
    @IBOutlet var lblViewType: UILabel!
    
    var productListAry = [Product]()
    var filterAry:NSMutableArray = []
    var addFavTag:Int = 0
    var catTitle:String = String()
    var filterID:String = String()
    var page:Int = 1
    var pageCount = Double()
    var limit:String = "6"
    var productsCount:String = ""
    var viewType = String()
    
    var isSorted : Bool = false
    var sortPage:Int = 1
    var sortPageCount = Double()
    var setFramesCount:NSString = ""
    var sortKey : String = "created_at"
    var orderKey : String = "DESC"
    var isScrolledOnce : Bool = false

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        updateToken()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        setLeftNavigationButton()
        self.setRightNavigationButton()
        
        if let type = UserDefaults.standard.value(forKey: "VIEW_TYPE")
        {
            viewType = type as! String
        }
        else
        {
            viewType = "grid"
            lblViewType.text = NSLocalizedString("Grid View", comment: "")
        }
        
        if viewType == ""
        {
            viewType = "grid"
            lblViewType.text = NSLocalizedString("Grid View", comment: "")
        }
        
        if viewType == "list"
        {
            let nib = UINib(nibName: "ListCollectionViewCell", bundle: nil)
            self.productListCollectionView.register(nib, forCellWithReuseIdentifier: "listCell")
            imgViewType.image = UIImage (named: "listView.png")
            reuseIdentifier = "listCell"
            lblViewType.text = NSLocalizedString("List View", comment: "")
        }
        else if viewType == "full"
        {
            let nib = UINib(nibName: "FullCollectionViewCell", bundle: nil)
            self.productListCollectionView.register(nib, forCellWithReuseIdentifier: "fullViewCell")
            imgViewType.image = UIImage (named: "fullView.png")
            reuseIdentifier = "fullViewCell"
            lblViewType.text = NSLocalizedString("Full View", comment: "")
        }
        else
        {
            let nib = UINib(nibName: "GridCollectionViewCell", bundle: nil)
            self.productListCollectionView.register(nib, forCellWithReuseIdentifier: "gridCell")
            imgViewType.image = UIImage (named: "gridView.png")
            reuseIdentifier = "gridCell"
            lblViewType.text = NSLocalizedString("Grid View", comment: "")
        }
        
        self.title = catTitle
        self.updateCartBadge()
        
        self.tabBarController?.tabBar.isHidden = false
        
        self.delay(2, closure:
            {
                if let bool = UserDefaults.standard.string(forKey: "filter")
                {
                    print(bool)
                    if bool == "1"
                    {
                        self.page = 1
                        isFiltered = true
                    }
                }
                
                self.updatePage()
        })
    }
    
    func updatePage()
    {
        if isFiltered
        {
            buttonsView.isHidden = false
            
             SharedManager.showHUD(viewController: self)
            
            var urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(self.categoryID)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=\(sortKey)& searchCriteria[sortOrders][0][direction]=\(orderKey)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)"
            
            if isFiltered
            {
                urlStr = "\(urlStr)\(filterUrlStr)"
            }
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet() {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        SharedManager.dismissHUD(viewController: self)
                        
                        if responseObject.result.isSuccess
                        {
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items")
                                {
                                    if "\(result)" != "<null>"
                                    {
                                        var products = [Product]()
                                        
                                        if let dataResponse = result as? NSArray {
                                            for data in dataResponse {
                                                if let dataDic = data as? NSDictionary {
                                                    products.append(Product(data: dataDic))
                                                }
                                            }
                                        }
                                        
                                        self.productListAry = products
                                        
                                        if self.productListAry.count == 0
                                        {
                                            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                        }
                                        else
                                        {
                                            self.productsCount = "\(((responseObject.result.value!) as AnyObject).value(forKey: "total_count")!)"
                                            self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                                            self.productListCollectionView.reloadData()
                                            
                                            self.setFrames()
                                        }
                                    }
                                    else
                                    {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            if productListAry.count == 0
            {
                buttonsView.isHidden = false
                
                // SharedManager.showHUD(viewController: self)
                
                var urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(self.categoryID)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=\(sortKey)& searchCriteria[sortOrders][0][direction]=\(orderKey)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)"
                
                if isFiltered
                {
                    urlStr = "\(urlStr)\(filterUrlStr)"
                }
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.get.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON
                        { (responseObject) -> Void in
                            SharedManager.dismissHUD(viewController: self)
                            
                            if responseObject.result.isSuccess
                            {
                                if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                {
                                    if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items")
                                    {
                                        if "\(result)" != "<null>" {
                                            var products = [Product]()
                                            
                                            if let dataResponse = result as? NSArray {
                                                for data in dataResponse {
                                                    if let dataDic = data as? NSDictionary {
                                                        products.append(Product(data: dataDic))
                                                    }
                                                }
                                            }
                                            
                                            self.productListAry = products
                                            
                                            if self.productListAry.count == 0
                                            {
                                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                            }
                                            else
                                            {
                                                self.productsCount = "\(((responseObject.result.value!) as AnyObject).value(forKey: "total_count")!)"
                                                self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                                                self.productListCollectionView.reloadData()
                                                
                                                self.setFrames()
                                            }
                                        }
                                        else
                                        {
                                            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                        }
                                    }
                                    else
                                    {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                }
                            }
                            if responseObject.result.isFailure
                            {
                                let error : Error = responseObject.result.error!
                                print(error.localizedDescription)
                            }
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
            else
            {
                imgViewType.image = UIImage (named: "gridView.png")
                reuseIdentifier = "gridCell"
                viewType = "grid"
                lblViewType.text = NSLocalizedString("Grid View", comment: "")
                
                UserDefaults.standard.set("grid", forKey: "viewType")
                
                let nib = UINib(nibName: "GridCollectionViewCell", bundle: nil)
                self.productListCollectionView.register(nib, forCellWithReuseIdentifier: "gridCell")
                
                self.productListCollectionView.reloadData()
                
                self.productListCollectionView.frame.origin.y = 0
                self.productListCollectionView.frame.size.height = self.view.frame.size.height
            }
        }
        
        self.productListCollectionView.reloadData()
        self.setFrames()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.title = nil
        isSorted = false
        
        isFiltered = false
        UserDefaults.standard.removeObject(forKey: "filter")
    }
    
    func setFrames()
    {
       /* for (index,tempDic) in self.productListAry.enumerated()
        {
            let temp:NSMutableDictionary = tempDic as! NSMutableDictionary
            let priceWithOutSymbol:String = String((temp.object(forKey: "price") as! String).characters.dropFirst())
            let myNumber = self.formatCurrency(value: Double(priceWithOutSymbol)!)
            
            temp.setObject(myNumber, forKey: "price_sort" as NSCopying)
            self.productListAry.replaceObject(at: index, with: temp)
        }*/
        
        if viewType == "list"
        {
            let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            flowlayout.minimumInteritemSpacing = 1.0
            flowlayout.minimumLineSpacing = 1.0
            flowlayout.sectionInset.left = 1.0;
            flowlayout.sectionInset.right = 1.0;
            flowlayout.itemSize.width = (self.productListCollectionView.frame.size.width - 2)
            flowlayout.itemSize.height = 130;
            self.productListCollectionView .reloadData()
            self.productListCollectionView.collectionViewLayout = flowlayout
        }
            
        else if viewType == "full"
        {
            let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            flowlayout.minimumInteritemSpacing = 1.0
            flowlayout.minimumLineSpacing = 1.0
            flowlayout.sectionInset.left = 1.0;
            flowlayout.sectionInset.right = 1.0;
            flowlayout.itemSize.width = (self.productListCollectionView.frame.size.width - 2)
            flowlayout.itemSize.height = self.productListCollectionView.frame.size.width + 74;
            self.productListCollectionView .reloadData()
            self.productListCollectionView.collectionViewLayout = flowlayout
        }
        else
        {
            let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            flowlayout.minimumInteritemSpacing = 1.0
            flowlayout.minimumLineSpacing = 1.0
            flowlayout.sectionInset.left = 1.0;
            flowlayout.sectionInset.right = 1.0;
            flowlayout.itemSize.width = (self.productListCollectionView.frame.size.width - 3) / 2
            flowlayout.itemSize.height = 250;
            self.productListCollectionView .reloadData()
            self.productListCollectionView.collectionViewLayout = flowlayout
        }
    }
    
    //MARK: CollectionView Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productListAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let product = self.productListAry[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        cell.title.text = product.name
        
        var imageUrl = ""
        
        if let url = product.smallImage {
            imageUrl = url
        }
        
        if let title = product.title {
            cell.title.text = title
        }
        
        cell.splPriceLbl.text = product.price
        
        if let specialPrice = product.specialPrice {
            if viewType == "grid" {
                cell.title.frame.origin.y = 155
                cell.splPriceLbl.frame.origin.y = 195
            }
            
            cell.priceLbl.isHidden = false
            cell.splPriceLbl.text = specialPrice
            cell.priceLbl.text = product.price
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: cell.priceLbl.text!)
            attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            cell.priceLbl.attributedText = attributeString
        }
        else
        {
            cell.priceLbl.isHidden = true
            
            if viewType == "grid"
            {
                cell.title.frame.origin.y = 160
                cell.splPriceLbl.frame.origin.y = 203
            }
        }
        
        if viewType == "full"
        {
            if languageID == "2"
            {
                cell.splPriceLbl.textAlignment = .left
                cell.priceLbl.textAlignment = .left
            }
            else
            {
                cell.splPriceLbl.textAlignment = .right
                cell.priceLbl.textAlignment = .right
            }
        }
        
        if viewType == "list"
        {
            cell.splPriceLbl.sizeToFit()
            cell.priceLbl.sizeToFit()
            
            cell.priceLbl.frame.origin.x = cell.splPriceLbl.frame.origin.x + cell.splPriceLbl.frame.width + 5
        }
        
        let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20")
        
        var activityLoader = UIActivityIndicatorView()
        activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityLoader.center = cell.pinImage.center
        activityLoader.startAnimating()
        cell.pinImage.addSubview(activityLoader)
        
        cell.pinImage.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
            
            if image != nil
            {
                activityLoader.stopAnimating()
            }else
            {
                print("image not found")
                cell.pinImage.image = UIImage(named: "no_image")
                activityLoader.stopAnimating()
            }
        })
        
        cell.pinImage.contentMode = UIViewContentMode.scaleAspectFit
        
        let str2 = product.sku
        
        if self.checkProductInWishList(str2) == true {
            cell.imgFav.image = UIImage(named: "ic_favourite")
        } else {
            cell.imgFav.image = UIImage(named: "ic_unfavourite")
        }
        
        cell.addToFav.addTarget(self, action: #selector(ProductListVC.addToFav(_:)), for: UIControlEvents.touchUpInside)
        cell.addToFav.tag = (indexPath as NSIndexPath).row
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = self.productListAry[indexPath.row]
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        viewController.productSku = product.sku
        viewController.productID = "\(product.id)"
        self.present(viewController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProductDetailsSegue"
        {
           // let productDetailObj = segue.destination as! ProductDetailVC
         //   productDetailObj.productID = "\(String(describing: (self.productListAry.object(at: (sender! as AnyObject).row) as! NSDictionary).value(forKey: "id")))"
        }
        else
        {
            let Obj = segue.destination as! FilterVC
            Obj.filterAry = filterAry
        }
    }
    
    //MARK: Pagination
    func pullToRefresh()
    {
        if (self.isScrolledOnce == true)
        {
            return
        }
        self.isScrolledOnce = true
        
        if page != Int(self.pageCount) + 1 && !categoryID.isEmpty
        {
            page += 1
            var urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(self.categoryID)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=\(sortKey)& searchCriteria[sortOrders][0][direction]=\(orderKey)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)"
            
            if isFiltered
            {
                urlStr = "\(urlStr)\(filterUrlStr)"
            }
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet() {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items")
                                {
                                    if "\(result)" != "<null>"
                                    {
                                        var products = [Product]()
                                        
                                        if let dataResponse = result as? NSArray {
                                            for data in dataResponse {
                                                if let dataDic = data as? NSDictionary {
                                                    products.append(Product(data: dataDic))
                                                }
                                            }
                                        }
                                        
                                        self.productListAry.append(contentsOf: products)
                                        
                                        //  self.setFrames()
                                        self.productListCollectionView.reloadData()
                                    }
                                    else
                                    {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                        self.isScrolledOnce = false
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            SharedManager.dismissHUD(viewController: self)
            self.isScrolledOnce = false
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if scrollView == self.productListCollectionView
        {
            let offset: CGPoint = scrollView.contentOffset
            let bounds: CGRect = scrollView.bounds
            let size: CGSize = scrollView.contentSize
            let inset: UIEdgeInsets = scrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 10
            if y > h + reload_distance
            {
                if isScrolledOnce == false
                {
                    self.pullToRefresh()
                }
            }
        }
    }
    
    @objc func addToFav(_ sender : UIButton) {
        if(UserDefaults.standard.object(forKey: "USER_ID") != nil) {
            self.wishlist(index: sender.tag)
        } else {
            addFavTag = sender.tag
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            viewController.delegate = self
            self.present(viewController, animated: true, completion: { () -> Void in
            })
        }
    }
    
    func loginSuccess() {
        self.wishlist(index: addFavTag)
    }
    
    func wishlist(index: Int) {
        let product = self.productListAry[index]
        let isHave:Bool = self.checkProductInWishList("\(product.sku)")
        
        if isHave {
            self.deleteWishlist(productId: "\(product.id)")
            Tools.removeFromWishList(sku: product.sku)
            self.showToast(message: NSLocalizedString("Successfully removed from your wishlist", comment: ""))
        } else {
            self.addToWishlist(productId: "\(product.id)")
            Tools.addToWishList(product: product)
            self.showToast(message: NSLocalizedString("Successfully added to your wishlist", comment: ""))
        }
        self.productListCollectionView.reloadData()
    }
    
    func addToWishlist(productId : String) {
        let request = AddWishlist(productId: productId)
        request.start()
    }
    
    func deleteWishlist(productId : String) {
        let request = DeleteWishlist(productId: productId)
        request.start()
    }
    
    func loginFailure() {
        
    }
    
    //MARK: Sorting
    @IBAction func sortAction(_ sender: AnyObject)
    {
        let picker = BSModalPickerView.init(values: sortItems())
        
        picker?.present(in: self.view, with: {(_ madeChoice: Bool) -> Void in
            if madeChoice {
                
                switch(Int((picker?.selectedIndex)!))
                {
                case 0:
                    self.sortKey = "name"
                    self.orderKey = "ASC"
                    break
                case 1:
                    self.sortKey = "name"
                    self.orderKey = "DESC"
                    break
                case 2:
                    self.sortKey = "price"
                    self.orderKey = "ASC"
                    break
                case 3:
                    self.sortKey = "price"
                    self.orderKey = "DESC"
                    break
                case 5:
                    self.sortKey = "position"
                    self.orderKey = "ASC"
                    break
                case 6:
                    self.sortKey = "position"
                    self.orderKey = "DESC"
                    break
                default:
                    self.sortKey = "created_at"
                    self.orderKey = "DESC"
                    break
                    
                }
                self.sorting()
            }
            else
            {
                
            }
        })
    }
    
    func sorting()
    {
        var urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(self.categoryID)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=\(sortKey)& searchCriteria[sortOrders][0][direction]=\(orderKey)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)"
        
        if isFiltered
        {
            urlStr = "\(urlStr)\(filterUrlStr)"
        }
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items")
                            {
                                if "\(result)" != "<null>"
                                {
                                    var products = [Product]()
                                    
                                    if let dataResponse = result as? NSArray {
                                        for data in dataResponse {
                                            if let dataDic = data as? NSDictionary {
                                                products.append(Product(data: dataDic))
                                            }
                                        }
                                    }
                                    
                                    self.productListAry = products
                                    
                                    if self.productListAry.count == 0
                                    {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                    }
                                    else
                                    {
                                        self.productsCount = "\(((responseObject.result.value!) as AnyObject).value(forKey: "total_count")!)"
                                        self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                                        self.productListCollectionView.reloadData()
                                        self.page = 1
                                        self.setFrames()
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }

    }
    
    //MARK: Filter
    @IBAction func filterAction(_ sender: AnyObject)
    {
        if productListAry.count != 0
        {
            let VC = self.storyboard!.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
            VC.catgryID = self.categoryID
            self.present(VC, animated: true, completion: nil)
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("Filter is not Available", comment: ""), viewController: self)        
        }
    }
    
    func reviewsBtnClick(_ sender: UIButton) {
        let product = self.productListAry[sender.tag]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // instantiate your desired ViewController
        let VC = storyboard.instantiateViewController(withIdentifier: "ReviewSegue") as! ReviewsVC
     //   VC.reviewsAry = (self.productListAry.object(at: (index as NSIndexPath).row) as AnyObject).value(forKey: "reviews_list") as! NSArray
        
        if let title = product.title {
            VC.productName = title
        }
        
        
        VC.revProductID = "\(product.id)"
        self.navigationController!.pushViewController(VC, animated: true)
    }
    
    @IBAction func clickChangeView(_ sender: Any)
    {
        if viewType == "grid"
        {
            reuseIdentifier = "listCell"
            viewType = "list"
            imgViewType.image = UIImage (named: "listView.png")
            
            let nib = UINib(nibName: "ListCollectionViewCell", bundle: nil)
            self.productListCollectionView.register(nib, forCellWithReuseIdentifier: "listCell")
            lblViewType.text = NSLocalizedString("List View", comment: "")
            
            UserDefaults.standard.set("list", forKey: "viewType")
            
            self.productListCollectionView.reloadData()
            setFrames()
        }
        else if viewType == "list"
        {
            reuseIdentifier = "fullViewCell"
            viewType = "full"
            imgViewType.image = UIImage (named: "fullView.png")
            lblViewType.text = NSLocalizedString("Full View", comment: "")
            
            let nib = UINib(nibName: "FullCollectionViewCell", bundle: nil)
            self.productListCollectionView.register(nib, forCellWithReuseIdentifier: "fullViewCell")
            
            UserDefaults.standard.set("full", forKey: "viewType")
            
            self.productListCollectionView.reloadData()
            setFrames()
        }
        else
        {
            reuseIdentifier = "gridCell"
            viewType = "grid"
            imgViewType.image = UIImage (named: "gridView.png")
            lblViewType.text = NSLocalizedString("Grid View", comment: "")
            
            UserDefaults.standard.set("grid", forKey: "viewType")
            
            let nib = UINib(nibName: "GridCollectionViewCell", bundle: nil)
            self.productListCollectionView.register(nib, forCellWithReuseIdentifier: "gridCell")
            
            self.productListCollectionView.reloadData()
            setFrames()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
