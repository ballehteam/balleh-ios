//
//  TrackOrderVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 20/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit

class TrackOrderVC: ParentViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setLeftNavigationButton()
        let cartCount = "\(String(describing: UserDefaults.standard.string(forKey: kAddToCart)))"
        self.setRightNavigationButton()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
