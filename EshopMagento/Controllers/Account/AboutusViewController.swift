//
//  AboutusViewController.swift
//  EshopMagento
//
//  Created by Abirami Bala on 09/04/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit

class AboutusViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    var urlToLoad : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        if urlToLoad == "" {
            urlToLoad = "https://balleh.com/en/about-us"
        }
        
        SharedManager.showHUD(viewController: self)
        
        webView.loadRequest(URLRequest(url: URL(string: urlToLoad)!))
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SharedManager.dismissHUD(viewController: self)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
