//
//  AddressListVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 20/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire
import Intercom

class AddressListVC: ParentViewController, UITabBarControllerDelegate  {
    
    @IBOutlet weak var editAcView: UIView!
    @IBOutlet weak var myAddressTable: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var streetTxt: UITextField!
    @IBOutlet weak var zipCodeTxt: UITextField!
    @IBOutlet weak var phoneNumTxt: UITextField!
    @IBOutlet weak var editAcDone: UIButton!
    @IBOutlet var lblAddressType: UILabel!
    @IBOutlet var btnDefaultBilling: UIButton!
    @IBOutlet var btnDefaultShipping: UIButton!
    
    var shippingAddressArr = NSMutableArray()
    var billingAddressArr = NSMutableArray()
    
    var countryListAry = NSArray()
    var cityArr = NSArray()
    var typeStr: String = ""
    var addressId: String = ""
    var userDetails = NSDictionary()
    
    var myAddressArr:NSMutableArray = NSMutableArray()
    var allAddress:NSMutableArray = NSMutableArray()
    
    var isDefaultShipping = "0"
    var isDefaultBilling = "0"
    
    var picker: UIPickerView!
    
    var selectedIndex = Int()
    var pickerType = String()
    
    var selectedIndexPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        
        self.cityTxt.inputView = picker
        self.cityTxt.delegate = self
        
        setLeftNavigationButton()
        self.setRightNavigationButton()
        expandedSection.removeAllObjects()
        self.title = NSLocalizedString("My Address", comment: "")
        
        self.editAcView.isHidden = true
        
        if languageID == "2"
        {
            self.firstNameTxt.textAlignment = .right
            self.lastNameTxt.textAlignment = .right
            self.streetTxt.textAlignment = .right
            self.cityTxt.textAlignment = .right
            self.zipCodeTxt.textAlignment = .right
            self.countryTxt.textAlignment = .right
            self.stateTxt.textAlignment = .right
        }
        else
        {
            self.firstNameTxt.textAlignment = .left
            self.lastNameTxt.textAlignment = .left
            self.streetTxt.textAlignment = .left
            self.cityTxt.textAlignment = .left
            self.zipCodeTxt.textAlignment = .left
            self.countryTxt.textAlignment = .left
            self.stateTxt.textAlignment = .left
        }
        
        scrollView.contentSize = CGSize(width: self.view.bounds.width - 6, height: 800)
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.refreshList(notification:)), name: NSNotification.Name(rawValue: "getName"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if (UserDefaults.standard.object(forKey: "USER_ID") != nil)
        {
            userIDStr = UserDefaults.standard.object(forKey: "USER_ID") as! String
        }
        
        let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
        userDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
        
        let address = userDetails.value(forKey: "addresses") as! NSArray
        print(address)
        
        self.shippingAddressArr.removeAllObjects()
        self.billingAddressArr.removeAllObjects()
        self.myAddressArr.removeAllObjects()
        
        for i in 0..<address.count
        {
            var defaultShipAddress = "0"
            var defaultBillAddress = "0"
            if let bool = (address.object(at: i) as AnyObject).value(forKey: "default_shipping")
            {
                defaultShipAddress = "\(bool)"
            }
            if let bool = (address.object(at: i) as AnyObject).value(forKey: "default_billing")
            {
                defaultBillAddress = "\(bool)"
            }
            
            if defaultShipAddress == "1"
            {
                shippingAddressArr.add(address.object(at: i))
            }
            if defaultBillAddress == "1"
            {
                billingAddressArr.add(address.object(at: i))
            }
        }
        
        for i in 0..<address.count
        {
            var defaultShipAddress = "0"
            var defaultBillAddress = "0"
            if let bool = (address.object(at: i) as AnyObject).value(forKey: "default_shipping")
            {
                defaultShipAddress = "\(bool)"
            }
            if let bool = (address.object(at: i) as AnyObject).value(forKey: "default_billing")
            {
                defaultBillAddress = "\(bool)"
            }
            
            if defaultShipAddress == "0" && defaultBillAddress == "0"
            {
                self.myAddressArr.add(address.object(at: i))
            }
        }
        
        allAddress = address.mutableCopy() as! NSMutableArray
        
      //  self.myAddressArr = address.mutableCopy() as! NSMutableArray
        
        if self.allAddress.count != 0
        {
            self.myAddressTable.isHidden = false
            self.myAddressTable.reloadData()
        }
        else
        {
            self.myAddressTable.isHidden = true
            
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your address list is Empty!", comment: ""))\n\(NSLocalizedString("Add a new one", comment: ""))", viewController: self)
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == mainTableView
        {
            return self.mainTblArray.count
        }
        else if tableView == myAddressTable
        {
            var count = 0
            
            if shippingAddressArr.count != 0
            {
                count = count + 1
            }
            if billingAddressArr.count != 0
            {
                count = count + 1
            }
            if myAddressArr.count != 0
            {
                count = count + 1
            }
            
            return count
        }
        else
        {
            if categoryDict.count != 0
            {
                return categoryDict.count
            }
            else
            {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == mainTableView
        {
            return mainTblArray[section].count
        }
        else if tableView == myAddressTable
        {
            if billingAddressArr.count != 0 && shippingAddressArr.count != 0
            {
                if section == 0
                {
                    return billingAddressArr.count
                }
                else if section == 1
                {
                    return shippingAddressArr.count
                }
                else
                {
                    return self.myAddressArr.count
                }
            }
            else if billingAddressArr.count == 0 && shippingAddressArr.count == 0
            {
                return myAddressArr.count
            }
            else
            {
                if billingAddressArr.count != 0
                {
                    if section == 0
                    {
                        return billingAddressArr.count
                    }
                    else
                    {
                        return self.myAddressArr.count
                    }
                }
                else
                {
                    if section == 0
                    {
                        return shippingAddressArr.count
                    }
                    else
                    {
                        return self.myAddressArr.count
                    }
                }
            }
        }
        else
        {
            if categoryDict.count != 0
            {
                return ((categoryDict[section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count
            }
            else
            {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView == mainTableView
        {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 18))
            let label = UILabel(frame: CGRect(x: 10, y: 5, width: tableView.frame.size.width - 20, height: 25))
            label.font = UIFont.appFontWith(size: 15)
            label.textColor = UIColor.white
            let string = "\(titleArr[section])"
            label.text = string
            view.addSubview(label)
            view.backgroundColor = UIColor(red: 91.0 / 255.0, green: 91.0 / 255.0, blue: 91.0 / 255.0, alpha: 1.0)
            return view
            
            /* let view = DetailCell ()
             view.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 44)
             view.backgroundColor = .green
             // view.imgArrow.image = UIImage (named: "left-arrow.png")
             // view.title.text = "Main category"
             // view.imgIcon.image = UIImage (named: "ic_favorite.png")
             
             return view*/
        }
        else if tableView == myAddressTable
        {
            let CellIdentifier: String = "sectionHeader"
            let headerView: AddressTableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as! AddressTableViewCell?
            
            if billingAddressArr.count != 0 && shippingAddressArr.count != 0
            {
                if section == 0
                {
                    headerView?.lblAddressType.text = NSLocalizedString("Billing Address", comment: "")
                    
                    if languageID == "1"
                    {
                        lblAddressType.textAlignment = .left
                    }
                    else
                    {
                        lblAddressType.textAlignment = .right
                    }
                    
                    
                }
                else if section == 1
                {
                    headerView?.lblAddressType.text = NSLocalizedString("Shipping Address", comment: "")
                    
                    if languageID == "1"
                    {
                        lblAddressType.textAlignment = .left
                    }
                    else
                    {
                        lblAddressType.textAlignment = .right
                    }
                }
                else
                {
                    headerView?.lblAddressType.text = NSLocalizedString("Additional Addresses", comment: "")
                    
                    if languageID == "1"
                    {
                        lblAddressType.textAlignment = .left
                    }
                    else
                    {
                        lblAddressType.textAlignment = .right
                    }
                }
            }
            else if billingAddressArr.count == 0 && shippingAddressArr.count == 0
            {
                headerView?.lblAddressType.text = NSLocalizedString("Additional Addresses", comment: "")
            }
            else
            {
                if billingAddressArr.count != 0
                {
                    if section == 0
                    {
                        headerView?.lblAddressType.text = NSLocalizedString("Billing Address", comment: "")
                    }
                    else
                    {
                        headerView?.lblAddressType.text = NSLocalizedString("Additional Addresses", comment: "")
                    }
                }
                else
                {
                    if section == 0
                    {
                        headerView?.lblAddressType.text = NSLocalizedString("Shipping Address", comment: "")
                    }
                    else
                    {
                        headerView?.lblAddressType.text = NSLocalizedString("Additional Addresses", comment: "")
                    }
                }
            }
            
            if headerView == nil
            {
                print("No cells with matching CellIdentifier loaded from your storyboard")
                
                //   NSException.raise("headerView == nil..", format: "No cells with matching CellIdentifier loaded from your storyboard")
            }
            return headerView!
        }
        else
        {
            let sectionView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 40))
            sectionView.backgroundColor = .white
            
            sectionView.tag = section
            let viewLabel: UILabel = UILabel(frame: CGRect(x: 15, y: 0, width: mainTableView.frame.size.width - 10, height: 40))
            viewLabel.backgroundColor = UIColor.clear
            viewLabel.textColor = UIColor.black
            viewLabel.font = UIFont.appFontWith(size: 15)
            viewLabel.text =  (categoryDict.object(at: section) as AnyObject).value(forKey: "name") as? String
            sectionView.addSubview(viewLabel)
            
            let line: UILabel = UILabel(frame: CGRect(x: 5, y: 49, width: mainTableView.frame.size.width - 10, height: 1))
            line.backgroundColor = UIColor.lightGray
            sectionView.addSubview(line)
            
            catID.add("\((categoryDict.object(at: section) as AnyObject).value(forKey: "id")!)")
            let arrowImg = UIImageView(frame: CGRect(x: mainTableView.frame.size.width - 40, y: 12, width: 16, height: 17))
            arrowImg.contentMode = .scaleAspectFit
            let bool = (categoryDict[section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool
            if bool == false
            {
                arrowImg.image = UIImage(named: "remove")
                if languageID == "1"
                {
                    arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 18, height: 15)
                }
                else
                {
                    arrowImg.frame = CGRect(x: 10, y: 12, width: 18, height: 15)
                }
                arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 18, height: 15)
            }
            else
            {
                arrowImg.image = UIImage(named: "add")
                if languageID == "1"
                {
                    arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 18, height: 15)
                }
                else
                {
                    arrowImg.frame = CGRect(x: 10, y: 12, width: 18, height: 15)
                }
            }
            
            sectionView.addSubview(arrowImg)
            /********** Add a custom Separator with Section view *******************/
            let separatorLineView: UIView = UIView(frame: CGRect(x: 0, y: 40, width: mainTableView.frame.size.width, height: 0.3))
            separatorLineView.backgroundColor = UIColor.lightGray
            //separatorLineView.layer.borderWidth = 0.5
            // sectionView.addSubview(separatorLineView)
            /********** Add UITapGestureRecognizer to SectionView   **************/
            let headerTapped: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(self.sectionHeaderTapped(_:)))
            sectionView.addGestureRecognizer(headerTapped)
            
            sectionView.frame.size.height = 40
            
            return sectionView
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == mainTableView
        {
            if indexPath.section != 1
            {
                if indexPath.section == 0
                {
                    if indexPath.row != 0
                    {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                        
                        /* if cell1 == nil {
                         var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                         for view: UIView in views {
                         if (view is UITableViewCell) {
                         cell1 = view as? DetailCell
                         }
                         }
                         }*/
                        
                        let subArr = mainTblArray[indexPath.section]
                        cell1.title.text = subArr[indexPath.row]
                        
                        let subArr2 = iconsArr[indexPath.section]
                        cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                        if languageID == "1"
                        {
                            cell1.imgArrow.image = UIImage(named: "left-arrow")
                        }
                        else
                        {
                            cell1.imgArrow.image = UIImage(named: "right-arrow1")
                        }
                        cell1.imgArrow.contentMode = .scaleAspectFit
                        cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                        cell1.imgIcon.tintColor = UIColor.gray
                        cell1.selectionStyle = .none
                        
                        return cell1
                    }
                    else
                    {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as! LanguageCell
                        
                        if languageID == "1"
                        {
                            //selectedSegmentIndex = 0
                            cell1.segmentView.selectedSegmentIndex = 0
                        }
                        else
                        {
                            cell1.segmentView.selectedSegmentIndex = 1
                        }
                        
                        cell1.segmentView.addTarget(self, action: #selector(ParentViewController.clickLang(_:)), for: UIControlEvents.valueChanged)
                        
                        return cell1
                    }
                }
                else
                {
                    let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                    
                    /* if cell1 == nil {
                     var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                     for view: UIView in views {
                     if (view is UITableViewCell) {
                     cell1 = view as? DetailCell
                     }
                     }
                     }*/
                    
                    let subArr = mainTblArray[indexPath.section]
                    cell1.title.text = subArr[indexPath.row]
                    
                    let subArr2 = iconsArr[indexPath.section]
                    cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                    if languageID == "1"
                    {
                        cell1.imgArrow.image = UIImage(named: "left-arrow")
                    }
                    else
                    {
                        cell1.imgArrow.image = UIImage(named: "right-arrow1")
                    }
                    cell1.imgArrow.contentMode = .scaleAspectFit
                    cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                    cell1.imgIcon.tintColor = UIColor.gray
                    cell1.selectionStyle = .none
                    
                    return cell1
                }
            }
            else
            {
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "CatCell") as! categoryTCell
                cell1.insideTableView.isScrollEnabled = false
                cell1.insideTableView.dataSource = self
                cell1.insideTableView.delegate = self
                cell1.insideTableView.reloadData()
                cell1.selectionStyle = .none
                
                return cell1
            }
        }
        else if tableView == myAddressTable
        {
            let cell:AddressTableViewCell = self.myAddressTable.dequeueReusableCell(withIdentifier: "addressCell") as! AddressTableViewCell
            
            var valuesArr = NSMutableArray()
            
            if billingAddressArr.count != 0 && shippingAddressArr.count != 0
            {
                if indexPath.section == 0
                {
                    valuesArr = billingAddressArr
                }
                else if indexPath.section == 1
                {
                    valuesArr = shippingAddressArr
                }
                else
                {
                    valuesArr = myAddressArr
                }
            }
            else if billingAddressArr.count == 0 && shippingAddressArr.count == 0
            {
                valuesArr = myAddressArr
            }
            else
            {
                if billingAddressArr.count != 0
                {
                    if indexPath.section == 0
                    {
                        valuesArr = billingAddressArr
                    }
                    else
                    {
                        valuesArr = myAddressArr
                    }
                }
                else
                {
                    if indexPath.section == 0
                    {
                        valuesArr = shippingAddressArr
                    }
                    else
                    {
                        valuesArr = myAddressArr
                    }
                }
            }
            
            let name = NSString(format: "%@ %@",((valuesArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "firstname") as! String), ((valuesArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "lastname") as! String))
            cell.lblName.text =  name as String
            
            cell.lblAddress.text = ((valuesArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "street") as AnyObject).object(at: 0) as? String
            
            cell.lblCity.text = (valuesArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "city") as? String
            
            cell.lblState.text = (valuesArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "postcode") as? String
            cell.lblCountry.text = (valuesArr.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "country_id") as? String
            
            cell.btnDelete.addTarget(self, action: #selector(self.deleteAction(_:)), for: UIControlEvents.touchUpInside)
            cell.btnDelete.tag = (indexPath as NSIndexPath).row
            cell.btnEdit.addTarget(self, action: #selector(self.editAction(_:)), for: UIControlEvents.touchUpInside)
            cell.btnEdit.tag = (indexPath as NSIndexPath).row
            
            return cell
        }
        else
        {
            var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if cell == nil
            {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            }
            
            cell?.textLabel?.font = UIFont.appFontWith(size: 15)
            if languageID == "2"
            {
                cell?.textLabel?.textAlignment = .right
            }
            cell?.textLabel?.text = (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "name") as? String
            cell?.selectionStyle = .none
            
            return cell!
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == mainTableView
        {
            if indexPath.section != 1
            {
                return 44
            }
            else
            {
                if categoryDict.count != 0
                {
                    var collapsed = false
                    
                    var expandedRowHeight = 0
                    
                    for i in 0..<expandedSection.count
                    {
                        let index = Int("\(expandedSection[i])")!
                        
                        expandedRowHeight = expandedRowHeight + (((categoryDict[index] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count * 40)
                    }
                    
                    return CGFloat(expandedRowHeight) + CGFloat(categoryDict.count * 44)
                }
                else
                {
                    return UITableViewAutomaticDimension
                }
            }
        }
        else if tableView == myAddressTable
        {
            return 122
        }
        else
        {
            let collapsed = ((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool)
            
            if collapsed
            {
                return 0
            }
            else
            {
                return 40
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == mainTableView
        {
            if section == 0
            {
                return 0
            }
            else
            {
                return 35
            }
        }
        else if tableView == myAddressTable
        {
            return 29
        }
        else
        {
            return 44
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == mainTableView
        {
            let subArr = mainTblArray[indexPath.section]

            let action = subArr[indexPath.row]
            
            if action == NSLocalizedString("Home", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Login", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.present(viewController, animated: true, completion: nil)
            }
            else if action == NSLocalizedString("Deals", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Live Chat", comment: "")
            {
                Intercom.presentMessenger()
            }
            else if action == NSLocalizedString("Track Order", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Share", comment: "")
            {
                let textToShare = [ Tools.shareText ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
        else if tableView == myAddressTable
        {
            
        }
        else
        {
            let id = "\(String(describing: (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "id")!))"
            let name = "\(String(describing: (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "name")!))"
            
            print("Category ID: \(id)")
            categoryID = id
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            viewController.categoryID = id
            viewController.isFromHome = true
            viewController.catTitle = name
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func deleteAction(_ sender : UIButton) {
      //  let index:IndexPath = IndexPath(row: sender.tag, section: 0)
      //  let defaultAddId =  ((self.myAddressArr.object(at: index.row) as AnyObject).value(forKeyPath: "default_address_id") as AnyObject) as! String
        
        let selectedAddId = "\((self.myAddressArr.object(at: sender.tag) as AnyObject).value(forKeyPath: "id")!)"
        
        let alt: UIAlertController = UIAlertController(title: "Information", message: "Are you sure, Do want to delete this Address?", preferredStyle: UIAlertControllerStyle.alert)
        alt.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            
            let addresses = NSMutableArray()
            addresses.addObjects(from: self.allAddress as! [Any])
            
            for i in 0..<addresses.count
            {
                let id = "\((addresses.object(at: i) as AnyObject).value(forKey: "id")!)"
                
                if selectedAddId == id
                {
                    self.allAddress.removeObject(at: i)
                }
            }
            
            let customer = [
                "firstname": self.userDetails.value(forKey: "firstname") as! String,
                "lastname": self.userDetails.value(forKey: "lastname") as! String,
                "website_id": 1,
                "email": self.userDetails.value(forKey: "email") as! String,
                "addresses": self.allAddress,
                ] as [String : Any]
            //"state_id": self.stateTxt.text!,
            let params = ["customer": customer] as [String : Any]
            
            let urlStr = "\(ConfigUrl.baseUrl)customers/me"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.put.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)
                print(jsonString! as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            self.refreshAddress()
                            SharedManager.showAlertWithMessage(title: "", alertMessage: "\(NSLocalizedString("Address deleted", comment: ""))", viewController: self)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }))
        
        alt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) -> Void in
            
            alt.dismiss(animated: true)
        }))
        self.present(alt, animated: true, completion: nil)
    }
    
    @objc func editAction(_ sender : UIButton)
    {
        let touchPoint = sender.convert(CGPoint.zero, to: myAddressTable)
        let indexPath = myAddressTable.indexPathForRow(at: touchPoint)! as IndexPath
        
        self.getCountryListNew()
        typeStr = "edit"
        
        self.lblAddressType.text = NSLocalizedString("EDIT ADDRESS", comment: "")
        self.editAcView.isHidden = false
        
      //  let indexPath = selectedIndexPath
        
        if indexPath.section == 0
        {
            if(self.billingAddressArr.count != 0)
            {
                self.firstNameTxt.text = "\((self.billingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "firstname")!)"
                self.lastNameTxt.text = "\((self.billingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "lastname")!)"
                self.cityTxt.text = "\((self.billingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "city")!)"
                self.zipCodeTxt.text = "\((self.billingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "postcode")!)"
                self.countryTxt.text = "\((self.billingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "country_id")!)"
                self.stateTxt.text = "\((self.billingAddressArr.object(at: indexPath.row) as AnyObject).value(forKeyPath: "region.region")!)"
                self.streetTxt.text = "\(((self.billingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "street") as AnyObject).object(at: 0))"
                self.phoneNumTxt.text = "\((self.billingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "telephone")!)"
                addressId = "\((self.billingAddressArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "id")!)"
                countryID = "\((self.billingAddressArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "country_id")!)" as NSString
                
                isDefaultBilling = "1"
                btnDefaultBilling.setImage(UIImage (named: "ic_check_box"), for: .normal)
                
                if let bool = (self.billingAddressArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "default_shipping")
                {
                    isDefaultShipping = "\(bool)"
                    
                    if isDefaultShipping == "1"
                    {
                        btnDefaultShipping.setImage(UIImage (named: "ic_check_box"), for: .normal)
                    }
                    else
                    {
                        btnDefaultShipping.setImage(UIImage (named: "ic_uncheck_box"), for: .normal)
                    }
                }
                else
                {
                    btnDefaultShipping.setImage(UIImage (named: "ic_uncheck_box"), for: .normal)
                }
              //  stateID = (self.billingAddressArr.object(at: sender.tag) as! NSDictionary).value(forKey: "zone_id") as! NSString
            }
        }
        else if indexPath.section == 1
        {
            if(self.shippingAddressArr.count != 0)
            {
                self.firstNameTxt.text = "\((self.shippingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "firstname")!)"
                self.lastNameTxt.text = "\((self.shippingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "lastname")!)"
                self.cityTxt.text = "\((self.shippingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "city")!)"
                self.zipCodeTxt.text = "\((self.shippingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "postcode")!)"
                self.countryTxt.text = "\((self.shippingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "country_id")!)"
                self.stateTxt.text = "\((self.shippingAddressArr.object(at: indexPath.row) as AnyObject).value(forKeyPath: "region.region")!)"
                self.streetTxt.text = "\(((self.shippingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "street") as AnyObject).object(at: 0))"
                self.phoneNumTxt.text = "\((self.shippingAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "telephone")!)"
                addressId = "\((self.shippingAddressArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "id")!)"
                countryID = "\((self.shippingAddressArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "country_id")!)" as NSString
                
                isDefaultShipping = "1"
                btnDefaultShipping.setImage(UIImage (named: "ic_check_box"), for: .normal)
                
                if let bool = (self.shippingAddressArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "default_billing")
                {
                    isDefaultBilling = "\(bool)"
                    
                    if isDefaultBilling == "1"
                    {
                        btnDefaultBilling.setImage(UIImage (named: "ic_check_box"), for: .normal)
                    }
                    else
                    {
                        btnDefaultBilling.setImage(UIImage (named: "ic_uncheck_box"), for: .normal)
                    }
                }
                else
                {
                    btnDefaultBilling.setImage(UIImage (named: "ic_uncheck_box"), for: .normal)
                }
               // stateID = (self.shippingAddressArr.object(at: sender.tag) as! NSDictionary).value(forKey: "zone_id") as! NSString
            }
        }
        else
        {
            if(self.myAddressArr.count != 0)
            {
                self.firstNameTxt.text = "\((self.myAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "firstname")!)"
                self.lastNameTxt.text = "\((self.myAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "lastname")!)"
                self.cityTxt.text = "\((self.myAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "city")!)"
                self.zipCodeTxt.text = "\((self.myAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "postcode")!)"
                self.countryTxt.text = "\((self.myAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "country_id")!)"
                self.stateTxt.text = "\((self.myAddressArr.object(at: indexPath.row) as AnyObject).value(forKeyPath: "region.region")!)"
                self.streetTxt.text = "\(((self.myAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "street") as AnyObject).object(at: 0))"
                self.phoneNumTxt.text = "\((self.myAddressArr.object(at: indexPath.row) as AnyObject).value(forKey: "telephone")!)"
                addressId = "\((self.myAddressArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "id")!)"
                countryID = "\((self.myAddressArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "country_id")!)" as NSString
                
                isDefaultShipping = "0"
                isDefaultBilling = "0"
                btnDefaultShipping.setImage(UIImage (named: "ic_uncheck_box"), for: .normal)
                btnDefaultBilling.setImage(UIImage (named: "ic_uncheck_box"), for: .normal)
                
              //  stateID = (self.myAddressArr.object(at: sender.tag) as! NSDictionary).value(forKey: "zone_id") as! NSString
            }
        }
    }
    
    @IBAction func clickEditAcDone(_ sender: Any)
    {
        self.editAcView.isHidden = true
        
        if typeStr == "new"
        {
            self.addNew()
        }
        else
        {
            self.edit(sender as! UIButton)
        }
    }
    
    @IBAction func clickCancel(_ sender: Any)
    {
        self.editAcView.isHidden = true
        self.isDefaultBilling = "0"
        self.isDefaultShipping = "0"
    }
    
    @IBAction func clickDefaultShipping(_ sender: Any)
    {
        if isDefaultShipping == "1"
        {
            isDefaultShipping = "0"
            btnDefaultShipping.setImage(UIImage (named: "ic_uncheck_box"), for: .normal)
        }
        else
        {
            isDefaultShipping = "1"
            btnDefaultShipping.setImage(UIImage (named: "ic_check_box"), for: .normal)
        }
    }
    
    @IBAction func clickDefaultBilling(_ sender: Any)
    {
        if isDefaultBilling == "1"
        {
            isDefaultBilling = "0"
            btnDefaultBilling.setImage(UIImage (named: "ic_uncheck_box"), for: .normal)
        }
        else
        {
            isDefaultBilling = "1"
            btnDefaultBilling.setImage(UIImage (named: "ic_check_box"), for: .normal)
        }
    }
    
    @IBAction func clickAddAddress(_ sender: Any)
    {
        self.getCountryListNew()
        typeStr = "new"
        
        self.editAcView.isHidden = false
        self.lblAddressType.text = NSLocalizedString("ADD A NEW ADDRESS", comment: "")
        
        self.phoneNumTxt.text = ""
        self.firstNameTxt.text = ""
        self.lastNameTxt.text = ""
        self.streetTxt.text = ""
        self.cityTxt.text = ""
        self.zipCodeTxt.text = ""
        self.stateTxt.text = ""
        self.countryTxt.text = ""
    }
    
    @IBAction func countryAction(_ sender: AnyObject)
    {
        stateName = ""
        self.view.endEditing(true)
        
        if countryArr.count <= 1
        {
            if countryArr.count == 1
            {
                countryTxt.text = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: 0) as? String
                countryName = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: 0) as! String
                countryID = (countryArr.value(forKey: "id") as AnyObject).object(at: 0) as! NSString
            }
        }
        else
        {
            pickerType = "country"
            picker.reloadAllComponents()
            picker.selectRow(0, inComponent: 0, animated: true)
        }
        
        /*var countryNames = [String]()
        let country_id = NSMutableArray()
        
        for i in 0 ..< self.countryListAry.count
        {
            let name = (self.countryListAry.value(forKey: "name") as AnyObject).object(at: i)
            countryNames.append(name as! String)
            country_id.add((self.countryListAry.value(forKey: "country_id") as! NSArray).object(at: i) as! String)
        }
        pickerType = "country"*/
    }
    
    @IBAction func stateAction(_ sender: AnyObject)
    {
        // self.showStateList()
        self.view.endEditing(true)
        
        for i in 0..<countryArr.count
        {
            let id = "\((countryArr.value(forKey: "id") as AnyObject).object(at: 0))"
            
            if id == "\(countryID)"
            {
                stateArr = ((countryArr.object(at: i) as AnyObject).value(forKey: "available_regions") as! NSArray).mutableCopy() as! NSMutableArray
            }
        }
        
        if stateArr.count <= 1
        {
            if stateArr.count == 1
            {
                stateTxt.text = (stateArr.value(forKey: "name") as AnyObject).object(at: 0) as? String
                stateName = (stateArr.value(forKey: "name") as AnyObject).object(at: 0) as! String
                stateID = (stateArr.value(forKey: "id") as AnyObject).object(at: 0) as! NSString
            }
        }
        else
        {
            pickerType = "state"
            picker.reloadAllComponents()
            picker.selectRow(0, inComponent: 0, animated: true)
        }
    }
    
    @IBAction func clickCity(_ sender: Any)
    {
        pickerType = "city"
        picker.reloadAllComponents()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    func refreshList(notification: NSNotification)
    {
        self.countryTxt.text = (countryName as NSString) as String
        self.stateTxt.text = (stateName as NSString) as String
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func addNew()
    {
        if(self.firstNameTxt.hasText && self.lastNameTxt.hasText && self.cityTxt.hasText && self.streetTxt.hasText && self.zipCodeTxt.hasText && self.phoneNumTxt.hasText && self.stateTxt.hasText)
        {
            if self.phoneNumTxt.text?.count != 10
            {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "Mobile number must be 10 characters", viewController: self)
            }
            else
            {
                let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
                userDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
                
                let region = NSMutableDictionary()
                
                region.setObject("\((stateArr.object(at: selectedIndex) as AnyObject).value(forKey: "id")!)", forKey: "region_id" as NSCopying)
                region.setObject("\((stateArr.object(at: selectedIndex) as AnyObject).value(forKey: "name")!)", forKey: "region" as NSCopying)
                region.setObject("\((stateArr.object(at: selectedIndex) as AnyObject).value(forKey: "code")!)", forKey: "region_code" as NSCopying)
                
                var address : [String : Any] = [:]
                if allAddress.count != 0
                {
                    address = ["firstname": self.firstNameTxt.text!,
                               "lastname": self.lastNameTxt.text!,
                               "company": "",
                               "city": self.cityTxt.text!,
                               "region": region,
                               "street": [self.streetTxt.text!],
                               "postcode": self.zipCodeTxt.text!,
                               "country_id": countryIDStr,
                               "telephone": self.phoneNumTxt.text!,
                               "default_billing":isDefaultBilling,
                               "default_shipping":isDefaultShipping
                        ] as [String : Any]
                }
                else
                {
                    address = ["firstname": self.firstNameTxt.text!,
                               "lastname": self.lastNameTxt.text!,
                               "company": "",
                               "city": self.cityTxt.text!,
                               "region": region,
                               "street": [self.streetTxt.text!],
                               "postcode": self.zipCodeTxt.text!,
                               "country_id": countryIDStr,
                               "telephone": self.phoneNumTxt.text!,
                               "default_billing":"1",
                               "default_shipping":"1"
                        ] as [String : Any]
                }
                
                
                allAddress.add(address)
                
                let customer = [
                    "firstname": userDetails.value(forKey: "firstname") as! String,
                    "lastname": userDetails.value(forKey: "lastname") as! String,
                    "website_id": 1,
                    "email": userDetails.value(forKey: "email") as! String,
                    "addresses": allAddress,
                    ] as [String : Any]
                //"state_id": self.stateTxt.text!,
                let params = ["customer": customer] as [String : Any]
                
                let urlStr = "\(ConfigUrl.baseUrl)customers/me"
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.put.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                let setTemp: [String : Any] = params
                
                if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                    let jsonString = String(data: jsonData , encoding: .utf8)
                    print(jsonString! as Any)
                    request.httpBody = jsonData
                }
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON { (responseObject) -> Void in
                        SharedManager.dismissHUD(viewController: self)
                        
                        if responseObject.result.isSuccess
                        {
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                print(responseObject.result.value!)
                                self.refreshAddress()
                                SharedManager.showAlertWithMessage(title: "", alertMessage: "\(NSLocalizedString("Address added Successfully", comment: ""))", viewController: self)
                            }
                            else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                            {
                                let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                    
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    self.present(viewController, animated: true, completion: nil)
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                        self.isDefaultBilling = "0"
                        self.isDefaultShipping = "0"
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Information", comment: ""))", alertMessage: NSLocalizedString("Please enter all the details", comment: ""), viewController: self)
        }
    }
    
    func edit(_ sender : UIButton)
    {
        let touchPoint = sender.convert(CGPoint.zero, to: myAddressTable)
        let indexPath = myAddressTable.indexPathForRow(at: touchPoint)! as IndexPath
        
        if(self.firstNameTxt.hasText && self.lastNameTxt.hasText && self.cityTxt.hasText && self.streetTxt.hasText && self.zipCodeTxt.hasText && self.phoneNumTxt.hasText && self.stateTxt.hasText)
        {
            if self.phoneNumTxt.text?.count != 10
            {
                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "Mobile number must be 10 characters", viewController: self)
            }
            else
            {
               // let indexPath = selectedIndexPath
                
                var tag = 0
                
                for i in 0..<self.allAddress.count
                {
                    let id = "\((allAddress.object(at: i) as AnyObject).value(forKey: "id")!)"
                    
                    if addressId == id
                    {
                        tag = i
                    }
                }
                
                let selectedAddress = (allAddress.object(at: tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if isDefaultBilling == "1"
                {
                    for i in 0..<allAddress.count
                    {
                        let addressDetail = (allAddress.object(at: i) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        addressDetail.setObject("0", forKey: "default_billing" as NSCopying)
                        allAddress.replaceObject(at: i, with: addressDetail)
                    }
                }
                
                if isDefaultShipping == "1"
                {
                    for i in 0..<allAddress.count
                    {
                        let addressDetail = (allAddress.object(at: i) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        addressDetail.setObject("0", forKey: "default_shipping" as NSCopying)
                        allAddress.replaceObject(at: i, with: addressDetail)
                    }
                }
                
                let region = "\(String(describing: selectedAddress.value(forKeyPath: "region.region")))"
                
                if region != self.stateTxt.text
                {
                    for i in 0..<stateArr.count
                    {
                        let id = (stateArr.object(at: i) as AnyObject).value(forKey: "name") as! String
                        
                        if id == region
                        {
                            let region = NSMutableDictionary()
                            
                            region.setObject("\((stateArr.object(at: i) as AnyObject).value(forKey: "id")!)", forKey: "region_id" as NSCopying)
                            region.setObject("\((stateArr.object(at: i) as AnyObject).value(forKey: "name")!)", forKey: "region" as NSCopying)
                            region.setObject("\((stateArr.object(at: i) as AnyObject).value(forKey: "code")!)", forKey: "region_code" as NSCopying)
                            
                            selectedAddress.setValue(region, forKey: "region")
                        }
                    }
                }
                
                selectedAddress.setValue(self.firstNameTxt.text!, forKey: "firstname")
                selectedAddress.setValue(self.lastNameTxt.text!, forKey: "lastname")
                selectedAddress.setValue(self.cityTxt.text!, forKey: "city")
                selectedAddress.setValue(self.phoneNumTxt.text!, forKey: "telephone")
                selectedAddress.setValue(self.zipCodeTxt.text!, forKey: "postcode")
                selectedAddress.setValue("", forKey: "company")
                selectedAddress.setValue(countryIDStr, forKey: "country_id")
                selectedAddress.setValue([self.streetTxt.text!], forKey: "street")
                selectedAddress.setObject(isDefaultBilling, forKey: "default_billing" as NSCopying)
                selectedAddress.setObject(isDefaultShipping, forKey: "default_shipping" as NSCopying)
                
                allAddress.replaceObject(at: tag, with: selectedAddress)
                
                let customer = [
                    "firstname": userDetails.value(forKey: "firstname") as! String,
                    "lastname": userDetails.value(forKey: "lastname") as! String,
                    "website_id": 1,
                    "email": userDetails.value(forKey: "email") as! String,
                    "addresses": allAddress,
                    ] as [String : Any]
                //"state_id": self.stateTxt.text!,
                let params = ["customer": customer] as [String : Any]
                
                let urlStr = "\(ConfigUrl.baseUrl)customers/me"
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.put.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
                
                let setTemp: [String : Any] = params
                
                if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                    let jsonString = String(data: jsonData , encoding: .utf8)
                    print(jsonString! as Any)
                    request.httpBody = jsonData
                }
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON { (responseObject) -> Void in
                        SharedManager.dismissHUD(viewController: self)
                        
                        if responseObject.result.isSuccess
                        {
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                self.refreshAddress()
                                SharedManager.showAlertWithMessage(title: "", alertMessage: "\(NSLocalizedString("Your changes saved successfully", comment: ""))", viewController: self)
                                
                                self.isDefaultBilling = "0"
                                self.isDefaultShipping = "0"
                            }
                            else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                            {
                                let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                    
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    self.present(viewController, animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Information", comment: ""))", alertMessage: NSLocalizedString("Please enter all the details", comment: ""), viewController: self)
        }
    }
    
    @IBAction func clickPickerDone(_ sender: Any)
    {
        if pickerType == "country"
        {
            countryTxt.text = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: selectedIndex) as? String
            countryName = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: selectedIndex) as! String
            countryID = (countryArr.value(forKey: "id") as AnyObject).object(at: selectedIndex) as! NSString
        }
        else if pickerType == "state"
        {
            stateTxt.text = (stateArr.value(forKey: "name") as AnyObject).object(at: selectedIndex) as? String
            stateName = (stateArr.value(forKey: "name") as AnyObject).object(at: selectedIndex) as! String
            stateID = (stateArr.value(forKey: "id") as AnyObject).object(at: selectedIndex) as! NSString
            
            self.getCityList()
        }
        else
        {
            if languageID == "1"
            {
                cityTxt.text = (cityArr.value(forKey: "city") as AnyObject).object(at: selectedIndex) as? String
            }
            else
            {
                cityTxt.text = (cityArr.value(forKey: "city_ar") as AnyObject).object(at: selectedIndex) as? String
            }
        }
    }
    
    func refreshAddress()
    {
        if(UserDefaults.standard.string(forKey: "USER_ID") != nil)
        {
            userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
            print(userIDStr)
            
            let urlStr = "\(ConfigUrl.baseUrl)customers/me"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        SharedManager.dismissHUD(viewController: self)
                        
                        if responseObject.result.isSuccess
                        {
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                // print(responseObject.result.value!)
                                
                                if "\(responseObject.result.value!)" != "<null>"
                                {
                                    let data = NSKeyedArchiver.archivedData(withRootObject: responseObject.result.value!)
                                    UserDefaults.standard.set(data, forKey: kUserDetails)
                                    self.viewWillAppear(true)
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No User Information Available", comment: ""))", viewController: self)
                                }
                            }
                            else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                            {
                                let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                    
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    self.present(viewController, animated: true, completion: nil)
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
    }
    
    func getCountryListNew()
    {
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "\(ConfigUrl.baseUrl)directory/countries"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            let result = responseObject.result.value! as! NSArray
                            countryArr = result.mutableCopy() as! NSMutableArray
                            stateArr = ((countryArr.object(at: 0) as AnyObject).value(forKey: "available_regions") as! NSArray).mutableCopy() as! NSMutableArray
                            self.countryIDStr = (((responseObject.result.value!) as AnyObject).object(at: 0) as AnyObject).value(forKeyPath: "id") as! String
                            countryID = self.countryIDStr as NSString
                            
                            if countryArr.count <= 1
                            {
                                if countryArr.count == 1
                                {
                                    self.countryTxt.text = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: 0) as? String
                                    self.countryName = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: 0) as! String
                                    countryID = (countryArr.value(forKey: "id") as AnyObject).object(at: 0) as! NSString
                                }
                                
                                for i in 0..<countryArr.count
                                {
                                    let id = "\((countryArr.value(forKey: "id") as AnyObject).object(at: 0))"
                                    
                                    if id == "\(countryID)"
                                    {
                                        stateArr = ((countryArr.object(at: i) as AnyObject).value(forKey: "available_regions") as! NSArray).mutableCopy() as! NSMutableArray
                                    }
                                }
                                
                                if stateArr.count <= 1
                                {
                                    if stateArr.count == 1
                                    {
                                        self.stateTxt.text = (stateArr.value(forKey: "name") as AnyObject).object(at: 0) as? String
                                        stateName = (stateArr.value(forKey: "name") as AnyObject).object(at: 0) as! String
                                        stateID = (stateArr.value(forKey: "id") as AnyObject).object(at: 0) as! NSString
                                        
                                        self.getCityList()
                                    }
                                }
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func getCityList()
    {
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "https://balleh.com/mobifilter/index/mcity?country_id=\(countryID)&state_id=\(stateID)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            let result = responseObject.result.value! as! NSArray
                            self.cityArr = result
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
}

extension AddressListVC: UIPickerViewDelegate, UIPickerViewDataSource {
    //MARK: PickerView Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if pickerType == "country"
        {
            return countryArr.count
        }
        else if pickerType == "state"
        {
            return stateArr.count
        }
        else
        {
            return cityArr.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerType == "country"
        {
            let name = (countryArr.value(forKey: "full_name_english") as AnyObject).object(at: row) as! String
            
            return name
        }
        else if pickerType == "state"
        {
            let name = (stateArr.value(forKey: "name") as AnyObject).object(at: row) as! String
            
            return name
        }
        else
        {
            var name = ""
            if languageID == "1"
            {
                name = (cityArr.value(forKey: "city") as AnyObject).object(at: row) as! String
            }
            else
            {
                name = (cityArr.value(forKey: "city_ar") as AnyObject).object(at: row) as! String
            }
            
            return name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedIndex = row
        if languageID == "1"
        {
            cityTxt.text = (cityArr.value(forKey: "city") as AnyObject).object(at: selectedIndex) as? String
        }
        else
        {
            cityTxt.text = (cityArr.value(forKey: "city_ar") as AnyObject).object(at: selectedIndex) as? String
        }
    }
    
}

extension AddressListVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.cityTxt { return false }
        return true
    }
}
