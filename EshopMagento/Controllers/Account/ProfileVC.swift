//
//  ProfileVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 20/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire

class ProfileVC: ParentViewController,UITabBarControllerDelegate,loginIntimation
{
    
    @IBOutlet weak var contactMsgTxt: UITextField!
    @IBOutlet weak var contactMobileTxt: UITextField!
    @IBOutlet weak var contactEmailTxt: UITextField!
    @IBOutlet weak var contactNameTxt: UITextField!
    @IBOutlet weak var contactLayerView: UIView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var editAcLayerView: UIView!
    @IBOutlet weak var editAcView: UIView!
    @IBOutlet weak var extrasView: UIView!
    @IBOutlet weak var myOrderView: UIView!
    @IBOutlet weak var myAcView: UIView!
    @IBOutlet weak var changePassSmallView: UIView!
    @IBOutlet weak var changePassView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
  //  @IBOutlet weak var phoneNumberTxt: UITextField!
    @IBOutlet weak var newPasswordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var oldPasswordTxt: UITextField!
  //  @IBOutlet weak var notificationSwitch: UISwitch!
    var isFromDidLoad:Bool = false
    
    // Button Outlets
    @IBOutlet weak var btnEditAc: UIButton!
    @IBOutlet weak var btnChangePwd: UIButton!
    @IBOutlet weak var btnEditAdd: UIButton!
   // @IBOutlet weak var btnNotif: UILabel!
    @IBOutlet weak var btnHistory: UIButton!
    @IBOutlet weak var btnRewards: UIButton!
    @IBOutlet weak var btnAbout: UIButton!
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    
    var profileDetailsAry:NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        expandedSection.removeAllObjects()
        setLeftNavigationButtonHome()
        let cartCount = "\(String(describing: UserDefaults.standard.string(forKey: kAddToCart)))"
        self.setRightNavigationButton()
        self.title = NSLocalizedString("Account", comment: "")
        
        updateToken()
        // Do any additional setup after loading the view.
    }
    
    func loginSuccess() {
        
        isFromDidLoad = false
        self.showData()
    }
    
    func loginFailure() {
        
        isFromDidLoad = false
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func myRewardsAction(_ sender: AnyObject)
    {
        creditTypeStr = "rewards"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let tabbarObj:UITabBarController = self.tabBarController!
        tabbarObj.delegate = self
        
        contactView.isHidden = true
        editAcView.isHidden = true
        changePassView.isHidden = true
        
        self.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.myAcView.frame.size.height + self.myOrderView.frame.size.height + self.extrasView.frame.size.height + 50);
        
        //self.scrollView.translatesAutoresizingMaskIntoConstraints = true
        
        if(UserDefaults.standard.string(forKey: "USER_ID") == nil && isFromDidLoad == false)
        {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")], [""], [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
            
            isFromDidLoad = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            viewController.delegate = self
            
            self.present(viewController, animated: false, completion: { () -> Void in
                //
            })
        }
        else
        {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: "")], [""], [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
            
            self.showData()
        }
        
        self.myAcView.layer.cornerRadius = 8
        self.myAcView.clipsToBounds = true
        self.myOrderView.layer.cornerRadius = 8
        self.myOrderView.clipsToBounds = true
        self.extrasView.layer.cornerRadius = 8
        self.extrasView.clipsToBounds = true
        self.changePassSmallView.layer.cornerRadius = 8
        self.changePassSmallView.clipsToBounds = true
        self.editAcLayerView.layer.cornerRadius = 8
        self.editAcLayerView.clipsToBounds = true
        self.contactLayerView.layer.cornerRadius = 8
        self.contactLayerView.clipsToBounds = true
        
        self.myAcView.layer.shadowColor = UIColor.black.cgColor
        self.myAcView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.myAcView.layer.shadowOpacity = 0.7
        self.myAcView.layer.shadowRadius = 4.0
        
        self.myOrderView.layer.shadowColor = UIColor.black.cgColor
        self.myOrderView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.myOrderView.layer.shadowOpacity = 0.7
        self.myOrderView.layer.shadowRadius = 4.0
        
        self.extrasView.layer.shadowColor = UIColor.black.cgColor
        self.extrasView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.extrasView.layer.shadowOpacity = 0.7
        self.extrasView.layer.shadowRadius = 4.0
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        _ = self.navigationController?.popToRootViewController(animated: false)
        
    }
    
    func showData() {
        if(UserDefaults.standard.string(forKey: "USER_ID") != nil) {
            userIDStr = UserDefaults.standard.string(forKey: "USER_ID")!
            isFromDidLoad = true
            print(userIDStr)
            
            let urlStr = "\(ConfigUrl.baseUrl)customers/me"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet() {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        SharedManager.dismissHUD(viewController: self)
                        
                        if responseObject.result.isSuccess {
                            if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                                // print(responseObject.result.value!)
                                
                                if "\(responseObject.result.value!)" != "<null>" {
                                    let data = NSKeyedArchiver.archivedData(withRootObject: responseObject.result.value!)
                                    UserDefaults.standard.set(data, forKey: kUserDetails)
                                    
                                    print(responseObject.result.value!)
                                    
                                    self.firstNameTxt.text = (responseObject.result.value! as AnyObject).value(forKey: "firstname") as? String
                                    self.lastNameTxt.text = (responseObject.result.value! as AnyObject).value(forKey: "lastname") as? String
                                    self.emailTxt.text = (responseObject.result.value! as AnyObject).value(forKey: "email") as? String
                                    self.txtMobile.text = ""
                                } else {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No User Information Available", comment: ""))", viewController: self)
                                }
                            } else if "\(String(describing: responseObject.response!.statusCode))" == "401" {
                                let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                    
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    self.present(viewController, animated: true, completion: nil)
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                            } else {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure {
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            } else {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
    }
    
    /*func save()
    {
        if(self.firstNameTxt.hasText && self.lastNameTxt.hasText && self.emailTxt.hasText)
        {
            var value:String = String()
            value = NSString(format: "address_id=%@&customer_id=%@&firstname=%@&lastname=%@",((self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "address_id") as? String)!,((self.profileDetailsAry.object(at: 0) as AnyObject).value(forKey: "customer_id") as? String)!,self.firstNameTxt.text!,self.lastNameTxt.text!) as String
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.updateAddressWith(value, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Information", comment: ""))", alertMessage: result.value(forKeyPath: "Message") as! String, viewController: self)
                }
                else{
                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Information", comment: ""))", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Information", comment: ""))", alertMessage: NSLocalizedString("Please enter all the details", comment: ""), viewController: self)
        }
    }*/
    
    @IBAction func cancelChangePasswordAction(_ sender: AnyObject)
    {
        contactView.isHidden = true
        editAcView.isHidden = true
        changePassView.isHidden = true
        oldPasswordTxt.text = ""
        newPasswordTxt.text = ""
        confirmPasswordTxt.text = ""
        
        contactNameTxt.text = ""
        contactMsgTxt.text = ""
        contactEmailTxt.text = ""
        contactMobileTxt.text = ""
    }
    
    @IBAction func doneChangePasswordAction(_ sender: AnyObject)
    {
        if !oldPasswordTxt.hasText
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Enter your Old Password", comment: ""))", viewController: self)
        }
        else if !self.newPasswordTxt.hasText
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Enter new password to update", comment: ""))", viewController: self)
        }
        else if !confirmPasswordTxt.hasText
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("Please Re-Enter new password to confirm", comment: ""))", viewController: self)
        }
        else
        {
            self.view.endEditing(true)
            SharedManager.showHUD(viewController: self)
            
            let params = [
                "currentPassword": self.oldPasswordTxt.text!,
                "newPassword": self.newPasswordTxt.text!
            ]
            
            let urlStr = "\(ConfigUrl.baseUrl)customers/me/password"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.put.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)
                print(jsonString! as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON { (responseObject) -> Void in
                    
                    SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: "\(NSLocalizedString("Your Password changed successfully", comment: ""))", viewController: self)
                            self.changePassView.isHidden = true
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
    }
    
    @IBAction func changePasswordAction(_ sender: AnyObject)
    {
        changePassView.isHidden = false
        changePassView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    @IBAction func clickEditAcDone(_ sender: Any)
    {
        if(self.firstNameTxt.hasText && self.lastNameTxt.hasText && self.emailTxt.hasText)
        {
            let urlStr = "\(ConfigUrl.baseUrl)customers/me"
            
            let customer = [
                "firstname": self.firstNameTxt.text!,
                "lastname": self.lastNameTxt.text!,
                "email": self.emailTxt.text!,
                "telephone" : self.txtMobile.text!,
                "websiteId": 1
            ] as [String : Any]
            
           // let address = []
            
            let params = ["customer" : customer,
                ] as [String : Any]
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.put.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(userIDStr)", forHTTPHeaderField: "Authorization")
            
            let setTemp: [String : Any] = params
            
            if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
                let jsonString = String(data: jsonData , encoding: .utf8)
                print(jsonString! as Any)
                request.httpBody = jsonData
            }
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        SharedManager.dismissHUD(viewController: self)
                        
                        if responseObject.result.isSuccess
                        {
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                // print(responseObject.result.value!)
                                
                                if "\(responseObject.result.value!)" != "<null>"
                                {
                                    SharedManager.showAlertWithMessage(title: "", alertMessage: "\(NSLocalizedString("Your Changes has been saved successfully", comment: ""))", viewController: self)
                                    self.editAcView.isHidden = true
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Something went wrong", comment: ""))", viewController: self)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Information", comment: ""))", alertMessage: NSLocalizedString("Please enter all the details", comment: ""), viewController: self)
        }
    }
    
    @IBAction func clickEditAccount(_ sender: Any)
    {
        editAcView.isHidden = false
        editAcView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    @IBAction func clickContactUs(_ sender: Any)
    {
        if languageID == "2"
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AboutusViewController") as! AboutusViewController
            viewController.urlToLoad = "https://balleh.com/ar/contact"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AboutusViewController") as! AboutusViewController
            viewController.urlToLoad = "https://balleh.com/en/contact"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
       // contactView.isHidden = false
       // contactView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    @IBAction func clickAboutUs(_ sender: Any) {
        if languageID == "2" {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AboutusViewController") as! AboutusViewController
            viewController.urlToLoad = "https://balleh.com/ar/about-us"
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AboutusViewController") as! AboutusViewController
            viewController.urlToLoad = "https://balleh.com/en/about-us"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
   /* @IBAction func clickContactDone(_ sender: Any)
    {
        if(self.contactNameTxt.hasText && self.contactMsgTxt.hasText && self.contactEmailTxt.hasText && self.contactMobileTxt.hasText)
        {
            let mainDict:NSMutableDictionary = NSMutableDictionary()
            
            mainDict.setObject(self.contactNameTxt.text!, forKey: "firstname" as NSCopying)
            mainDict.setObject(self.contactMsgTxt.text!, forKey: "message" as NSCopying)
            mainDict.setObject(self.contactEmailTxt.text!, forKey: "email" as NSCopying)
            mainDict.setObject(self.contactMobileTxt.text!, forKey: "phone" as NSCopying)
            
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: mainDict ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText = NSString(data: theJSONData!,
                                       encoding: String.Encoding.utf8.rawValue)
            
            SharedManager.showHUD(viewController: self)
            BusinessManager.submitContactFormWith(theJSONText as! String, completionHandler: { (result) -> () in
                SharedManager.dismissHUD(viewController: self)
                
                let code = result.value(forKey: "status") as! String
                if code == "200"
                {
                    let alt: UIAlertController = UIAlertController(title: "Information", message: result.value(forKey: "Message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alt.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        self.contactView.isHidden = true
                    }))
                    self.present(alt, animated: true, completion: nil)
                }
                else{
                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Information", comment: ""))", alertMessage: result.value(forKeyPath: "message") as! String, viewController: self)
                }
            })
        }
        else
        {
            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Information", comment: ""))", alertMessage: NSLocalizedString("Please enter all the details", comment: ""), viewController: self)
        }
    }*/
    
    @IBAction func clickChangeAddress(_ sender: Any)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func notificationSettingsSwitch(_ sender: AnyObject)
    {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func logoutAction(_ sender: AnyObject) {
        let logoutAlert = UIAlertController(title: nil, message: Texts.To_Logout, preferredStyle: .alert)
        
        let logout = UIAlertAction(title: NSLocalizedString(Texts.Logout, comment: ""), style: .destructive) { (action) in
            self.isFromDidLoad = false
            cartID = ""
            UserDefaults.standard.removeObject(forKey: kUserDetails)
            UserDefaults.standard.removeObject(forKey: kAddToCart)
            
            UserDefaults.standard.removeObject(forKey: "USER_ID")
            UserDefaults.standard.removeObject(forKey: "COUPON")
            UserDefaults.standard.removeObject(forKey: "VOUCHER")
            UserDefaults.standard.removeObject(forKey: "REWARDS")
            userIDStr = ""
            cartCount = ""
            isGuest = true
            
            self.gotoRootViewController()
            self.setRightNavigationButton()
            
            let appDomain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
            self.tabBarController?.selectedIndex = 0
        }
        
        logoutAlert.addAction(logout)
        logoutAlert.addAction(UIAlertAction(title: NSLocalizedString(Texts.Cancel, comment: ""), style: .default))
        
        self.present(logoutAlert, animated: true, completion: nil)
    }
    
}
