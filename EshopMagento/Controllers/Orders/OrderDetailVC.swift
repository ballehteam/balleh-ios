//
//  OrderDetailVC.swift
//  EshopMagento
//
//  Created by Apple on 10/04/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit

class OrderDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var viewOrderInfo: UIView!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    // Shipping and Payment
    @IBOutlet weak var viewOrderDetails: UIView!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblBillingAddress: UILabel!
    @IBOutlet weak var lblShippingMethod: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    
    // Totals
    @IBOutlet weak var viewOrderTotals: UIView!
    @IBOutlet weak var viewTaxTotals: UIView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblShippingCost: UILabel!
    @IBOutlet weak var lblPaymentCost: UILabel!
    @IBOutlet weak var lblTax: UILabel!
    @IBOutlet weak var lblWithoutTax: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var lblPaymentCostTitle: UILabel!
    
    var detailsDict = NSDictionary()
    var itemsArr = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(detailsDict)
        
        itemsArr = detailsDict.value(forKey: "items") as! NSArray
        self.tblList.reloadData()
        
        let billingAddress = detailsDict.value(forKey: "billing_address")! as AnyObject
        
        self.lblShippingAddress.text = "\(billingAddress.value(forKey: "firstname")!) \(billingAddress.value(forKey: "lastname")!),\n\((billingAddress.value(forKey: "street") as AnyObject).object(at: 0))\n\(billingAddress.value(forKey: "city")!),\n\(billingAddress.value(forKey: "postcode")!) - \(billingAddress.value(forKey: "country_id")!)\nTel: \(billingAddress.value(forKey: "telephone")!)"
        
    //    let shippingAddress = ((((detailsDict.value(forKey: "extension_attributes")! as AnyObject).value(forKey: "shipping_assignments") as AnyObject).object(at: 0) as AnyObject).value(forKey: "shipping") as AnyObject).value(forKey: "address") as AnyObject
        
    //    self.lblShippingAddress.text = "\(shippingAddress.value(forKey: "firstname")!) \(shippingAddress.value(forKey: "lastname")!),\n\((shippingAddress.value(forKey: "street") as AnyObject).object(at: 0))\n\(shippingAddress.value(forKey: "city")!),\n\(shippingAddress.value(forKey: "postcode")!) - \(shippingAddress.value(forKey: "country_id")!)\nTel: \(shippingAddress.value(forKey: "telephone")!)"
        
        self.lblOrderID.text = "\(NSLocalizedString("Order ID", comment: "")) : #\(detailsDict.value(forKey: "increment_id")!)"
        self.lblStatus.text = "\(detailsDict.value(forKey: "status")!)"
        self.lblShippingMethod.text = "\(detailsDict.value(forKey: "shipping_description")!)"
        
        let paymentMethod = "\((detailsDict.value(forKey: "payment") as AnyObject).value(forKey: "method")!)"
        
        if paymentMethod.contains("cashon")
        {
            self.lblPaymentMethod.text = "\(((detailsDict.value(forKey: "payment") as AnyObject).value(forKey: "additional_information") as AnyObject).object(at: 0))"
        }
        else
        {
            self.lblPaymentMethod.text = paymentMethod
        }
        self.lblOrderDate.text = "\(NSLocalizedString("Placed on", comment: "")) \(detailsDict.value(forKey: "created_at")!)"
        
        // Totals
        let subTotal = "\(detailsDict.value(forKey: "base_subtotal")!)"
        self.lblSubTotal.text = "\(Tools.getCurrency()) \(subTotal)"
        
        let shipCost = "\(detailsDict.value(forKey: "base_shipping_amount")!)"
        self.lblShippingCost.text = "\(Tools.getCurrency()) \(shipCost)"
        
        let withoutTax = Float(subTotal)! + Float(shipCost)!
        self.lblWithoutTax.text = "\(Tools.getCurrency()) \(withoutTax)"
        
        let tax = "\(detailsDict.value(forKey: "base_tax_amount")!)"
        self.lblTax.text = "\(Tools.getCurrency()) \(tax)"
        
        let total = "\(detailsDict.value(forKey: "base_grand_total")!)"
        self.lblGrandTotal.text = "\(Tools.getCurrency()) \(total)"
        
        let paymentCost = Float(total)! - Float(withoutTax + Float(tax)!)
        
        var height = CGFloat()
        
        if paymentCost != 0
        {
            self.lblPaymentCost.text = "\(Tools.getCurrency()) \(paymentCost)"
            self.viewTaxTotals.frame.origin.y = self.lblPaymentCostTitle.frame.origin.y + self.lblPaymentCostTitle.frame.size.height + 5
           // height = 232
        }
        else
        {
            self.viewTaxTotals.frame.origin.y = self.lblPaymentCostTitle.frame.origin.y
          //  height = 188
        }
        
        // Frames
        
        var frame = self.tblList.frame
        frame.size.width = self.view.bounds.width - 16
        frame.size.height = CGFloat(self.itemsArr.count * 117)
        self.tblList.frame = frame
        self.tblList.translatesAutoresizingMaskIntoConstraints = true
        
        frame = self.viewOrderTotals.frame
        frame.size.width = self.view.bounds.width - 16
       // frame.size.height = height
        frame.origin.y = self.tblList.frame.origin.y + self.tblList.frame.size.height + 7
        self.viewOrderTotals.frame = frame
        self.viewOrderTotals.translatesAutoresizingMaskIntoConstraints = true
        
        frame = self.viewOrderDetails.frame
        frame.size.width = self.view.bounds.width - 16
        frame.origin.y = self.viewOrderTotals.frame.origin.y + self.viewOrderTotals.frame.size.height + 7
        self.viewOrderDetails.frame = frame
        self.viewOrderDetails.translatesAutoresizingMaskIntoConstraints = true
        
      //  self.mainScrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: self.tblList.frame.size.height + self.viewOrderDetails.frame.size.height + self.viewOrderDetails.frame.size.height + 100)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return itemsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:OrderDetailCell = self.tblList.dequeueReusableCell(withIdentifier: "orderCell") as! OrderDetailCell
        
        cell.lblName.text = "\(String(describing: (itemsArr[indexPath.row] as AnyObject).value(forKey: "name")!))"
        cell.lblSku.text = "Sku : \(String(describing: (itemsArr[indexPath.row] as AnyObject).value(forKey: "sku")!))"
        cell.lblPrice.text = "\(Tools.getCurrency()) \(String(describing: (itemsArr[indexPath.row] as AnyObject).value(forKey: "price")!))"
        cell.lblQtyOrdered.text = "\(NSLocalizedString("Ordered", comment: ""))  : \(String(describing: (itemsArr[indexPath.row] as AnyObject).value(forKey: "qty_ordered")!))"
        cell.lblQtyCanceled.text = "\(NSLocalizedString("Canceled", comment: ""))  : \(String(describing: (itemsArr[indexPath.row] as AnyObject).value(forKey: "qty_canceled")!))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 116
    }
}
