//
//  OrderHistoryCell.swift
//  EshopMagento
//
//  Created by Apple on 01/05/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell
{
    
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnDetail: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
