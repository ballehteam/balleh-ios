//
//  MyOrderVC.swift
//  EshopMagento
//
//  Created by Apple on 10/04/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire

class MyOrderVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblList: UITableView!
    var ordersArr = NSMutableArray()
    
    var page:Int = 1
    var pageCount = Double()
    var limit:String = "5"
    var productsCount:String = ""
    var isScrolledOnce : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // V1/orders?searchCriteria[filter_groups][0][filters][0][field]=customer_email&searchCriteria[filter_groups][0][filters][0][value]=sasikumara@exlcart.com&searchCriteria[pageSize]=1&searchCriteria[currentPage]=1
        
        let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
        let userDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
        
        let email = userDetails.value(forKey: "email")!
        
       // var result = NSMutableArray()
        let urlStr = "\(ConfigUrl.baseUrl)orders?searchCriteria[filter_groups][0][filters][0][field]=customer_email&searchCriteria[filter_groups][0][filters][0][value]=\(email)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)&searchCriteria[sortOrders][0][field]=created_at&searchCriteria[sortOrders][0][direction]=DESC"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print((responseObject.result.value! as AnyObject).value(forKey: "items")!)
                            self.ordersArr = ((responseObject.result.value! as AnyObject).value(forKey: "items") as! NSArray).mutableCopy() as! NSMutableArray
                            
                            if self.ordersArr.count == 0
                            {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Your order list is empty", comment: ""))", viewController: self)
                            }
                            else
                            {
                                self.productsCount = "\(((responseObject.result.value!) as AnyObject).value(forKey: "total_count")!)"
                                self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                            }
                            
                            print(self.ordersArr.value(forKey: "items"))
                            self.tblList.reloadData()
                        }
                        else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                        {
                            let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                self.present(viewController, animated: true, completion: nil)
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Pagination
    func pullToRefresh()
    {
        if (self.isScrolledOnce == true)
        {
            return
        }
        self.isScrolledOnce = true
        
        if page != Int(self.pageCount) + 1 && !categoryID.isEmpty
        {
            page += 1
            
            let data = UserDefaults.standard.object(forKey: kUserDetails) as! Data
            let userDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
            
            let email = userDetails.value(forKey: "email")!
            
            // var result = NSMutableArray()
            let urlStr = "\(ConfigUrl.baseUrl)orders?searchCriteria[filter_groups][0][filters][0][field]=customer_email&searchCriteria[filter_groups][0][filters][0][value]=\(email)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)&searchCriteria[sortOrders][0][field]=created_at&searchCriteria[sortOrders][0][direction]=DESC"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                print((responseObject.result.value! as AnyObject).value(forKey: "items")!)
                                
                                let result = (responseObject.result.value! as AnyObject).value(forKey: "items") as! NSArray
                                self.ordersArr.addObjects(from: result as! [Any])
                                
                                self.tblList.reloadData()
                            }
                            else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                            {
                                let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                                    
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    self.navigationController?.pushViewController(viewController, animated: true)
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                        self.isScrolledOnce = false
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            SharedManager.dismissHUD(viewController: self)
            self.isScrolledOnce = false
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let offset: CGPoint = scrollView.contentOffset
        let bounds: CGRect = scrollView.bounds
        let size: CGSize = scrollView.contentSize
        let inset: UIEdgeInsets = scrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 10
        if y > h + reload_distance
        {
            if isScrolledOnce == false
            {
                self.pullToRefresh()
            }
        }
    }
    
    // MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return ordersArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:OrderHistoryCell = self.tblList.dequeueReusableCell(withIdentifier: "orderCell") as! OrderHistoryCell
        
        let items = ((ordersArr[indexPath.row] as AnyObject).value(forKey: "items") as AnyObject).object(at: 0) as AnyObject
        print(items)
        
        let address = (ordersArr[indexPath.row] as AnyObject).value(forKey: "billing_address") as AnyObject
        
        if let firstName = address.value(forKey: "firstname") as? String,
            let lastName = address.value(forKey: "lastname") as? String {
            cell.customerName.text = "\(firstName) \(lastName)"
        }
        
        cell.lblOrderID.text = "\(NSLocalizedString("Order ID", comment: "")) : #\((ordersArr[indexPath.row] as AnyObject).value(forKey: "increment_id")!)"
        cell.lblDate.text = "\(NSLocalizedString("Placed on", comment: "")) \(items.value(forKey: "created_at")!)"
        cell.lblAmount.text = "\(Tools.getCurrency()) \(items.value(forKey: "price_incl_tax")!)"
        
        let status = "\((ordersArr[indexPath.row] as AnyObject).value(forKey: "status")!)"
        
        if status.lowercased().contains("cancel")
        {
            cell.lblStatus.textColor = .red
        }
        else if status.lowercased().contains("complete")
        {
            cell.lblStatus.textColor = .green
        }
        else if status.lowercased().contains("pending")
        {
            cell.lblStatus.textColor = .blue
        }
        else
        {
            cell.lblStatus.textColor = .black
        }
        
        cell.lblStatus.text = status.uppercased()
        
        cell.btnDetail.layer.borderColor = positiveBtnColor.cgColor
        cell.btnDetail.tag = indexPath.row
        cell.btnDetail.addTarget(self, action: #selector(self.clickViewDetail(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 182
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    @objc func clickViewDetail(_ sender:AnyObject!)
    {
        let details = ordersArr.object(at: sender.tag)
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
        viewController.detailsDict = details as! NSDictionary
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
