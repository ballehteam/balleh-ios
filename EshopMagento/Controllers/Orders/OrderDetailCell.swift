//
//  OrderDetailCell.swift
//  EshopMagento
//
//  Created by Apple on 01/05/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit

class OrderDetailCell: UITableViewCell
{
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQtyOrdered: UILabel!
    @IBOutlet weak var lblQtyCanceled: UILabel!
    @IBOutlet weak var lblSku: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
