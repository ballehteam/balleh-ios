//
//  SearchVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 20/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire

var reuseIdentifierSearch = "gridCell"

class SearchVC: ParentViewController,UITabBarControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, loginIntimation {
    
    @IBOutlet weak var consSearchbarHeight: NSLayoutConstraint!
    @IBOutlet weak var consViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewSort: UIView!
    @IBOutlet weak var imgViewType: UIImageView!
    @IBOutlet weak var lblViewType: UILabel!
    @IBOutlet weak var lblSortby: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchCollectionView: UICollectionView!
    
    var searchResultAry = [Product]()
    var searchText:String = ""
    
    var viewType = String()
    var page:Int = 1
    var pageCount = Double()
    var limit:String = "8"
    var productsCount:String = ""
    var addFavTag:Int = 0
    
    var sortKey : String = "created_at"
    var orderKey : String = "DESC"
    var isScrolledOnce : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSort.sizeToFit()
        
        var frame = self.searchBar.frame
        frame.origin.y = 0
        frame.size.height = 55
        frame.size.width = self.view.bounds.width
        self.searchBar.frame = frame
        searchBar.translatesAutoresizingMaskIntoConstraints = true
        
        frame = self.viewSort.frame
        frame.origin.y = self.searchBar.frame.origin.y + self.searchBar.frame.size.height
        frame.size.height = 60
        frame.size.width = self.view.bounds.width
        self.viewSort.frame = frame
        viewSort.translatesAutoresizingMaskIntoConstraints = true
        
        
        // Search Bar in nav
        
//        if #available(iOS 11.0, *) {
//            navigationController?.navigationBar.prefersLargeTitles = true
//            let searchController = UISearchController(searchResultsController: self)
//            navigationItem.searchController = searchController
//            navigationItem.hidesSearchBarWhenScrolling = false
//        } else {
//            // Fallback on earlier versions
//        }
        
        frame = self.searchCollectionView.frame
        frame.origin.y = self.viewSort.frame.origin.y + self.viewSort.frame.size.height
        frame.size.height = self.view.bounds.height - (self.searchBar.frame.size.height + self.viewSort.frame.size.height + 64 + 49)
        frame.size.width = self.view.bounds.width
        self.searchCollectionView.frame = frame
        searchCollectionView.translatesAutoresizingMaskIntoConstraints = true
        
        setLeftNavigationButtonHome()
        self.setRightNavigationButton()
        updateToken()
        
      //  self.searchCollectionView.frame.origin.y = self.searchBar.frame.origin.y + self.searchBar.frame.size.height
      //  self.searchCollectionView.frame.size.height = self.view.bounds.height - (self.searchBar.frame.origin.y + self.searchBar.frame.size.height + (self.tabBarController?.tabBar.frame.size.height)!)
        
        self.title = NSLocalizedString("Search", comment: "")
        self.setupSwipeGestureRecognizer()
        let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowlayout.minimumInteritemSpacing = 1.0
        flowlayout.minimumLineSpacing = 1.0
        flowlayout.sectionInset.left = 1.0;
        flowlayout.sectionInset.right = 1.0;
        flowlayout.itemSize.width = (self.searchCollectionView.frame.width - 3) / 2
        flowlayout.itemSize.height = 255;
        self.searchCollectionView.collectionViewLayout = flowlayout
        self.searchCollectionView.translatesAutoresizingMaskIntoConstraints = true
        self.searchCollectionView.reloadData()
        lblSortby.text = NSLocalizedString("Sort by", comment: "")
        
        self.fetchSearchResult(NSLocalizedString("a", comment: ""))
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.setRightNavigationButton()
        
        if let id = UserDefaults.standard.string(forKey: "USER_ID")
        {
            userIDStr = id
        }
        
        if userIDStr == ""
        {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        }
        else
        {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        }
        
        if let type = UserDefaults.standard.value(forKey: "VIEW_TYPE")
        {
            viewType = type as! String
        }
        else
        {
            viewType = "grid"
            lblViewType.text = NSLocalizedString("Grid View", comment: "")
        }
        
        if viewType == ""
        {
            viewType = "grid"
            lblViewType.text = NSLocalizedString("Grid View", comment: "")
        }
        
        if viewType == "list"
        {
            let nib = UINib(nibName: "ListCollectionViewCell", bundle: nil)
            self.searchCollectionView.register(nib, forCellWithReuseIdentifier: "listCell")
            imgViewType.image = UIImage (named: "listView.png")
            reuseIdentifier = "listCell"
            lblViewType.text = NSLocalizedString("List View", comment: "")
        }
        else if viewType == "full"
        {
            let nib = UINib(nibName: "FullCollectionViewCell", bundle: nil)
            self.searchCollectionView.register(nib, forCellWithReuseIdentifier: "fullViewCell")
            imgViewType.image = UIImage (named: "fullView.png")
            reuseIdentifier = "fullViewCell"
            lblViewType.text = NSLocalizedString("Full View", comment: "")
        }
        else
        {
            let nib = UINib(nibName: "GridCollectionViewCell", bundle: nil)
            self.searchCollectionView.register(nib, forCellWithReuseIdentifier: "gridCell")
            imgViewType.image = UIImage (named: "gridView.png")
            reuseIdentifier = "gridCell"
            lblViewType.text = NSLocalizedString("Grid View", comment: "")
        }
        setFrames()
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        _ = self.navigationController?.popToRootViewController(animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.searchResultAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierSearch, for: indexPath) as! CollectionViewCell

        let product = self.searchResultAry[indexPath.row]
        
        cell.title.text = product.name
        
        var imageUrl = ""
        
        if let url = product.smallImage {
            imageUrl = url
        }
        
        if let title = product.title {
            cell.title.text = title
        }
        
        cell.splPriceLbl.text = product.price
        
        if let splPrice = product.specialPrice {
            if viewType == "grid"
            {
                cell.title.frame.origin.y = 155
                cell.splPriceLbl.frame.origin.y = 195
            }
            cell.priceLbl.isHidden = false
            cell.splPriceLbl.text = splPrice
            cell.priceLbl.text = product.price
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: cell.priceLbl.text!)
            attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            cell.priceLbl.attributedText = attributeString
        }
        else
        {
            cell.priceLbl.isHidden = true
            if viewType == "grid"
            {
                cell.title.frame.origin.y = 160
                cell.splPriceLbl.frame.origin.y = 203
            }
        }
        
        if viewType == "list"
        {
            cell.splPriceLbl.sizeToFit()
            cell.priceLbl.sizeToFit()
            
            cell.priceLbl.frame.origin.x = cell.splPriceLbl.frame.origin.x + cell.splPriceLbl.frame.width + 5
        }
        
        if viewType == "full"
        {
            if languageID == "2"
            {
            cell.splPriceLbl.textAlignment = .left
            cell.priceLbl.textAlignment = .left
            }
            else
            {
                cell.splPriceLbl.textAlignment = .right
                cell.priceLbl.textAlignment = .right
            }
        }
        
        let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20")
        
        var activityLoader = UIActivityIndicatorView()
        activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityLoader.center = cell.pinImage.center
        activityLoader.startAnimating()
        cell.pinImage.addSubview(activityLoader)
        
        cell.pinImage.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
            
            if image != nil
            {
                activityLoader.stopAnimating()
            }else
            {
                print("image not found")
                cell.pinImage.image = UIImage(named: "no_image")
                activityLoader.stopAnimating()
            }
        })
        
        cell.pinImage.contentMode = UIViewContentMode.scaleAspectFit
        
        let str2 = product.sku
        
        if self.checkProductInWishList(str2) == true
        {
            cell.imgFav.image = UIImage(named: "ic_favourite")
        }
        else
        {
            cell.imgFav.image = UIImage(named: "ic_unfavourite")
        }
        
        cell.addToFav.addTarget(self, action: #selector(self.addToFav(_:)), for: UIControlEvents.touchUpInside)
        cell.addToFav.tag = (indexPath as NSIndexPath).row
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let product = self.searchResultAry[indexPath.row]
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        viewController.productSku = product.sku
        viewController.productID = "\(product.id)"
        self.present(viewController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProductDetailsSegue1" {
            let productDetailObj = segue.destination as! ProductDetailVC
            productDetailObj.productID = "\(self.searchResultAry[(sender! as AnyObject).row].id)"
        }
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        searchBar.tintColor = .blue
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
        self.tabBarController?.selectedIndex = 0
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("Please Enter anything to search", comment: ""), viewController: self)
        } else {
            searchText = searchBar.text!
            self.view.endEditing(true)
            self.fetchSearchResult(searchText)
        }
    }
    
    func fetchSearchResult(_ searchKey:String) {
        SharedManager.showHUD(viewController: self)
        let urlStr = "\(ConfigUrl.baseUrl)products?searchCriteria[filter_groups][0][filters][0][field]=name&searchCriteria[filter_groups][0][filters][0][value]=%\(searchKey)%&searchCriteria[filter_groups][0][filters][0][condition_type]=like&searchCriteria[sortOrders][0][field]=\(sortKey)& searchCriteria[sortOrders][0][direction]=\(orderKey)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items")
                            {
                                if "\(result)" != "<null>"
                                {
                                    print(responseObject.result.value)
                                    var products = [Product]()
                                    
                                    if let dataResponse = result as? NSArray {
                                        for data in dataResponse {
                                            if let dataDic = data as? NSDictionary {
                                                products.append(Product(data: dataDic))
                                            }
                                        }
                                    }
                                    
                                    self.searchResultAry = products
                                    
                                    if self.searchResultAry.count == 0
                                    {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Nothing found for", comment: "")) \(searchKey)", viewController: self)
                                    }
                                    else
                                    {
                                        self.productsCount = "\(((responseObject.result.value!) as AnyObject).value(forKey: "total_count")!)"
                                        self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                                        self.searchCollectionView.reloadData()
                                        
                                        self.setFrames()
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Nothing found for", comment: "")) \(searchKey)", viewController: self)
                                }
                                self.searchCollectionView.reloadData()
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("Nothing found for", comment: "")) \(searchKey)", viewController: self)
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
    }
    
    @IBAction func clickChangeView(_ sender: Any)
    {
        if viewType == "grid"
        {
            reuseIdentifierSearch = "listCell"
            
            viewType = "list"
            imgViewType.image = UIImage (named: "listView.png")
            
            let nib = UINib(nibName: "ListCollectionViewCell", bundle: nil)
            self.searchCollectionView.register(nib, forCellWithReuseIdentifier: "listCell")
            lblViewType.text = NSLocalizedString("List View", comment: "")
            
            UserDefaults.standard.set("list", forKey: "viewType")
            
            self.searchCollectionView.reloadData()
            setFrames()
            
        }
        else if viewType == "list"
        {
            reuseIdentifierSearch = "fullViewCell"
            viewType = "full"
            
            imgViewType.image = UIImage (named: "fullView.png")
            lblViewType.text = NSLocalizedString("Full View", comment: "")
                        
            let nib = UINib(nibName: "FullCollectionViewCell", bundle: nil)
            self.searchCollectionView.register(nib, forCellWithReuseIdentifier: "fullViewCell")
            
            UserDefaults.standard.set("full", forKey: "viewType")
            
            self.searchCollectionView.reloadData()
            setFrames()
        }
        else
        {
            reuseIdentifierSearch = "gridCell"
            viewType = "grid"
            UserDefaults.standard.set("grid", forKey: "viewType")
            
            imgViewType.image = UIImage (named: "gridView.png")
            lblViewType.text = NSLocalizedString("Grid View", comment: "")
           
            let nib = UINib(nibName: "GridCollectionViewCell", bundle: nil)
            self.searchCollectionView.register(nib, forCellWithReuseIdentifier: "gridCell")
            self.searchCollectionView.reloadData()
            setFrames()
        }
    }
    
    func setFrames()
    {
        if viewType == "list"
        {
            let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            flowlayout.minimumInteritemSpacing = 1.0
            flowlayout.minimumLineSpacing = 1.0
            flowlayout.sectionInset.left = 1.0;
            flowlayout.sectionInset.right = 1.0;
            flowlayout.itemSize.width = (self.searchCollectionView.frame.size.width - 2)
            flowlayout.itemSize.height = 130;
            self.searchCollectionView .reloadData()
            self.searchCollectionView.collectionViewLayout = flowlayout
        }
            
        else if viewType == "full"
        {
            let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            flowlayout.minimumInteritemSpacing = 1.0
            flowlayout.minimumLineSpacing = 1.0
            flowlayout.sectionInset.left = 1.0;
            flowlayout.sectionInset.right = 1.0;
            flowlayout.itemSize.width = (self.searchCollectionView.frame.size.width - 2)
            flowlayout.itemSize.height = self.searchCollectionView.frame.size.width + 74;
            self.searchCollectionView .reloadData()
            self.searchCollectionView.collectionViewLayout = flowlayout
        }
        else
        {
            let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            flowlayout.minimumInteritemSpacing = 1.0
            flowlayout.minimumLineSpacing = 1.0
            flowlayout.sectionInset.left = 1.0;
            flowlayout.sectionInset.right = 1.0;
            flowlayout.itemSize.width = (self.searchCollectionView.frame.size.width - 3) / 2
            flowlayout.itemSize.height = 250;
            self.searchCollectionView .reloadData()
            self.searchCollectionView.collectionViewLayout = flowlayout
        }
    }
    
    //MARK: Sorting
    @IBAction func sortAction(_ sender: AnyObject) {
        let picker = BSModalPickerView.init(values: sortItems())
        
        picker?.present(in: self.view, with: {(_ madeChoice: Bool) -> Void in
            if madeChoice {
                switch(Int((picker?.selectedIndex)!)) {
                case 0:
                    self.sortKey = "name"
                    self.orderKey = "ASC"
                    break
                case 1:
                    self.sortKey = "name"
                    self.orderKey = "DESC"
                    break
                case 2:
                    self.sortKey = "price"
                    self.orderKey = "ASC"
                    break
                case 3:
                    self.sortKey = "price"
                    self.orderKey = "DESC"
                    break
                case 5:
                    self.sortKey = "position"
                    self.orderKey = "ASC"
                    break
                case 6:
                    self.sortKey = "position"
                    self.orderKey = "DESC"
                    break
                default:
                    self.sortKey = "created_at"
                    self.orderKey = "DESC"
                    break
                }
                self.fetchSearchResult(self.searchBar.text!)
            } else {
            }
        })
    }
    
    //MARK: Pagination
    func pullToRefresh() {
        if (self.isScrolledOnce == true) {
            return
        }
        self.isScrolledOnce = true
                
        if page != Int(self.pageCount) + 1 && searchText != "" {
            page += 1
            let urlStr = "\(ConfigUrl.baseUrl)products?searchCriteria[filter_groups][0][filters][0][field]=name&searchCriteria[filter_groups][0][filters][0][value]=%\(searchText)%&searchCriteria[filter_groups][0][filters][0][condition_type]=like&searchCriteria[sortOrders][0][field]=\(sortKey)& searchCriteria[sortOrders][0][direction]=\(orderKey)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)"
            
            SharedManager.showHUD(viewController: self)
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet() {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        if responseObject.result.isSuccess {
                            SharedManager.dismissHUD(viewController: self)
                            if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                                if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items") {
                                    if "\(result)" != "<null>" {
                                        var products = [Product]()
                                        
                                        if let dataResponse = result as? NSArray {
                                            for data in dataResponse {
                                                if let dataDic = data as? NSDictionary {
                                                    products.append(Product(data: dataDic))
                                                }
                                            }
                                        }
                                        
                                        self.searchResultAry.append(contentsOf: products)
                                        
                                        // self.setFrames()
                                        self.searchCollectionView.reloadData()
                                    } else {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                        self.isScrolledOnce = false
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        }
        else
        {
            SharedManager.dismissHUD(viewController: self)
            self.isScrolledOnce = false
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let offset: CGPoint = scrollView.contentOffset
        let bounds: CGRect = scrollView.bounds
        let size: CGSize = scrollView.contentSize
        let inset: UIEdgeInsets = scrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 10
        if y > h + reload_distance
        {
            if isScrolledOnce == false
            {
                self.pullToRefresh()
            }
        }
    }
    
    //MARK: Add to Wishlist
    
    @objc func addToFav(_ sender : UIButton) {
        if(UserDefaults.standard.object(forKey: "USER_ID") != nil) {
            self.wishlist(index: sender.tag)
        } else {
            addFavTag = sender.tag
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            viewController.delegate = self
            self.present(viewController, animated: true, completion: { () -> Void in
            })
        }
    }
    
    func wishlist(index: Int) {
        let product = self.searchResultAry[index]
        let isHave:Bool = self.checkProductInWishList("\(product.sku)")
        
        if isHave {
            self.deleteWishlist(productId: "\(product.id)")
            Tools.removeFromWishList(sku: product.sku)
            self.showToast(message: NSLocalizedString("Successfully removed from your wishlist", comment: ""))
        } else {
            self.addToWishlist(productId: "\(product.id)")
            Tools.addToWishList(product: product)
            self.showToast(message: NSLocalizedString("Successfully added to your wishlist", comment: ""))
        }
        self.searchCollectionView.reloadData()
    }
    
    func addToWishlist(productId : String) {
        let request = AddWishlist(productId: productId)
        request.start()
    }
    
    func deleteWishlist(productId : String) {
        let request = DeleteWishlist(productId: productId)
        request.start()
    }
    
    func loginSuccess()
    {
        self.wishlist(index: addFavTag)
    }
    
    func loginFailure() {
        
    }
}
