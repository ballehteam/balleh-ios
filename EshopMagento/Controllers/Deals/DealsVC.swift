//
//  DealsVC.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 20/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import Intercom

class DealsVC: ParentViewController, loginIntimation, UICollectionViewDelegateFlowLayout {
    
    var reuseIdentifier = "gridCell"
    
    @IBOutlet weak var viewBlur: UIView!
    @IBOutlet weak var viewSubCategories: UIView!
    @IBOutlet weak var tblSubCategories: UITableView!
    @IBOutlet weak var mainTableVw: UITableView!
    @IBOutlet weak var lblCatTitle: UILabel!
    
    var subCategoryAry2 = NSMutableArray()
    var imageUrl:URL!
    var productListAry:NSMutableArray = []
    var addFavTag:Int = 0
    var page:Int = 1
    var pageCount = Double()
    var filterID:String = String()
    var limit:String = "6"
    var categoryID:String = ""
    var filterAry:NSMutableArray = []
    var productsCount:NSString = ""
    var catTitle:String = ""
    
    var isScrolledOnce : Bool = false
    var sortPage:Int = 1
    var sortPageCount = Double()
    var sortProductsCount:NSString = ""
    var isSorted : Bool = false
    var sortKey = "created_at"
    var orderKey = "DESC"
    var isFromHome = Bool()
    var isDeals = Bool()
    
    var subIds = String()
    
    lazy var emptyFooter : UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 25))
        let label = UILabel(frame: CGRect(x: view.frame.origin.x + 10, y: 0, width: view.frame.width - 20, height: view.frame.height))
        label.font = UIFont.appFontWith(size: 16)
        label.textColor = UIColor.white
        let string = NSLocalizedString("No deals currently", comment: "")
        label.text = string
        view.addSubview(label)
        view.backgroundColor = .lightGray
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated:true)
        self.navigationItem.hidesBackButton = true
        expandedSection.removeAllObjects()
        self.categoryID = dealsID
        catTitle = "Deals"
        self.title = NSLocalizedString("Deals", comment: "")
        setLeftNavigationButtonHome()
        mainTableVw.tableFooterView = UIView()
        tblSubCategories.tableFooterView = UIView()
        self.mainTableVw.dataSource = self
        self.mainTableVw.delegate = self
        
        let urlStr = "\(ConfigUrl.baseUrl)dealsslider"
        SharedManager.showHUD(viewController: self)
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        print(setFinalURl)
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            print(responseObject.result.value)
                            if let result = responseObject.result.value as? NSArray,
                                result.count != 0 {
                                self.productListAry = result.mutableCopy() as! NSMutableArray
                                self.mainTableVw.tableFooterView = UIView()
                            } else {
                                self.mainTableVw.tableFooterView = self.emptyFooter
                            }
                            
                            self.mainTableVw.reloadData()
                            
                            SharedManager.dismissHUD(viewController: self)
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                    //  SharedManager.dismissHUD(viewController: self)
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        
        if productListAry.count == 0 {
            //SharedManager.showHUD(viewController: self)
            
            
        } else {
            
        }
        
    }
    
    func updatePage(id : String)
    {
        SharedManager.showHUD(viewController: self)
        
        var urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(id)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=\(sortKey)& searchCriteria[sortOrders][0][direction]=\(orderKey)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=1"
        
        if isFiltered
        {
            urlStr = "\(urlStr)\(filterUrlStr)"
        }
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                    if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                        if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items") {
                            if "\(result)" != "<null>" {
                                self.productListAry = (result as! NSArray).mutableCopy() as! NSMutableArray
                                
                                if self.productListAry.count == 0 {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                } else {
                                    self.productsCount = "\(((responseObject.result.value!) as AnyObject).value(forKey: "total_count")!)" as NSString
                                    self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                                    
                                    self.mainTableVw.reloadData()
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                        }
                    }
                    else
                    {
                        SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                    }
                }
                if responseObject.result.isFailure
                {
                    SharedManager.dismissHUD(viewController: self)
                    let error : Error = responseObject.result.error!
                    print(error.localizedDescription)
                }
                // SharedManager.dismissHUD(viewController: self)
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.hidesBackButton = true
        if let id = UserDefaults.standard.string(forKey: "USER_ID") {
            userIDStr = id
        }
        
        if userIDStr == "" {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        } else {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        }
        
        self.updateCartBadge()
        self.navigationItem.hidesBackButton = true
        self.setRightNavigationButton()
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeRightMenu))
        viewBlur.addGestureRecognizer(tap)
        
        self.viewBlur.isHidden = true
        self.viewSubCategories.frame.origin.x = self.view.bounds.width
        
        self.delay(2, closure: {
                if let bool = UserDefaults.standard.string(forKey: "filter") {
                    print(bool)
                    if bool == "1" {
                        isFiltered = true
                    }
                }
                
                if isFiltered {
                    self.page = 1
                    self.updatePage(id: self.subIds)
                }
        })
    }
    
    @objc func closeRightMenu()
    {
        self.viewBlur.isHidden = true
        
        UIView.animate(withDuration: 0.50, animations: {
            self.viewSubCategories.frame.origin.x = self.view.bounds.width
        })
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        isSorted = false
        self.isFromHome = false
        
        isFiltered = false
        UserDefaults.standard.removeObject(forKey: "filter")
    }
    
    //MARK: TableView methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == mainTableVw {
            return self.productListAry.count
        } else if tableView == mainTableView {
            return self.mainTblArray.count
        } else if tableView == tblSubCategories {
            return 1
        } else {
            if categoryDict.count != 0 {
                return categoryDict.count
            } else {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mainTableVw {
            return 1
        } else if tableView == mainTableView {
            return mainTblArray[section].count
        } else if tableView == tblSubCategories {
            return subCategoryAry2.count
        } else {
            if categoryDict.count != 0 {
                return ((categoryDict[section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count
            } else {
                return 0
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == mainTableVw {
            let customAttributes = self.productListAry.object(at: section) as AnyObject
            let title = customAttributes.value(forKey: "title") as? String
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 18))
            let label = UILabel(frame: CGRect(x: 10, y: 5, width: tableView.frame.size.width - 20, height: 25))
            label.font = UIFont.appFontWith(size: 15)
            label.textColor = UIColor.darkGray
            label.text = title
            view.addSubview(label)
            view.backgroundColor = UIColor.white
            return view
        } else if tableView == mainTableView {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 18))
            let label = UILabel(frame: CGRect(x: 10, y: 5, width: tableView.frame.size.width - 20, height: 25))
            label.font = UIFont.appFontWith(size: 15)
            label.textColor = UIColor.white
            let string = "\(titleArr[section])"
            label.text = string
            view.addSubview(label)
            view.backgroundColor = UIColor(red: 91.0 / 255.0, green: 91.0 / 255.0, blue: 91.0 / 255.0, alpha: 1.0)
            return view
            
            /* let view = DetailCell ()
             view.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 44)
             view.backgroundColor = .green
             // view.imgArrow.image = UIImage (named: "left-arrow.png")
             // view.title.text = "Main category"
             // view.imgIcon.image = UIImage (named: "ic_favorite.png")
             
             return view*/
        } else if tableView == tblSubCategories {
            let emptyView = UIView()
            return emptyView
        } else {
            let sectionView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 40))
            sectionView.backgroundColor = .white
            
            sectionView.tag = section
            let viewLabel: UILabel = UILabel(frame: CGRect(x: 15, y: 0, width: mainTableView.frame.size.width - 30, height: 40))
            viewLabel.backgroundColor = UIColor.clear
            viewLabel.textColor = UIColor.black
            viewLabel.font = UIFont.appFontWith(size: 15)
            viewLabel.text =  (categoryDict.object(at: section) as AnyObject).value(forKey: "name") as? String
            sectionView.addSubview(viewLabel)
            
            let line: UILabel = UILabel(frame: CGRect(x: 5, y: 49, width: mainTableView.frame.size.width - 10, height: 1))
            line.backgroundColor = UIColor.lightGray
            sectionView.addSubview(line)
            
            catID.add("\((categoryDict.object(at: section) as AnyObject).value(forKey: "id")!)")
            let arrowImg = UIImageView(frame: CGRect(x: mainTableView.frame.size.width - 40, y: 12, width: 16, height: 17))
            arrowImg.contentMode = .scaleAspectFit
            let bool = (categoryDict[section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool
            if bool == false {
                arrowImg.image = UIImage(named: "remove")
                
                if languageID == "1" {
                    arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 18, height: 15)
                } else {
                    arrowImg.frame = CGRect(x: 10, y: 12, width: 18, height: 15)
                }
            }
            else
            {
                arrowImg.image = UIImage(named: "add")
                if languageID == "1"
                {
                    
                    arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 18, height: 15)
                }
                else
                {
                    arrowImg.frame = CGRect(x: 10, y: 12, width: 18, height: 15)
                }
            }
            
            sectionView.addSubview(arrowImg)
            /********** Add a custom Separator with Section view *******************/
            let separatorLineView: UIView = UIView(frame: CGRect(x: 0, y: 40, width: mainTableView.frame.size.width, height: 0.3))
            separatorLineView.backgroundColor = UIColor.lightGray
            //separatorLineView.layer.borderWidth = 0.5
            // sectionView.addSubview(separatorLineView)
            /********** Add UITapGestureRecognizer to SectionView   **************/
            let headerTapped: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(self.sectionHeaderTapped(_:)))
            sectionView.addGestureRecognizer(headerTapped)
            
            sectionView.frame.size.height = 40
            
            return sectionView
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == mainTableVw {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "dealCell", for: indexPath) as! DealsCell
            
            let customAttributes = self.productListAry.object(at: (indexPath as NSIndexPath).section) as AnyObject
            
            var imageUrl = customAttributes.value(forKey: "image_url") as! String
            
            imageUrl = customAttributes.value(forKey: "image_url") as! String
            
            let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
            
            var activityLoader = UIActivityIndicatorView()
            activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityLoader.center = cell.dealImage.center
            activityLoader.startAnimating()
            cell.dealImage.addSubview(activityLoader)
            cell.dealImage.contentMode = .scaleAspectFit
            
            cell.dealImage.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
                print(image?.size.height)
                if image != nil {
                    activityLoader.stopAnimating()
                } else {
                    print("image not found")
                    cell.dealImage.image = UIImage(named: "no_image")
                    activityLoader.stopAnimating()
                }
            })
            
            return cell
        }
        else if tableView == mainTableView
        {
            if indexPath.section != 1
            {
                if indexPath.section == 0
                {
                    if indexPath.row != 0
                    {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                        
                        /* if cell1 == nil {
                         var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                         for view: UIView in views {
                         if (view is UITableViewCell) {
                         cell1 = view as? DetailCell
                         }
                         }
                         }*/
                        
                        let subArr = mainTblArray[indexPath.section]
                        cell1.title.text = subArr[indexPath.row]
                        
                        let subArr2 = iconsArr[indexPath.section]
                        cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                        if languageID == "1"
                        {
                            cell1.imgArrow.image = UIImage(named: "left-arrow")
                        }
                        else
                        {
                            cell1.imgArrow.image = UIImage(named: "right-arrow1")
                        }
                        cell1.imgArrow.contentMode = .scaleAspectFit
                        cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                        cell1.imgIcon.tintColor = UIColor.gray
                        cell1.selectionStyle = .none
                        
                        return cell1
                    }
                    else
                    {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as! LanguageCell
                        
                        if languageID == "1"
                        {
                            //selectedSegmentIndex = 0
                            cell1.segmentView.selectedSegmentIndex = 0
                        }
                        else
                        {
                            cell1.segmentView.selectedSegmentIndex = 1
                        }
                        
                        cell1.segmentView.addTarget(self, action: #selector(ParentViewController.clickLang(_:)), for: UIControlEvents.valueChanged)
                        
                        return cell1
                    }
                }
                else
                {
                    let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                    
                    /* if cell1 == nil {
                     var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                     for view: UIView in views {
                     if (view is UITableViewCell) {
                     cell1 = view as? DetailCell
                     }
                     }
                     }*/
                    
                    let subArr = mainTblArray[indexPath.section]
                    cell1.title.text = subArr[indexPath.row]
                    
                    let subArr2 = iconsArr[indexPath.section]
                    cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                    if languageID == "1"
                    {
                        cell1.imgArrow.image = UIImage(named: "left-arrow")
                    }
                    else
                    {
                        cell1.imgArrow.image = UIImage(named: "right-arrow1")
                    }
                    cell1.imgArrow.contentMode = .scaleAspectFit
                    cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                    cell1.imgIcon.tintColor = UIColor.gray
                    cell1.selectionStyle = .none
                    
                    return cell1
                }
            }
            else
            {
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "CatCell") as! categoryTCell
                cell1.insideTableView.isScrollEnabled = false
                cell1.insideTableView.dataSource = self
                cell1.insideTableView.delegate = self
                cell1.insideTableView.reloadData()
                cell1.selectionStyle = .none
                
                return cell1
            }
        }
        else if tableView == tblSubCategories
        {
            var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if cell == nil
            {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            }
            cell?.textLabel?.font = UIFont.appFontWith(size: 14)
            cell?.textLabel?.text = (subCategoryAry2.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
            
            return cell!
        }
        else
        {
            var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if cell == nil
            {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            }
            
            cell?.textLabel?.font = UIFont.appFontWith(size: 15)
            if languageID == "2"
            {
                cell?.textLabel?.textAlignment = .right
            }
            cell?.textLabel?.text = (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "name") as? String
            cell?.selectionStyle = .none
            
            return cell!
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblSubCategories
        {
            var ary = NSArray()
            
            subCategoryID = "\(String(describing: (subCategoryAry2.object(at: (indexPath as NSIndexPath).row) as! NSObject).value(forKey: "id")!))" as NSString
            
            SharedManager.showHUD(viewController: self)
            
            let urlStr = "\(ConfigUrl.baseUrl)categories?rootCategoryId=\(subCategoryID)&depth=1"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        SharedManager.dismissHUD(viewController: self)
                        
                        if responseObject.result.isSuccess
                        {
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                ary = (responseObject.result.value! as AnyObject).value(forKey: "children_data") as! NSArray
                                
                                if ary.count > 0
                                {
                                    //  self.subCategoryAry2.removeAllObjects()
                                    
                                    let VC = self.storyboard!.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
                                    VC.isFromHome = true
                                    VC.catTitle = "\(String(describing: (responseObject.result.value! as AnyObject).value(forKey: "name")!))"
                                    VC.categoryID = "\(String(describing: (responseObject.result.value! as AnyObject).value(forKey: "id")!))"
                                    //  self.present(VC, animated: false, completion: nil)
                                    self.navigationController?.pushViewController(VC, animated: false)
                                }
                                else
                                {
                                    self.viewSubCategories.isHidden = true
                                    self.viewBlur.isHidden = true
                                    
                                    let VC = self.storyboard!.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
                                    
                                    VC.categoryID = "\((self.subCategoryAry2.object(at: (indexPath as NSIndexPath).row) as! NSObject).value(forKey: "id")!)"
                                    self.isFromHome = false
                                    VC.catTitle = (self.subCategoryAry2.object(at: (indexPath as NSIndexPath).row) as! NSObject).value(forKey: "name") as! String
                                    
                                    self.navigationController?.pushViewController(VC, animated: true)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }

        }
        else if tableView == mainTableView
        {
            let subArr = mainTblArray[indexPath.section]

            let action = subArr[indexPath.row]
            
            if action == NSLocalizedString("Home", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Login", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.present(viewController, animated: true, completion: nil)
            }
            else if action == NSLocalizedString("Deals", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Live Chat", comment: "")
            {
                Intercom.presentMessenger()
            }
            else if action == NSLocalizedString("Track Order", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Share", comment: "")
            {
                let textToShare = [ Tools.shareText ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                
                self.present(activityViewController, animated: true, completion: nil)
            }
        } else if tableView == mainTableVw {
            let customAttributes = self.productListAry.object(at: indexPath.section) as AnyObject
            if let category = customAttributes.value(forKey: "category_id") as? String {
                performSegue(withIdentifier: "deal", sender: category)
            }
        } else {
            let id = "\(String(describing: (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "id")!))"
            print("Category ID: \(id)")
            categoryID = id
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            viewController.subIds = id
            viewController.isFromHome = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "deal" {
            if let vc = segue.destination as? DealsProductsVC,
                let category = sender as? String {
                vc.categoryID = category
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == mainTableVw {
            return CGFloat(200)
        } else if tableView == mainTableView {
            if indexPath.section != 1 {
                return 44
            } else {
                if categoryDict.count != 0 {
                    var collapsed = false
                    
                    var expandedRowHeight = 0
                    
                    for i in 0..<expandedSection.count
                    {
                        let index = Int("\(expandedSection[i])")!
                        
                        expandedRowHeight = expandedRowHeight + (((categoryDict[index] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count * 40)
                    }
                    
                    return CGFloat(expandedRowHeight) + CGFloat(categoryDict.count * 44)
                } else {
                    return UITableViewAutomaticDimension
                }
            }
        } else if tableView == tblSubCategories {
            return 40
        } else {
            let collapsed = ((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool)
            
            if collapsed {
                return 0
            } else {
                return 40
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == mainTableVw {
            return 35
        } else if tableView == mainTableView {
            if section == 0 {
                return 0
            } else {
                return 35
            }
        } else if tableView == tblSubCategories {
            return 0
        } else {
            return 44
        }
    }
    
    //MARK: Pagination
    func pullToRefresh() {
        if isSorted == true {
            /*if sortPage != Int(self.sortPageCount) + 1 && !categoryID.isEmpty
             {
             sortPage += 1
             BusinessManager.getSortedProductList(categoryID, languageID: languageID as String, page:sortPage, limit:limit, sort:sortKey, order:orderKey, filters:filterID, completionHandler: { (result) -> () in
             
             SharedManager.dismissHUD(viewController: self)
             
             self.productListAry.addObjects(from: result.value(forKey: "products") as! NSMutableArray as [AnyObject])
             
             self.mainTableVw.reloadData()
             self.sortProducts()
             })
             }
             else
             {
             SharedManager.dismissHUD(viewController: self)
             }*/
        } else {
            if (self.isScrolledOnce == true) {
                return
            }
            self.isScrolledOnce = true
            
            if page != Int(self.pageCount) + 1 && !categoryID.isEmpty {
                page += 1
                var urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(self.subIds)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=created_at& searchCriteria[sortOrders][0][direction]=DESC&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)"
                
                if isFiltered {
                    urlStr = "\(urlStr)\(filterUrlStr)"
                }
                
                SharedManager.showHUD(viewController: self)
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.get.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
                
                if Connectivity.isConnectedToInternet() {
                    Alamofire.request(request).responseJSON
                        { (responseObject) -> Void in
                            if responseObject.result.isSuccess {
                                SharedManager.dismissHUD(viewController: self)
                                if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                                    if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items") {
                                        print("\(result)")
                                        if "\(result)" != "<null>" {
                                            // self.productListAry = (result as! NSArray).mutableCopy() as! NSMutableArray
                                            self.productListAry.addObjects(from: (result as! NSArray) as! [Any])
                                            
                                            self.mainTableVw.reloadData()
                                        } else {
                                            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                        }
                                    } else {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                    }
                                } else {
                                    SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                }
                            }
                            if responseObject.result.isFailure {
                                SharedManager.dismissHUD(viewController: self)
                                let error : Error = responseObject.result.error!
                                print(error.localizedDescription)
                            }
                            self.isScrolledOnce = false
                    }
                } else {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            } else {
                SharedManager.dismissHUD(viewController: self)
                self.isScrolledOnce = false
            }
        }
        // SharedManager.dismissHUD(viewController: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableVw {
            let offset: CGPoint = scrollView.contentOffset
            let bounds: CGRect = scrollView.bounds
            let size: CGSize = scrollView.contentSize
            let inset: UIEdgeInsets = scrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 10
            if y > h + reload_distance {
                if isScrolledOnce == false {
                    self.pullToRefresh()
                }
            }
        }
    }
    
    func loginSuccess() {
        let index:IndexPath = IndexPath(row: addFavTag, section: 0)
        
        let isHave:Bool = self.checkProductInWishList("\(self.productListAry.value(forKey: "sku"))")
        
        if isHave == true {
            if UserDefaults.standard.object(forKey: kAddToWishlist) != nil
            {
                let data = UserDefaults.standard.object(forKey: kAddToWishlist) as! Data
                let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                
                let tempArr = NSMutableArray()
                tempArr.addObjects(from: favAry as! [Any])
                
                if isHave
                {
                    let str2 = self.productListAry.value(forKey: "sku") as! String
                    
                    for i in 0..<tempArr.count
                    {
                        let str1 = (tempArr[i] as AnyObject).value(forKey: "sku") as! String
                        
                        if str1 == str2
                        {
                            favAry.removeObject(at: i)
                            
                            //  self.addToWishListBtn.setBackgroundImage(UIImage (named: "ic_unfavourite"), for: .normal)
                        }
                    }
                }
                let tempData = NSKeyedArchiver.archivedData(withRootObject: favAry)
                UserDefaults.standard.set(tempData, forKey: kAddToWishlist)
            }
        }
        else
        {
            if UserDefaults.standard.object(forKey: kAddToWishlist) != nil
            {
                let data = UserDefaults.standard.object(forKey: kAddToWishlist) as! Data
                let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
                
                //   let currentArr : NSDictionary  = self.productListAry.objectAtIndex(sender.tag) as NSDictionary
                //  favAry.add(currentArr)
                
                let tempData = NSKeyedArchiver.archivedData(withRootObject: favAry)
                UserDefaults.standard.set(tempData, forKey: kAddToWishlist)
            }
            else
            {
                //  let currentArr : NSDictionary  = self.productListAry.objectAtIndex(sender.tag) as NSDictionary
                
                let favAry = NSMutableArray()
                // favAry.add(currentArr)
                
                let data1 = NSKeyedArchiver.archivedData(withRootObject: favAry)
                UserDefaults.standard.set(data1, forKey: kAddToWishlist)
            }
            //  self.addToWishListBtn.setBackgroundImage(UIImage (named: "ic_unfavourite"), for: .normal)
        }
        self.mainTableVw.reloadData()
    }
    
    func loginFailure()
    {
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
