//
//  DealsProductsVC.swift
//  EshopMagento
//
//  Created by Abdalrahim Abdullah on 01/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import Intercom

class DealsProductsVC: ParentViewController, UICollectionViewDataSource, UICollectionViewDelegate, loginIntimation, UICollectionViewDelegateFlowLayout {
    
    var reuseIdentifier = "gridCell"
    
    @IBOutlet weak var viewBlur: UIView!
    @IBOutlet weak var viewSubCategories: UIView!
    @IBOutlet weak var tblSubCategories: UITableView!
    @IBOutlet weak var mainTableVw: UITableView!
    @IBOutlet weak var lblCatTitle: UILabel!
    
    var subCategoryAry2 = NSMutableArray()
    var imageUrl:URL!
    var imageUrlStr:String = ""
    
    var productListAry = [Product]()
    
    var addFavTag:Int = 0
    var page:Int = 1
    var pageCount = Double()
    var filterID:String = String()
    var limit:String = "6"
    var categoryID:String = ""
    var filterAry:NSMutableArray = []
    var productsCount:NSString = ""
    var catTitle:String = ""
    
    var viewType = String()
    var isScrolledOnce : Bool = false
    var sortPage:Int = 1
    var sortPageCount = Double()
    var sortProductsCount:NSString = ""
    var isSorted : Bool = false
    var sortKey = "created_at"
    var orderKey = "DESC"
    var isFromHome = Bool()
    var isDeals = Bool()
    
    var subIds = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        expandedSection.removeAllObjects()
        catTitle = "Deals"
        self.title = NSLocalizedString("Deals", comment: "")
        setLeftNavigationButtonHome()
        
        let urlStr = "\(ConfigUrl.baseUrl)categories/\(self.categoryID)"
        SharedManager.showHUD(viewController: self)
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        print(setFinalURl)
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            let result = (responseObject.result.value!) as AnyObject
                            print(result)
                            
                            if let children = result.value(forKey: "children") {
                                self.subIds = "\(self.categoryID),\(children as! String)"
                            } else {
                                self.subIds = "\(self.categoryID)"
                            }
                            
                            self.updatePage(id: self.subIds)
                            
                            if let attributes = result.value(forKey: "custom_attributes")
                            {
                                for i in 0..<(attributes as AnyObject).count
                                {
                                    let name = (attributes as AnyObject)[i].value(forKey: "attribute_code") as! String
                                    
                                    if name == "image"
                                    {
                                        let value = (attributes as AnyObject)[i].value(forKey: "value") as! String
                                        self.imageUrlStr = "\(ConfigUrl.imageUrl)\(value)"
                                    }
                                }
                            }
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                    //  SharedManager.dismissHUD(viewController: self)
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        
        if productListAry.count == 0
        {
            //SharedManager.showHUD(viewController: self)
            
            
        }
        else
        {
            
        }
        
        /* if !categoryID.isEmpty
         {
         BusinessManager.getFilterProducts(categoryID, languageID: languageID as String, completionHandler: { (result) -> () in
         
         self.filterAry = result.value(forKey: "filters") as! NSMutableArray
         })
         }*/
        
        mainTableVw.tableFooterView = UIView()
        tblSubCategories.tableFooterView = UIView()
        self.mainTableVw.dataSource = self
        self.mainTableVw.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func updatePage(id : String)
    {
        SharedManager.showHUD(viewController: self)
        
        var urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(id)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=\(sortKey)& searchCriteria[sortOrders][0][direction]=\(orderKey)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=1"
        
        if isFiltered
        {
            urlStr = "\(urlStr)\(filterUrlStr)"
        }
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200" {
                            if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items") {
                                if "\(result)" != "<null>" {
                                    var products = [Product]()
                                    
                                    if let dataResponse = result as? NSArray {
                                        for data in dataResponse {
                                            if let dataDic = data as? NSDictionary {
                                                products.append(Product(data: dataDic))
                                            }
                                        }
                                    }
                                    
                                    self.productListAry = products
                                    
                                    if self.productListAry.count == 0 {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                    } else {
                                        self.productsCount = "\(((responseObject.result.value!) as AnyObject).value(forKey: "total_count")!)" as NSString
                                        self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                                        
                                        self.sortProducts()
                                        self.mainTableVw.reloadData()
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
                    // SharedManager.dismissHUD(viewController: self)
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        if let id = UserDefaults.standard.string(forKey: "USER_ID") {
            userIDStr = id
        }
        
        if userIDStr == "" {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        } else {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        }
        
        self.updateCartBadge()
        
        self.setRightNavigationButton()
        
        if let type = UserDefaults.standard.value(forKey: "VIEW_TYPE")
        {
            viewType = type as! String
        }
        else
        {
            viewType = "grid"
        }
        
        if viewType == ""
        {
            viewType = "grid"
        }
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeRightMenu))
        viewBlur.addGestureRecognizer(tap)
        
        self.viewBlur.isHidden = true
        self.viewSubCategories.frame.origin.x = self.view.bounds.width
    
        
        self.delay(2, closure: {
            if let bool = UserDefaults.standard.string(forKey: "filter") {
                print(bool)
                if bool == "1" {
                    isFiltered = true
                }
            }
            
            if isFiltered {
                self.page = 1
                self.updatePage(id: self.subIds)
            }
        })
    }
    
    //MARK: Filter by Categories
    @IBAction func clickCategories(_ sender: AnyObject) {
        if self.subCategoryAry2.count == 0 {
            
        } else {
            if !isFromHome {
                self.categoryID = "\((subCategoryAry2[0] as AnyObject).value(forKey: "parent_id")!)"
            }
        }
        
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "\(ConfigUrl.baseUrl)categories?rootCategoryId=\(self.categoryID)&depth=1"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet() {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let ary = (responseObject.result.value! as AnyObject).value(forKey: "children_data") as! NSArray
                            
                            if ary.count > 0 {
                                self.subCategoryAry2 = ary.mutableCopy() as! NSMutableArray
                                
                                self.lblCatTitle.text = "\(String(describing: (responseObject.result.value! as AnyObject).value(forKey: "name")!))"
                                
                                self.viewBlur.isHidden = false
                                self.viewSubCategories.isHidden = false
                                
                                self.tblSubCategories.reloadData()
                                
                                UIView.animate(withDuration: 0.50, animations: {
                                    self.viewSubCategories.frame.origin.x = self.view.bounds.width - self.viewSubCategories.frame.width
                                })
                            } else {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("There is no Sub categories available", comment: ""))", viewController: self)
                            }
                        } else {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        } else {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    @objc func closeRightMenu()
    {
        self.viewBlur.isHidden = true
        
        UIView.animate(withDuration: 0.50, animations: {
            self.viewSubCategories.frame.origin.x = self.view.bounds.width
        })
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        isSorted = false
        self.isFromHome = false
        
        isFiltered = false
        UserDefaults.standard.removeObject(forKey: "filter")
    }
    
    func sortProducts()
    {
        let cell:SubCategoryTableViewCell = self.mainTableVw.dequeueReusableCell(withIdentifier: "collectionCell") as! SubCategoryTableViewCell
        
        /* for (index,tempDic) in self.productListAry.enumerated()
         {
         let temp:NSMutableDictionary = tempDic as! NSMutableDictionary
         
         let priceWithOutSymbol:String = String((temp.object(forKey: "price") as! String).characters.dropFirst())
         
         let myNumber = self.formatCurrency(value: Double(priceWithOutSymbol)!)
         
         temp.setObject(myNumber, forKey: "price_sort" as NSCopying)
         self.productListAry.replaceObject(at: index, with: temp)
         }*/
        
        if viewType == "list"
        {
            let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            flowlayout.minimumInteritemSpacing = 1.0
            flowlayout.minimumLineSpacing = 1.0
            flowlayout.sectionInset.left = 1.0;
            flowlayout.sectionInset.right = 1.0;
            flowlayout.itemSize.width = self.view.bounds.size.width - 2
            flowlayout.itemSize.height = 130;
            cell.productListCollectVw.collectionViewLayout = flowlayout
            self.mainTableVw .reloadData()
        }
        else if viewType == "full"
        {
            let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            flowlayout.minimumInteritemSpacing = 1.0
            flowlayout.minimumLineSpacing = 1.0
            flowlayout.sectionInset.left = 1.0;
            flowlayout.sectionInset.right = 1.0;
            flowlayout.itemSize.width = self.view.bounds.size.width - 2
            cell.productListCollectVw.collectionViewLayout = flowlayout
            //  flowlayout.itemSize.height = self.view.bounds.size.width + 74;
            self.mainTableVw .reloadData()
        }
        else
        {
            let flowlayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            flowlayout.minimumInteritemSpacing = 1.0
            flowlayout.minimumLineSpacing = 1.0
            flowlayout.sectionInset.left = 1.0;
            flowlayout.sectionInset.right = 1.0;
            flowlayout.itemSize.width = self.view.bounds.size.width - 3 / 2
            flowlayout.itemSize.height = 250;
            cell.productListCollectVw.collectionViewLayout = flowlayout
            self.mainTableVw .reloadData()
        }
    }
    
    //MARK: TableView methods
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == mainTableVw
        {
            return 2
        }
        else if tableView == mainTableView
        {
            return self.mainTblArray.count
        }
        else if tableView == tblSubCategories
        {
            return 1
        }
        else
        {
            if categoryDict.count != 0
            {
                return categoryDict.count
            }
            else
            {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mainTableVw {
            return 1
        } else if tableView == mainTableView {
            return mainTblArray[section].count
        } else if tableView == tblSubCategories {
            return subCategoryAry2.count
        } else {
            if categoryDict.count != 0 {
                return ((categoryDict[section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count
            } else {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == mainTableVw {
            if section == 0 {
                return nil
            } else {
                let CellIdentifier: String = "section2Cell"
                let headerView: SubCategoryTableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as! SubCategoryTableViewCell?
                
                headerView?.btnFilter.addTarget(self, action: #selector(self.filterAction(_:)), for: .touchUpInside)
                
                headerView?.btnSort.addTarget(self, action: #selector(self.sortAction(_:)), for: .touchUpInside)
                
                headerView?.lblSortby.text = NSLocalizedString("Sort by", comment: "")
                headerView?.lblFilter.text = NSLocalizedString("Filter", comment: "")
                headerView?.lblCategories.text = NSLocalizedString("Categories", comment: "")
                
                if viewType == "list"
                {
                    headerView?.imgViewType.image = UIImage (named: "listView.png")
                    headerView?.lblViewType.text = NSLocalizedString("List View", comment: "")
                }
                else if viewType == "full"
                {
                    headerView?.imgViewType.image = UIImage (named: "fullView.png")
                    headerView?.lblViewType.text = NSLocalizedString("Full View", comment: "")
                }
                else
                {
                    headerView?.imgViewType.image = UIImage (named: "gridView")
                    headerView?.lblViewType.text = NSLocalizedString("Grid View", comment: "")
                }
                
                if headerView == nil
                {
                    print("No cells with matching CellIdentifier loaded from your storyboard")
                    
                    //   NSException.raise("headerView == nil..", format: "No cells with matching CellIdentifier loaded from your storyboard")
                }
                return headerView!
            }
        }
        else if tableView == mainTableView
        {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 18))
            let label = UILabel(frame: CGRect(x: 10, y: 5, width: tableView.frame.size.width - 20, height: 25))
            label.font = UIFont.appFontWith(size: 15)
            label.textColor = UIColor.white
            let string = "\(titleArr[section])"
            label.text = string
            view.addSubview(label)
            view.backgroundColor = UIColor(red: 91.0 / 255.0, green: 91.0 / 255.0, blue: 91.0 / 255.0, alpha: 1.0)
            return view
            
            /* let view = DetailCell ()
             view.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 44)
             view.backgroundColor = .green
             // view.imgArrow.image = UIImage (named: "left-arrow.png")
             // view.title.text = "Main category"
             // view.imgIcon.image = UIImage (named: "ic_favorite.png")
             
             return view*/
        }
        else if tableView == tblSubCategories
        {
            let emptyView = UIView()
            return emptyView
        }
        else
        {
            let sectionView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 40))
            sectionView.backgroundColor = .white
            
            sectionView.tag = section
            let viewLabel: UILabel = UILabel(frame: CGRect(x: 15, y: 0, width: mainTableView.frame.size.width - 10, height: 40))
            viewLabel.backgroundColor = UIColor.clear
            viewLabel.textColor = UIColor.black
            viewLabel.font = UIFont.appFontWith(size: 16)
            viewLabel.text =  (categoryDict.object(at: section) as AnyObject).value(forKey: "name") as? String
            sectionView.addSubview(viewLabel)
            
            let line: UILabel = UILabel(frame: CGRect(x: 5, y: 49, width: mainTableView.frame.size.width - 10, height: 1))
            line.backgroundColor = UIColor.lightGray
            sectionView.addSubview(line)
            
            catID.add("\((categoryDict.object(at: section) as AnyObject).value(forKey: "id")!)")
            let arrowImg = UIImageView(frame: CGRect(x: mainTableView.frame.size.width - 40, y: 12, width: 16, height: 17))
            arrowImg.contentMode = .scaleAspectFit
            let bool = (categoryDict[section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool
            if bool == false
            {
                arrowImg.image = UIImage(named: "remove")
                
                if languageID == "1"
                {
                    arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 18, height: 15)
                }
                else
                {
                    arrowImg.frame = CGRect(x: 10, y: 12, width: 18, height: 15)
                }
                arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 18, height: 15)
            }
            else
            {
                arrowImg.image = UIImage(named: "add")
                if languageID == "1"
                {
                    arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 18, height: 15)
                }
                else
                {
                    arrowImg.frame = CGRect(x: 10, y: 12, width: 18, height: 15)
                }
            }
            
            sectionView.addSubview(arrowImg)
            /********** Add a custom Separator with Section view *******************/
            let separatorLineView: UIView = UIView(frame: CGRect(x: 0, y: 40, width: mainTableView.frame.size.width, height: 0.3))
            separatorLineView.backgroundColor = UIColor.lightGray
            //separatorLineView.layer.borderWidth = 0.5
            // sectionView.addSubview(separatorLineView)
            /********** Add UITapGestureRecognizer to SectionView   **************/
            let headerTapped: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(self.sectionHeaderTapped(_:)))
            sectionView.addGestureRecognizer(headerTapped)
            
            sectionView.frame.size.height = 40
            
            return sectionView
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == mainTableVw
        {
            if indexPath.section == 0
            {
                
                let cell:SubCategoryTableViewCell = self.mainTableVw.dequeueReusableCell(withIdentifier: "bannerCell") as! SubCategoryTableViewCell
                
                if self.imageUrlStr.contains("no_image") || self.imageUrlStr == ""
                {
                    cell.imgBanner.isHidden = true
                    cell.imgBanner.frame.size.height = 0
                    cell.lblExplore.frame.origin.y = 0
                }
                else
                {
                    cell.imgBanner.isHidden = false
                    
                    let imageUrl1 = "\(self.imageUrlStr)"
                    
                    let trimmedUrl = imageUrl1.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
                    
                    var activityLoader = UIActivityIndicatorView()
                    activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                    activityLoader.center = cell.imgBanner.center
                    activityLoader.startAnimating()
                    cell.imgBanner.addSubview(activityLoader)
                    
                    cell.imgBanner.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
                        
                        if image != nil
                        {
                            activityLoader.stopAnimating()
                        }
                        else
                        {
                            print("image not found")
                            
                            cell.imgBanner.isHidden = true
                            cell.imgBanner.frame.size.height = 0
                            cell.lblExplore.frame.origin.y = 0
                            self.imageUrlStr = ""
                            
                            activityLoader.stopAnimating()
                        }
                    })
                }
                
                return cell
            }
            else
            {
                let cell:SubCategoryTableViewCell = self.mainTableVw.dequeueReusableCell(withIdentifier: "collectionCell") as! SubCategoryTableViewCell
                
                cell.productListCollectVw.dataSource = self
                cell.productListCollectVw.delegate = self
                
                let layout = CollectionViewLayout()
                layout.viewType = self.viewType
                
                if viewType == "list"
                {
                    let nib = UINib(nibName: "ListCollectionViewCell", bundle: nil)
                    cell.productListCollectVw.register(nib, forCellWithReuseIdentifier: "listCell")
                    reuseIdentifier = "listCell"
                    
                    layout.itemHeight = 130
                }
                else if viewType == "full"
                {
                    let nib = UINib(nibName: "FullCollectionViewCell", bundle: nil)
                    cell.productListCollectVw.register(nib, forCellWithReuseIdentifier: "fullViewCell")
                    // cell.imgViewType.image = UIImage (named: "fullView.png")
                    reuseIdentifier = "fullViewCell"
                    
                    layout.itemHeight = 435
                }
                else
                {
                    let nib = UINib(nibName: "GridCollectionViewCell", bundle: nil)
                    cell.productListCollectVw.register(nib, forCellWithReuseIdentifier: "gridCell")
                    //  cell.imgViewType.image = UIImage (named: "gridView.png")
                    reuseIdentifier = "gridCell"
                    
                    layout.itemHeight = 250
                }
                
                cell.productListCollectVw.reloadData()
                cell.productListCollectVw.collectionViewLayout = layout
                
                return cell
            }
        }
        else if tableView == mainTableView
        {
            if indexPath.section != 1
            {
                if indexPath.section == 0
                {
                    if indexPath.row != 0
                    {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                        
                        /* if cell1 == nil {
                         var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                         for view: UIView in views {
                         if (view is UITableViewCell) {
                         cell1 = view as? DetailCell
                         }
                         }
                         }*/
                        
                        let subArr = mainTblArray[indexPath.section]
                        cell1.title.text = subArr[indexPath.row]
                        
                        let subArr2 = iconsArr[indexPath.section]
                        cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                        if languageID == "1"
                        {
                            cell1.imgArrow.image = UIImage(named: "left-arrow")
                        }
                        else
                        {
                            cell1.imgArrow.image = UIImage(named: "right-arrow1")
                        }
                        cell1.imgArrow.contentMode = .scaleAspectFit
                        cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                        cell1.imgIcon.tintColor = UIColor.gray
                        cell1.selectionStyle = .none
                        
                        return cell1
                    }
                    else
                    {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as! LanguageCell
                        
                        if languageID == "1"
                        {
                            //selectedSegmentIndex = 0
                            cell1.segmentView.selectedSegmentIndex = 0
                        }
                        else
                        {
                            cell1.segmentView.selectedSegmentIndex = 1
                        }
                        
                        cell1.segmentView.addTarget(self, action: #selector(ParentViewController.clickLang(_:)), for: UIControlEvents.valueChanged)
                        
                        return cell1
                    }
                }
                else
                {
                    let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                    
                    /* if cell1 == nil {
                     var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                     for view: UIView in views {
                     if (view is UITableViewCell) {
                     cell1 = view as? DetailCell
                     }
                     }
                     }*/
                    
                    let subArr = mainTblArray[indexPath.section]
                    cell1.title.text = subArr[indexPath.row]
                    
                    let subArr2 = iconsArr[indexPath.section]
                    cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                    if languageID == "1"
                    {
                        cell1.imgArrow.image = UIImage(named: "left-arrow")
                    }
                    else
                    {
                        cell1.imgArrow.image = UIImage(named: "right-arrow1")
                    }
                    cell1.imgArrow.contentMode = .scaleAspectFit
                    cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                    cell1.imgIcon.tintColor = UIColor.gray
                    cell1.selectionStyle = .none
                    
                    return cell1
                }
            }
            else
            {
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "CatCell") as! categoryTCell
                cell1.insideTableView.isScrollEnabled = false
                cell1.insideTableView.dataSource = self
                cell1.insideTableView.delegate = self
                cell1.insideTableView.reloadData()
                cell1.selectionStyle = .none
                
                return cell1
            }
        }
        else if tableView == tblSubCategories
        {
            var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if cell == nil
            {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            }
            cell?.textLabel?.font = UIFont.appFontWith(size: 14)
            cell?.textLabel?.text = (subCategoryAry2.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "name") as? String
            
            return cell!
        }
        else
        {
            var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if cell == nil
            {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            }
            
            cell?.textLabel?.font = UIFont.appFontWith(size: 15)
            if languageID == "2"
            {
                cell?.textLabel?.textAlignment = .right
            }
            cell?.textLabel?.text = (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "name") as? String
            cell?.selectionStyle = .none
            
            return cell!
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblSubCategories
        {
            var ary = NSArray()
            
            subCategoryID = "\(String(describing: (subCategoryAry2.object(at: (indexPath as NSIndexPath).row) as! NSObject).value(forKey: "id")!))" as NSString
            
            SharedManager.showHUD(viewController: self)
            
            let urlStr = "\(ConfigUrl.baseUrl)categories?rootCategoryId=\(subCategoryID)&depth=1"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        SharedManager.dismissHUD(viewController: self)
                        
                        if responseObject.result.isSuccess
                        {
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                ary = (responseObject.result.value! as AnyObject).value(forKey: "children_data") as! NSArray
                                
                                if ary.count > 0
                                {
                                    //  self.subCategoryAry2.removeAllObjects()
                                    
                                    let VC = self.storyboard!.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
                                    VC.isFromHome = true
                                    VC.catTitle = "\(String(describing: (responseObject.result.value! as AnyObject).value(forKey: "name")!))"
                                    VC.categoryID = "\(String(describing: (responseObject.result.value! as AnyObject).value(forKey: "id")!))"
                                    //  self.present(VC, animated: false, completion: nil)
                                    self.navigationController?.pushViewController(VC, animated: false)
                                }
                                else
                                {
                                    self.viewSubCategories.isHidden = true
                                    self.viewBlur.isHidden = true
                                    
                                    let VC = self.storyboard!.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
                                    
                                    VC.categoryID = "\((self.subCategoryAry2.object(at: (indexPath as NSIndexPath).row) as! NSObject).value(forKey: "id")!)"
                                    self.isFromHome = false
                                    VC.catTitle = (self.subCategoryAry2.object(at: (indexPath as NSIndexPath).row) as! NSObject).value(forKey: "name") as! String
                                    
                                    self.navigationController?.pushViewController(VC, animated: true)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
            
        }
        else if tableView == mainTableView
        {
            let subArr = mainTblArray[indexPath.section]
            
            let action = subArr[indexPath.row]
            
            if action == NSLocalizedString("Home", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Login", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.present(viewController, animated: true, completion: nil)
            }
            else if action == NSLocalizedString("Deals", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Live Chat", comment: "")
            {
                Intercom.presentMessenger()
            }
            else if action == NSLocalizedString("Track Order", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Share", comment: "")
            {
                let textToShare = [ Tools.shareText ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
        else if tableView == mainTableVw
        {
            
        }
        else
        {
            let id = "\(String(describing: (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "id")!))"
            print("Category ID: \(id)")
            categoryID = id
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            viewController.categoryID = id
            viewController.isFromHome = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == mainTableVw
        {
            if indexPath.section == 0
            {
                var height = CGFloat()
                
                if indexPath.row == 0
                {
                    if self.imageUrlStr.contains("no_image") || self.imageUrlStr == ""
                    {
                        height = 0
                    }
                    else
                    {
                        height = 175 + 38
                    }
                }
                return height
            }
            else
            {
                var height = CGFloat()
                
                if viewType == "list"
                {
                    height = CGFloat((130 * self.productListAry.count) + (1 * (self.productListAry.count) + 1))
                }
                else if viewType == "full"
                {
                    height = CGFloat((435 * self.productListAry.count) + (1 * (self.productListAry.count) + 1))
                }
                else
                {
                    if self.productListAry.count % 2 == 0
                    {
                        height = CGFloat((255 * self.productListAry.count/2) + (1 * (self.productListAry.count) + 1))
                    }
                    else
                    {
                        height = CGFloat((255 * (self.productListAry.count+1)/2) + (1 * (self.productListAry.count + 1) + 1))
                    }
                }
                
                return CGFloat(height)
            }
        }
        else if tableView == mainTableView
        {
            if indexPath.section != 1
            {
                return 44
            }
            else
            {
                if categoryDict.count != 0
                {
                    var collapsed = false
                    
                    var expandedRowHeight = 0
                    
                    for i in 0..<expandedSection.count
                    {
                        let index = Int("\(expandedSection[i])")!
                        
                        expandedRowHeight = expandedRowHeight + (((categoryDict[index] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count * 40)
                    }
                    
                    return CGFloat(expandedRowHeight) + CGFloat(categoryDict.count * 44)
                }
                else
                {
                    return UITableViewAutomaticDimension
                }
            }
        }
        else if tableView == tblSubCategories
        {
            return 40
        }
        else
        {
            let collapsed = ((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool)
            
            if collapsed
            {
                return 0
            }
            else
            {
                return 40
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == mainTableVw
        {
            if section == 0
            {
                return 0
            }
            else
            {
                return 63
            }
        }
        else if tableView == mainTableView
        {
            if section == 0
            {
                return 0
            }
            else
            {
                return 35
            }
        }
        else if tableView == tblSubCategories
        {
            return 0
        }
        else
        {
            return 44
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.productListAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifier, for: indexPath) as! CollectionViewCell
        let product = self.productListAry[indexPath.row]
        
        cell.title.text = product.name
        
        var imageUrl = ""
        
        if let url = product.smallImage {
            imageUrl = url
        }
        
        if let title = product.title {
            cell.title.text = title
        }
        
        cell.splPriceLbl.text = product.price
        
        if let splPrice = product.specialPrice {
            if viewType == "grid" {
                cell.title.frame.origin.y = 155
                cell.splPriceLbl.frame.origin.y = 195
            }
            
            cell.priceLbl.isHidden = false
            cell.splPriceLbl.text = splPrice
            cell.priceLbl.text = product.price
            
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: cell.priceLbl.text!)
            attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            cell.priceLbl.attributedText = attributeString
        }
        else
        {
            cell.priceLbl.isHidden = true
            if viewType == "grid"
            {
                cell.title.frame.origin.y = 160
                cell.splPriceLbl.frame.origin.y = 203
            }
        }
        
        if viewType == "full"
        {
            if languageID == "2"
            {
                cell.splPriceLbl.textAlignment = .left
                cell.priceLbl.textAlignment = .left
            }
            else
            {
                cell.splPriceLbl.textAlignment = .right
                cell.priceLbl.textAlignment = .right
            }
        }
        
        if viewType == "list"
        {
            cell.splPriceLbl.sizeToFit()
            cell.priceLbl.sizeToFit()
            
            cell.priceLbl.frame.origin.x = cell.splPriceLbl.frame.origin.x + cell.splPriceLbl.frame.width + 5
        }
        
        //  let imageUrl =  (self.productListAry.object(at: (indexPath as NSIndexPath).row) as! NSDictionary).value(forKey: "image") as! String
        let trimmedUrl = imageUrl.trimmingCharacters(in: CharacterSet(charactersIn: "")).replacingOccurrences(of: " ", with: "%20") as String
        
        var activityLoader = UIActivityIndicatorView()
        activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityLoader.center = cell.pinImage.center
        activityLoader.startAnimating()
        cell.pinImage.addSubview(activityLoader)
        cell.pinImage.contentMode = .scaleAspectFit
        
        cell.pinImage.sd_setImage(with: URL(string: trimmedUrl), completed: { (image, error, imageCacheType, imageUrl) in
            
            if image != nil
            {
                activityLoader.stopAnimating()
            }else
            {
                
                if self.viewType == "full"
                {
                    cell.pinImage.contentMode = .center
                }
                print("image not found")
                cell.pinImage.image = UIImage(named: "no_image")
                activityLoader.stopAnimating()
            }
        })
        
        cell.pinImage.contentMode = UIViewContentMode.scaleAspectFit
        
        let str2 = product.sku
        
        if self.checkProductInWishList(str2 as String) == true
        {
            let image = UIImage(named: "ic_favourite") as UIImage!
            cell.imgFav.image = image
        }
        else
        {
            let image = UIImage(named: "ic_unfavourite") as UIImage!
            cell.imgFav.image = image
        }
        cell.addToFav.addTarget(self, action: #selector(SubCategoryVC.addToFav(_:)), for: UIControlEvents.touchUpInside)
        cell.addToFav.tag = (indexPath as NSIndexPath).row
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = self.productListAry[indexPath.row]
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        viewController.productSku = product.sku
        viewController.productID = "\(product.id)"
        self.present(viewController, animated: true, completion: nil)
    }
    
    //MARK: Pagination
    func pullToRefresh()
    {
        if isSorted == true
        {
            /*if sortPage != Int(self.sortPageCount) + 1 && !categoryID.isEmpty
             {
             sortPage += 1
             BusinessManager.getSortedProductList(categoryID, languageID: languageID as String, page:sortPage, limit:limit, sort:sortKey, order:orderKey, filters:filterID, completionHandler: { (result) -> () in
             
             SharedManager.dismissHUD(viewController: self)
             
             self.productListAry.addObjects(from: result.value(forKey: "products") as! NSMutableArray as [AnyObject])
             
             self.mainTableVw.reloadData()
             self.sortProducts()
             })
             }
             else
             {
             SharedManager.dismissHUD(viewController: self)
             }*/
        }
        else
        {
            if (self.isScrolledOnce == true)
            {
                return
            }
            self.isScrolledOnce = true
            
            if page != Int(self.pageCount) + 1 && !categoryID.isEmpty
            {
                page += 1
                var urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(self.subIds)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=created_at& searchCriteria[sortOrders][0][direction]=DESC&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=\(page)"
                
                if isFiltered
                {
                    urlStr = "\(urlStr)\(filterUrlStr)"
                }
                
                SharedManager.showHUD(viewController: self)
                
                let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
                var request = URLRequest(url: URL(string: setFinalURl)!)
                request.httpMethod = HTTPMethod.get.rawValue
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
                
                if Connectivity.isConnectedToInternet()
                {
                    Alamofire.request(request).responseJSON
                        { (responseObject) -> Void in
                            if responseObject.result.isSuccess
                            {
                                SharedManager.dismissHUD(viewController: self)
                                if "\(String(describing: responseObject.response!.statusCode))" == "200"
                                {
                                    if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items")
                                    {
                                        print("\(result)")
                                        if "\(result)" != "<null>"
                                        {
                                            var products = [Product]()
                                            
                                            if let dataResponse = result as? NSArray {
                                                for data in dataResponse {
                                                    if let dataDic = data as? NSDictionary {
                                                        products.append(Product(data: dataDic))
                                                    }
                                                }
                                            }
                                            
                                            self.productListAry.append(contentsOf: products)
                                            
                                            self.sortProducts()
                                            self.mainTableVw.reloadData()
                                        }
                                        else
                                        {
                                            SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                        }
                                    }
                                    else
                                    {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                                }
                            }
                            if responseObject.result.isFailure
                            {
                                SharedManager.dismissHUD(viewController: self)
                                let error : Error = responseObject.result.error!
                                print(error.localizedDescription)
                            }
                            self.isScrolledOnce = false
                    }
                }
                else
                {
                    SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
                }
            }
            else
            {
                SharedManager.dismissHUD(viewController: self)
                self.isScrolledOnce = false
            }
        }
        // SharedManager.dismissHUD(viewController: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if scrollView == self.mainTableVw
        {
            let offset: CGPoint = scrollView.contentOffset
            let bounds: CGRect = scrollView.bounds
            let size: CGSize = scrollView.contentSize
            let inset: UIEdgeInsets = scrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 10
            if y > h + reload_distance
            {
                if isScrolledOnce == false
                {
                    self.pullToRefresh()
                }
            }
        }
    }
    
    //MARK: Sorting
    @IBAction func sortAction(_ sender: AnyObject)
    {
        let picker = BSModalPickerView.init(values: sortItems())
        picker?.present(in: self.view, with: {(_ madeChoice: Bool) -> Void in
            if madeChoice
            {
                
                switch(Int(bitPattern: (picker?.selectedIndex)!))
                {
                case 0:
                    self.sortKey = "name"
                    self.orderKey = "ASC"
                    break
                case 1:
                    self.sortKey = "name"
                    self.orderKey = "DESC"
                    break
                case 2:
                    self.sortKey = "price"
                    self.orderKey = "ASC"
                    break
                case 3:
                    self.sortKey = "price"
                    self.orderKey = "DESC"
                    break
                case 5:
                    self.sortKey = "position"
                    self.orderKey = "ASC"
                    break
                case 6:
                    self.sortKey = "position"
                    self.orderKey = "DESC"
                    break
                default:
                    self.sortKey = "created_at"
                    self.orderKey = "DESC"
                    break
                    
                }
                self.updatePage(id: self.subIds)
            }
            else
            {
                
            }
        })
    }
    
    func sorting()
    {
        var urlStr = "\(ConfigUrl.baseUrl)products?fields=items[id,sku,name,price,custom_attributes],total_count&searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]=\(self.categoryID)&searchCriteria[filterGroups][0][filters][0][conditionType]=in&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=\(sortKey)& searchCriteria[sortOrders][0][direction]=\(orderKey)&searchCriteria[pageSize]=\(limit)&searchCriteria[currentPage]=1"
        SharedManager.showHUD(viewController: self)
        
        if isFiltered
        {
            urlStr = "\(urlStr)\(filterUrlStr)"
        }
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            if let result = ((responseObject.result.value!) as AnyObject).value(forKey: "items")
                            {
                                if "\(result)" != "<null>"
                                {
                                    var products = [Product]()
                                    
                                    if let dataResponse = result as? NSArray {
                                        for data in dataResponse {
                                            if let dataDic = data as? NSDictionary {
                                                products.append(Product(data: dataDic))
                                            }
                                        }
                                    }
                                    
                                    self.productListAry = products
                                    
                                    if self.productListAry.count == 0
                                    {
                                        SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))!", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                    }
                                    else
                                    {
                                        self.productsCount = "\(((responseObject.result.value!) as AnyObject).value(forKey: "total_count")!)" as NSString
                                        self.pageCount = Double(self.productsCount as String)!/Double(self.limit)!
                                        self.page = 1
                                        self.mainTableVw.reloadData()
                                    }
                                }
                                else
                                {
                                    SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                                }
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "\(NSLocalizedString("Sorry", comment: ""))", alertMessage: "\(NSLocalizedString("No Products Available", comment: ""))", viewController: self)
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    //MARK: Filter
    @IBAction func filterAction(_ sender: AnyObject)
    {
        if productListAry.count != 0
        {
            let VC = self.storyboard!.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
            VC.catgryID = self.categoryID
            self.present(VC, animated: true, completion: nil)
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("Filter is not Available", comment: ""), viewController: self)
        }
        
    }
    
    //MARK: Favourites
    
    @objc func addToFav(_ sender : UIButton) {
        if(UserDefaults.standard.object(forKey: kUserDetails) != nil) {
            self.wishlist(index: sender.tag)
        } else {
            addFavTag = sender.tag
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // instantiate your desired ViewController
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            viewController.delegate = self
            self.present(viewController, animated: true, completion: { () -> Void in
                //
            })
        }
    }
    
    func wishlist(index: Int) {
        let product = self.productListAry[index]
        let isHave:Bool = self.checkProductInWishList("\(product.sku)")
        
        if isHave {
            self.deleteWishlist(productId: "\(product.id)")
            Tools.removeFromWishList(sku: product.sku)
            self.showToast(message: NSLocalizedString("Successfully removed from your wishlist", comment: ""))
        } else {
            self.addToWishlist(productId: "\(product.id)")
            Tools.addToWishList(product: product)
            self.showToast(message: NSLocalizedString("Successfully added to your wishlist", comment: ""))
        }
        self.mainTableVw.reloadData()
    }
    
    
    func addToWishlist(productId : String) {
        let request = AddWishlist(productId: productId)
        request.start()
    }
    
    func deleteWishlist(productId : String) {
        let request = DeleteWishlist(productId: productId)
        request.start()
    }
    
    func loginSuccess() {
        self.wishlist(index: addFavTag)
    }
    
    func loginFailure() {}
    
    @IBAction func clickChangeView(_ sender: Any)
    {
        if viewType == "grid"
        {
            reuseIdentifier = "listCell"
            viewType = "list"
            UserDefaults.standard.set("list", forKey: "viewType")
            
            sortProducts()
        }
        else if viewType == "list"
        {
            reuseIdentifier = "fullViewCell"
            viewType = "full"
            UserDefaults.standard.set("full", forKey: "viewType")
            
            sortProducts()
        }
        else
        {
            reuseIdentifier = "gridCell"
            viewType = "grid"
            UserDefaults.standard.set("grid", forKey: "viewType")
            
            sortProducts()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
