//
//  Wishllist.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 14/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//


class WishList {
    
    let itemId : String
    let wishlistId: String
    let productId: String
    
    var name: String?
    let sku:String
    var price:String?
    
    var specialPrice : String?
    var smallImage : String?
    
    
    init(data:NSDictionary) {
        
        if let itemId = data["wishlist_item_id"] {
            self.itemId = "\(itemId)"
        } else {
            self.itemId = ""
        }
        
        if let wishlistId = data["wishlist_id"] {
            self.wishlistId = "\(wishlistId)"
        } else {
            self.wishlistId = ""
        }
        
        if let productId = data["product_id"] {
            self.productId = "\(productId)"
        } else {
            self.productId = ""
        }
        
        if let product = data["product"] as? NSDictionary {
            
            let title : String = {
                if languageID == "1" {
                    return "full_name_en"
                } else {
                    return "full_name_ar"
                }
            }()
            
            if let name = product[title] {
                self.name = "\(name)"
            }
            
            if let image = product["small_image"] {
                self.smallImage = "\(ConfigUrl.imageUrl)\(image)"
            }
            
            if let sku = product["sku"] as? String {
                self.sku = sku
            } else {
                self.sku = ""
            }
            
            if let price = product["price"] {
                let value : String = {
                    if let amount = price as? Double {
                        return "\(String(format: "%.2f", amount))"
                    } else if let value = price as? Int {
                        return "\(value)"
                    } else {
                        return "\(price)"
                    }
                }()
                self.price = "\(value) \(Tools.getCurrency())"
            }
            
            if let specialPrice = product["special_price"],
                "\(specialPrice)" != "<null>"{
                let value : String = {
                    if let amount = specialPrice as? Double {
                        return "\(String(format: "%.2f", amount))"
                    } else if let value = specialPrice as? Int {
                        return "\(value)"
                    } else {
                        return "\(specialPrice)"
                    }
                }()
                self.specialPrice = "\(value) \(Tools.getCurrency())"
            }
        } else {
            self.sku = ""
        }
        
    }
}
