//
//  Product.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 11/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Foundation

class Product {
    
    let name: String
    let sku:String
    let price:String
    let id: Int
    
    var title : String?
    var specialPrice : String?
    var smallImage : String?
    
    let data : NSDictionary
    init(data:NSDictionary) {
        
        self.data = data
        
        if let name = data["name"] {
            self.name = "\(name)"
        } else {
            self.name = ""
        }
        
        if let sku = data["sku"] {
            self.sku = "\(sku)"
        } else {
            self.sku = ""
        }
        
        if let price = data["price"] {
            let value : String = {
                if let amount = price as? Double {
                    return "\(String(format: "%.2f", amount))"
                } else if let value = price as? Int {
                    return "\(value)"
                } else {
                    return "\(price)"
                }
            }()
            self.price = "\(value) \(Tools.getCurrency())"
        } else {
            self.price = ""
        }
        
        if let id = data["id"] as? Int {
            self.id = id
        } else {
            self.id = 0
        }
        
        if let customAttributes = data["custom_attributes"] as? NSArray {
            for i in 0..<customAttributes.count {
                let attribute = customAttributes[i] as AnyObject
                
                let name = attribute.value(forKey: "attribute_code") as! String
                
                let title : String = {
                    if languageID == "1" {
                        return "full_name_en"
                    } else {
                        return "full_name_ar"
                    }
                }()
                
                
                if name == title {
                    self.title = "\(attribute.value(forKey: "value")!)"
                    
                } else if name == "small_image" {
                    let value = "\(attribute.value(forKey: "value") as AnyObject)"
                    self.smallImage = "\(ConfigUrl.imageUrl)\(value)"
                    
                } else if name == "special_price" {
                    let value : String = {
                        if let amount = attribute.value(forKey: "value") as? Double {
                            return "\(String(format: "%.2f", amount))"
                        } else if let value = attribute.value(forKey: "value") as? Int {
                            return "\(value)"
                        } else {
                            return "\(attribute.value(forKey: "value")!)"
                        }
                    }()
                    self.specialPrice = "\(value) \(Tools.getCurrency())"
                    
                } else if name == "meta_title" {
                    self.title = "\(attribute.value(forKey: "value")!)"
                }
            }
        }
    }
}

