//
//  Texts.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 08/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Foundation

class Texts {
    static let SAR = NSLocalizedString("SAR", comment: "")
    static let Cancel = NSLocalizedString("Cancel", comment: "")
    static let Logout = NSLocalizedString("Logout", comment: "")
    
    static let YourOrder = NSLocalizedString("Your order Id is:", comment: "")
    
    static let To_Logout = NSLocalizedString("Are you sure you want to logout?", comment: "")
    static let NO_INTERNET_CONNECTION = NSLocalizedString("The Internet connection appears to be offline", comment: "")
    static let Wishlist_Added = NSLocalizedString("added to wishlist", comment: "")
    static let Wishlist_Removed = NSLocalizedString("removed from wishlist", comment: "")
    static let REQUIRED_FILED = NSLocalizedString("Please fill in all fields", comment:"User must input the data")
    static let AuthFail = NSLocalizedString("Authentication failure", comment: "")
    
    
    // Sortings
    
    //["Name (A - Z)", "Name (Z - A)","Price (Low > High)","Price (High > Low)", "Position up", "Position down"]
    
    static let sortA_Z = NSLocalizedString("Name (A - Z)", comment: "")
    
    static let sortZ_A = NSLocalizedString("Name (Z - A)", comment: "")
    
    static let sortLowHigh = NSLocalizedString("Price (Low > High)", comment: "")
    
    static let sortHighLow = NSLocalizedString("Price (High > Low)", comment: "")
    
    static let sortUp = NSLocalizedString("Position up", comment: "")
    
    static let sortDown = NSLocalizedString("Position down", comment: "")
}

