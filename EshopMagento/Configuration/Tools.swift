//
//  Tools.swift
//  Balleh
//
//  Created by Abdalrahim Abdullah on 07/11/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class Tools {
    static func getLinkInt(_ link:String) -> ([Int]) {
        let splited = link.split(separator: ",")
        let links : [Int] = {
            var ints = [Int]()
            for some in splited {
                ints.append(Int(some)!)
            }
            return ints
        }()
        return links
    }
    
    static let shareText = ""
    
    static func sortItems() -> [String] {
        return [Texts.sortA_Z, Texts.sortZ_A, Texts.sortLowHigh, Texts.sortHighLow, Texts.sortUp,Texts.sortDown]
    }
    
    static func alertWithOk(message: String) -> UIAlertController {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default)
        //alert.view.tintColor = Tools.dark
        alert.addAction(cancelAction)
        return alert
    }
    
    static func headerImage() -> UIImage {
        if languageID == "1" {
            return UIImage(named: "balleh")!
        } else {
            return UIImage(named: "balleh_ar")!
        }
    }
    
    static func getCurrency() -> String {
        if languageID == "1" {
            return "SAR"
        } else {
            return "ريال"
        }
    }
    
    static func removeFromWishList(sku: String) {
        
        if UserDefaults.standard.object(forKey: kAddToWishlist) != nil {
            let isHave:Bool = Tools.checkProductInWishList(sku)
            let data = UserDefaults.standard.object(forKey: kAddToWishlist) as! Data
            let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            let tempArr = NSMutableArray()
            tempArr.addObjects(from: favAry as! [Any])
            for i in 0..<tempArr.count {
                let str1 = (tempArr[i] as AnyObject).value(forKey: "sku") as! String
                
                if str1 == sku {
                    favAry.removeObject(at: i)
                }
            }
            
            let tempData = NSKeyedArchiver.archivedData(withRootObject: favAry)
            UserDefaults.standard.set(tempData, forKey: kAddToWishlist)
        }
    }
    
    static func addToWishList(product: Any) {
//        if UserDefaults.standard.object(forKey: kAddToWishlist) != nil {
//            let data = UserDefaults.standard.object(forKey: kAddToWishlist) as! Data
//            let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
//
//            let currentArr : NSDictionary  = self.productDetailsDic
//            favAry.add(currentArr)
//
//            let tempData = NSKeyedArchiver.archivedData(withRootObject: favAry)
//            UserDefaults.standard.set(tempData, forKey: kAddToWishlist)
//        } else {
//            let currentArr : NSDictionary  = self.productDetailsDic
//            let favAry = NSMutableArray()
//            favAry.add(currentArr)
//
//            let data1 = NSKeyedArchiver.archivedData(withRootObject: favAry)
//            UserDefaults.standard.set(data1, forKey: kAddToWishlist)
//        }
        let dictionary : NSDictionary = {
            if let aProduct = product as? Product {
                return aProduct.data
            } else {
                if let aProduct = product as? NSMutableDictionary {
                    return aProduct
                } else {
                    return NSDictionary()
                }
            }
        }()
        
        if let data = UserDefaults.standard.object(forKey: kAddToWishlist) as? Data {
            let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            favAry.add(dictionary)
            
            let tempData = NSKeyedArchiver.archivedData(withRootObject: favAry)
            UserDefaults.standard.set(tempData, forKey: kAddToWishlist)
        } else {
            let favAry = NSMutableArray()
            favAry.add(dictionary)
            
            let data1 = NSKeyedArchiver.archivedData(withRootObject: favAry)
            UserDefaults.standard.set(data1, forKey: kAddToWishlist)
        }
    }
    
    
    static func cartText(text: String) -> String {
        let lowText = text.lowercased()
        if languageID == "1" {
            return text
        } else {
            let percent = lowText.index(after: .init(encodedOffset: 1))
            print(percent)
            if lowText.contains("subtotal") {
                return "المجموع"
            } else if lowText.contains("discount") {
                return "الخصم"
            } else if lowText.contains("shipping & handling") {
                return "الشحن والمناولة (الشحن - خلال 48 ساعة)"
            } else if lowText.contains("cash on delivery") {
                return "الدفع عند الاستلام"
            } else if lowText.contains("vat") {
                guard let percent = lowText.split(separator: "t").last else {
                    return "ضريبة القيمة المضافة(%5)"
                }
                return "ضريبة القيمة المضافة\(percent)"
            } else if lowText.contains("grand total") {
                return "المبلغ الإجمالي الكلي"
            } else {
                return text
            }
        }
    }
    
    static func checkProductInWishList(_ productId:String) -> Bool {
        var isAleadyHave:Bool = false
        
        if UserDefaults.standard.object(forKey: kAddToWishlist) != nil {
            let data = UserDefaults.standard.object(forKey: kAddToWishlist) as! Data
            let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            for tempDic in favAry {
                let tempCart:NSDictionary = tempDic as! NSDictionary
                
                let str1 = tempCart.value(forKey: "sku") as! NSString
                print(str1, productId)
                if str1.isEqual(to: productId as String) {
                    isAleadyHave = true
                }
            }
        }
        return isAleadyHave
    }
    
    static func open(link : String) -> SFSafariViewController? {
        if let url = URL(string: link) {
            let safariVC = SFSafariViewController(url: url)
            if #available(iOS 10.0, *) {
//                safariVC.preferredBarTintColor = Tools.dark
//                safariVC.preferredControlTintColor = Tools.yellow
                
            }
            if #available(iOS 11.0, *) {
                safariVC.configuration.entersReaderIfAvailable = true
            }
            
            return safariVC
        } else {
            return nil
        }
    }
}
