//
//  Constant.swift
//  EshopMagento
//
//  Created by Abdalrahim Abdullah on 25/10/2018.
//  Copyright © 2018 Cura Company Limited. All rights reserved.
//

import Foundation

public let kGETMethod = "GET"
public let kPOSTMethod = "POST"

public let kLanguageID = "_kLanguage_ID"
public let kSortOrder = "_kSort_Order_List"
public let kCountryList = "_kCountry_List"

public let kAddToCart = "_kADDTOCART_CONSTANT"
public let kAddToCartOptions = "_KADDTOCART_CONSTANT_OPTIONS"
public let kAddToWishlist = "_KADDTOWISHLIST_CONSTANT"
public let kUserDetails = "_k_USER_DETAILS"
public let kProfileInfo = "_kPROFILE_INFO"

public let kEmailValidAlertMessage = "Please enter valid email Id"
public let kEmptyTextFieldAlertMessage = "Please enter all the details"
public let kAlertTitleMessage = "Information"
public let kAddToCartAlertMessage = "Your cart is empty!"
public let kProductMaximumQuantity = "This Product is not available in the desired Quantity or not in Stock"

public let kProductMinimunQuantity = "Minimum quantity should be 1"
public let kProductWishlistAlertMessage = "Your favorite item list is empty!"
public let kProductOrdersAlertMessage = "Your order list is empty!"
public let kEmptyProductAlertMessage = "No products Available"
public let kEmptyFilterAlertMessage = "Filter is not Available"

public let kReviewAlertMessage = "Please enter the review"
public let kEmptySearchAlertMessage = "Try with some other keyword."
public let kPasswordMismatchAlertMessage = "Passwords does not matched"
