//
//  AppDelegate.swift
//  EshopMagento
//
//  Created by Adyas Iinfotech on 19/03/18.
//  Copyright © 2018 Adyas Iinfotech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import Intercom
import GoogleMaps
import GooglePlaces
import Firebase
import FirebaseMessaging
import UserNotifications
import FBSDKCoreKit
import TwitterKit
import OneSignal
import Appsee
import OAuthSwift

var languageID : String = ""
var userIDStr:String = ""
var cartID : String = ""
var cartCount : String = ""
var isGuest = Bool()
var baseToken = "ljeygdxd6krq1uqxos1gk33568ivrgcx"
let consumerKey = "l5bSOV1wLEt9DvRR9YLW9CB3p"
let consumerSecret = "86HPGJKd8cWwTrrX8xz41zUPZqPElo76pI3MJ9hF81iAo40H6V"

var deviceId : String = {
    return UIDevice.current.identifierForVendor!.uuidString
}()

let lang: String = {
    if let language = Locale.current.languageCode,
        language == "ar" {
        return language
    } else {
        return "en"
    }
}()

var tokenStr:String = ""
var themeColor = UIColor ()
var positiveBtnColor = UIColor()
var negativeBtnColor = UIColor ()
var MY_TOKEN = ""

//let appFont = UIFont(name: NSLocalizedString("Roboto-Light", comment: ""), size: CGFloat())

var cartList = NSMutableArray()
var mainCartDict = NSMutableDictionary()
var shippingAddressDict = NSMutableDictionary()
var shippingMethodDict = NSMutableDictionary()
var paymentMethodDict = NSMutableDictionary()

var isFiltered = false
var filterUrlStr : String = ""

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // UA-116546572-1
        
        // Base Theme
        themeColor = UIColor(rgb: 0x7fd5eb)
        positiveBtnColor = UIColor(rgb: 0xeb67a0)
        negativeBtnColor = UIColor.darkGray
        
        //let arabicFont = "GESSTwoLight-Light"
        //let englishFont = "Roboto-Light"
        
        // Navigation Bar
        UINavigationBar.appearance().barTintColor = themeColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        //UINavigationBar.appearance().tintColor = UIColor.white
        //UIApplication.accessibilityLanguage()
        // Text Keyboard
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().toolbarTintColor = themeColor
        
        if let id = UserDefaults.standard.string(forKey: "language_id"),
            id != "" {
            languageID = id
            if id == "1" {
                ConfigUrl.baseUrl = "https://balleh.com/index.php/rest/en/V1/"
            } else {
                ConfigUrl.baseUrl = "https://balleh.com/index.php/rest/ar/V1/"
            }
            
        } else {
            if lang == "en" {
                languageID = "1"
                ConfigUrl.baseUrl = "https://balleh.com/index.php/rest/en/V1/"
            } else {
                languageID = "2"
                ConfigUrl.baseUrl = "https://balleh.com/index.php/rest/ar/V1/"
            }
            
            
        }
        
        if let cart = UserDefaults.standard.string(forKey: "CART_ID")
        {
            cartID = cart
        }
        
        // FireBase
        FirebaseApp.configure()
        
        // Twitter Login
        TWTRTwitter.sharedInstance().start(withConsumerKey:consumerKey, consumerSecret:consumerSecret)
        
        // Google Maps
        GMSServices.provideAPIKey("AIzaSyCooZgkYA_PL_GeXd-zymYfAXZu_UHztvY")
        GMSPlacesClient.provideAPIKey("AIzaSyCooZgkYA_PL_GeXd-zymYfAXZu_UHztvY")
        
        // Live Chat
        Intercom.setApiKey("ios_sdk-014f6c6a930ed75683a436505c77bbbf8d2a5ed0", forAppId: "vyzpmf4c")
        Intercom.registerUnidentifiedUser()
        Intercom.setLauncherVisible(false)
        Intercom.unreadConversationCount()
        
        // Appsee
        Appsee.start()
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: true]
        
        OneSignal.initWithLaunchOptions(
            launchOptions,
            appId: "4f45600a-a660-4bdb-b7cf-3018b65c93cc",
            handleNotificationReceived: { notification in
                if notification?.payload.additionalData != nil {
                    let additionalData = notification?.payload.additionalData
                    print(additionalData!)
                }
        },
            handleNotificationAction: nil,
            settings: onesignalInitSettings
        )
        // 04fea69e-391b-4510-9334-9800c7f03ad0
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        if let id = status.subscriptionStatus.userId {
            print("\nOneSignal UserId:", id)
        }
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        
        //Test key  Intercom.setApiKey("ios_sdk-70e6422e4c9c57b8eecd16470d8e4cac82d89f4b", forAppId: "f0yimkoc")
      /*  window.intercomSettings = {
            app_id: "abc123",
            alignment: 'left',
            horizontal_padding: 20,
            vertical_padding: 20
        };*/
        
        // Google Analytics
        
       /* guard let gai = GAI.sharedInstance() else
        {
            assert(false, "Google Analytics not configured correctly")
        }
        gai.tracker(withTrackingId: "UA-116546572-1")
        // Optional: automatically report uncaught exceptions.
        gai.trackUncaughtExceptions = true
        
        // Optional: set Logger to VERBOSE for debug information.
        // Remove before app release.
        gai.logger.logLevel = .verbose;*/
        
        // Facebook Integration
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Push Notification
        
        Messaging.messaging().delegate = self
        
      /*  if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()*/

        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        print(url)
        if (url.host == "oauth-callback") && (options[.sourceApplication] as? String == "com.apple.SafariViewService") {
            OAuthSwift.handle(url: url)
        } else {
            return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        }
        return true
        //return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        // Add any custom logic here.
        return handled
    }
    
   /* func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }*/
    
    @objc func refreshToken()
    {
        let params = [
            "username": "sasikumara@exlcart.com",
            "password": "qwerty@123"
        ]
        
        let urlStr = "\(ConfigUrl.baseUrl)integration/customer/token"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      //  request.setValue((Defaults.object(forKey: XTRM_UserDefautKeys.oAuthTokenKey) as! String), forHTTPHeaderField: "Authorization")
        
        let setTemp: [String : Any] = params
        
        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
            let jsonString = String(data: jsonData , encoding: .utf8)
            print(jsonString as Any)
            request.httpBody = jsonData
        }
        
        Alamofire.request(request).responseJSON { (responseObject) -> Void in
            
            if responseObject.result.isSuccess
            {
                if "\(String(describing: responseObject.response!.statusCode))" == "200"
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let today = dateFormatter.string(from: Date())
                    UserDefaults.standard.set("\(today)", forKey: "TOKEN-TIMER")
                    
                    tokenStr = responseObject.result.value! as! String
                    UserDefaults.standard.set(tokenStr, forKey: "API-TOKEN")
                    print("Refreshed Token: \(tokenStr)")
                }
                else
                {
                    print(((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String)
                }
            }
            if responseObject.result.isFailure
            {
                let error : Error = responseObject.result.error!
                print(error.localizedDescription)
            }
            
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        self.ConnectToFCM()
        // Timer
        if let lastOpened = UserDefaults.standard.value(forKey: "TOKEN-TIMER") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          //  let today = dateFormatter.string(from: Date())
            // let todate = dateFormatter.date(from: today)
            let lastDate = dateFormatter.date(from: lastOpened as! String)
            
            let dateComponentsFormatter = DateComponentsFormatter()
            dateComponentsFormatter.allowedUnits = [.year,.month,.weekOfMonth,.day,.hour,.minute,.second]
            dateComponentsFormatter.maximumUnitCount = 1
            dateComponentsFormatter.unitsStyle = .full
            
            let interval = Date().timeIntervalSince(lastDate!)
           // let diff = dateComponentsFormatter.string(from: interval)!
            
            if interval > 3000 {
                refreshToken()
            }
        } else {
            refreshToken()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    /// Connect to the firebsse and generate the token
    func ConnectToFCM() {
        Messaging.messaging().shouldEstablishDirectChannel = true
        if let token = InstanceID.instanceID().token() {
            MY_TOKEN = token
            print("DCS: " + token)
        }
    }
}

/*@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo as NSDictionary
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        let imgData = userInfo.value(forKey: "data")! as! NSDictionary
        
        let url = imgData.value(forKey: "attachment-url")
        
        let imgUrl = URL(string: url as! String)!
        
        //  1. Create Notification Content
        let content = UNMutableNotificationContent()
        
        
        //  2. Create Notification Attachment
        
        URLSession.shared.downloadTask(with: imgUrl)
        {(location, response, error) in
            
            print("location: \(location!)")
            
            if error == nil
            {
                if let location = location
                {
                    // Move temporary file to remove .tmp extension
                    
                    let tmpDirectory = NSTemporaryDirectory()
                    
                    let tmpFile = "file://".appending(tmpDirectory).appending(imgUrl.lastPathComponent)
                    
                    print("tmpFile: \(tmpFile)")
                    
                    let tmpUrl = URL(string: tmpFile)!
                    
                    print("tmpUrl: \(tmpUrl)")
                    
                    try! FileManager.default.moveItem(at: location, to: tmpUrl)
                    
                    // Add the attachment to the notification content
                    
                    if let attachment = try? UNNotificationAttachment(identifier: "attachment", url: tmpUrl) {
                        
                        content.attachments = [attachment]
                        
                        print("attachment: \(content.attachments)")
                        
                        //  3. Create Notification Request
                        
                        let request = UNNotificationRequest.init(identifier: String.UNNotificationRequest.NormalLocalPush.rawValue,
                                                                 
                                                                 content: content, trigger: nil)
                        
                        content.title = "\(userInfo.value(forKeyPath: "aps.alert.title")!)"
                        content.body = "\(userInfo.value(forKeyPath: "aps.alert.body")!)"
                        content.sound = UNNotificationSound.default()
                        content.badge = (UIApplication.shared.applicationIconBadgeNumber + 1) as NSNumber;
                        content.categoryIdentifier = String.UNNotificationCategory.Normal.rawValue
                        
                        //  4. Add to NotificationCenter
                        
                        let center = UNUserNotificationCenter.current()
                        
                        center.add(request)
                    }
                }
            }
            else
            {
                print("Error: \(error!)")
            }
            }.resume()
        
        // Change this to your preferred presentation option
        completionHandler([
            ])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo as NSDictionary
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        let imgData = userInfo.value(forKey: "data")! as! NSDictionary
        
        let url = imgData.value(forKey: "attachment-url")
        
        let imgUrl = URL(string: url as! String)!
        
        //  1. Create Notification Content
        let content = UNMutableNotificationContent()
        content.title = "\(userInfo.value(forKeyPath: "aps.alert.title")!)"
        content.body = "\(userInfo.value(forKeyPath: "aps.alert.body")!)"
        content.sound = UNNotificationSound.default()
        content.badge = (UIApplication.shared.applicationIconBadgeNumber + 1) as NSNumber;
        content.categoryIdentifier = String.UNNotificationCategory.Normal.rawValue   //
        
        //  2. Create Notification Attachment
        
        URLSession.shared.downloadTask(with: imgUrl) { (location, response, error) in
            
            if let location = location {
                
                // Move temporary file to remove .tmp extension
                
                let tmpDirectory = NSTemporaryDirectory()
                
                let tmpFile = "file://".appending(tmpDirectory).appending(imgUrl.lastPathComponent)
                
                let tmpUrl = URL(string: tmpFile)!
                
                try! FileManager.default.moveItem(at: location, to: tmpUrl)
                
                
                
                // Add the attachment to the notification content
                
                if let attachment = try? UNNotificationAttachment(identifier: "", url: tmpUrl) {
                    
                    content.attachments = [attachment]
                }
            }
            
            }.resume()
        
        
        //  3. Create Notification Request
        let request = UNNotificationRequest.init(identifier: String.UNNotificationRequest.NormalLocalPush.rawValue,
                                                 content: content, trigger: nil)
        
        //  4. Add to NotificationCenter
        let center = UNUserNotificationCenter.current()
        center.add(request)
        
        let responseNotificationRequestIdentifier = response.notification.request.identifier
        
        if responseNotificationRequestIdentifier == String.UNNotificationRequest.NormalLocalPush.rawValue ||
            responseNotificationRequestIdentifier == String.UNNotificationRequest.LocalPushWithTrigger.rawValue ||
            responseNotificationRequestIdentifier == String.UNNotificationRequest.LocalPushWithCustomUI1.rawValue ||
            responseNotificationRequestIdentifier == String.UNNotificationRequest.LocalPushWithCustomUI2.rawValue {
            
            let actionIdentifier = response.actionIdentifier
            switch actionIdentifier {
            case String.UNNotificationAction.Accept.rawValue:
                
                break
            case String.UNNotificationAction.Reject.rawValue:
                
                break
            case String.UNNotificationAction.Input.rawValue:
                
                break
            case UNNotificationDismissActionIdentifier:
                
                break
            case UNNotificationDefaultActionIdentifier:
                
                break
            default:
                break
            }
        }
        
        completionHandler()
    }
}*/

