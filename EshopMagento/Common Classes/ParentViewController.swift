//
//  ParentViewController.swift
//  Restaurant
//
//  Created by Adyas Iinfotech on 02/06/17.
//  Copyright © 2017 Adyas Iinfotech. All rights reserved.
//

import UIKit
import Alamofire
import Intercom

/*struct Categories {
    var id: String!
    var parentId: String!
    var name: String!
    var isActive: String!
    var level: String!
    var position: String!
    var productCount: String!
    var child: [SubCategories]!
    var collapsed: Bool!

    init(_ catDetails: [String:AnyObject], collapsed: Bool = true)
    {
        self.id = catDetails["id"] as! String
        self.parentId = catDetails["parent_id"] as! String
        self.name = catDetails["name"] as! String
        self.isActive = catDetails["is_active"] as! String
        self.position = catDetails["position"] as! String
        self.level = catDetails["level"] as! String
        self.productCount = catDetails["product_count"] as! String
        self.child = catDetails["children_data"] as! [SubCategories]
        self.collapsed = collapsed
    }
}

struct SubCategories {
    var id: String!
    var parentId: String!
    var name: String!
    var isActive: String!
    var level: String!
    var position: String!
    var productCount: String!
    var child: [AnyObject]!
    var collapsed: Bool!
    
    init(_ catDetails: [String:AnyObject], collapsed: Bool = true)
    {
        self.id = catDetails["id"] as! String
        self.parentId = catDetails["parent_id"] as! String
        self.name = catDetails["name"] as! String
        self.isActive = catDetails["is_active"] as! String
        self.position = catDetails["position"] as! String
        self.level = catDetails["level"] as! String
        self.productCount = catDetails["product_count"] as! String
        self.child = catDetails["children_data"] as! [AnyObject]
        self.collapsed = collapsed
    }
}*/

//var languageID:String = "1"
var countryID: NSString = ""
var stateID:NSString = ""
var countryName:String = ""
var stateName:String = ""
var categoryID:String = ""
var subCategoryID:NSString = ""
var mainCategoryID:String = ""
var creditTypeStr:NSString = ""
var catID = NSMutableArray()
var cartTotalAmount:String = ""
var dealsID : String = ""
var countryArr = NSMutableArray()
var stateArr = NSMutableArray()
var expandedSection = NSMutableArray()

var orderStatus = ""

extension UIViewController {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.bounds.width - 20, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont.appFontWith(size: 14)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }

@objc class ParentViewController: UIViewController, UIGestureRecognizerDelegate,  UITableViewDelegate, UITableViewDataSource, CollapsibleTableViewHeaderDelegate, MainResponse {
    
    func onSuccess(message: String) { }
    
    func onFail(message: String) { }
    
    //Side menu
    var mainTableView = UITableView()
    var categoryDict = NSMutableArray()
    var index = NSIndexPath()
    
    var menuView = UIView()
    
    func sortItems() -> [String] {
        return [Texts.sortA_Z, Texts.sortZ_A, Texts.sortLowHigh, Texts.sortHighLow, Texts.sortUp,Texts.sortDown]
    }
    
    static let Currency : String = {
        return Tools.getCurrency()
    }()
    
    func showAlertWith(message:String){
        self.present(Tools.alertWithOk(message: message), animated: true, completion: nil)
    }
    
    var mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")],
                        [""],
                        [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
    ]
    
    var iconsArr = [["", "home-grey","ic_person"],
                        [""],
                        ["livechat", "ic_share"]
    ]
    
    var imageArr = ["ic_settings","ic_favorite","ic_settings","ic_favorite", "ic_settings", "ic_favorite", "ic_settings"]
    
    var titleArr : NSMutableArray = ["", NSLocalizedString("Categories", comment: ""), NSLocalizedString("Help", comment: "")]
    
  /*  var sections = [
        Section(name: "Main Category", child: ["Sub category", "Sub category", "Sub category"]),
        Section(name: "Main Category 2", child: ["Sub category", "Sub category"]),
        ]*/
    
    var sideMenu = SideView()
    
    var deliverTypeStr = ""
    var sidemenuTypeStr = String()
    var countryIDStr:String = ""
    
   // var userIDStr = String()
    var countryName:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateToken()
        expandedSection.removeAllObjects()
        
        mainTableView.dataSource = self
        mainTableView.delegate = self
        
        //mainTableView.separatorInset = .zero
        
        mainTableView.register(UINib(nibName: "DetailCell", bundle: nil), forCellReuseIdentifier: "DetailCell")
        mainTableView.register(UINib(nibName: "categoryTCell", bundle: nil), forCellReuseIdentifier: "CatCell")
        mainTableView.register(UINib(nibName: "LanguageCell", bundle: nil), forCellReuseIdentifier: "LanguageCell")
        
        menuView.backgroundColor = .clear
        menuView.frame = CGRect(x:0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height-49)
        
        if languageID == "1"
        {
            self.mainTableView.frame = CGRect(x: -(UIScreen.main.bounds.size.width-120), y: 0, width: UIScreen.main.bounds.size.width-120, height: self.menuView.frame.height)
        }
        else
        {
            self.mainTableView.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-120, height: self.menuView.frame.height)
        }
        
        
        let gesView = UIView()
        gesView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        gesView.frame = self.menuView.frame
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeMenu))
        gesView.addGestureRecognizer(tap)
        menuView.addSubview(gesView)
        menuView.addSubview(self.mainTableView)
        mainTableView.tableFooterView = UIView()
        
        if let id = UserDefaults.standard.string(forKey: "USER_ID")
        {
            userIDStr = id
        }
        
        if userIDStr == ""
        {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        }
        else
        {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        expandedSection.removeAllObjects()
        if let id = UserDefaults.standard.string(forKey: "USER_ID")
        {
            userIDStr = id
        }
        
        if userIDStr == ""
        {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: ""), NSLocalizedString("Login", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        }
        else
        {
            mainTblArray = [[NSLocalizedString("Language", comment: ""), NSLocalizedString("Home", comment: "")],
                            [""],
                            [NSLocalizedString("Live Chat", comment: ""), NSLocalizedString("Share", comment: "")]
            ]
        }
    }
    
    func changeIcon(imageVw: UIImageView, color: UIColor)
    {
        imageVw.image = imageVw.image!.withRenderingMode(.alwaysTemplate)
        imageVw.tintColor = color
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    //MARK: Navigation Bar Buttons
    
    @objc func closeMenu()
    {
        if languageID == "1"
        {
            UIView.animate(withDuration: 0.50, animations: { () -> Void in
                
                self.mainTableView.frame = CGRect(x: -(UIScreen.main.bounds.size.width-120), y: 0, width: UIScreen.main.bounds.size.width-120, height: self.menuView.frame.height)
            }, completion: { (bol) -> Void in
                self.menuView.removeFromSuperview()
                self.mainTableView.removeFromSuperview()
            })
        }
        else
        {
            UIView.animate(withDuration: 0.50, animations: { () -> Void in
                
                self.mainTableView.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-120, height: self.menuView.frame.height)
            }, completion: { (bol) -> Void in
                self.menuView.removeFromSuperview()
                self.mainTableView.removeFromSuperview()
            })
        }
    }
    
    func setTitle()
    {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 25))
        imageView.contentMode = .scaleAspectFit
        let image = Tools.headerImage()
        imageView.image = image
      //  navigationItem.titleView = imageView
        
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 50, height: 25)
      //  button.setTitle("Button", for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.setBackgroundImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.clickTitle), for: .touchUpInside)
        self.navigationItem.titleView = button
    }
    
    @objc func clickTitle()
    {
        self.gotoRootViewController()
    }
    
    func setLeftNavigationButtonHome()
    {
        setTitle()
        getCategories()
        setupSwipeGestureRecognizer()
        
        var image = UIImage(named: "ico-menu")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let button = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.clickMenu(sender:)))
        self.navigationItem.leftBarButtonItem = button
    }
    
    func setLeftNavigationButton()
    {
        setTitle()
        getCategories()
        setupSwipeGestureRecognizer()
        
        var image = UIImage(named: "ico-menu")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
      //  self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.clickMenu(sender:)))
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(image, for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
        btn1.addTarget(self, action: #selector(self.clickMenu(sender:)), for: .touchUpInside)
        btn1.tag = 1
        
        if languageID == "1"
        {
            image = UIImage(named: "right-arrow")
        }
        else
        {
            image = UIImage(named: "left-arrowback")
        }
       // image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 20, height: 25)
        
        let imageVw = UIImageView()
        imageVw.contentMode = .scaleToFill
        imageVw.frame = CGRect(x: 0, y: 0, width: 20, height: 25)
        imageVw.image = image
        
        let btn2 = UIButton(type: .custom)
       // btn2.setImage(image, for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 20, height: 25)
        btn2.addTarget(self, action: #selector(self.clickClose(sender:)), for: .touchUpInside)
        btn2.tag = 2
        
        view.addSubview(imageVw)
        view.addSubview(btn2)
        
        let item2 = UIBarButtonItem(customView: btn1)
        let item1 = UIBarButtonItem(customView: view)
        
        self.navigationItem.setLeftBarButtonItems([item1, item2], animated: true)
    }
    
    func setRightNavigationButton()
    {
        if cartCount == "" || cartCount == "nil" || cartCount == "0"
        {
            var image = UIImage(named: "cart")
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.clickCart(sender:)))
        }
        else
        {
            // badge label
            let label = UILabel(frame: CGRect(x: 15, y: -4, width: 20, height: 20))
            label.layer.borderColor = UIColor.clear.cgColor
            label.layer.borderWidth = 2
            label.layer.cornerRadius = label.bounds.size.height / 2
            label.textAlignment = .center
            label.layer.masksToBounds = true
            label.textColor = .white
            label.font = UIFont.appFontWith(size: 12)
            label.backgroundColor = positiveBtnColor
            label.text = cartCount
            
            var image = UIImage(named: "cart")
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            
            let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
            rightButton.setBackgroundImage(image, for: .normal)
            rightButton.addTarget(self, action: #selector(clickCart), for: .touchUpInside)
            rightButton.addSubview(label)
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
            
        }
    }
    
    func setupSwipeGestureRecognizer()
    {
        //For left swipe
        let swipeGestureLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.swipedScreen))
        swipeGestureLeft.direction = .left
        self.view.addGestureRecognizer(swipeGestureLeft)
        
        //For right swipe
        let swipeGestureRight = UISwipeGestureRecognizer(target: self, action: #selector(self.swipedScreen))
        swipeGestureRight.direction = .right
        self.view.addGestureRecognizer(swipeGestureRight)
        
    }
    
    @objc func swipedScreen(gesture: UISwipeGestureRecognizer)
    {
        if languageID == "1"
        {
            if gesture.direction == .left
            {
                self.closeMenu()
            }
            else if gesture.direction == .right
            {
                getCategories()
                
                menuView.addSubview(self.mainTableView)
                self.view.addSubview(menuView)
                
                self.mainTableView.reloadData()
                
                UIView.animate(withDuration: 0.50, animations: {
                    self.mainTableView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-120, height: self.menuView.frame.height)
                })
            }
        }
        else
        {
            if gesture.direction == .right
            {
                self.closeMenu()
            }
            else if gesture.direction == .left
            {
                getCategories()
                
                menuView.addSubview(self.mainTableView)
                self.view.addSubview(menuView)
                
                self.mainTableView.reloadData()
                
                UIView.animate(withDuration: 0.50, animations: {
                    self.mainTableView.frame = CGRect(x: UIScreen.main.bounds.size.width - (UIScreen.main.bounds.size.width-120) , y: 0, width: UIScreen.main.bounds.size.width-120, height: self.menuView.frame.height)
                })
            }
        }
        
    }
    
    @objc func clickMenu(sender:UIBarButtonItem)
    {
        if self.mainTableView.frame.origin.x != 0
        {
            getCategories()
            
            menuView.addSubview(self.mainTableView)
            self.view.addSubview(menuView)
            
            self.mainTableView.reloadData()
            
            if languageID == "1"
            {
                UIView.animate(withDuration: 0.50, animations: {
                    self.mainTableView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-120, height: self.menuView.frame.height)
                })
            }
            else
            {
                UIView.animate(withDuration: 0.50, animations: {
                    self.mainTableView.frame = CGRect(x: UIScreen.main.bounds.size.width - (UIScreen.main.bounds.size.width-120) , y: 0, width: UIScreen.main.bounds.size.width-120, height: self.menuView.frame.height)
                })
            }
        }
        else
        {
            self.closeMenu()
        }
    }
    
    @objc func clickCart(sender:UIBarButtonItem)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.present(viewController, animated: true, completion: nil)
    }
    
    @objc func clickClose(sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: {})
    }
    
    func didTapSomeButton(_ getController: UIViewController!)
    {
        if "\(getController!)".lowercased().contains("login")
        {
            let next = self.storyboard?.instantiateViewController(withIdentifier: "\(getController!)") as! LoginVC
            self.present(next, animated: true, completion: nil)
        }
        else if "\(getController!)".lowercased().contains("viewcontroller")
        {
            let next = self.storyboard?.instantiateViewController(withIdentifier: "\(getController!)") as! ViewController
            self.navigationController?.pushViewController(next, animated: false)
        }
        else if "\(getController!)".lowercased().contains("deals")
        {
            let next = self.storyboard?.instantiateViewController(withIdentifier: "\(getController!)") as! DealsVC
            self.navigationController?.pushViewController(next, animated: true)
        }
        else if "\(getController!)".lowercased().contains("chat")
        {
            let next = self.storyboard?.instantiateViewController(withIdentifier: "\(getController!)") as! ChatVC
            self.navigationController?.pushViewController(next, animated: true)
        }
        else if "\(getController!)".lowercased().contains("order")
        {
            let next = self.storyboard?.instantiateViewController(withIdentifier: "\(getController!)") as! TrackOrderVC
            self.navigationController?.pushViewController(next, animated: true)
        }
        else if "\(getController!)".lowercased().contains("share")
        {
            
        }
        else
        {
            let next = self.storyboard?.instantiateViewController(withIdentifier: "\(getController!)") as! ProductListVC
            self.navigationController?.pushViewController(next, animated: true)
        }
    }
    
    // MARK: Categories
    
    func getCategories()
    {
        if self.categoryDict.count == 0
        {
            SharedManager.showHUD(viewController: self)
            
            let urlStr = "\(ConfigUrl.baseUrl)categories?depth=2"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.get.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        SharedManager.dismissHUD(viewController: self)
                        if responseObject.result.isSuccess
                        {
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                let tempDict = (responseObject.result.value! as AnyObject).value(forKey: "children_data") as AnyObject
                                
                                let array = NSMutableArray()
                                
                                for i in 0..<tempDict.count
                                {
                                    let temp = (tempDict.object(at: i) as! NSDictionary).mutableCopy() as AnyObject
                                    temp.setObject(true, forKey: "isCollapsed" as NSCopying)
                                    
                                    /* let allProducts = NSMutableDictionary()
                                     allProducts.setObject(temp.value(forKeyPath: "id")!, forKey: "id" as NSCopying)
                                     allProducts.setObject(temp.value(forKeyPath: "is_active")!, forKey: "is_active" as NSCopying)
                                     allProducts.setObject(temp.value(forKeyPath: "product_count")!, forKey: "product_count" as NSCopying)
                                     allProducts.setObject("All \(temp.value(forKeyPath: "name")!)", forKey: "name" as NSCopying)*/
                                    
                                    if "\(temp.value(forKeyPath: "name")!)".lowercased()  == "deals"
                                    {
                                        dealsID = "\(temp.value(forKeyPath: "id")!)"
                                    }
                                    
                                    
                                    let child = ((tempDict.object(at: i) as AnyObject).value(forKey: "children_data") as! NSArray).mutableCopy()
                                    // (child as AnyObject).add(allProducts)
                                    
                                    temp.setObject (child, forKey: "children_data" as NSCopying)
                                    
                                    if "\(temp.value(forKeyPath: "name")!)".lowercased()  == NSLocalizedString("deal", comment: "") || "\(temp.value(forKeyPath: "name")!)".lowercased()  == NSLocalizedString("subscription", comment: "") || "\(temp.value(forKeyPath: "name")!)".lowercased()  == NSLocalizedString("brands", comment: "")
                                    {
                                        print("\(temp.value(forKeyPath: "name")!)".lowercased())
                                    }
                                    else
                                    {
                                        array.add(temp)
                                    }
                                    
                                }
                                self.categoryDict = array
                                self.mainTableView.reloadData()
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
            
        }
    }
    
    func getSubCategories(catID: String)
    {
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "\(ConfigUrl.baseUrl)categories?rootCategoryId=3&depth=1"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            // print(responseObject.result.value!)
                            // subCategoryArr = (responseObject.result.value! as AnyObject).value(forKey: "children_data") as! NSMutableArray
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    //MARK: Country List
    
    func getCountryList()
    {
        SharedManager.showHUD(viewController: self)
        
        let urlStr = "\(ConfigUrl.baseUrl)directory/countries"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    SharedManager.dismissHUD(viewController: self)
                    if responseObject.result.isSuccess
                    {
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            let result = responseObject.result.value! as! NSArray
                            countryArr = result.mutableCopy() as! NSMutableArray
                            stateArr = ((countryArr.object(at: 0) as AnyObject).value(forKey: "available_regions") as! NSArray).mutableCopy() as! NSMutableArray
                            self.countryIDStr = (((responseObject.result.value!) as AnyObject).object(at: 0) as AnyObject).value(forKeyPath: "id") as! String
                            countryID = self.countryIDStr as NSString
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
    
    //MARK: Bar Buttons
    
    func clickBarButtonBack()
    {
        self.dismiss(animated: true, completion: {})
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func fitTableFrame(table: UITableView)//, container: UIView, addHeight: CFloat)
    {
        table.frame.size.height = table.contentSize.height
    }
    
    func converToJSon(values: AnyObject) -> AnyObject
    {
        var jsonStr:String = ""
        do
        {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: values, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonStr = NSString.init(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        }
            
        catch {
            print("error")
        }
        
        return jsonStr as AnyObject
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func formatCurrency(value: Double) -> String
    {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2;
        formatter.locale = Locale(identifier: Locale.current.identifier)
        let result = formatter.string(from: value as NSNumber);
        return result!;
    }
    
    func gotoRootViewController()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarcontroller") as! UITabBarController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window?.rootViewController = viewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func updateToken()
    {
        if let token = UserDefaults.standard.value(forKey: "API-TOKEN")
        {
            tokenStr = token as! String
        }
        
        print("Token: \(tokenStr)")
    }
    
    func updateCartBadge()
    {
        /*if let data = UserDefaults.standard.object(forKey: kAddToCart) as? Data
        {
            let cartListAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            let tabItem = self.tabBarController?.tabBar.items![3] as UITabBarItem!// as Array!
            
            var count:Int = 0
            
            if (cartListAry.count > 0)
            {
                for i in 0 ..< cartListAry.count
                {
                    let quantity = (cartListAry.object(at: i) as AnyObject).object(forKey: "userQuantity") as! String
                    count = count + Int(quantity)!
                    tabItem?.badgeValue = NSString(format: "%d", count) as String
                }
            }
            else
            {
                tabItem?.badgeValue = nil
            }
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TableView Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == mainTableView
        {
            return self.mainTblArray.count
        }
        else
        {
            if categoryDict.count != 0
            {
                return categoryDict.count
            }
            else
            {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == mainTableView
        {
            return mainTblArray[section].count
            
        }
        else
        {
            if categoryDict.count != 0
            {
                return ((categoryDict[section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count
            }
            else
            {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == mainTableView
        {
            if indexPath.section != 1
            {
                if indexPath.section == 0
                {
                    if indexPath.row != 0
                    {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                        
                        /* if cell1 == nil {
                         var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                         for view: UIView in views {
                         if (view is UITableViewCell) {
                         cell1 = view as? DetailCell
                         }
                         }
                         }*/
                        
                        let subArr = mainTblArray[indexPath.section]
                        cell1.title.text = subArr[indexPath.row]
                        
                        let subArr2 = iconsArr[indexPath.section]
                        cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                        if languageID == "1"
                        {
                            cell1.imgArrow.image = UIImage(named: "left-arrow")
                        }
                        else
                        {
                            cell1.imgArrow.image = UIImage(named: "right-arrow1")
                        }
                        cell1.imgArrow.contentMode = .scaleAspectFit
                        cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                        cell1.imgIcon.tintColor = UIColor.gray
                        cell1.selectionStyle = .none
                        
                        return cell1
                    }
                    else
                    {
                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as! LanguageCell
                        
                        if languageID == "1"
                        {
                            //selectedSegmentIndex = 0
                            cell1.segmentView.selectedSegmentIndex = 0
                        }
                        else
                        {
                            cell1.segmentView.selectedSegmentIndex = 1
                        }
                        
                        cell1.segmentView.addTarget(self, action: #selector(ParentViewController.clickLang(_:)), for: UIControlEvents.valueChanged)
                        
                        return cell1
                    }
                }
                else
                {
                    let cell1 = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
                    
                    /* if cell1 == nil {
                     var views = Bundle.main.loadNibNamed("DetailCell", owner: nil, options: nil)
                     for view: UIView in views {
                     if (view is UITableViewCell) {
                     cell1 = view as? DetailCell
                     }
                     }
                     }*/
                    
                    let subArr = mainTblArray[indexPath.section]
                    cell1.title.text = subArr[indexPath.row]
                    
                    let subArr2 = iconsArr[indexPath.section]
                    cell1.imgIcon.image = UIImage(named: subArr2[indexPath.row])
                    if languageID == "1"
                    {
                        cell1.imgArrow.image = UIImage(named: "left-arrow")
                    }
                    else
                    {
                        cell1.imgArrow.image = UIImage(named: "right-arrow1")
                    }
                    cell1.imgArrow.contentMode = .scaleAspectFit
                    cell1.imgIcon.image = cell1.imgIcon.image?.withRenderingMode(.alwaysTemplate)
                    cell1.imgIcon.tintColor = UIColor.gray
                    cell1.selectionStyle = .none
                    
                    return cell1
                }
            }
            else
            {
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "CatCell") as! categoryTCell
                
                cell1.insideTableView.isScrollEnabled = false
                cell1.insideTableView.dataSource = self
                cell1.insideTableView.delegate = self
                cell1.insideTableView.reloadData()
                cell1.selectionStyle = .none
                
                return cell1
            }
        }
        else
        {
            var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if cell == nil
            {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            }
            
            cell?.textLabel?.font = UIFont.appFontWith(size: 12)
            cell?.textLabel?.textColor = .darkGray
            cell?.textLabel?.frame.origin.x = 20
            
            if languageID == "2"
            {
                cell?.textLabel?.textAlignment = .right
            }
            cell?.textLabel?.text = (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "name") as? String
            
            cell?.selectionStyle = .none
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView == mainTableView
        {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 18))
            let label = UILabel(frame: CGRect(x: 10, y: 5, width: tableView.frame.size.width - 20, height: 25))
            label.font = UIFont.appFontWith(size: 16)
            label.textColor = UIColor.white
            let string = "\(titleArr[section])"
            label.text = string
            view.addSubview(label)
            view.backgroundColor = .lightGray
            return view
            
            /* let view = DetailCell ()
             view.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 44)
             view.backgroundColor = .green
             // view.imgArrow.image = UIImage (named: "left-arrow.png")
             // view.title.text = "Main category"
             // view.imgIcon.image = UIImage (named: "ic_favorite.png")
             
             return view*/
        }
        else
        {
            let sectionView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 40))
            sectionView.backgroundColor = .white
            
            sectionView.tag = section
            let viewLabel: UILabel = UILabel(frame: CGRect(x: 15, y: 0, width: mainTableView.frame.size.width - 30, height: 40))
            if languageID == "1" {
                viewLabel.frame = CGRect(x: 15, y: 0, width: mainTableView.frame.size.width - 30, height: 40)
            } else {
                viewLabel.frame = CGRect(x: 15, y: 0, width: mainTableView.frame.size.width - 30, height: 40)
            }
            viewLabel.backgroundColor = UIColor.clear
            viewLabel.textColor = UIColor.black
            viewLabel.font = UIFont.appFontWith(size: 15)
            viewLabel.text =  (categoryDict.object(at: section) as AnyObject).value(forKey: "name") as? String
            sectionView.addSubview(viewLabel)
            
            let line: UILabel = UILabel(frame: CGRect(x: 5, y: 49, width: mainTableView.frame.size.width - 10, height: 1))
            line.backgroundColor = UIColor.lightGray
            sectionView.addSubview(line)
            
            catID.add("\((categoryDict.object(at: section) as AnyObject).value(forKey: "id")!)")
            let arrowImg = UIImageView(frame: CGRect(x: mainTableView.frame.size.width - 40, y: 12, width: 16, height: 17))
            arrowImg.contentMode = .scaleAspectFit
            let bool = (categoryDict[section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool
            if bool == false {
                arrowImg.image = UIImage(named: "remove")
                
                if languageID == "1" {
                    arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 18, height: 15)
                } else {
                    arrowImg.frame = CGRect(x: 10, y: 12, width: 18, height: 15)
                }
            } else {
                arrowImg.image = UIImage (named: "add")
                if languageID == "1" {
                    arrowImg.frame = CGRect(x: mainTableView.frame.size.width - 28, y: 12, width: 15, height: 18)
                } else {
                    arrowImg.frame = CGRect(x: 10, y: 12, width: 15, height: 18)
                }
            }
            
            sectionView.addSubview(arrowImg)
            /********** Add a custom Separator with Section view *******************/
            let separatorLineView: UIView = UIView(frame: CGRect(x: 0, y: 40, width: mainTableView.frame.size.width, height: 0.3))
            separatorLineView.backgroundColor = UIColor.lightGray
            //separatorLineView.layer.borderWidth = 0.5
            // sectionView.addSubview(separatorLineView)
            /********** Add UITapGestureRecognizer to SectionView   **************/
            let headerTapped: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(self.sectionHeaderTapped(_:)))
            sectionView.addGestureRecognizer(headerTapped)
            
            sectionView.frame.size.height = 40
            
            return sectionView
        }
    }
    
    @objc func sectionHeaderTapped(_ gestureRecognizer: UITapGestureRecognizer)
    {
        // let touchPoint = sender.convert(CGPoint.zero, to:tblFoodList)
        // let indexPath = tblFoodList.indexPathForRow(at: touchPoint)! as IndexPath
        
        let indexPath: IndexPath = IndexPath(row: 0, section: gestureRecognizer.view!.tag)
        if (indexPath as NSIndexPath).row == 0
        {
            if expandedSection.contains(gestureRecognizer.view!.tag)
            {
                expandedSection.remove(gestureRecognizer.view!.tag)
            }
            else
            {
                expandedSection.add(gestureRecognizer.view!.tag)
            }

            var collapsed = ((categoryDict[gestureRecognizer.view!.tag] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool)
            
            for i in 0 ..< categoryDict.count
            {
               /* if expandedSection.contains(i)
                {
                    
                    // autoAdjustFrame()
                }*/
                
                let childArr = (categoryDict[gestureRecognizer.view!.tag] as AnyObject).value(forKeyPath: "children_data") as AnyObject
                
                if childArr.count != 0
                {
                    if collapsed == true
                    {
                        mainCategoryID = catID[gestureRecognizer.view!.tag] as! String
                        collapsed = false
                        (categoryDict[gestureRecognizer.view!.tag] as AnyObject).setValue(collapsed, forKey: "isCollapsed")
                        
                        let cell1 = mainTableView.dequeueReusableCell(withIdentifier: "CatCell") as! categoryTCell
                        
                        cell1.insideTableView.reloadSections(IndexSet(integer: i), with: .automatic)
                        
                        mainTableView.reloadSections(IndexSet(integer: 1), with: .automatic)
                    }
                    else
                    {
                        collapsed = true
                        (categoryDict[indexPath.section] as AnyObject).setValue(collapsed, forKey: "isCollapsed")
                        
                        let cell1 = mainTableView.dequeueReusableCell(withIdentifier: "CatCell") as! categoryTCell
                        cell1.insideTableView.reloadSections(IndexSet(integer: i), with: .automatic)
                        mainTableView.reloadSections(IndexSet(integer: 1), with: .automatic)
                    }
                }
                else
                {
                    let catID = "\((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "id")!)"
                    
                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
                    viewController.categoryID = catID
                    viewController.catTitle = "\((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "name")!)"
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == mainTableView
        {
            if section == 0
            {
                return 0
            }
            else
            {
                return 35
            }
        }
        else
        {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == mainTableView
        {
            if indexPath.section != 1
            {
                return 44
            }
            else
            {
                if categoryDict.count != 0
                {
                    var expandedRowHeight = 0
                    
                    for i in 0..<expandedSection.count
                    {
                        let index = Int("\(expandedSection[i])")!
                        
                        expandedRowHeight = expandedRowHeight + (((categoryDict[index] as AnyObject).value(forKeyPath: "children_data") as AnyObject).count * 40)
                    }
                    
                    return CGFloat(expandedRowHeight) + CGFloat(categoryDict.count * 44)
                }
                else
                {
                    return UITableViewAutomaticDimension
                }
            }
        }
        else
        {
            let collapsed = ((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool)
            
            if collapsed
            {
                return 0
            }
            else
            {
                return 40
            }
          //  return !((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool) ? 0 : 40.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == mainTableView
        {
            let subArr = mainTblArray[indexPath.section]
            
            let action = subArr[indexPath.row]
            
            if action == NSLocalizedString("Home", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Login", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.present(viewController, animated: true, completion: nil)
            }
            else if action == NSLocalizedString("Deals", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Live Chat", comment: "")
            {
                Intercom.presentMessenger()
            }
            else if action == NSLocalizedString("Track Order", comment: "")
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else if action == NSLocalizedString("Share", comment: "")
            {
                let textToShare = [ Tools.shareText ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
        else
        {
            let id = "\(String(describing: (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "id")!))"
            let name = "\(String(describing: (((categoryDict[indexPath.section] as AnyObject).value(forKeyPath: "children_data") as AnyObject).object(at: indexPath.row) as AnyObject).value(forKeyPath: "name")!))"
            
            print("Category ID: \(id)")
            categoryID = id
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            viewController.subIds = id
            viewController.isFromHome = true
            viewController.catTitle = name
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func clickLang(_ sender:UISegmentedControl!)
    {
        expandedSection.removeAllObjects()
        let index = sender.selectedSegmentIndex
        print(index)
        switch(sender.selectedSegmentIndex)
        {
        case 0:
            languageID = "1"
            
            UserDefaults.standard.set(languageID, forKey: "language_id")
            
            let selectedLanguage:Languages = Int(languageID) == 1 ? .en : .ar
            
            if #available(iOS 9.0, *)
            {
                LanguageManger.shared.setLanguage(language: selectedLanguage)
            }
            else
            {
                // Fallback on earlier versions
            }
            ConfigUrl.baseUrl = "https://balleh.com/index.php/rest/en/V1/"
            
            self.gotoRootViewController()
            break
        case 1:
            languageID = "2"
            
            UserDefaults.standard.set(languageID, forKey: "language_id")
            
            let selectedLanguage:Languages = Int(languageID) == 1 ? .en : .ar
            
            if #available(iOS 9.0, *)
            {
                LanguageManger.shared.setLanguage(language: selectedLanguage)
            }
            else
            {
                // Fallback on earlier versions
            }
            ConfigUrl.baseUrl = "https://balleh.com/index.php/rest/ar/V1/"
            
            self.gotoRootViewController()
            break
            
        default:
            break
            
        }
    }
    
    
    //MARK: Check Wishlist
    func checkProductInWishList(_ productId:String) -> Bool {
        var isAleadyHave:Bool = false
        
        if UserDefaults.standard.object(forKey: kAddToWishlist) != nil {
            let data = UserDefaults.standard.object(forKey: kAddToWishlist) as! Data
            let favAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            for tempDic in favAry {
                let tempCart:NSDictionary = tempDic as! NSDictionary
                
                let str1 = tempCart.value(forKey: "sku") as! NSString
                print(str1, productId)
                if str1.isEqual(to: productId as String) {
                    isAleadyHave = true
                }
            }
        }
        return isAleadyHave
    }
    
    //MARK: Check Cart
    func checkProductInCart(_ productId:String) -> Bool
    {
        var isAleadyHave:Bool = false
        
        if UserDefaults.standard.object(forKey: kAddToCart) != nil
        {
            let data = UserDefaults.standard.object(forKey: kAddToCart) as! Data
            let cartAry:NSMutableArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSMutableArray
            
            for tempDic in cartAry
            {
                let tempCart:NSDictionary = tempDic as! NSDictionary
                
                let str1 = tempCart.value(forKey: "id") as! NSString
                
                if str1.isEqual(to: productId as String)
                {
                    isAleadyHave = true
                }
            }
            
        }
        
        return isAleadyHave
    }
    
    func getstockStatus(productSku: String) -> Bool
    {
        var isAvail = Bool()
        let urlStr = "\(ConfigUrl.baseUrl)stockStatuses/\(productSku)"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(baseToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let result = "\(String(describing: (responseObject.result.value! as AnyObject).value(forKey: "stock_status")!))"
                            
                            print("Stock : \(result)")
                            
                            if result == "1"
                            {
                                isAvail = true
                            }
                            else
                            {
                                isAvail = false
                            }
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        
        /*DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            // Your code with delay
        }*/
        return isAvail
    }
    
    func getCartID(cusToken: String) -> String
    {
        let urlStr = "\(ConfigUrl.baseUrl)carts/mine"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        print("Get Cart ID : \(responseObject.result.value!)")
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            let result = responseObject.result.value! as AnyObject
                            cartID = "\(result.value(forKey: "id")!)"
                        }
                        else if "\(String(describing: responseObject.response!.statusCode))" == "404"
                        {
                            cartID = self.createCartID(cusToken: cusToken)
                        }
                        else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                        {
                            self.createGuestCartID(completionHandler: {(result) -> () in
                                cartID = result
                            })
                            
                            /*if userIDStr == ""
                             {
                             
                             }
                             else
                             {
                                if isGuest == false
                                {
                                    let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                             
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    self.present(viewController, animated: true, completion: nil)
                                }))
                                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
                                isGuest = true
                                if cartID == ""
                                {
                                    self.createGuestCartID(completionHandler: {(result) -> () in
                                    cartID = result
                                })
                             }
                             else
                             {
                                self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                                // print(result)
                                })
                             }
                        }))
                        self.present(alert, animated: true, completion: nil)
                             }
                             }*/
                        }
                        else
                        {
                            //  SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        return cartID
    }
    
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int)
    {
        let cell1 = mainTableView.dequeueReusableCell(withIdentifier: "CatCell") as! categoryTCell
        
        cell1.insideTableView.dataSource = self
        cell1.insideTableView.delegate = self
        
        var collapsed = ((categoryDict[section] as AnyObject).value(forKeyPath: "isCollapsed") as! Bool)
        
        if collapsed
        {
            collapsed = false
        }
        else
        {
            collapsed = true
        }
        
        // Toggle collapse
        (categoryDict[section] as AnyObject).setValue(collapsed, forKey: "isCollapsed")
        header.setCollapsed(collapsed)
        
        // Adjust the height of the rows inside the section
        cell1.insideTableView.beginUpdates()
        for i in 0 ..< (self.categoryDict[section] as AnyObject).count
        {
            cell1.insideTableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        cell1.insideTableView.endUpdates()
        
        //  cell1.insideTableView.frame.size.height = cell1.insideTableView.contentSize.height
        
        self.mainTableView.reloadData()
        // frames()
    }
    
    func createCartID(cusToken: String) -> String
    {
        let urlStr = "\(ConfigUrl.baseUrl)carts/mine"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            cartID = "\(responseObject.result.value!)"
                            self.refreshBillingAddress(cartId: cartID, cusToken: cusToken)
                        }
                        else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                        {
                            self.createGuestCartID(completionHandler: {(result) -> () in
                                cartID = result
                            })
                            
                            /*if userIDStr == ""
                             {
                             
                             }
                             else
                             {
                             if isGuest == false
                             {
                             let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                             alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                             
                             let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                             let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                             self.present(viewController, animated: true, completion: nil)
                             }))
                             alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
                             isGuest = true
                             if cartID == ""
                             {
                             self.createGuestCartID(completionHandler: {(result) -> () in
                             cartID = result
                             })
                             }
                             else
                             {
                             self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                             // print(result)
                             })
                             }
                             }))
                             self.present(alert, animated: true, completion: nil)
                             }
                             }*/
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
        return cartID
    }
    
    func refreshBillingAddress(cartId: String, cusToken : String)
    {
        let urlStr = "\(ConfigUrl.baseUrl)carts/mine/billing-address"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
        
        let address = NSDictionary()
        let params = ["address" : address,
                      "cartId" : cartId] as [String : Any]
        
        let setTemp: [String : Any] = params
        
        if let jsonData: Data = try? JSONSerialization.data(withJSONObject: setTemp, options: .prettyPrinted) {
            let jsonString = String(data: jsonData , encoding: .utf8)!
            print(jsonString as Any)
            request.httpBody = jsonData
        }
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print("success")
                            // cartID = "\(responseObject.result.value!)"
                        }
                        else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                        {
                            
                            self.createGuestCartID(completionHandler: {(result) -> () in
                                cartID = result
                            })
                            
                            /*if userIDStr == ""
                             {
                             
                             }
                             else
                             {
                             if isGuest == false
                             {
                             let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                             alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                             
                             let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                             let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                             self.present(viewController, animated: true, completion: nil)
                             }))
                             alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
                             isGuest = true
                             if cartID == ""
                             {
                             self.createGuestCartID(completionHandler: {(result) -> () in
                             cartID = result
                             })
                             }
                             else
                             {
                             self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                             // print(result)
                             })
                             }
                             }))
                             self.present(alert, animated: true, completion: nil)
                             }
                             }*/
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
        }
    }
    
   /* func deleteCartItem(cusToken: String, itemId: String, type: String)
    {
        if type == "user"
        {
            let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items/\(itemId)"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.delete.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
            
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                        }
                        else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                        {
                            let alert = UIAlertController(title: "Your Session Expired!", message: "Please Login to Continue", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(alert :UIAlertAction) in
                                
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }))
                            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartID)/items/\(itemId)"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.delete.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
          //  request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
            
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            print(responseObject.result.value!)
                            self.getGuestCartDetail(cartId: cartID, completionHandler: {_ in
                                
                            })
                        }
                        else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                        {
                            let alert = UIAlertController(title: "Your Session Expired!", message: "Please Login to Continue", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(alert :UIAlertAction) in
                                
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }))
                            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
    }*/
    
    func getCartDetails(cusToken: String, completionHandler:@escaping (_ result:NSMutableArray)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
        var result = NSMutableArray()
        let urlStr = "\(ConfigUrl.baseUrl)carts/mine/items"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
        
        if Connectivity.isConnectedToInternet()
        {
            Alamofire.request(request).responseJSON
                { (responseObject) -> Void in
                    
                    if responseObject.result.isSuccess
                    {
                        SharedManager.dismissHUD(viewController: self)
                        
                        if "\(String(describing: responseObject.response!.statusCode))" == "200"
                        {
                            result = (responseObject.result.value! as! NSArray).mutableCopy() as! NSMutableArray
                            print(responseObject.result.value!)
                            
                            var count = 0
                            
                            for i in 0..<result.count
                            {
                                let qty = "\((result[i] as AnyObject).value(forKey:"qty")!)"
                                
                                count = count + Int(qty)!
                            }
                            
                            cartCount = "\(count)"
                            
                            self.setRightNavigationButton()
                            
                            //   self.deleteCartItem(cusToken: cusToken, itemId: "\(result[0].value(forKey:"item_id")!)")
                        }
                        else if "\(String(describing: responseObject.response!.statusCode))" == "401"
                        {
                            
                            self.createGuestCartID(completionHandler: {(result) -> () in
                                cartID = result
                            })
                            
                            /*if userIDStr == ""
                             {
                             
                             }
                             else
                             {
                             if isGuest == false
                             {
                             let alert = UIAlertController(title: "\(NSLocalizedString("Your Session Expired", comment: ""))!", message: "\(NSLocalizedString("Please Login to Continue", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)
                             alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {(alert :UIAlertAction) in
                             
                             let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                             let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                             self.present(viewController, animated: true, completion: nil)
                             }))
                             alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert :UIAlertAction) in
                             isGuest = true
                             if cartID == ""
                             {
                             self.createGuestCartID(completionHandler: {(result) -> () in
                             cartID = result
                             })
                             }
                             else
                             {
                             self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                             // print(result)
                             })
                             }
                             }))
                             self.present(alert, animated: true, completion: nil)
                             }
                             }*/
                        }
                        else
                        {
                            // SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        SharedManager.dismissHUD(viewController: self)
                        let error : Error = responseObject.result.error!
                        print(error.localizedDescription)
                    }
            }
        }
        else
        {
            SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
            DispatchQueue.main.async
            {
                completionHandler(result)
            }
        }
    }
    
    // MARK: Guest cart
    func createGuestCartID(completionHandler:@escaping (_ result:String)->()) 
    {
        cartID = ""
        UserDefaults.standard.removeObject(forKey: kUserDetails)
        UserDefaults.standard.removeObject(forKey: kAddToCart)
        
        UserDefaults.standard.removeObject(forKey: "USER_ID")
        UserDefaults.standard.removeObject(forKey: "COUPON")
        UserDefaults.standard.removeObject(forKey: "VOUCHER")
        UserDefaults.standard.removeObject(forKey: "REWARDS")
        userIDStr = ""
        cartCount = ""
        isGuest = true
    
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            
            let urlStr = "\(ConfigUrl.baseUrl)guest-carts"
            
            let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
            var request = URLRequest(url: URL(string: setFinalURl)!)
            request.httpMethod = HTTPMethod.post.rawValue
            //  request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            //  request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
            
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                cartID = "\(responseObject.result.value!)"
                                UserDefaults.standard.set(cartID, forKey: "CART_ID")
                                self.getGuestCartDetail(cartId: cartID, completionHandler: {(result) -> () in
                                    // print(result)
                                })
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
            
            DispatchQueue.main.async
                {
                    completionHandler(cartID)
            }
        }
    }
    
    func getGuestCartDetail(cartId: String, completionHandler:@escaping (_ result:NSMutableArray)->())
    {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
        var result = NSMutableArray()
        let urlStr = "\(ConfigUrl.baseUrl)guest-carts/\(cartId)/items"
        
        let setFinalURl = urlStr.addingPercentEncoding (withAllowedCharacters: .urlQueryAllowed)!
        var request = URLRequest(url: URL(string: setFinalURl)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        // request.setValue("Bearer \(cusToken)", forHTTPHeaderField: "Authorization")
        
            if Connectivity.isConnectedToInternet()
            {
                Alamofire.request(request).responseJSON
                    { (responseObject) -> Void in
                        
                        if responseObject.result.isSuccess
                        {
                            SharedManager.dismissHUD(viewController: self)
                            
                            if "\(String(describing: responseObject.response!.statusCode))" == "200"
                            {
                                result = (responseObject.result.value! as! NSArray).mutableCopy() as! NSMutableArray
                                print(responseObject.result.value!)
                                
                                completionHandler(result)
                                
                                var count = 0
                                
                                for i in 0..<result.count
                                {
                                    let qty = "\((result[i] as AnyObject).value(forKey:"qty")!)"
                                    
                                    count = count + Int(qty)!
                                }
                                cartCount = "\(count)"
                                
                                self.setRightNavigationButton()
                                
                                //   self.deleteCartItem(cusToken: cusToken, itemId: "\(result[0].value(forKey:"item_id")!)")
                            }
                            else
                            {
                                SharedManager.showAlertWithMessage(title: "", alertMessage: ((responseObject.result.value) as AnyObject).value(forKeyPath: "message") as! String, viewController: self)
                            }
                        }
                        if responseObject.result.isFailure
                        {
                            SharedManager.dismissHUD(viewController: self)
                            let error : Error = responseObject.result.error!
                            print(error.localizedDescription)
                        }
                }
            }
            else
            {
                SharedManager.showAlertWithMessage(title: NSLocalizedString("Sorry", comment: ""), alertMessage: NSLocalizedString("The Internet connection appears to be offline", comment: ""), viewController: self)
            }
        
           /* DispatchQueue.main.async
                {
                    
            }*/
        }
    }
}

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
