//
//  UpdatesTableViewCell.h
//  UpdatesListView
//
//  Created by Tope on 10/11/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageCell : UITableViewCell

@property(nonatomic,retain )IBOutlet UISegmentedControl *segmentView;

@end
