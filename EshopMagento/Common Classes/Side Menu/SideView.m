//
//  SideView.m
//  MyPractice
//
//  Created by eph132 on 10/06/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import "SideView.h"
#import "CategoryMenu.h"
#import "DetailCell.h"
#import "SectionInfo.h"
#import "Balleh-Swift.h"

#define DEFAULT_ROW_HEIGHT 157
#define HEADER_HEIGHT 50

@interface SideView ()
-(void)openDrawer;
-(void)closeDrqwer;
@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSArray *categoryList;
- (void) setCategoryArray;
@end

@implementation SideView
{
    NSArray *topItems;
    NSMutableArray *subItems; // array of arrays
    
    int currentExpandedIndex;
    UITableView *_tableView;
    
    
}

@synthesize categoryList = _categoryList;
@synthesize openSectionIndex;
@synthesize sectionInfoArray;

SideView *side;

NSMutableArray *titleArray;
NSArray *sectionTitleArray;
NSMutableArray *imageArray;
NSMutableArray *vcArray;


- (id)init
{
    side = [[[NSBundle mainBundle] loadNibNamed:@"SideView"
                                          owner:self
                                        options:nil]
            objectAtIndex:0];
    
    
    [side sizeToFit];
    
   /* if (IS_IPHONE_4_OR_LESS)
    {
        float widthNew = 62;
        float heightNew = 62;
        
        //self.scrollView.frame = CGRectMake(0, 72, widthNew, heigtNew);
        
        CGRect frm = _imgUser.frame;
        frm.size.width = widthNew;
        frm.size.height = heightNew;
        _imgUser.frame = frm;
    }*/
   
    [side.imgUser.layer setShadowColor:[UIColor blackColor].CGColor];
    [side.imgUser.layer setShadowOpacity:0.8];
    [side.imgUser.layer setShadowRadius:3.0];
    [side.imgUser.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    side.imgUser.layer.masksToBounds = YES;
    
    
    CGRect basketTopFrame = side.frame;
    
    basketTopFrame.origin.x = -320;
    
   // NSLog(@"Width:%f",basketTopFrame.size.width);
   //  NSLog(@"Width:%f",basketTopFrame.size.height);
    
    
    side.frame = basketTopFrame;
   
    
    _closeView.hidden=YES;
    
        [self openDrawer];
    
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    
    
    
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [_gesView addGestureRecognizer:gestureRecognizer];
    
    [_menuTableView reloadData];
    
    return side;
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    
    [self closeDrawer:nil];
}


- (void)configureWith:(id<customProtocol>)delegate{
    //Configure the delegate it will manage the events
    //from subviews like buttons and other controls
    self.delegate = delegate;
    //.. to configure any subView
}


- (void) awakeFromNib
{
    
    
   // [self setCategoryArray];
    //self.menuTableView.sectionHeaderHeight = 45;
   // self.menuTableView.sectionFooterHeight = 0;
   // self.openSectionIndex = NSNotFound;
    
    sectionTitleArray = [NSArray arrayWithObjects:@"", @"Categories", @"Help", nil];
    
    vcArray = [[NSMutableArray alloc] initWithCapacity: 3];
    [vcArray insertObject:[NSMutableArray arrayWithObjects:@"ViewController",@"LoginVC",@"DealsVC", nil] atIndex:0];
    [vcArray insertObject:[NSMutableArray arrayWithObjects:@"ProductListVC",@"ProductListVC",@"ProductListVC",@"ProductListVC", @"ProductListVC", @"ProductListVC", @"ProductListVC", nil] atIndex:1];
    [vcArray insertObject:[NSMutableArray arrayWithObjects:@"ChatVC", @"TrackOrderVC", @"Share", nil] atIndex:2];
    
    titleArray = [[NSMutableArray alloc] initWithCapacity: 3];
    [titleArray insertObject:[NSMutableArray arrayWithObjects:@"Home",@"login",@"deals", nil] atIndex:0];
    [titleArray insertObject:[NSMutableArray arrayWithObjects:@"", nil] atIndex:1];
   // [titleArray insertObject:[NSMutableArray arrayWithObjects:@"Skin",@"Makeup",@"Hair",@"Bath & Body", @"Mom & Baby", @"Wellness & health", @"Men", nil] atIndex:1];
    [titleArray insertObject:[NSMutableArray arrayWithObjects:@"Live Chat", @"Track Order", @"Share", nil] atIndex:2];
    
    imageArray = [[NSMutableArray alloc] initWithCapacity: 3];
    [imageArray insertObject:[NSMutableArray arrayWithObjects:@"home-grey",@"ic_person",@"deals", nil] atIndex:0];
    [imageArray insertObject:[NSMutableArray arrayWithObjects:@"ic_settings",@"ic_favorite",@"ic_settings",@"ic_favorite", @"ic_settings", @"ic_favorite", @"ic_settings", nil] atIndex:1];
    [imageArray insertObject:[NSMutableArray arrayWithObjects:@"livechat", @"trackOrder", @"ic_share", nil] atIndex:2];
    
    NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_DATA"];
    
    NSDictionary *getUserData=[NSKeyedUnarchiver unarchiveObjectWithData:data2];
    
   // NSLog(@"Get Data:%@",getUserData);
    
    NSString *userName= [getUserData valueForKeyPath:@"user.name"];
    
    int genValue= [[getUserData valueForKeyPath:@"user.gender"]intValue];
    
    if(genValue==1)
    {
        _imgUser.image = [UIImage imageNamed:@"people2.jpg"];
    }
    else
    {
        _imgUser.image = [UIImage imageNamed:@"people2.jpg"];
    }
    
   // _lblName.text = [NSString stringWithFormat:@"HELLO %@",[userName uppercaseString]];
    
    
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * versionBuildString = [NSString stringWithFormat:@"Version: %@ (%@)", appVersionString, appBuildString];
    
        
    
    [super awakeFromNib];
    
}



- (id)initWithTitle:(NSString *)aTitle options:(NSArray *)aOptions {
    CGRect rect = [[UIScreen mainScreen] applicationFrame]; // portrait bounds
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        rect.size = CGSizeMake(rect.size.height, rect.size.width);
    }
    if (self = [super initWithFrame:rect])
    {
       
       
        
    }
    return self;
}





/*
- (id)initWithTitle:(NSString *)aTitle
            options:(NSArray *)aOptions
            handler:(void (^)(NSInteger anIndex))aHandlerBlock {
    
    
    
    
   
    
    if(self = [self initWithTitle:aTitle options:aOptions])
        self.handlerBlock = aHandlerBlock;
    
    return self;
}
 */

- (void)setUpTableView
{
}


#pragma mark - Instance Methods
- (void)showInView:(UIView *)aView animated:(BOOL)animated
{
    
    
    
 
    
  
    
    
    
    side = [[[NSBundle mainBundle] loadNibNamed:@"SideView"
                                         owner:self
                                       options:nil]
           objectAtIndex:0];
    
    CGRect basketTopFrame = side.frame;
    //basketTopFrame.origin.x = basketTopFrame.size.width;
    basketTopFrame.origin.x = -320;
    side.frame = basketTopFrame;
    
    //side.delegate = self;
    
    [aView addSubview: side];
    //side.hidden=YES;
    
    
    
    if (animated) {
        
        [self openDrawer];
    }
    
    [_menuTableView reloadData];
    
    
}

-(void)openDrawer
{
    side.hidden=NO;
    _closeView.hidden=YES;
    
    CGRect basketTopFrame = side.frame;
    //basketTopFrame.origin.x = basketTopFrame.size.width;
    basketTopFrame.origin.x = 0;
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         side.frame = basketTopFrame;
                         // basketBottom.frame = basketBottomFrame;
                     }
                     completion:^(BOOL finished)
     {
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            _closeView.hidden=NO;
         });
     }];
}

- (IBAction)closeDrawer:(id)sender {
    
    [_delegate clickClose:@"Close"];
    
    side.hidden=NO;
    _closeView.hidden=YES;
    CGRect basketTopFrame = side.frame;
    //basketTopFrame.origin.x = basketTopFrame.size.width;
    basketTopFrame.origin.x = -320;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         side.frame = basketTopFrame;
                         // basketBottom.frame = basketBottomFrame;
                     }
                     completion:^(BOOL finished)
     {
         [self removeFromSuperview];
     }];
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionTitleArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section != 1)
    {
        return 3;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        static NSString *CellIdentifier = @"CatCell";
        categoryTCell * cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell1 == nil)
        {
            NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"categoryTCell" owner:nil options:nil];
            
            for (UIView *view in views)
            {
                if([view isKindOfClass:[UITableViewCell class]])
                {
                    cell1 = (categoryTCell*)view;
                }
            }
        }
      //  [categoryTCell.tblCategory reloadData];
      //  categoryTCell.tblCategory.reloadData()
        
        return cell1;
    }
    else
    {
        static NSString *CellIdentifier = @"DetailCell";
        DetailCell * cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell1 == nil)
        {
            
            NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"DetailCell" owner:nil options:nil];
            
            for (UIView *view in views)
            {
                if([view isKindOfClass:[UITableViewCell class]])
                {
                    cell1 = (DetailCell*)view;
                }
            }
        }
        
       // cell1.title.text = titleArray[indexPath.section][indexPath.row];
       // cell1.imgIcon.image = [UIImage imageNamed:imageArray[indexPath.section][indexPath.row]];
        
        cell1.imgArrow.image= [UIImage imageNamed:@"left-arrow"];
        [cell1.imgArrow setContentMode:UIViewContentModeScaleAspectFit];
        
        cell1.imgIcon.image = [cell1.imgIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell1.imgIcon setTintColor:[UIColor grayColor]];
        
        return cell1;
    }
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section != 1)
    {
        return 44;
    }
    else
    {
        self.menuTableView.estimatedRowHeight = 44.0;
        self.menuTableView.rowHeight = UITableViewAutomaticDimension;
      //  categoryTCell * cell1 = [[categoryTCell alloc] init];
      //  return cell1.contentView.frame.size.height;
        return 200;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width - 20, 25)];
    [label setFont:[UIFont fontWithName:NSLocalizedString(@"Roboto-Light", @"") size:15]];
    label.textColor = [UIColor whiteColor];
    NSString *string =[NSString stringWithFormat:@"%@", sectionTitleArray[section]];
    
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:91.0/255.0 green:91.0/255.0 blue:91.0/255.0 alpha:1.0]];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 0.0f;
    }
    else
    {
        return 35.0f;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section != 1)
    {
       // [_delegate didTapSomeButton:vcArray[indexPath.section][indexPath.row]];
        
        [self closeDrawer:nil];
    }
   // [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) reloadBasetable
{
    [_menuTableView reloadData ];
}

/*- (IBAction)clickLogout:(id)sender
{
    [_delegate didTapSomeButton: @"Logout"];
    
    [self closeDrawer:nil];
}*/

//clickCategory

@end
